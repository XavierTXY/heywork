//
//  AppDelegate.swift
//  HeyWork
//
//  Created by XavierTanXY on 19/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import FBSDKLoginKit
import FBSDKCoreKit
import Stripe
import SVProgressHUD
import PopupDialog
import GoogleMaps
import GooglePlaces
import UserNotifications
import Reachability
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.


        

        
        
        SVProgressHUD.setBackgroundColor(UIColor.white)
        SVProgressHUD.setRingThickness(3)
        GMSPlacesClient.provideAPIKey(GOOGLE_MAP_API_KEY)
        GMSServices.provideAPIKey(GOOGLE_MAP_API_KEY)
        FirebaseApp.configure()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        if let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            DataService.ds.OS = currentVersion
            
            
        }
        
        Fabric.with([Crashlytics.self])
        Fabric.sharedSDK().debug = true
        

        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
        

        
        
        let defaults = UserDefaults.standard
        var uid = defaults.object(forKey: "uid") as? String
//        var user = defaults.object(forKey: "user") as? User

        if uid == nil {
            DataService.ds.login = false
            var storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "NavVC") as! UINavigationController
            self.window?.rootViewController = controller
            
        } else {
            DataService.ds.login = true
//            DataService.ds.currentUser = user
            DataService.ds.signIn(uid: uid!)
            
            var storyboard = UIStoryboard(name: "Home", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "OpenSans-SemiBold", size: 10)!], for: .normal)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "OpenSans-SemiBold", size: 10)!], for: .selected)
            self.window?.rootViewController = controller
            
            
        }
        
        STPPaymentConfiguration.shared().publishableKey = TEST_PUBLISH_KEY
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification(notification:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        NetworkManager.isUnreachable { _ in
            print("reach unreach")
        }
        
        // Check if launched from notification
        let notificationOption = launchOptions?[.remoteNotification]
        
        // 1
        if let notification = notificationOption as? [String: AnyObject] {
            
            // 2
            //            print("aps value \(aps)")
            if let taskID = notification["taskID"] as? String {
                goToNotification(taskID: taskID)
            }
            // 3
            //            (window?.rootViewController as? UITabBarController)?.selectedIndex = 1
        }
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        Messaging.messaging().shouldEstablishDirectChannel = false
        print("Disconnected from FCM.")
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        checkUpdate { (update) -> Void in
            
            if update {
                
                let popup = PopupDialog(title: "New Version", message: "We have made the app even better! Please go to App Store and update.", image: nil)
                
                
                let buttonTwo = DefaultButton(title: "Update", dismissOnTap: true) {
                    if let reviewURL = URL(string: "https://itunes.apple.com/us/app/unithing/id1302177232?ls=1&mt=8"), UIApplication.shared.canOpenURL(reviewURL) {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(reviewURL, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(reviewURL)
                        }
                    }
                }
                
                popup.addButtons([buttonTwo])
                popup.buttonAlignment = .horizontal
                
                // Present dialog
                self.window?.rootViewController?.present(popup, animated: true, completion: nil)
            }
        }
        connectToFCM()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if let messageID = userInfo["gcmMessageIDKey"] {
            print("Message ID: \(messageID)")
        }
        
        print(userInfo)
        // Print full message.

//        print("aps value 222 \(taskID)")
        
        if let taskID = userInfo["taskID"] as? String {
            goToNotification(taskID: taskID)
        }
        
        
        
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    
    func goToNotification(taskID: String) {
//        if let taskID = userInfo["taskID"] as? String {
        
            DataService.ds.REF_TASK.child(taskID).observeSingleEvent(of: .value, with: { (snapshot) in
                if ( snapshot.value is NSNull ) {
                } else {
                    
                    if let taskDict = snapshot.value as? Dictionary<String, AnyObject> {
                        //                    let key = snapshot.key
                        
                        let task = Task(taskKey: taskID, dict: taskDict)
                        
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        
                        if let taskVC = storyboard.instantiateViewController(withIdentifier: "TaskDetailVC") as? TaskDetailVC, let tabBarController = self.window?.rootViewController as? UITabBarController, let navController = tabBarController.selectedViewController as? UINavigationController {
                            
                            taskVC.task = task
                            
//                            if tabBarController.selectedIndex != 4 {
                                navController.pushViewController(taskVC, animated: true)
//                            }
                            
                        }
                        
                        
                    }
                }
            })
//        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("getting instance id token")
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            DataService.ds.PUSH_TOKEN = refreshedToken
            
            let defaults = UserDefaults.standard
            var uid = defaults.object(forKey: "uid") as? String
            if uid != nil {
                DataService.ds.addInstanceID()
            }
        }
        
        //f2ZbojVi_0Y:APA91bHR5e2p_h5TQGM89TAxwstiKEVraDeiaVXxyE15gii5kfif-220C2tCTIFGzVK-TzqWB7A11DXramAuknRQGP5DMCA-_Z-ai3XnVU0BSGcmJYaSoJbSWTBJbgrqvSwiSZWv84pM
        
    }
    

//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//
//        print("response value \(response)")
//    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    @objc func tokenRefreshNotification(notification: NSNotification) {
        //        let refreshedToken = FIRInstanceID.instanceID().token()
        //        print("instance id token \(refreshedToken)")
        
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token2: \(refreshedToken)")
            DataService.ds.PUSH_TOKEN = refreshedToken
            
            
            
        }
        
        connectToFCM()
        
    }
    
    func connectToFCM() {
        
        Messaging.messaging().connect { (error) in
            if error != nil {
                print("unable ro connect \(error?.localizedDescription)")
            } else {
                print("connected")
            }
        }
    }
    
    
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        //print("Handle push from background or closed" );
//        //print("%@", response.notification.request.content.userInfo);
//        
//        print("tapped on notification to vc")
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SomeNotification"), object: nil)
//        
//        if self.window!.rootViewController as? UITabBarController != nil {
//            var tababarController = self.window!.rootViewController as! UITabBarController
//            tababarController.selectedIndex = 2
//        }
//        
//        
//        //        var notiVC = self.window?.rootViewController?.childViewControllers[2]
//        //        self.window?.rootViewController?.present(notiVC!, animated: true, completion: nil)
//        completionHandler()
//    }
    
    func checkUpdate(completion: @escaping (Bool)->Void) {
        
        
        
        DataService.ds.REF_APP_CONFIG.observe(.value) { (snapshot) in
            print("lala \(snapshot)")
            if let appConfigDict = snapshot.value as? Dictionary<String, AnyObject> {
                print("VERSOION \(appConfigDict["isMaintenance"])")
                
                let appConfig = AppConfig(dictionary: appConfigDict)
                
                var emptyView = EmptyView.instanceFromNib(image: UIImage(named: "maintenance")!, title: "Maintenance on Progress", desc: appConfig.maintenanceMsg!) as! EmptyView
                emptyView.frame = UIApplication.shared.keyWindow!.bounds
                
                if appConfig.isMaintenance! {
                    UIApplication.shared.keyWindow!.addSubview(emptyView)
                    print("main on")
                } else {
//                    UIApplication.shared.keyWindow!.willRemoveSubview(emptyView)
//                    UIApplication.shared.keyWindow!.remove
                    emptyView.removeFromSuperview()
                    print("main off")
                    
                    if let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                        DataService.ds.OS = currentVersion
                        
                        if currentVersion != appConfig.version {

//                            let popup = PopupDialog(title: "New Version", message: "We have made the app even better! Please go to App Store and update.", image: nil)
//                            
//                            
//                            let buttonTwo = DefaultButton(title: "Update", dismissOnTap: true) {
//                                if let reviewURL = URL(string: "https://itunes.apple.com/us/app/unithing/id1302177232?ls=1&mt=8"), UIApplication.shared.canOpenURL(reviewURL) {
//                                    if #available(iOS 10.0, *) {
//                                        UIApplication.shared.open(reviewURL, options: [:], completionHandler: nil)
//                                    } else {
//                                        UIApplication.shared.openURL(reviewURL)
//                                    }
//                                }
//                            }
//                            
//                            popup.addButtons([buttonTwo])
//                            popup.buttonAlignment = .horizontal
//                            
//                            // Present dialog
//                            self.window?.rootViewController?.present(popup, animated: true, completion: nil)
                            
                            if appConfig.mustUpdate! {
                                completion(true)
                            } else {
                                completion(false)
                            }
                            
         
                        } else {
                            completion(false)
                        }
                        
                    }
                }

                
            }
        }

            
//        })
//        DataService.ds.REF_Version.observeSingleEvent(of: .value, with: { (snapshot) in
//            print("lala \(snapshot)")
//            if let latestVersion = snapshot.value as? String {
//                print("VERSOION \(latestVersion)")
//
//                if let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
//                    DataService.ds.OS = currentVersion
//                    if currentVersion != latestVersion {
//
//                        DataService.ds.REF_Update.observeSingleEvent(of: .value, with: { (snapshot) in
//
//                            if let update = snapshot.value as? Bool{
//
//                                if update {
//                                    completion(true)
//                                } else {
//                                    completion(false)
//                                }
//
//                            }
//
//                        })
//                    } else {
//                        completion(false)
//                    }
//
//                }
//
//            }
//
//        })
        
    }
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "HeyWork")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

