//
//  LanguageVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class LanguageCell: UITableViewCell {
    @IBOutlet weak var languageLbl: UILabel!
    @IBOutlet weak var levelLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func configureCell(language: String, level: String) {
        languageLbl.text = language
        levelLbl.text = level
        
    }
}
class LanguageVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var languages = [String]()
    var levels = [String]()
    
    var builtInLanguages = ["English", "Chinese", "Malay"]
    var builtInLevel = ["Basic", "Intermediate", "Fluent"]
    
    var languageDict = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        if Checker.checker.languages.count != 0 {
            self.languages = Checker.checker.languages
            self.levels = Checker.checker.levels
            self.languageDict = Checker.checker.languageDict
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        Checker.checker.languages = self.languages
        Checker.checker.levels = self.levels
        Checker.checker.languageDict = self.languageDict
        
        if languages.count != 0 {
            Checker.checker.languageChecked = true
        } else {
            Checker.checker.languageChecked = false
        }
    }

    @objc func addTapped() {
        
        ActionSheetMultipleStringPicker.show(withTitle: "Languages", rows: [
            builtInLanguages,
            builtInLevel
            ], initialSelection: [0, 0], doneBlock: {
                picker, indexes, values in
                
                let idx = indexes?[0] as! Int
                let idx2 = indexes?[1] as! Int

        
                
                self.languageDict[self.builtInLanguages[idx]] = self.builtInLevel[idx2]
                self.languages = Array(self.languageDict.keys)
                self.levels = Array(self.languageDict.values)
                self.tableView.reloadData()
//                print("values = \(values)")
//                print("indexes = \(indexes)")
//                print("picker = \(picker)")
                return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: self.view)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
//            tableView.beginUpdates()
            
            self.languageDict.removeValue(forKey: self.languages[indexPath.row])
            
            self.languages = Array(self.languageDict.keys)
            self.levels = Array(self.languageDict.values)
            
//            tableView.endUpdates()
            tableView.reloadData()
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageCell") as! LanguageCell
        cell.configureCell(language: languages[indexPath.row], level: levels[indexPath.row])
        return cell
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languages.count
    }

}
