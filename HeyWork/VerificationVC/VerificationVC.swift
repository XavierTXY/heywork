//
//  VerificationVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 18/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import SVProgressHUD
import ActionSheetPicker_3_0
import Firebase
import BLTNBoard


class VerificationVC: BaseViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var bankTitle: UIButton!
    @IBOutlet weak var saveBtn: RoundView!
    @IBOutlet weak var profilePicImg: UIImageView!
    @IBOutlet weak var profilePicView: RoundView!
    @IBOutlet weak var dobView: RoundView!
    @IBOutlet weak var dobBtn: UIButton!
    @IBOutlet weak var languageView: RoundView!
    @IBOutlet weak var bankView: RoundView!
    
    var email:String!
    var deadline: NSNumber!
    var dob:String!
    var imagePick: UIImagePickerController!
    var bulletinManager: BLTNItemManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePick = UIImagePickerController()
        imagePick.allowsEditing = true
        imagePick.delegate = self
        // Do any additional setup after loading the view.
        
        
        if DataService.ds.currentUser.profilePicUrl == NO_PIC {
            profilePicImg.image = UIImage(named: "ProfilePic")
        } else {
            profilePicImg.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: DataService.ds.currentUser.profilePicUrl)
        }
        
        if Checker.checker.verificationMode {
            bankTitle.setTitle("Credit Card", for: .normal)
        } else {
            bankTitle.setTitle("Bank Account", for: .normal)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
        

        
        changeBtnColor()
    
    }
    
    func changeBtnColor() {
        
        if DataService.ds.currentUser.profilePicUrl != NO_PIC {
            Checker.checker.profileChecked = true
        }
        
        if Checker.checker.profileChecked {
            profilePicView.backgroundColor = OPEN_COLOR
        } else {
            profilePicView.backgroundColor = BUTTON_COLOR
        }
        
        if DataService.ds.currentUser.dob != NO_DOB {
            Checker.checker.dobChecked = true
        }
        
        if Checker.checker.dobChecked {
            dobView.backgroundColor = OPEN_COLOR
        } else {
            dobView.backgroundColor = BUTTON_COLOR
        }
        
//        if Checker.checker.languageChecked {
//            languageView.backgroundColor = OPEN_COLOR
//        } else {
//            languageView.backgroundColor = BUTTON_COLOR
//        }
        
        if Checker.checker.verificationMode {
            if let cardID = DataService.ds.currentUser.cardID {
                Checker.checker.creditChecked = true
            } else {
                Checker.checker.creditChecked = false
            }
            
            if Checker.checker.creditChecked {
                bankView.backgroundColor = OPEN_COLOR
            } else {
                bankView.backgroundColor = BUTTON_COLOR
            }
        } else {
            if DataService.ds.currentUser.bankAccount != nil {
                Checker.checker.bankAccountChecked = true
            } else {
                Checker.checker.bankAccountChecked = false
            }
            
            if Checker.checker.bankAccountChecked {
                bankView.backgroundColor = OPEN_COLOR
            } else {
                bankView.backgroundColor = BUTTON_COLOR
            }
        }

        
        if Checker.checker.allChecked() {
            saveBtn.backgroundColor = OPEN_COLOR
        } else {
            saveBtn.backgroundColor = UIColor.lightGray
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

    @IBAction func dobTapped(_ sender: Any) {
        
        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
        var components = DateComponents()
        components.year = -MIN_AGE
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        
        let page3 = DatePickerBulletinItem()
        
        page3.datePicker.maximumDate = minDate
        
        page3.requiresCloseButton = true
        page3.appearance.actionButtonColor = MAIN_COLOR
        page3.isDismissable = true
        page3.image = UIImage(named: "birthday")
        
        page3.descriptionText = "Your Big Day"
        page3.actionButtonTitle = "Confirm"
        
        page3.actionHandler = { (item: BLTNActionItem) in
            //            self.locationManager.requestWhenInUseAuthorization()
            let datePicked = page3.datePicker.date
            
            let dateFormatter = ISO8601DateFormatter()
            
//            self.deadline = NSNumber(integerLiteral: Int(datePicked.timeIntervalSince1970))
            
            let a = "\(datePicked)"
            let fullNameArr = a.components(separatedBy: " ")
            let date = fullNameArr[0]
            
            let newArray = date.components(separatedBy: "-")
            self.dobBtn.setTitle("\(newArray[2])-\(newArray[1])-\(newArray[0])", for: .normal)
            self.dob = "\(newArray[2])-\(newArray[1])-\(newArray[0])"
            
            let monthArray = a.components(separatedBy: " ")
            var newDate = "\(monthArray[0])T\(monthArray[1])\(monthArray[2])"
            print("min date \(newDate)")
//            let dateFormatter = ISO8601DateFormatter()
            let wula = dateFormatter.date(from:newDate)!
            print(wula.timeIntervalSince1970)
            self.deadline = NSNumber(integerLiteral: Int(wula.timeIntervalSince1970))
            self.dobView.backgroundColor = OPEN_COLOR
            Checker.checker.dobChecked = true
            self.changeBtnColor()
            
            item.manager?.dismissBulletin(animated: true)
        }
        
        bulletinManager = BLTNItemManager(rootItem: page3)
        bulletinManager.backgroundViewStyle = .blurredDark
        
        bulletinManager.showBulletin(above: self)
        
        
//        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
//        var components = DateComponents()
//        components.year = -MIN_AGE
//        let minDate = Calendar.current.date(byAdding: components, to: Date())
//
//        let datePicker = ActionSheetDatePicker(title: "Date of Birth", datePickerMode: UIDatePicker.Mode.date, selectedDate: minDate, doneBlock: {
//            picker, value, index in
////            self.deadlineLbl.resignFirstResponder()
//
//            print(value!)
//            let a = "\(value!)"
//            let fullNameArr = a.components(separatedBy: " ")
//            let date = fullNameArr[0]
//
//            let newArray = date.components(separatedBy: "-")
//            self.dobBtn.setTitle("\(newArray[2])-\(newArray[1])-\(newArray[0])", for: .normal)
//            self.dob = "\(newArray[2])-\(newArray[1])-\(newArray[0])"
//
//            var isoDate = "\(value!)"
//            isoDate.trimmingCharacters(in: .whitespaces)
//            let monthArray = isoDate.components(separatedBy: " ")
//
//            var newDate = "\(monthArray[0])T\(monthArray[1])\(monthArray[2])"
//            print("min date \(newDate)")
//            let dateFormatter = ISO8601DateFormatter()
//            let wula = dateFormatter.date(from:newDate)!
//            print(wula.timeIntervalSince1970)
//            self.deadline = NSNumber(integerLiteral: Int(wula.timeIntervalSince1970))
//            self.dobView.backgroundColor = OPEN_COLOR
//            Checker.checker.dobChecked = true
//            self.changeBtnColor()
//            //            var dateString = self.deadlineLbl.text
//            //            var dateFormatter = DateFormatter()
//            //            dateFormatter.dateFormat = "yyyy-MM-dd"
//            //            var dateFromString = dateFormatter.date(from: <#T##String#>)
//
//
//            return
//        }, cancel: { ActionStringCancelBlock in return
////            self.deadlineLbl.resignFirstResponder()
//        }, origin: self.view)
//
//
//
//
//        datePicker?.maximumDate = minDate
//        //            datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
//        print(minDate)
//        datePicker?.show()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVC = segue.destination as? BasicDetailVC {
            destinationVC.userDetails["Email"] = self.email
        }
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
        Checker.checker.resetAll()
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        
        if !Checker.checker.profileChecked {
            ErrorAlert.errorAlert.showAlert(title: "Details Missing", msg: "Please choose a profile picture.", object: self)
        } else if !Checker.checker.dobChecked {
            ErrorAlert.errorAlert.showAlert(title: "Details Missing", msg: "Please enter your date of birth.", object: self)
        /*} else if !Checker.checker.languageChecked {
            ErrorAlert().createAlert(title: "Details Missing", msg: "Please select at least one language.", object: self)*/
        } else if Checker.checker.verificationMode {
            if !Checker.checker.creditChecked {
                ErrorAlert.errorAlert.showAlert(title: "Details Missing", msg: "Please enter your credit card detail", object: self)
            } else {
                addProfilePic()
            }
        } else if !Checker.checker.verificationMode {
            if !Checker.checker.bankAccountChecked {
                ErrorAlert.errorAlert.showAlert(title: "Details Missing", msg: "Please enter your bank detail", object: self)
            } else {
                addProfilePic()
            }
        
        } else {
            
            addProfilePic()

        }
        
        
        
    }
    func addProfilePic() {
        SVProgressHUD.show()
//        self.tableView.isUserInteractionEnabled = false
        
//        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfilePicCell
        
//        if cell.profilePic.image == #imageLiteral(resourceName: "ProfilePic") {
//            self.postToFirebase(imgUrl: NO_PIC)
//        } else {
            if let img = profilePicImg.image  {
                
                if let imgData = img.jpegData(compressionQuality:0.1) {
                    
                    let imgUid = NSUUID().uuidString
                    let metaData = StorageMetadata()
                    metaData.contentType = "image/jpeg"
                    
                    DataService.ds.REF_PROFILE_PIC.child(imgUid).putData(imgData, metadata: metaData) { (metaData, error) in
                        if error != nil {
                            print("Xavier: upload image to firebase failed")
                            print(error.debugDescription)
                        } else {
                            print("Xavier: successfuly upload img to firebase")
                            let downloadUrl = metaData?.downloadURL()?.absoluteString
                            if let url = downloadUrl {
                                self.postToFirebase(imgUrl: url)
                            }
                            
                        }
                        
                    }
                }
                
            }
//        }
    }
    
    func postToFirebase(imgUrl: String) {
        SVProgressHUD.dismiss()
        
        let userDict: Dictionary<String, AnyObject> = ["isVerified": true as AnyObject, "dob": self.dob as AnyObject/*,"languages": Checker.checker.languageDict as AnyObject*/, "profilePicUrl": imgUrl as AnyObject]
        
        DataService.ds.currentUser.updateVerification(userData: userDict)
        Checker.checker.resetAll()
        self.navigationController?.dismiss(animated: true, completion: nil)
        
        
        
    }
    
    @IBAction func profilePicTapped(_ sender: Any) {
        let alert = UIAlertController()
        
//        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfilePicCell
        if profilePicImg.image == UIImage(named: "ProfilePic") {
            
            alert.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
                self.imagePick.sourceType = UIImagePickerController.SourceType.camera;
                self.present(self.imagePick, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Photo library", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
                self.imagePick.sourceType = UIImagePickerController.SourceType.photoLibrary;
                self.present(self.imagePick, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                
            })
            
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            alert.addAction(UIAlertAction(title: "Choose another picture", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
                self.present(self.imagePick, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Remove current profile picture", style: .destructive , handler:{ (UIAlertAction)in
                self.profilePicImg.image = #imageLiteral(resourceName: "ProfilePic")
//                SVProgressHUD.show()
                Checker.checker.profileChecked = false
                self.profilePicView.backgroundColor = MAIN_COLOR
                self.changeBtnColor()
//                self.deleteImageInStorage()
//                self.postToFirebase(imgUrl: NO_PIC)
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                
            })
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            
            if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
//                let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfilePicCell
                profilePicImg.image = image
                Checker.checker.profileChecked = true
                changeBtnColor()
//                addProfilePic()
            }
            
        }
        
        /// if the request successfully done just dismiss
        imagePick.dismiss(animated: true, completion: nil)
    }
    @IBAction func bankDetailTapped(_ sender: Any) {
//        Checker.checker.bankChecked = true
//        let cardVC = CardVC()
        let storyBoard: UIStoryboard = UIStoryboard(name: "SettingSB", bundle: nil)
        
        if Checker.checker.verificationMode {
            
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
            self.navigationController?.pushViewController(newViewController, animated: true)
        } else {
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ReceivePaymentVC") as! ReceivePaymentVC
            self.navigationController?.pushViewController(newViewController, animated: true)
        }

//        self.present(newViewController, animated: true, completion: nil)
//        self.performSegue(withIdentifier: "CardVC", sender: nil)
//        self.bankView.backgroundColor = OPEN_COLOR
    }
}
