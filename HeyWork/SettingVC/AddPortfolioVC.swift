//
//  AddPortfolioVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 23/2/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import UIKit
import Firebase
import Gallery
import SVProgressHUD

class AddPortfolioVC: UIViewController,GalleryControllerDelegate,UITextViewDelegate {

    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img5: UIImageView!
    @IBOutlet weak var img6: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageStacks: UIStackView!
    var imageViewArray: [UIImageView]!
    
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var detailTV: UITextView!
    
    var gallery: GalleryController!
    var portImages = [UIImage]()
    
    var edit = false
    var portfolio: Portfolio!
    
    var imageUrls = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gallery = GalleryController()
        
        gallery.delegate = self
        
//        Config.Permission.image = UIImage(named: ImageList.Gallery.cameraIcon)
//        Config.Font.Text.bold = UIFont(name: FontList.OpenSans.bold, size: 14)!
//        Config.Camera.recordLocation = true
        Config.Camera.imageLimit = 6
        Config.tabsToShow = [.imageTab]
        detailTV.delegate = self
//        imageViews = [img1,img2,img3,img4,img5,img6]
        detailTV.text = "Describe your portfolio..."
        detailTV.textColor = UIColor.lightGray
        
        self.navigationController?.isNavigationBarHidden = false
        

        imageStacks.layer.cornerRadius = 10
        imageStacks.clipsToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        self.hideKeyboardWhenTappedAround()
        
        if edit {
            self.titleTF.text = self.portfolio.portTitle
            self.detailTV.text = self.portfolio.portDesc
            var idx = 0
            
            for (key, url) in self.portfolio.portImageUrls {
                imageUrls.append(url)
                switch(idx){
                case 0:
                    self.img1.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: url)
                                        if let img = img1.image {
                                            self.portImages.append(img)
                                        }
                    
                    break
                case 1:
                    self.img2.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: url)
                                        if let img = img2.image {
                                            self.portImages.append(img)
                                        }
                    break
                case 2:
                    self.img3.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: url)
                                        if let img = img3.image {
                                            self.portImages.append(img)
                                        }
                    break
                case 3:
                    self.img4.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: url)
                                        if let img = img4.image {
                                            self.portImages.append(img)
                                        }
                    break
                case 4:
                    self.img5.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: url)
                                        if let img = img5.image {
                                            self.portImages.append(img)
                                        }
                    break
                case 5:
                    self.img6.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: url)
                                        if let img = img6.image {
                                            self.portImages.append(img)
                                        }
                    break
                default: break
                    
                }
                
                idx = idx + 1
            }
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    

        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "updatePortfolioNotification"), object: self)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            if !edit {
                textView.text = nil
            }
            
            textView.textColor = UIColor.black
            
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Describe your portfolio..."
            textView.textColor = UIColor.lightGray
        }
    }

    @objc func keyboardWillShow(notification:NSNotification){
        
        print("will show")
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 20
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
 
    
    @IBAction func dismisTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addPicTapped(_ sender: Any) {
        present(gallery, animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        var idx = 0
        self.resetImages()
        
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            print(resolvedImages.count)
            for img in resolvedImages {
                self?.portImages.append(img!)
                
                switch(idx){
                    case 0:
                        self!.img1.image = img
                        break
                    case 1:
                        self!.img2.image = img
                        break
                    case 2:
                        self!.img3.image = img
                        break
                    case 3:
                        self!.img4.image = img
                        break
                    case 4:
                        self!.img5.image = img
                        break
                    case 5:
                        self!.img6.image = img
                        break
                    default: break
                    
                }
                
                idx = idx + 1
            }
            
            
            
            controller.dismiss(animated: true, completion: nil)
        })
        
   
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func resetImages() {
        let placeholder = UIImage(named: "addPic")
        self.portImages = []
        self.img1.image = placeholder
        self.img2.image = placeholder
        self.img3.image = placeholder
        self.img4.image = placeholder
        self.img5.image = placeholder
    }
    
    func getImages() {
        if self.img1.image != nil {
            self.portImages.append(self.img1.image!)
        }
        
        if self.img2.image != nil {
            self.portImages.append(self.img2.image!)
        }
        
        if self.img3.image != nil {
            self.portImages.append(self.img3.image!)
        }
        
        if self.img4.image != nil {
            self.portImages.append(self.img4.image!)
        }
        
        if self.img5.image != nil {
           self.portImages.append(self.img5.image!)
        }
        
        if self.img6.image != nil {
            self.portImages.append(self.img6.image!)
        }
    }
    
    @IBAction func postPortTapped(_ sender: Any) {
        
        guard let title = titleTF.text else {
           
            return
        }
        
        guard let detail = detailTV.text else {
            
            return
        }
        
//        if portImages.count == 0 && imageUrls.count != 0 {
//            self.getImages()
//        }
//
//        guard let images = self.portImages else {
//            ErrorAlert().show(title: "Missing Images", msg: "Please upload your portfolio as images for show off.", object: self)
//            return
//        }
//
        
            
        
        if title.count == 0 {
            ErrorAlert.errorAlert.showAlert(title: "Missing Title", msg: "Please enter the title of your portfolio.", object: self)
        } else if detail.count == 0 {
            ErrorAlert.errorAlert.showAlert(title: "Missing Description", msg: "Please enter the desciption of your portfolio.", object: self)
        } else {
            
            var updateObj = [String:AnyObject]()
            if edit {
                
                var portID  = self.portfolio.portID!
                if self.portImages.count != 0 {
                    
                    for (key,url) in DataService.ds.currentUser.portfolio {
                        updateObj["/users/\(DataService.ds.currentUser.userKey!)/portfolio/\(portID)/portImageUrl/"] = NSNull() as AnyObject
                    }
                    
                    DB_BASE.updateChildValues(updateObj) { (error, ref) in
                        
                        if error != nil {
                            ErrorAlert.errorAlert.showAlert(title: (error?.localizedDescription)!, msg: error.debugDescription, object: self)
                        } else {
                            DataService.ds.uploadPortfolio(images: self.portImages, vc: self, portTitle: title, portDesc: detail,portID: self.portfolio.portID, completion: {
                                NotificationCenter.default.post(name:
                                    NSNotification.Name(rawValue: "updatePortfolioNotification"), object: nil)
                                self.dismiss(animated: true, completion: nil)
                            })
                        }
                    }
                    
                } else {
                    
                    
//                    updateObj["/users/\(id)/portfolio/\(portID)/portImageUrl/url\(idx)"] = url as AnyObject
                    updateObj["/users/\(DataService.ds.currentUser.userKey!)/portfolio/\(portID)/portTitle/"] = title as AnyObject
                    updateObj["/users/\(DataService.ds.currentUser.userKey!)/portfolio/\(portID)/portDesc/"] = detail as AnyObject
                    
                    DB_BASE.updateChildValues(updateObj, withCompletionBlock: { (error, ref) in
                        
                        if error != nil {
                            ErrorAlert.errorAlert.showAlert(title: (error?.localizedDescription)!, msg: error.debugDescription, object: self)
                        } else {
                            SVProgressHUD.dismiss()
                            //                                        self.currentUser.portfolio[portID] = Portfolio(portKey: portID, dict: portDict)
                            
                            NotificationCenter.default.post(name:
                                NSNotification.Name(rawValue: "updatePortfolioNotification"), object: nil)
                            self.dismiss(animated: true, completion: nil)
                        }
                    })
                }

                
            } else {
                var portID = DataService.ds.REF_USER_PORT.childByAutoId().key
                
                if self.portImages.count == 0 {
                    ErrorAlert.errorAlert.showAlert(title: "Missing Images", msg: "Please upload your portfolio as images for show off.", object: self)
                } else {
                    
                    DataService.ds.uploadPortfolio(images: self.portImages, vc: self, portTitle: title, portDesc: detail, portID:portID, completion: {
                        NotificationCenter.default.post(name:
                            NSNotification.Name(rawValue: "updatePortfolioNotification"), object: nil)
                        self.dismiss(animated: true, completion: nil)
                    })
                }
               
            }
            
        }
        
        
    }
    
    
}
