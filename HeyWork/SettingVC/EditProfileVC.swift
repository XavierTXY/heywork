//
//  EditProfileVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 21/11/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import TagListView
import ActionSheetPicker_3_0
import Firebase
import PopupDialog
import SVProgressHUD
import BLTNBoard

class EditProfileCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var detailTF: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(title: String, data: String) {
        titleLbl.text = title
        detailTF.text = data
    }
    
}

class EditBioCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var bioTV: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        bioTV.layer.borderColor = UIColor.lightGray.cgColor
//        bioTV.layer.borderWidth = 1.0
        bioTV.layer.cornerRadius = 5.0
    }
    
    func configure(bio: String) {
//        titleLbl.text = title
        bioTV.text = bio
    }
    
}

class EditSkillCell: UITableViewCell {
    

    @IBOutlet weak var skillView: TagListView!
    var vc: EditProfileVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(skills: [String: Bool]) {
        for (skill, bo) in skills {
            if skill != "key" {
                skillView.addTag(skill)
            }
            
        }
    }
    
    @IBAction func addSkillTapped(_ sender: Any) {
        vc.addSkills()
    }
}


class EditProfileVC: UIViewController, UITableViewDataSource, UITableViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    var settings = [String]()
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var skillTag: TagListView!
    
    var dob: String?
    var skillDict: [String: Bool]!
    var madeChanges = false
    var changedPic = false
    var newSkill = [String:Bool]()
    var imagePick: UIImagePickerController!

    var bulletinManager: BLTNItemManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Edit Profile"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveTapped))
        
        imagePick = UIImagePickerController()
        imagePick.allowsEditing = true
        imagePick.delegate = self
        skillDict = DataService.ds.currentUser.skills
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
//        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.sizeToFit()
        
        profilePic.layer.cornerRadius = 5
        profilePic.clipsToBounds = true
        
        settings = ["Email", "Username", "Name", "Birthday", "Bio", "Skills"]
        
        
        if DataService.ds.currentUser.profilePicUrl == NO_PIC{
            profilePic.image = UIImage(named: "ProfilePic")
        } else {
            profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: DataService.ds.currentUser.profilePicUrl)
        }
        
        if DataService.ds.currentUser.dob != nil {
            self.dob = DataService.ds.currentUser.dob
        }
        
        
        self.hideKeyboardWhenTappedAround()
        
        for(skill, tick) in DataService.ds.currentUser.skills {
            if skill != "key" {
                Checker.checker.skills[skill] = skill
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        newSkill.removeAll()
        
        skillTag.removeAllTags()
        
//        if Checker.checker.skills.count != 0 {
        
//            newSkill["Use This App"] = true
//            skillTag.addTag("Use This App")
        
            for(skill, tick) in Checker.checker.skills {
                
                newSkill[skill] = true
                skillTag.addTag(skill)
            }
            
        
//        } else {
//            newSkill = DataService.ds.currentUser.skills
//
//            for(skill, tick) in DataService.ds.currentUser.skills {
//                if skill != "key" {
//                    skillTag.addTag(skill)
//                }
//
//            }
//        }
//

       
        

        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        Checker.checker.skills.removeAll()
//        let popup = PopupDialog(title: "Warning", message: "Do you want to save?")
//        popup.transitionStyle = .zoomIn
//        // Create buttons
//        let buttonOne = CancelButton(title: "Cancel") {
//            //            print("You canceled the car dialog.")
//        }
//        let buttonTwo = CancelButton(title: "Save") {
//            //            print("You canceled the car dialog.")
//        }
//
//        buttonTwo.backgroundColor = MAIN_COLOR
//        buttonTwo.titleColor = UIColor.white
//
//        popup.addButtons([buttonTwo,buttonOne])
//        self.present(popup, animated: true, completion: nil)
//
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileCell") as? EditProfileCell
            cell?.selectionStyle = .none
            cell?.detailTF.isUserInteractionEnabled = false
            cell?.accessoryType = .none
            cell?.configure(title: settings[indexPath.row], data: DataService.ds.currentUser.email)
            return cell!
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileCell") as? EditProfileCell
            cell?.selectionStyle = .none
            cell?.detailTF.isUserInteractionEnabled = false
            cell?.accessoryType = .none
            cell?.configure(title: settings[indexPath.row], data: "@\(DataService.ds.currentUser.userName)")
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileCell") as? EditProfileCell
            cell?.selectionStyle = .none
            cell?.detailTF.isUserInteractionEnabled = true
            cell?.accessoryType = .none
            cell?.configure(title: settings[indexPath.row], data: DataService.ds.currentUser.name)
            return cell!
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileCell") as? EditProfileCell
            cell?.selectionStyle = .none
            cell?.detailTF.isUserInteractionEnabled = false
            cell?.accessoryType = .none
            
            print(DataService.ds.currentUser.dob)
            if DataService.ds.currentUser.dob == NO_DOB || DataService.ds.currentUser.dob == nil {
                cell?.configure(title: settings[indexPath.row], data: "Please set your birthday")
            } else {
                cell?.configure(title: settings[indexPath.row], data: DataService.ds.currentUser.dob!)
            }
            
            return cell!
//        case 4:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileCell") as? EditProfileCell
//            cell?.selectionStyle = .none
//            cell?.detailTF.isUserInteractionEnabled = true
//            cell?.accessoryType = .none
//            cell?.configure(title: settings[indexPath.row], data: DataService.ds.currentUser.name)
//            return cell!
//        case 5:
//
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditBioCell") as? EditBioCell
            cell?.selectionStyle = .none
//            cell?.detailTF.isUserInteractionEnabled = false
//            cell?.accessoryType = .disclosureIndicator
            cell?.configure(bio: DataService.ds.currentUser.bio)
            return cell!
//        case 7:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "EditSkillCell") as? EditSkillCell
//            cell?.selectionStyle = .none
//            cell?.vc = self
//            //            cell?.detailTF.isUserInteractionEnabled = false
//            //            cell?.accessoryType = .disclosureIndicator
//            cell?.configure(skills: newSkill)
//            return cell!
        default:
            return UITableViewCell()
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        madeChanges = true
        switch indexPath.row {
        case 3:
            
            print("tap date")
            let cell = tableView.cellForRow(at: indexPath) as! EditProfileCell
            
            let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
            var components = DateComponents()
            components.year = -MIN_AGE
            let minDate = Calendar.current.date(byAdding: components, to: Date())
            
            let page3 = DatePickerBulletinItem()
            
            page3.datePicker.maximumDate = minDate
            
            page3.requiresCloseButton = true
            page3.appearance.actionButtonColor = MAIN_COLOR
            page3.isDismissable = true
            page3.image = UIImage(named: "birthday")
            
            page3.descriptionText = "Your Big Day"
            page3.actionButtonTitle = "Confirm"
            
            page3.actionHandler = { (item: BLTNActionItem) in
                //            self.locationManager.requestWhenInUseAuthorization()
                let datePicked = page3.datePicker.date
                
                let dateFormatter = ISO8601DateFormatter()
                
                //            self.deadline = NSNumber(integerLiteral: Int(datePicked.timeIntervalSince1970))
                
                let a = "\(datePicked)"
                let fullNameArr = a.components(separatedBy: " ")
                let date = fullNameArr[0]
                
                let newArray = date.components(separatedBy: "-")
                cell.detailTF.text = ("\(newArray[2])-\(newArray[1])-\(newArray[0])")
                self.dob = "\(newArray[2])-\(newArray[1])-\(newArray[0])"
                
                let monthArray = a.components(separatedBy: " ")
                var newDate = "\(monthArray[0])T\(monthArray[1])\(monthArray[2])"
                print("min date \(newDate)")
//                //            let dateFormatter = ISO8601DateFormatter()
//                let wula = dateFormatter.date(from:newDate)!
//                print(wula.timeIntervalSince1970)
//                self.deadline = NSNumber(integerLiteral: Int(wula.timeIntervalSince1970))
//                self.dobView.backgroundColor = OPEN_COLOR
//                Checker.checker.dobChecked = true
//                self.changeBtnColor()
                
                item.manager?.dismissBulletin(animated: true)
            }
            
            bulletinManager = BLTNItemManager(rootItem: page3)
            bulletinManager.backgroundViewStyle = .blurredDark
            
            bulletinManager.showBulletin(above: self)
            
            
//            let datePicker = ActionSheetDatePicker(title: "Date of Birth", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
//                picker, value, index in
//                //            self.deadlineLbl.resignFirstResponder()
//                
//                print(value!)
//                let a = "\(value!)"
//                let fullNameArr = a.components(separatedBy: " ")
//                let date = fullNameArr[0]
//                
//                let newArray = date.components(separatedBy: "-")
//                cell.detailTF.text = ("\(newArray[2])-\(newArray[1])-\(newArray[0])")
//                self.dob = ("\(newArray[2])-\(newArray[1])-\(newArray[0])")
//                var isoDate = "\(value!)"
//                isoDate.trimmingCharacters(in: .whitespaces)
//                let monthArray = isoDate.components(separatedBy: " ")
//                
////                self.dob = "\(monthArray[0]) - \(monthArray[1]) - \(monthArray[2])"
////                print(newDate)
////                let dateFormatter = ISO8601DateFormatter()
////                let wula = dateFormatter.date(from:newDate)!
////                print(wula.timeIntervalSince1970)
////                self.dob = NSNumber(integerLiteral: Int(wula.timeIntervalSince1970))
////                self.dobView.backgroundColor = OPEN_COLOR
////                Checker.checker.dobChecked = true
////                self.changeBtnColor()
//                //            var dateString = self.deadlineLbl.text
//                //            var dateFormatter = DateFormatter()
//                //            dateFormatter.dateFormat = "yyyy-MM-dd"
//                //            var dateFromString = dateFormatter.date(from: <#T##String#>)
//                
//                
//                return
//            }, cancel: { ActionStringCancelBlock in return
//                //            self.deadlineLbl.resignFirstResponder()
//            }, origin: self.view)
//            let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
//            var components = DateComponents()
//            components.year = -MIN_AGE
//            let minDate = Calendar.current.date(byAdding: components, to: Date())
//            
//            
//            datePicker?.maximumDate = minDate
//            //            datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
//            
//            datePicker?.show()
            break
            
        default:
            break
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 {
            return 50
        } else if indexPath.row == 4 {
            return 147
        } else {
            return 123
        }
//        if tableView.cellForRow(at: indexPath) is EditProfileCell {
//            return 50
//        } else if tableView.cellForRow(at: indexPath) is EditBioCell {
//            return 147
//        } else if tableView.cellForRow(at: indexPath) is EditSkillCell {
//            return 123
//        } else {
//            return 147
//        }
       
    }
    @IBAction func dismissTapped(_ sender: Any) {
        
        if madeChanges {
            
            self.saveUser()
            

        } else {
            self.dismiss(animated: true, completion: nil)
            Checker.checker.skills.removeAll()
        }

       
    }
    
    func checkName(name: String, errorMsg: String) -> Bool {
        
        
        //        Interaction().disableInteraction(msg: "Checking user name...")
        //        SVProgressHUD.show()
        //        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let trimmedName = name
        
        if( (trimmedName.contains(find: ".")) ) {
            ErrorAlert.errorAlert.showAlert(title: "Invalid \(errorMsg)", msg: "\(errorMsg) can't contain symbols", object: self)
            return false
        } else if (trimmedName.containsEmoji) {
            ErrorAlert.errorAlert.showAlert(title: "Invalid \(errorMsg)", msg: "\(errorMsg) can't contain emoji", object: self)
            return false
        } else if( trimmedName.count == 0 ){
            ErrorAlert.errorAlert.showAlert(title: "Invalid \(errorMsg)", msg: "\(errorMsg) can't be empty", object: self)
            return false
        } else {
            return true
        }
    }
    
    @IBAction func addSkillFromVC(_ sender: Any) {
        addSkills()
    }
    
    func addSkills() {
        self.madeChanges = true
        view.endEditing(true)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "SkillVC") as! SkillVC
       newViewController.fromEditProfileVC = true
        
        if Checker.checker.skills.count == 0 {
            for (type, tick) in DataService.ds.currentUser.skills {
                var idx = 0
                for skill in SKILLS {
                    if type == skill {
                        Checker.checker.skills[skill] = skill
                    }
                    idx = idx + 1
                }
          
                
            }
        }

        self.present(newViewController, animated: true, completion: nil)
    }
    
    func updateUser(imgUrl: String) {
        Checker.checker.skills.removeAll()
        
        let cell = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! EditProfileCell
        let cell2 = tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! EditProfileCell
        let cell3 = tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as! EditBioCell
//        let cell4 = tableView.cellForRow(at: IndexPath(row: 5, section: 0)) as! EditProfileCell
//        let cell5 = tableView.cellForRow(at: IndexPath(row: 6, section: 0)) as! EditBioCell
        
        if self.checkName(name: cell.detailTF.text!, errorMsg: "Name") {
//            if self.checkName(name: cell2.detailTF.text!, errorMsg: "First Name") {
//                if self.checkName(name: cell3.detailTF.text!, errorMsg: "Last Name") {
            if self.dob != nil {
                if self.dob != NO_DOB {
                    
                    let userDict: Dictionary<String, AnyObject> = ["name": cell.detailTF.text! as AnyObject, "skills": self.newSkill as AnyObject,"bio": cell3.bioTV.text as AnyObject, "profilePicUrl": imgUrl as AnyObject, "dob": self.dob! as AnyObject]
                    DataService.ds.currentUser.updateProfile(userData: userDict)
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    let userDict: Dictionary<String, AnyObject> = ["name": cell.detailTF.text! as AnyObject, "skills": self.newSkill as AnyObject,"bio": cell3.bioTV.text as AnyObject, "profilePicUrl": imgUrl as AnyObject, "dob": NO_DOB as AnyObject]
                    DataService.ds.currentUser.updateProfile(userData: userDict)
                    self.navigationController?.popViewController(animated: true)
                    
                }
            } else {
                let userDict: Dictionary<String, AnyObject> = ["name": cell.detailTF.text! as AnyObject, "skills": self.newSkill as AnyObject,"bio": cell3.bioTV.text as AnyObject, "profilePicUrl": imgUrl as AnyObject, "dob": NO_DOB as AnyObject]
                DataService.ds.currentUser.updateProfile(userData: userDict)
                self.navigationController?.popViewController(animated: true)
            }
            SVProgressHUD.dismiss()

//                    if self.dob != nil {
//
//
//                        print(userDict)
//                    } else {
//
//                        print(userDict)
//                    }
//
            
                    
//                }
//            }
        }
    }
    
    func saveUser() {

        
        let alert = UIAlertController(title: "New Changes", message: "Do you want to save new changes?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            Checker.checker.skills.removeAll()
            self.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Save", style: .default) { (action) in
            
//            self.updateUser()
            self.addProfilePic()
            
            
            
        })
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func saveTapped(_ sender: Any) {
        //save
        self.view.endEditing(true)
        
        if newSkill.count == 0 {
            ErrorAlert.errorAlert.showAlert(title: "Skills needed", msg: "Please remember to add at least one skill", object: self)
        } else {
            self.addProfilePic()
        }
        
    }
    @IBAction func profilePicTapped(_ sender: Any) {
        let alert = UIAlertController()
        
        //        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfilePicCell

//        if profilePic.image == UIImage(named: "ProfilePic") {
        
            alert.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
                self.imagePick.sourceType = UIImagePickerController.SourceType.camera;
                self.present(self.imagePick, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Photo library", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
                self.imagePick.sourceType = UIImagePickerController.SourceType.photoLibrary;
                self.present(self.imagePick, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                
            })
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
            self.present(alert, animated: true, completion: nil)
        
      
        
        
//        } else {
//
//            alert.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
//                self.imagePick.sourceType = UIImagePickerController.SourceType.camera;
//                self.present(self.imagePick, animated: true, completion: nil)
//            }))
//
//            alert.addAction(UIAlertAction(title: "Photo library", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
//                self.imagePick.sourceType = UIImagePickerController.SourceType.photoLibrary;
//                self.present(self.imagePick, animated: true, completion: nil)
//            }))
//
//            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//
//            })
//
//            self.present(alert, animated: true, completion: nil)
//
//
//            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//
//            })
//
//            self.present(alert, animated: true, completion: nil)
//        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            
            if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                //                let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfilePicCell
                self.profilePic.image = image
                madeChanges = true
                changedPic = true
//                Checker.checker.profileChecked = true
//                changeBtnColor()
                //                addProfilePic()
            }
            
        }
        
        /// if the request successfully done just dismiss
        imagePick.dismiss(animated: true, completion: nil)
    }
    
    func addProfilePic() {
        SVProgressHUD.show()
        //        self.tableView.isUserInteractionEnabled = false
        
        //        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfilePicCell
        
        //        if cell.profilePic.image == #imageLiteral(resourceName: "ProfilePic") {
        //            self.postToFirebase(imgUrl: NO_PIC)
        //        } else {
//        if !madeChanges {
//            self.navigationController?.popViewController(animated: true)
//            SVProgressHUD.dismiss()
//        } else {
            if profilePic.image != UIImage(named: "ProfilePic") {
                if changedPic {
                    if let img = profilePic.image  {
                        
                        if let imgData = img.jpegData(compressionQuality:0.1) {
                            
                            let imgUid = NSUUID().uuidString
                            let metaData = StorageMetadata()
                            metaData.contentType = "image/jpeg"
                            
                            DataService.ds.REF_PROFILE_PIC.child(imgUid).putData(imgData, metadata: metaData) { (metaData, error) in
                                if error != nil {
                                    print("Xavier: upload image to firebase failed")
                                    print(error.debugDescription)
                                } else {
                                    print("Xavier: successfuly upload img to firebase")
                                    
                                    let downloadUrl = metaData?.downloadURL()?.absoluteString
                                    if let url = downloadUrl {
                                        //                            self.postToFirebase(imgUrl: url)
                                        self.updateUser(imgUrl: url)
                                    }
                                    
                                }
                                
                            }
                        }
                        
                    }
                } else {
                    self.updateUser(imgUrl: DataService.ds.currentUser.profilePicUrl)
                }

            } else {
                self.updateUser(imgUrl: NO_PIC)
            }

//        }


        //        }
    }
    
//    func postToFirebase(imgUrl: String) {
////        SVProgressHUD.dismiss()
//
//        let userDict: Dictionary<String, AnyObject> = ["isVerified": true as AnyObject, "dob": self.deadline as AnyObject,"languages": Checker.checker.languageDict as AnyObject,"bank": ["cardNo":"123", "cardName": "ABC", "cardExDate":"18/18", "CVV": "123"] as AnyObject, "profilePicUrl": imgUrl as AnyObject]
//
//        DataService.ds.currentUser.updateVerification(userData: userDict)
//        Checker.checker.resetAll()
//        self.navigationController?.dismiss(animated: true, completion: nil)
//
//
//
//    }
}
