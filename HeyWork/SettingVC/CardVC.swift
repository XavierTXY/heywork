//
//  CardVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 5/12/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import BetterSegmentedControl
import PopupDialog

class CardCell: UITableViewCell {
    @IBOutlet weak var cardNumLbl: UILabel!
    @IBOutlet weak var expLbl: UILabel!
    @IBOutlet weak var brandImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func configureCell(card: BankCard) {
        cardNumLbl.text = card.cardNo
        expLbl.text = card.cardExDate
        
        switch(card.cardBrand) {
        case AMEX:
            brandImgView.image = UIImage(named: "american-express")
            break
        case DINER:
            brandImgView.image = UIImage(named: "diners-club")
            break
        case DISCOVER:
            brandImgView.image = UIImage(named: "discover")
            break
        case JCB:
            brandImgView.image = UIImage(named: "jcb")
            break
        case MASTER:
            brandImgView.image = UIImage(named: "mastercard")
            break
        case UNION_PAY:
            brandImgView.image = UIImage(named: "western-union")
            break
        case VISA:
            brandImgView.image = UIImage(named: "visa")
            break
        case UNKNOWN:
            brandImgView.image = UIImage(named: "credit-card")
            break
        default:
            break
        }
    }
}

class BankCell: UITableViewCell {
    @IBOutlet weak var bankNameLbl: UILabel!
    @IBOutlet weak var bankNumLbl: UILabel!
    @IBOutlet weak var bankImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(account: BankAccount) {
        bankNameLbl.text = account.bankName
        bankNumLbl.text = account.accNo
    }
    
}

class AddCreditBankCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(setupName: String) {
       print("cell")
    }
}

class CardVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var segControl: BetterSegmentedControl!
    var cards = [BankCard]()
    var accounts = [BankAccount]()
    var makePaymentMode = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Payment Method"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        segControl.segments = [LabelSegment(text: "Make Payment"), LabelSegment(text: "Receive Payment")]
        segControl.addTarget(self, action: #selector(valueChanged), for: .valueChanged)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        cards.removeAll()
        accounts.removeAll()
        
        if DataService.ds.currentUser.bankCard == nil  {
//            cards.removeAll()
//            addBtn.isHidden = false
        } else {
//            addBtn.isHidden = true
            cards.append(DataService.ds.currentUser.bankCard!)
        }
        
        if DataService.ds.currentUser.bankAccount == nil  {
            //            cards.removeAll()
//            addBtn.isHidden = false
        } else {
//            addBtn.isHidden = true
            print("added a bank accoount")
            accounts.append(DataService.ds.currentUser.bankAccount!)
        }
        
        if DataService.ds.currentUser.bankCard != nil && DataService.ds.currentUser.bankAccount != nil {
            addBtn.isHidden = true
        } else {
            addBtn.isHidden = false
        }
        self.tableView.reloadData()
//        print(DataService.ds.currentUser.bankCard?.cardNo)
        
    }
    
//
//    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
////        if section == 0 {
////            return "Credit Card"
////        } else if section == 1 {
////            return "Bank Account"
////        } else {
////            return ""
////        }
//
//        return "hi"
//    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return
//    }
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 2
//    }
//
    @objc func valueChanged() {
        self.tableView.alpha = 0.0
        
        
        if segControl.index == 0 {
            makePaymentMode = true
        } else if segControl.index == 1 {
             makePaymentMode = false
        }

        UIView.animate(withDuration: 0.1, delay: 0.1, options: [], animations: { self.tableView.alpha = 1.0 }, completion: { (bool) in
            
        })
        
        self.tableView.reloadData()
        
        
        
    }
    
    func switchType() {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            makePaymentMode = true

            
        case 1:
            makePaymentMode = false

        default:
            break
        }
        
        self.tableView.reloadData()
    }

    
    @IBAction func typeTapped(_ sender: Any) {
        switchType()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   
        if makePaymentMode {
            if DataService.ds.currentUser.bankCard == nil  {
                return 0
            } else {
                return cards.count
            }
        } else {
            if DataService.ds.currentUser.bankAccount == nil  {
                return 0
            } else {
                return accounts.count
            }
        }
//        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        
        if makePaymentMode {
            if DataService.ds.currentUser.bankCard != nil  {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell") as? CardCell
                cell?.selectionStyle = .none
                //        cell?.detailTF.isUserInteractionEnabled = false
                //        cell?.accessoryType = .none
                cell?.configureCell(card: cards[indexPath.row])
                return cell!
            } else {
                return UITableViewCell()
            }
        } else {
            if DataService.ds.currentUser.bankAccount != nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BankCell") as? BankCell
                cell?.selectionStyle = .none
                cell?.configureCell(account: accounts[indexPath.row])
                return cell!
            } else {
                return UITableViewCell()
            }
        }

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if makePaymentMode {
            let popup = PopupDialog(title: "Make Payment Options", message: "")
            popup.transitionStyle = .zoomIn

            
            if DataService.ds.currentUser.bankCard != nil  {
                
                // Create buttons
                let buttonOne = CancelButton(title: "Cancel") {
                    //            print("You canceled the car dialog.")
                }
                
                let buttonTwo = DefaultButton(title: "Add New Credit Card") {
                    //            print("You canceled the car dialog.")
                    self.performSegue(withIdentifier: "PaymentVC", sender: nil)
                }
                
                let buttonThree = DestructiveButton(title: "Remove Credit Card") {
                    //            print("You canceled the car dialog.")
                    if DataService.ds.currentUser.isWorking! {
                        //popout saying cant
                        ErrorAlert.errorAlert.showAlert(title: "Unable to Remove", msg: "You are working on a task!", object: self)
                    } else {
                        let popup2 = PopupDialog(title: "Remove Credit Card", message: "Are you sure to remove?")
                        popup2.transitionStyle = .zoomIn
                        
                        
                        
                        let button1 = CancelButton(title: "Cancel") {
                            //            print("You canceled the car dialog.")
                        }
                        
                        let button2 = DestructiveButton(title: "Remove") {
                            DataService.ds.currentUser.removeCreditCard {
                                self.tableView.reloadData()
                            }
                        }
                        
                        popup2.addButtons([button1,button2])
                        self.present(popup2, animated: true, completion: nil)
                    }
                        //
                }
//                buttonTwo.backgroundColor = MAIN_COLOR
//                buttonTwo.titleColor = UIColor.white
                
                popup.addButtons([buttonTwo,buttonThree,buttonOne])
                self.present(popup, animated: true, completion: nil)
//                let alert = UIAlertController(title: "Make Payment Options", message: "", preferredStyle: .alert)
//
//                alert.addAction(UIAlertAction(title: "Add New Credit Card", style: .default, handler: { (action) in
//                    self.performSegue(withIdentifier: "PaymentVC", sender: nil)
//                }))
//
//                alert.addAction(UIAlertAction(title: "Remove Credit Card", style: .destructive, handler: { (action) in
//                    if DataService.ds.currentUser.isWorking! {
//                        //popout saying cant
//                    } else {
//                        DataService.ds.currentUser.removeCreditCard {
//                            self.tableView.reloadData()
//                        }
//                    }
//
//                }))
//
//                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//
//                self.present(alert, animated: true, completion: nil)
            }
        } else {
            
            let popup = PopupDialog(title: "Receive Payment Options", message: "")
            popup.transitionStyle = .zoomIn
            
            if DataService.ds.currentUser.bankAccount != nil  {
                
                let buttonOne = CancelButton(title: "Cancel") {
                    //            print("You canceled the car dialog.")
                }
                
                let buttonTwo = DefaultButton(title: "Add New Bank Account") {
                    //            print("You canceled the car dialog.")
                    self.performSegue(withIdentifier: "ReceivePaymentVC", sender: nil)
                }
                
                let buttonThree = DestructiveButton(title: "Remove Bank Account") {
                    //            print("You canceled the car dialog.")
//                    if DataService.ds.currentUser.isWorking! {
//                        //popout saying cant
//                    } else {
                        let popup2 = PopupDialog(title: "Remove Bank Account", message: "Are you sure to remove?")
                        popup2.transitionStyle = .zoomIn
                        
                        let button1 = DefaultButton(title: "Cancel") {
                            //            print("You canceled the car dialog.")
                        }
                        
                        let button2 = DestructiveButton(title: "Remove") {
                            DataService.ds.currentUser.removeBankAccount {
                                self.tableView.reloadData()
                            }
                        }
                        
                        popup2.addButtons([button1,button2])
                        self.present(popup2, animated: true, completion: nil)
                        
                        

                    }

//                }
                
//                buttonTwo.backgroundColor = MAIN_COLOR
//                buttonTwo.titleColor = UIColor.white
                
                popup.addButtons([buttonTwo,buttonThree,buttonOne])
                self.present(popup, animated: true, completion: nil)
//                let alert = UIAlertController(title: "Receive Payment Options", message: "", preferredStyle: .alert)
//
//                alert.addAction(UIAlertAction(title: "Add New Bank Account", style: .default, handler: { (action) in
//                    self.performSegue(withIdentifier: "ReceivePaymentVC", sender: nil)
//                }))
//
//                alert.addAction(UIAlertAction(title: "Remove Bank Account", style: .destructive, handler: { (action) in
//                    if DataService.ds.currentUser.isWorking! {
//                        //popout saying cant
//                    } else {
//                        DataService.ds.currentUser.removeBankAccount {
//                            self.tableView.reloadData()
//                        }
//
//                    }
//                }))
//
//                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//
//                self.present(alert, animated: true, completion: nil)
            }
            
        }
        

//        self.performSegue(withIdentifier: "PaymentVC", sender: nil)
    }

    @IBAction func addTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Add Payment Method", message: "", preferredStyle: .alert)
        
        let popup2 = PopupDialog(title: "Add Payment Method", message: "")
        popup2.transitionStyle = .zoomIn
        
//        let button1 = DefaultButton(title: "Cancel") {
//            //            print("You canceled the car dialog.")
//        }
//
//        let button2 = DestructiveButton(title: "Remove") {
//            DataService.ds.currentUser.removeBankAccount {
//                self.tableView.reloadData()
//            }
//        }
//
//        popup2.addButtons([button1,button2])
        
        
        if DataService.ds.currentUser.bankCard == nil  {
            
            let button1 = DefaultButton(title: "Add Credit/Debit Card") {
                self.performSegue(withIdentifier: "PaymentVC", sender: nil)
            }
            popup2.addButton(button1)
//            alert.addAction(UIAlertAction(title: "Add Credit Card", style: .default, handler: { (action) in
//                self.performSegue(withIdentifier: "PaymentVC", sender: nil)
//            }))
        }
        
        if DataService.ds.currentUser.bankAccount == nil  {
            let button2 = DefaultButton(title: "Add Receiving Account") {
                self.performSegue(withIdentifier: "ReceivePaymentVC", sender: nil)
            }
            popup2.addButton(button2)
//            alert.addAction(UIAlertAction(title: "Add Receiving Account", style: .default, handler: { (action) in
//                self.performSegue(withIdentifier: "ReceivePaymentVC", sender: nil)
//            }))
        }
        
        if DataService.ds.currentUser.bankAccount == nil || DataService.ds.currentUser.bankCard == nil {
            let button3 = CancelButton(title: "Cancel") {
                //            print("You canceled the car dialog.")
            }
            popup2.addButton(button3)
//            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//            self.present(alert, animated: true, completion: nil)
            
            
            self.present(popup2, animated: true, completion: nil)
        }
        
        
        
        
        
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let dest = segue.destination as? PaymentVC {
//            if DataService.ds.currentUser.bankCard != nil  {
//                dest.card = DataService.ds.currentUser.bankCard!
//            }
//
//        }
//    }
}
