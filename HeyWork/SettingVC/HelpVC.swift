//
//  HelpVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 10/12/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import PopupDialog

class HelpCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(title: String) {
        titleLbl.text = title
    }
}
class HelpVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var helps = ["Terms and Conditions" , "Privacy" , "Contact Us"]
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return helps.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpCell", for: indexPath) as! HelpCell
        
        cell.configure(title: helps[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            guard let url = URL(string: TC_LINK) else { return }
            UIApplication.shared.openURL(url)
            break
        case 1:
            guard let url = URL(string: PRIVACY_LINK) else { return }
            UIApplication.shared.openURL(url)
            break
        case 2:
            showFeedbackDialog()
            break
        default:
            break
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func showFeedbackDialog() {
        let feedVC = FeedbackView(nibName: "FeedbackView", bundle: nil)
        
       
            let popup = PopupDialog(viewController: feedVC, buttonAlignment: .horizontal, transitionStyle: .zoomIn, tapGestureDismissal: false)
            
            // Create first button
            let buttonOne = CancelButton(title: "Cancel", height: 60) {
                
            }
            
            // Create second button
            let buttonTwo = DefaultButton(title: "Submit", height: 60) {
                
                if feedVC.feedbackTV.text!.count == 0 {
                    ErrorAlert.errorAlert.showAlert(title: "Feedback Needed!", msg: "Can't be emtpty", object: self)
                } else {
                    DataService.ds.currentUser.uploadFeedback(feedback: ["username": DataService.ds.currentUser!.userName as AnyObject, "feedback": feedVC.feedbackTV!.text as AnyObject, "time": Date().getCurrentTime() as AnyObject], completion: { (error) in
                        
//                        if error != nil {
//                            ErrorAlert().show(title: error.localizedDescription, msg: error.localizedDescription, object: self)
//
                        ErrorAlert.errorAlert.showAlert(title: "Done", msg: "Thanks for your feedback!", object: self)
                    })
                }
                
            }
            
            // Add buttons to dialog
            popup.addButtons([buttonOne, buttonTwo])
            
            // Present dialog
            self.present(popup, animated: true, completion: nil)
        
        
        
    }

}
