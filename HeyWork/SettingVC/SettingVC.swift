//
//  SettingVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 21/11/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import PopupDialog

class SettingCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var notiView: UIView!
    
    @IBOutlet weak var switchBtn: UISwitch!
    
    var settingVC: SettingVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        notiView.layer.cornerRadius = notiView.frame.width / 2;
        notiView.layer.masksToBounds = true
//        notiView
    }
    
    func configureCell(title: String, hideView: Bool, hideSwitch: Bool) {
        titleLbl.text = title
        
        notiView.isHidden = hideView
        switchBtn.isHidden = hideSwitch
        
        if !hideSwitch {
            self.accessoryType = .none
            if let p = DataService.ds.currentUser.currentLocation {
                switchBtn.setOn(true, animated: true)
            } else {
                switchBtn.setOn(false, animated: true)
            }
        } else {
            self.accessoryType = .disclosureIndicator
        }
        
        if title == "Logout" {
            titleLbl.textColor = UIColor.red
        } else {
            titleLbl.textColor = MAIN_COLOR
        }
        
    }
    @IBAction func switchTapped(_ sender: Any) {
        
        if switchBtn.isOn {
            DataService.ds.currentUser.updateNotificationPreference(add: true)
        } else {
            DataService.ds.currentUser.updateNotificationPreference(add: false)
        }
    }
}

class SettingVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var footerLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var settings = [[String]]()
    
    @IBOutlet weak var footerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
//        tableView.tableFooterView = UIView()
        
        self.navigationItem.title = "Settings"
        
        settings = [["Profile","Portfolio","Payment Methods","Transaction History","Reviews","Notifications","Help", "Rate Our App","Get notified for new tasks nearby"],["Logout"]]
        
        
        footerLbl.text = "Version \(DataService.ds.OS!)\n©2019 HeyWork.Co.\nAll rights reserved."

        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.clearBadge(index: 4)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return self.footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if section == 2 {
            return 175
        } else {
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as? SettingCell
        cell?.selectionStyle = .none
        
        if indexPath.row == 5 && DataService.ds.currentUser.notiCount > 0 {
            cell?.configureCell(title: settings[indexPath.section][indexPath.row], hideView: false, hideSwitch: true)
        } else if indexPath.row == 8 {
            cell?.configureCell(title: settings[indexPath.section][indexPath.row], hideView: true, hideSwitch: false)
        } else {
            cell?.configureCell(title: settings[indexPath.section][indexPath.row], hideView: true, hideSwitch: true)
        }
        
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 9
        } else{
            return 1
        }
//        return settings.count
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return UIView()
//    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "General"
        } else if section == 1 {
            return "Logout"
        } else {
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        switch indexPath.row {
        case 0:
            //profile
            if indexPath.section == 0 {
                self.performSegue(withIdentifier: "EditProfileVC", sender: nil)
            } else if indexPath.section == 1 {
                let popup = PopupDialog(title: "Logout", message: "Are you sure to sign out?")
                popup.buttonAlignment = .horizontal
                popup.transitionStyle = .zoomIn
                // Create buttons
                let buttonOne = CancelButton(title: "Cancel") {
                    //            print("You canceled the car dialog.")
                }
                
                let buttonTwo = DestructiveButton(title: "Logout") {
                    //            print("You canceled the car dialog.")
                    DataService.ds.signOut {
                        VCSegue().segueVC(bundleName: "Main", controllerName: "NavVC", vc: self)
                    }
                }
                buttonTwo.backgroundColor = MAIN_COLOR
                buttonTwo.titleColor = UIColor.white
                
                popup.addButtons([buttonOne,buttonTwo])
                
                self.present(popup, animated: true, completion: nil)
            }
            
            break
        case 1:
            //portfolio
            self.performSegue(withIdentifier: "PortfolioVC", sender: nil)
            break
        case 2:
            //payment
            self.performSegue(withIdentifier: "CardVC", sender: nil)
            
            break
        case 3:
            self.performSegue(withIdentifier: "PaymentHistoryVC", sender: nil)
            break
        case 4:
            //reviews
            self.performSegue(withIdentifier: "ReviewVC", sender: nil)
            break
        case 5:
            self.performSegue(withIdentifier: "NotificationVC", sender: nil)
            break
        case 6:
             self.performSegue(withIdentifier: "HelpVC", sender: nil)
            break
        case 7:
//            showReview()
            DataService.ds.showReview(vc: self, fromSetting: true)
            break
        default:
            break
        }
    }
    
//    func showReview() {
//        let reviewVC = ReviewView(nibName: "ReviewView", bundle: nil)
//
//
//
//        let popup = PopupDialog(viewController: reviewVC, buttonAlignment: .horizontal, transitionStyle: .zoomIn, tapGestureDismissal: false)
//
//        //        if let lbl = reviewVC.titleLbl {
//        reviewVC.titleLbl.text = "How was everything?"
//        //        }
//
//        reviewVC.descLbl.text = "how's your experience with us so far?"
//
//        let popup2 = PopupDialog(title: "Thanks You!", message: "We appreciate your rating.\nWould you like to rate us on App Store?")
//        // Create first button
//        let buttonOne = CancelButton(title: "Cancel", height: 60) {
//
//        }
//
//        let buttonTwo = DefaultButton(title: "Submit", height: 60) {
//
//            if reviewVC.reviewTF.text!.count == 0 {
//                ErrorAlert.errorAlert.showAlert(title: "Review Needed!", msg: "Can't be emtpty", object: self)
//            } else {
//
//                DataService.ds.REF_APP_REVIEW.child(DataService.ds.currentUser.userKey!).updateChildValues(["stars" : reviewVC.ratingView.rating, "review": reviewVC.reviewTF.text!])
//
//
//                let button2 = DefaultButton(title: "Yes", height: 60) {
//                    // 1.
//
////                    let productURL = "https://apps.apple.com/app/id1467610312"
//                    let productURL = NSURL(string: "https://apps.apple.com/app/id1467610312")
//                    var components = URLComponents(url: productURL as! URL, resolvingAgainstBaseURL: false)
//
//
//                    // 2.
//                    components?.queryItems = [
//                        URLQueryItem(name: "action", value: "write-review")
//                    ]
//
//                    // 3.
//                    guard let writeReviewURL = components?.url else {
//                        return
//                    }
//
//                    // 4.
//                    UIApplication.shared.open(writeReviewURL)
//                }
//
//
//                if reviewVC.ratingView.rating > 2 {
//                    popup2.addButtons([button2, buttonOne])
//                    self.present(popup2, animated: true, completion: nil)
//                }
//
//
//            }
//
//        }
//
//        // Add buttons to dialog
//        popup.addButtons([buttonOne, buttonTwo])
//
//        // Present dialog
//        self.present(popup, animated: true, completion: nil)
//    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ReviewVC {
            destinationVC.user = DataService.ds.currentUser
            destinationVC.hideBtn = true
        }
        
        if let destinationVC = segue.destination as? PortfolioVC {
            destinationVC.user = DataService.ds.currentUser
            destinationVC.visitingMode = false
        }
    }
    
    

}
