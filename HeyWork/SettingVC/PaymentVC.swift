//
//  PaymentVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 3/12/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import Stripe
import SVProgressHUD
//import CreditCardForm

class PaymentVC: UIViewController, STPPaymentCardTextFieldDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource/*, STPAddCardViewControllerDelegate*/ {

//    @IBOutlet weak var paymentCardView: CreditCardFormView!
    let paymentTextField = STPPaymentCardTextField()

    @IBOutlet weak var cardNumTxtField: UITextField!
//    @IBOutlet weak var expTxtField: UITextField!
    @IBOutlet weak var cvvTxtField: UITextField!
    @IBOutlet weak var yearTxtField: UITextField!
    @IBOutlet weak var monthTxtField: UITextField!
    
    var stripeUtil = StripeUtil()
    
    var cards = [AnyObject]()
    var card: BankCard?
    
    var toolBar: UIToolbar!
    var selectedData: String?
    var monthPicker: UIPickerView!
    var yearPicker: UIPickerView!
    
    var selectedMonth: String?
    var selectedYear: String?
    
    var pickerMonth: [String] = [String]()
    var pickerYear: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(false, animated: false)
//        let addCardViewController = STPAddCardViewController()
//        addCardViewController.delegate = self
        
        monthPicker = UIPickerView()
        monthPicker.delegate = self
        monthPicker.dataSource = self
        selectedMonth = ""
        
        yearPicker = UIPickerView()
        yearPicker.delegate = self
        yearPicker.dataSource = self
        selectedYear = ""
        loadPicker()
        
        self.hideKeyboardWhenTappedAround()
        
        cardNumTxtField.delegate = self
        monthTxtField.delegate = self
        yearTxtField.delegate = self
        cvvTxtField.delegate = self
        
        paymentTextField.delegate = self
        
        // Do any additional setup after loading the view.
        paymentTextField.frame = CGRect(x: 15, y: 199, width: self.view.frame.size.width - 30, height: 44)
        paymentTextField.translatesAutoresizingMaskIntoConstraints = false
        paymentTextField.borderWidth = 0
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0, y: paymentTextField.frame.size.height - width, width:  paymentTextField.frame.size.width, height: paymentTextField.frame.size.height)
        border.borderWidth = width
        paymentTextField.layer.addSublayer(border)
        paymentTextField.layer.masksToBounds = true
        
        view.addSubview(paymentTextField)
        
//        NSLayoutConstraint.activate([
//            paymentTextField.topAnchor.constraint(equalTo: paymentCardView.bottomAnchor, constant: 20),
//            paymentTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
//            paymentTextField.widthAnchor.constraint(equalToConstant: self.view.frame.size.width-20),
//            paymentTextField.heightAnchor.constraint(equalToConstant: 44)
//            ])
        
        if card != nil {
//            let dateArr = card?.cardExDate.components(separatedBy: "/")
//            paymentCardView.paymentCardTextFieldDidChange(cardNumber: card?.cardNo, expirationYear: UInt(dateArr![0])!, expirationMonth: UInt(dateArr![1])!, cvc: card?.cardCVV)
//            paymentTextField.numberPlaceholder = card?.cardNo
//            paymentTextField.cvcPlaceholder = card?.cardCVV
//            paymentTextField.expirationPlaceholder = card?.cardExDate
            
            cardNumTxtField.text = card?.cardNo
            
            let dateArr = card?.cardExDate.components(separatedBy: "/")
//            expTxtField.text = card?.cardExDate
            monthTxtField.text = dateArr![0]
            yearTxtField.text = dateArr![1]
            selectedMonth = dateArr![0]
            selectedYear = dateArr![1]
            
//            cvvTxtField.text = card?.cardCVV
            
        }
        

    }
    
    func loadPicker() {
        toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: Selector(("cancelPicker")))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: Selector(("cancelPicker")))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        monthPicker.backgroundColor = UIColor.white
        yearPicker.backgroundColor = UIColor.white
        monthTxtField.inputView = self.monthPicker
        monthTxtField.inputAccessoryView = toolBar
        
        yearTxtField.inputView = self.yearPicker
        yearTxtField.inputAccessoryView = toolBar
        
        loadPickerData()
    }
    
    func loadPickerData() {

        pickerMonth = ["01", "02", "03", "04", "05","06","07","08","09","10","11","12"]
        pickerYear = ["19", "20", "21", "22","23","24","25","26","27","28","29"]
    }
    
    @objc func cancelPicker() {
        monthTxtField.resignFirstResponder()
        yearTxtField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if monthTxtField.isFirstResponder {
            monthPicker?.selectRow(0, inComponent: 0, animated: true)
//            if selectedMonth == "" {
                selectedMonth = pickerMonth[0]
//            }
            
            monthTxtField.text = selectedMonth
        }
        
        if yearTxtField.isFirstResponder {
            yearPicker?.selectRow(0, inComponent: 0, animated: true)
//            if selectedYear == "" {
                selectedYear = pickerYear[0]
//            }
            
            yearTxtField.text = selectedYear
        }
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == monthPicker {
            return pickerMonth.count
        } else {
            return pickerYear.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == monthPicker {
            return pickerMonth[row]
        } else {
            return pickerYear[row]
        }
    
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //        nameTxtField.resignFirstResponder()
        if pickerView == monthPicker {
            selectedMonth = pickerMonth[row]
            monthTxtField.text = selectedMonth
//            return pickerMonth.count
        } else {
            selectedYear = pickerYear[row]
            yearTxtField.text = selectedYear
        }
//        selectedData = pickerData[row]
//        countryTF.text = selectedData
    }
    
//    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
//        navigationController?.popViewController(animated: true)
//    }
//
//    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
//        MyAPIClient.sharedClient.completeCharge(with: token, amount: 1, shippingAddress: <#STPAddress?#>) { result in
//            switch result {
//            // 1
//            case .success:
//                completion(nil)
//
//                let alertController = UIAlertController(title: "Congrats", message: "Your payment was successful!", preferredStyle: .alert)
//                let alertAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
//                    self.navigationController?.popViewController(animated: true)
//                })
//                alertController.addAction(alertAction)
//                self.present(alertController, animated: true)
//            // 2
//            case .failure(let error):
//                completion(error)
//            }
//        }
//    }
//    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
//        paymentCardView.paymentCardTextFieldDidChange(cardNumber: textField.cardNumber, expirationYear: textField.expirationYear, expirationMonth: textField.expirationMonth, cvc: textField.cvc)
//    }
//    
//    func paymentCardTextFieldDidEndEditingExpiration(_ textField: STPPaymentCardTextField) {
//        paymentCardView.paymentCardTextFieldDidEndEditingExpiration(expirationYear: textField.expirationYear)
//    }
//    
//    func paymentCardTextFieldDidBeginEditingCVC(_ textField: STPPaymentCardTextField) {
//        paymentCardView.paymentCardTextFieldDidBeginEditingCVC()
//    }
//    
//    func paymentCardTextFieldDidEndEditingCVC(_ textField: STPPaymentCardTextField) {
//        paymentCardView.paymentCardTextFieldDidEndEditingCVC()
//    }
    
    
    @IBAction func saveTapped(_ sender: Any) {
        
        SVProgressHUD.show()
      
        if cardNumTxtField.text?.count == 0 {
            ErrorAlert.errorAlert.showAlert(title: "Error", msg: "Card number can't be empty", object: self)
        } else if cvvTxtField.text?.count == 0 {
            ErrorAlert.errorAlert.showAlert(title: "Error", msg: "CVV can't be empty", object: self)
        } else if monthTxtField.text?.count == 0 {
            ErrorAlert.errorAlert.showAlert(title: "Error", msg: "Month must be selected", object: self)
        } else if yearTxtField.text?.count == 0 {
            ErrorAlert.errorAlert.showAlert(title: "Error", msg: "Year must be selected", object: self)
        } else {
//            let params = paymentTextField.cardParams
            let params = STPCardParams()
            params.cvc = cvvTxtField.text!
            params.number = cardNumTxtField.text!
//            let cardDate =  expTxtField.text!.components(separatedBy: "/")
            if let month = selectedMonth {
                params.expMonth = UInt(month)!
            } else {
                params.expMonth = UInt("01")!
            }
            
            if let year = selectedYear {
                params.expYear = UInt(year)!
            } else {
                params.expYear = UInt("19")!
            }
            
            
            print(params)
            //check if the customerId exist
            if let tokenId = DataService.ds.currentUser.stripeKey {
                print("existing Customer")
                //if yes, call the createCard method of our stripeUtil object, pass customer id
                self.stripeUtil.createCard(stripeId: tokenId, card: params, completion: { (success) in
                    //there is a new card !
                    if DataService.ds.currentUser.cardID != nil {
                        self.stripeUtil.deleteCard(cardID: DataService.ds.currentUser.cardID!, stripeKey: tokenId, completion: { (result) in
                            
                            if let result = result {
                                if let deleted = result["deleted"] as? Bool {
                                    if deleted {
                                        self.stripeUtil.getCardsList(completion: { (result) in
                                            if let result = result {
                                                self.cards = result
                                                
                                                if let cardID = self.cards[0]["id"] as? String {
                                                    let last4 = Int(params.number!)! % 10000
                                                    DataService.ds.currentUser.updateBank(cardID: cardID, card: BankCard(no: "**** **** **** \(last4)", date: "\(params.expMonth)/\(params.expYear)", cvv: params.cvc!, brand: (self.cards[0]["brand"] as? String)!), completion: {
                                                        self.navigationController?.popViewController(animated: true)
                                                        SVProgressHUD.dismiss()
                                                    })
                                                    
                                                }
                                                
                                                
                                            }
                                        })
                                    }
                                }
                            }
                        })
                    } else {
                        self.stripeUtil.getCardsList(completion: { (result) in
                            if let result = result {
                                self.cards = result
                                
                                if let cardID = self.cards[0]["id"] as? String {
                                    let last4 = Int(params.number!)! % 10000
                                    DataService.ds.currentUser.updateBank(cardID: cardID, card: BankCard(no: "**** **** **** \(last4)", date: "\(params.expMonth)/\(params.expYear)", cvv: params.cvc!, brand: (self.cards[0]["brand"] as? String)!), completion: {
                                        self.navigationController?.popViewController(animated: true)
                                        SVProgressHUD.dismiss()
                                    })
                                    
                                }
                                
                                
                            }
                        })
                    }

                    
                })
            } else {
                print("new Customer")
                //if not, create the user with our createUser method


                self.stripeUtil.createStripeUser(card: params, completion: { (success) in



                    self.stripeUtil.getCardsList(completion: { (result) in
                        if let result = result {
                            self.cards = result
                            print("CARD")
                            print(self.cards)
                            if let cardID = self.cards[0]["id"] as? String {
                                let last4 = Int(params.number!)! % 10000
                                DataService.ds.currentUser.updateBank(cardID: cardID, card: BankCard(no: "**** **** **** \(last4)", date: "\(params.expMonth)/\(params.expYear)", cvv: params.cvc!, brand: (self.cards[0]["brand"] as? String)!), completion: {
                                    self.navigationController?.popViewController(animated: true)
                                    SVProgressHUD.dismiss()
                                })

                            }
             
                  

                         
                            
                           
//                            DataService.ds.currentUser.updateBank(card: BankCard(no: params.number!, date: "\(params.expMonth)/\(params.expYear)", cvv: params.cvc!)) {
//                                    self.navigationController?.popViewController(animated: true)
//                                }


                        }
                    })
                })

            }
//
        }
        
    }
    
}
