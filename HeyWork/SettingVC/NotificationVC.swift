//
//  NotificationVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 26/5/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet weak var notiLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    
    @IBOutlet weak var profilePicImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        profilePicImageView.layer.cornerRadius = profilePicImageView.frame.height / 2
        profilePicImageView.clipsToBounds = true
    }
    
    func configureCell(noti: Notification, lastNotiTime: NSNumber) {
        let bold1 = noti.userName!
        let bold2 = noti.taskTitle!
        let normal = " has made an offer on your task - "
        
        let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16)]
        let attributedString1 = NSMutableAttributedString(string:bold1, attributes:attrs)
        let attributedString2 = NSMutableAttributedString(string:bold2, attributes:attrs)
        
        let normalString = NSMutableAttributedString(string:normal)
        
        attributedString1.append(normalString)
        attributedString1.append(attributedString2)
        
        notiLbl.attributedText = attributedString1
        
        let seconds = noti.timeStamp!.doubleValue
        let timesStampDate = NSDate(timeIntervalSince1970: seconds)
        
        timeLbl.text = DateHelper.dateHelper.timeAgoSinceDate(timesStampDate as Date, currentDate: NSDate() as Date, numericDates: false)
        
        profilePicImageView.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: noti.userProfilePicUrl!)
//        notiLbl.text = "\(noti.userName!) has made an offer on your task - \(noti.taskTitle!)"
        
        if noti.timeStamp!.intValue > lastNotiTime.intValue {
            self.backgroundColor = UIColor.init(hex: "EFEFEF")
        } else {
            self.backgroundColor = .white
        }
    }
    
}
class NotificationVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var timer: Timer?
    var notifications = [Notification]()
    var notificationDict = [String: Notification]()
    var emptyView: EmptyView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        
        emptyView = EmptyView.instanceFromNib(image: UIImage(named: "noNoti")!, title: "Hmm..", desc: "It is very quiet here.") as! EmptyView
        emptyView.frame = self.tableView.bounds
        self.tableView.addSubview(emptyView)
        emptyView.showView(show: false, tableView: self.tableView)
        
        
        observeUserNotification()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        DataService.ds.currentUser.clearNotiCount {
            
        }
    }
    
    func observeUserNotification() {
        
//        isListening = true
        
        let uid = DataService.ds.currentUser.userKey!
        print("haha\(uid)")
        let ref = DataService.ds.REF_USER_NOTIFICATION.child(uid)
        
        
        DataService.ds.notiObserverListener = ref.observe(.childAdded, with: {(snapshot) in
            
            let notiID = snapshot.key
            let notiRef = DataService.ds.REF_USER_NOTIFICATION.child(uid)

            if let dictionary = snapshot.value as? [String: Any] {
                
                let noti = Notification(key: snapshot.key, dictionary: dictionary)
                
                self.notifications.append(noti)
                self.notificationDict[notiID] = noti

                
                self.attemptToReloadTable()
                
                
                
            }
            
            print(notiID)
            
            
            
        })

        self.attemptToReloadTable()
    }

    func attemptToReloadTable() {
        self.timer?.invalidate()
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.reloadTable), userInfo: nil, repeats: false)
        //        reloadTable()
    }
    
    @objc func reloadTable() {
        
        self.notifications = Array(self.notificationDict.values)
//        self.tasks = Array(self.taskKeyDict.values)
        
//        self.tasks.sort(by: { (m1, m2) -> Bool in
//
//            return (m1.timeStamp?.intValue)! > (m2.timeStamp?.intValue)!
//        })
//
        self.notifications.sort(by: { (m1, m2) -> Bool in
            
            return (m1.timeStamp?.intValue)! > (m2.timeStamp?.intValue)!
        })
        self.tableView.reloadData()
        
    
        
        if self.notifications.count == 0 {
            emptyView.showView(show: true, tableView: self.tableView)
        } else {
            emptyView.showView(show: false, tableView: self.tableView)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as? NotificationCell
        cell?.selectionStyle = .none
        let noti = notifications[indexPath.row]
        
        cell?.configureCell(noti: noti, lastNotiTime: DataService.ds.currentUser.lastNotificationTime)
        
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }

}
