//
//  ReceivePaymentVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 17/2/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import UIKit

class ReceivePaymentVC: UIViewController,UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var bankNameTF: UITextField!
    @IBOutlet weak var accountNumberTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    
    var bankPicker: UIPickerView!
    var toolBar: UIToolbar!
    var pickerData: [String] = [String]()
    var selectedData: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        bankNameTF.delegate = self
        bankPicker = UIPickerView()
        bankPicker.delegate = self
        bankPicker.dataSource = self
        selectedData = ""

        self.hideKeyboardWhenTappedAround()
        loadPicker()
        // Do any additional setup after loading the view.
    }
    
    func loadPicker() {
        toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: Selector(("donePicker")))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: Selector(("cancelPicker")))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        bankPicker.backgroundColor = UIColor.white
        bankNameTF.inputView = self.bankPicker
        bankNameTF.inputAccessoryView = toolBar
        
        loadPickerData()
    }
    
    @objc func cancelPicker() {
        bankNameTF.resignFirstResponder()
    }
    @objc func donePicker() {
        bankNameTF.resignFirstResponder()
        bankNameTF.text = self.selectedData!
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if bankNameTF.isFirstResponder {
            bankPicker?.selectRow(0, inComponent: 0, animated: true)
            if selectedData == "" {
                selectedData = pickerData[0]
            }

            bankNameTF.text = selectedData
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //        nameTxtField.resignFirstResponder()
        selectedData = pickerData[row]
        //        countryTF.text = selectedData
    }
    
    func loadPickerData() {
        //        Malacca
        //        Negeri Sembilan
        //        Pahang
        //        Perak
        //        Perlis
        //        Penang
        //        Sabah
        //        Sarawak
        //        Selangor
        //        Terengganu
        pickerData = ["DBS/POSB", "UOB", "Citibank", "Maybank", "Standard Chartered","CIMB","RHB","HSBC","MayBabnk","Hong Leong","OCBC","Other"]
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 16
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if selectedData == "" {
            ErrorAlert.errorAlert.showAlert(title: "Bank Name", msg: "Please enter your bank name", object: self)
        } else if accountNumberTF.text?.count == 0 {
//            ErrorAlert().show(title: "Bank Account Number", msg: "Please enter your bank account number", object: self)
            ErrorAlert.errorAlert.showAlert(title: "Bank Account Number", msg: "Please enter your bank account number", object: self)
        } else if nameTF.text?.count == 0 {
            ErrorAlert.errorAlert.showAlert(title: "Receipient Name", msg: "Please enter the receipient name", object: self)
        } else {
            let account = BankAccount(accNo: accountNumberTF.text!, bankName: bankNameTF.text!, accName: nameTF.text!, new: true)
            DataService.ds.currentUser.updateBankAccount(account: account) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
