//
//  PortfolioDetailVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 21/3/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import UIKit
import ImageSlideshow
import SDWebImage
//import Spring

protocol DeletePortDelegate {
    func receiveDeletedPortUpdate(deletedPort: Portfolio)
}

class PortfolioDetailVC: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var detailTV: UITextView!
    var delegate: DeletePortDelegate?
    
    @IBOutlet weak var optionBtn: UIButton!
    @IBOutlet weak var slideshow: ImageSlideshow!

    var port: Portfolio!
    var urls = [SDWebImageSource]()
    
    @IBOutlet weak var detailTextField: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationItem.title = item.name!
         NotificationCenter.default.addObserver(self, selector: #selector(getDataUpdate), name: NSNotification.Name(rawValue: "updatePortfolioNotification"), object: nil)

      
//        detailTextField.delegate = self
        
        if self.port.posterID != DataService.ds.currentUser.userKey! {
            self.optionBtn.isHidden = true
        } else {
            self.optionBtn.isHidden = false
        }
        
        
//        let localSource = [ImageSource(imageString: "Shimano-logo-vector")!, ImageSource(imageString: "panasonic")!, ImageSource(imageString: "Mitsubishi_Electric_logo.svg")!, ImageSource(imageString: "Meiban_Group_BW")!]
        
        //        slideshow.slideshowInterval = 5.0
        slideshow.pageControlPosition = .underScrollView
        slideshow.contentScaleMode = UIView.ContentMode.scaleAspectFit
        
        //        let pageControl = UIPageControl()
        //        pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        //        pageControl.pageIndicatorTintColor = UIColor.black
        slideshow.pageControl.currentPageIndicatorTintColor = MAIN_COLOR
        slideshow.pageControl.pageIndicatorTintColor = UIColor.white
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.currentPageChanged = { page in
            print("current page:", page)
        }
        
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        
        
//        slideshow.setImageInputs(urls)
        //        SDWebImageSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        slideshow.addGestureRecognizer(recognizer)
        
        configurePort()
        
        //        UIBarButtonItem(image: UIImage(named: "shopping-cart"), style: .plain, target: self, action: #selector(cartTapped)
        
        
        //        if DataService.ds.currentUser.userKey! == ADMIN_ID {
        //            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editTapped))
        //        } else {
        //            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "shopping-cart"), style: .plain, target: self, action: #selector(cartTapped))
        //        }
        
    }
    
    @objc private func getDataUpdate() {
        
        port.portImageUrls.removeAll()
        slideshow.setImageInputs([])
        self.port = DataService.ds.currentUser.portfolio[self.port.portID]
        
        configurePort()
    }
    
    @IBAction func optionTapped(_ sender: Any) {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete Portfolio", style: .destructive) { (action) in
            let innerMenu = UIAlertController(title: nil, message: "Are you sure?", preferredStyle: .actionSheet)
            
            let yesAction = UIAlertAction(title: "Delete", style: .default) { (action) in
                DataService.ds.deletePort(portID: self.port.portID, completion: {
                    
                    self.delegate?.receiveDeletedPortUpdate(deletedPort: self.port)
                    
                    self.dismiss(animated: true, completion: nil)
                }, vc: self)
                
            }
            
            let noAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
                
            }
            innerMenu.addAction(yesAction)
            innerMenu.addAction(noAction)
            
            if let popoverController = innerMenu.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
            
            if let popoverController = innerMenu.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
            
            self.present(innerMenu, animated: true, completion: nil)
            
        }
        
        let editAction = UIAlertAction(title: "Edit Portfolio", style: .default) { (action) in
            self.performSegue(withIdentifier: "AddPortfolioVC", sender: nil)
            
        }
        
        optionMenu.addAction(editAction)
        optionMenu.addAction(deleteAction)
        
        
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        addAccessoryView()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        self.detailTV.setContentOffset(CGPoint.zero, animated: false)
    }
    
//    @objc func editTapped() {
//        performSegue(withIdentifier: "AddVC", sender: nil)
//    }
//    @objc func cartTapped() {
//        if !DataService.ds.login {
//            performSegue(withIdentifier: "NumberVC", sender: nil)
//        } else {
//            performSegue(withIdentifier: "CartVC", sender: nil)
//        }
//    }
//
//    func bottomCartTapped() {
//
//        if !DataService.ds.login {
//            performSegue(withIdentifier: "NumberVC", sender: nil)
//        } else {
//            self.performSegue(withIdentifier: "TypeVC", sender: nil)
//        }
//    }
    
    func configurePort() {
//        self.conditionLbl.text = "\(item.condition!) - \(item.brand!)"
        self.titleLbl.text = port.portTitle
        self.detailTV.text = port.portDesc
        //        self.detailTV.setContentOffset(CGPoint.zero, animated: false)
        
        print(port.portImageUrls.count)
//        if port.portImageUrls.count != 0 {
            for idx in 0...port.portImageUrls.count - 1 {
                if let url = port.portImageUrls["url\(idx)"] {
                    urls.append(SDWebImageSource(urlString: url)!)
                }
                
                
            }
            
            slideshow.setImageInputs(urls)
//        }

    }
    
    @objc func didTap() {
        let fullScreenController = slideshow.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? AddPortfolioVC {
            destinationVC.edit = true
            destinationVC.portfolio = self.port
            
            
        }
    }
    
//    public func addAccessoryView() {
//
//        itemBottomBar = Bundle.main.loadNibNamed("ItemBottomBar", owner: self, options: nil)?.first as! ItemBottomBar
//        itemBottomBar.itemVC = self
//        detailTextField.inputAccessoryView = itemBottomBar
//        detailTextField.becomeFirstResponder()
//
//    }
    
//    @IBAction func typeTapped(_ sender: Any) {
//        self.performSegue(withIdentifier: "TypeVC", sender: nil)
//    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let destinationVC = segue.destination as? TypeVC {
//
//            destinationVC.item = self.item
//
//        }
//
//        if let destinationVC = segue.destination as? AddVC {
//            destinationVC.item = self.item
//        }
//    }
    
//
//    @objc func maximizeView(_ sender: AnyObject) {
//        SpringAnimation.spring(duration: 0.7, animations: {
//            //            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
//            //            self.layerView.backgroundColor = UIColor.clear
//        })
//        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.default, animated: true)
//    }
//
//    @objc func minimizeView(_ sender: AnyObject) {
//        SpringAnimation.spring(duration: 0.7, animations: {
//            //            self.view.transform = CGAffineTransform(scaleX: 0.935, y: 0.935)
//            //            self.layerView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.15)
//        })
//        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)
//    }
//    
//    func goToNumberVC(){
//        if !DataService.ds.login {
//            performSegue(withIdentifier: "NumberVC", sender: nil)
//        } else {
//            performSegue(withIdentifier: "TypeVC", sender: nil)
//        }
//        
//    }
    
}
