//
//  PaymentHistoryVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 23/2/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import UIKit
import Firebase
import BetterSegmentedControl

class BillCell: UITableViewCell {

    @IBOutlet weak var profilePicImage: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var assignedLbl: UILabel!
    @IBOutlet weak var billReleaseLbl: UILabel!
    @IBOutlet weak var billDateLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
    @IBOutlet weak var ModeLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profilePicImage.layer.cornerRadius = 5
    }
    
    func configureCell(bill: Bill, paymentMode: Bool) {
        
        titleLbl.text = bill.billDesc!
        assignedLbl.text = bill.payeeUsername!
        statusLbl.text = bill.billStatus!
        profilePicImage.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: bill.payeeProfilePicUrl!)
        
        let billDate = bill.billDate
        let date = NSDate(timeIntervalSince1970: billDate!)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        self.billDateLbl.text = dateString
        
        if let billReleaseDate = bill.billReleaseDate {
            let billReleaseDate = bill.billReleaseDate
            
            let date2 = NSDate(timeIntervalSince1970: billReleaseDate!)
            let dateString2 = dayTimePeriodFormatter.string(from: date2 as Date)
            
            self.billReleaseLbl.text = dateString2
        } else {
            self.billReleaseLbl.text = " - "
        }
        
        if paymentMode {
            ModeLbl.text = "Credited:"
        } else {
            ModeLbl.text = "Debited:"
        }
        
        amountLbl.text = "\(bill.billAmount!)SGD"

        
    }
    
    
}

class PaymentHistoryVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

//    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var segControl: BetterSegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    @IBOutlet weak var totalModeLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    var creditBills = [Bill]()
    var debitBills = [Bill]()
    
    var paymentMode = true //credit if true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        segControl.segments = [LabelSegment(text: "Tasker"), LabelSegment(text: "Poster")]
        segControl.addTarget(self, action: #selector(valueChanged), for: .valueChanged)
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = MAIN_COLOR
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        // Do any additional setup after loading the view.
        
        
        loadBills()
        self.valueChanged()
        
        
        
        //        self.tableView.reloadData()
//        switchType()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.navigationBar.isTranslucent = false
//        self.navigationController?.navigationBar.barTintColor = MAIN_COLOR
//        self.navigationController?.navigationBar.tintColor = UIColor.white
//        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.navigationBar.barTintColor = UIColor.white
//        self.navigationController?.navigationBar.tintColor = MAIN_COLOR
//        self.navigationController?.navigationBar.shouldRemoveShadow(false)
        super.viewWillDisappear(true)
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    func loadBills() {
        self.creditBills = DataService.ds.currentUser.getBillArray(credit: true)
        self.debitBills = DataService.ds.currentUser.getBillArray(credit: false)
    }
    
    @objc func refresh() {
        
        if self.refreshControl.isRefreshing {
            
            DataService.ds.updateUser {
                Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.reloadDataWhenPulled), userInfo: nil, repeats: false)
                
            }
            
            
        }
        
        
        
    }
    
    @objc func reloadDataWhenPulled() {
        self.refreshControl.endRefreshing()
        self.loadBills()
        self.tableView.reloadData()
    }
  
    @objc func valueChanged() {
//        self.tableView.alpha = 0.0
        
        
        if segControl.index == 0 {
//            makePaymentMode = true
            paymentMode = true
            totalAmountLbl.text = "\(DataService.ds.currentUser.totalCredit!)"
            totalModeLbl.text = "Total Earning:"
        } else if segControl.index == 1 {
//            makePaymentMode = false
            paymentMode = false
            totalAmountLbl.text = "\(DataService.ds.currentUser.totalDebit!)"
            totalModeLbl.text = "Total Spending:"
        }
        
//        UIView.animate(withDuration: 0.1, delay: 0.1, options: [], animations: { self.tableView.alpha = 1.0 }, completion: { (bool) in
//
//        })
        
        self.tableView.reloadData()
        
        
        
    }
//    func switchType() {
//        switch segmentedControl.selectedSegmentIndex {
//        case 0:
//            paymentMode = true
//            totalAmountLbl.text = "\(DataService.ds.currentUser.totalCredit!)"
//            totalModeLbl.text = "Total Earning:"
//        case 1:
//            paymentMode = false
//            totalAmountLbl.text = "\(DataService.ds.currentUser.totalDebit!)"
//            totalModeLbl.text = "Total Spending:"
//        default:
//            break
//        }
//
//        self.tableView.reloadData()
//    }
    
//    @IBAction func typeTapped(_ sender: Any) {
//        self.switchType()
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if paymentMode {
            return creditBills.count
        } else {
            return debitBills.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BillCell") as? BillCell
        cell?.selectionStyle = .none
        if paymentMode {
            cell?.configureCell(bill: creditBills[indexPath.row], paymentMode: paymentMode)
        } else {
            cell?.configureCell(bill: debitBills[indexPath.row], paymentMode: paymentMode)
        }
        
        return cell!
    }

}
