
import UIKit
import AVFoundation


import UIKit

class PortfolioCell: UICollectionViewCell {

    
    
    @IBOutlet weak var containerView: RoundView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 6
        containerView.layer.masksToBounds = true
    }
    
    func configureCell(port: Portfolio) {
       
        titleLbl.text = port.portTitle
        descLbl.text = port.portDesc
        
        imageView.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: port.portImageUrls["url0"]!)
        
    }
//    var photo: Photo? {
//        didSet {
//            if let photo = photo {
//                imageView.image = photo.image
//                captionLabel.text = photo.caption
//                commentLabel.text = photo.comment
//            }
//        }
//    }
    
}

class PortfolioVC: UICollectionViewController, DeletePortDelegate {
    
    
    
//    var photos = Photo.allPhotos()
    
//    var images = [CGFloat]()
    var portfolio = [Portfolio]()
    var selectedPort: Portfolio!
    
    var user: User!
    
    var visitingMode = true
    var emptyViewInner: EmptyViewInnerTable!
    var emptyView: EmptyView!
    
//    override var preferredStatusBarStyle : UIStatusBarStyle {
//        return UIStatusBarStyle.lightContent
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Portfolio"
        

        
        NotificationCenter.default.addObserver(self, selector: #selector(getDataUpdate), name: NSNotification.Name(rawValue: "updatePortfolioNotification"), object: nil)
//        self.navigationController?.navigationBar.layer.masksToBounds = false
//        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
//        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
//        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
//        self.navigationController?.navigationBar.layer.shadowRadius = 2
        
//        collectionView?.backgroundColor = UIColor.clear
//        collectionView?.contentInset = UIEdgeInsets(top: 23, left: 10, bottom: 10, right: 10)
        // Set the PinterestLayout delegate
        if let layout = collectionView?.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        
        


        
        
        if !visitingMode {
            
            emptyView = EmptyView.instanceFromNib(image: UIImage(named: "portfolio")!, title: "No portfolio", desc: "Your portfolio is empty.") as! EmptyView
            emptyView.frame = self.collectionView.bounds
            self.collectionView.addSubview(emptyView)
            emptyView.showViewCollectionView(show: false, collectionView: self.collectionView)
            
            self.addBtn()
            collectionView?.contentInset = UIEdgeInsets(top: 23, left: 10, bottom: 10, right: 10)
            
        } else {
            emptyViewInner = EmptyViewInnerTable.instanceFromNib(image: UIImage(named: "portfolio")!, title: "No portfolio", desc: "Your portfolio is empty.") as! EmptyViewInnerTable
            emptyViewInner.frame = self.collectionView.bounds
            self.collectionView.addSubview(emptyViewInner)
            emptyViewInner.showViewCollectionView(show: false, collectionView: self.collectionView)
            
            self.addCloseBtn()
            collectionView?.contentInset = UIEdgeInsets(top: 100, left: 10, bottom: 10, right: 10)
        }
        
        
        if user.portfolio.count == 0 {
            if !visitingMode {
                emptyView.showViewCollectionView(show: true, collectionView: self.collectionView)
            } else {
                emptyViewInner.showViewCollectionView(show: true, collectionView: self.collectionView)
            }
            
        } else {
            for(portKey, port) in user.portfolio {
                portfolio.append(port)
                //            images.append(CGFloat(port.portImageHeight))
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
    
//
    }
    func receiveDeletedPortUpdate(deletedPort port: Portfolio) {
        print("hh")
        
//        self.getDataUpdate()
//
//        self.collectionView.reloadData()
//
//        var idx = 0
//        for existingPort in portfolio {
//            if existingPort.portID == port.portID {
//                portfolio.remove(at: idx)
//            }
//            idx = idx + 1
//        }
////        for(portKey, port) in DataService.ds.currentUser.portfolio {
////            portfolio.append(port)
////            //            images.append(CGFloat(port.portImageHeight))
////        }
//        self.collectionView.reloadData()
    }
    
    @objc private func getDataUpdate() {
        portfolio.removeAll()
        for(portKey, port) in DataService.ds.currentUser.portfolio {
            portfolio.append(port)
            //            images.append(CGFloat(port.portImageHeight))
        }
        
        UIView.animate(views: collectionView.visibleCells, animations: TABLE_ANIMATION, completion: {
        })
        self.collectionView.reloadData()
        
    }


    func addBtn() {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "addBtn"), for: .normal)
        button.addTarget(self, action: #selector(addTapped), for: .touchUpInside)
//        button.setTitle("NEXT", for: .normal)
        button.frame.size = CGSize(width: 30, height: 30)
        self.view.addSubview(button)
        
        //set constrains
        button.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            button.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor, constant: -20).isActive = true
            button.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 30).isActive = true
        } else {
            button.rightAnchor.constraint(equalTo: self.view.layoutMarginsGuide.rightAnchor, constant: -20).isActive = true
            button.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor, constant: 30).isActive = true
        }
    }
    
    
    func addCloseBtn() {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "closeBtn"), for: .normal)
        button.addTarget(self, action: #selector(closeTapped), for: .touchUpInside)
        //        button.setTitle("NEXT", for: .normal)
        button.frame.size = CGSize(width: 30, height: 30)
        self.view.addSubview(button)
        
        //set constrains
        button.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            button.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor, constant: 20).isActive = true
            button.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        } else {
            button.leftAnchor.constraint(equalTo: self.view.layoutMarginsGuide.leftAnchor, constant: 20).isActive = true
            button.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor, constant: 10).isActive = true
        }
    }
    
    @objc func addTapped() {
        self.performSegue(withIdentifier: "AddPortfolioVC", sender: nil)
    }
    
    @objc func closeTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension PortfolioVC {
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return portfolio.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PortfolioCell", for: indexPath)
        if let annotateCell = cell as? PortfolioCell {
//            annotateCell.photo = photos[indexPath.item]
            annotateCell.configureCell(port: portfolio[indexPath.row])
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        self.selectedPort = self.portfolio[indexPath.row]
        self.performSegue(withIdentifier: "PortfolioDetailVC", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVC = segue.destination as? PortfolioDetailVC {
            destinationVC.port = self.selectedPort
            destinationVC.delegate = self
        }
    }
    
}

//MARK: - PINTEREST LAYOUT DELEGATE
extension PortfolioVC : PinterestLayoutDelegate {
    
    // 1. Returns the photo height
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
        return CGFloat(portfolio[indexPath.row].portImageHeight)
    }
    
}
