//
//  ReviewVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 8/12/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import Firebase
import Cosmos
import BetterSegmentedControl


class ReviewCell: UITableViewCell {
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var reviewLbl: UILabel!
//    @IBOutlet weak var reviewTxtLbl: UITextView!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profilePic.layer.cornerRadius = 5
    }
    
    func configureCell(review: Review) {
        
        if review.posterImageUrl != NO_PIC {
            self.profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: review.posterImageUrl)
        } else {
            self.profilePic.backgroundColor = UIColor.yellow
        }
        
        self.reviewLbl.text = review.reviewMsg
        self.usernameLbl.text = review.posterUsername
        
        let seconds = review.time.doubleValue
        let date = NSDate(timeIntervalSince1970: seconds)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        self.timeLbl.text = dateString
        
    }
    
    
}
class ReviewVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var percentageLbl: UILabel!
    @IBOutlet weak var centerStackView: UIStackView!
    @IBOutlet weak var segControl: BetterSegmentedControl!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
//    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var starView: CosmosView!
    @IBOutlet weak var totalReviewLbl: UILabel!
    @IBOutlet weak var ratingCircleView: UIView!
    
    var taskerReviews = [Review]()
    var posterReviews = [Review]()
    let shapeLayer = CAShapeLayer()
    
    var taskerMode = true //tasker review if true
    
    var user: User!
    var hideBtn = false
    
    var emptyView: EmptyView!
    
    @IBOutlet weak var closeBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        emptyView = EmptyView.instanceFromNib(image: UIImage(named: "review")!, title: "No reviews", desc: "Start to receive reviews after you completed a task.") as! EmptyView
        emptyView.frame = self.tableView.bounds
        self.tableView.addSubview(emptyView)
        emptyView.showView(show: false, tableView: self.tableView)
        
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = MAIN_COLOR
        
//        if user.userKey == DataService.ds.currentUser.userKey {
//            self.closeBtn.isHidden
//        }
        
        segControl.segments = [LabelSegment(text: "Tasker"), LabelSegment(text: "Poster")]
        segControl.addTarget(self, action: #selector(valueChanged), for: .valueChanged)
        
//        fetchReview()
        
//        if DataService.ds.currentUser.posterReview.count - 1 != 0 {
//
//        }
        
        loadCircleView()
        loadReviews()
//        loadCircleView()

        if hideBtn {
            self.closeBtn.isHidden = true
        }
        
        
//        self.tableView.reloadData()
//        switchType()
        self.valueChanged()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.navigationBar.isTranslucent = false
//        self.navigationController?.navigationBar.barTintColor = MAIN_COLOR
//        self.navigationController?.navigationBar.tintColor = UIColor.white
//        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.navigationBar.barTintColor = UIColor.white
//        self.navigationController?.navigationBar.tintColor = MAIN_COLOR
//        self.navigationController?.navigationBar.shouldRemoveShadow(false)
        super.viewWillDisappear(true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        
    }
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    func loadCircleView() {
        // let's start by drawing a circle somehow
        
        ratingCircleView.isUserInteractionEnabled = true
        let center = ratingCircleView.center
        
        // create my track layer
        let trackLayer = CAShapeLayer()
        
        let circularPath = UIBezierPath(arcCenter: center, radius: 50, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        trackLayer.path = circularPath.cgPath
        
        trackLayer.strokeColor = UIColor.lightGray.cgColor
        trackLayer.lineWidth = 5
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = CAShapeLayerLineCap.round
        ratingCircleView.layer.addSublayer(trackLayer)
        
        //        let circularPath = UIBezierPath(arcCenter: center, radius: 100, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        shapeLayer.path = circularPath.cgPath
        
        shapeLayer.strokeColor = MAIN_COLOR.cgColor
        shapeLayer.lineWidth = 5
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = CAShapeLayerLineCap.round
        
        shapeLayer.strokeEnd = 0
        
        ratingCircleView.layer.addSublayer(shapeLayer)
        
        
//        ratingCircleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
    }
    
    func updateCircle(ratingValue: Double) {
        print("Attempting to animate stroke")
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        
        basicAnimation.toValue = ratingValue * 0.8
        
        //0.8 = 100%
        //0.4 = 50%
        
        
    
        basicAnimation.duration = 1
        
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        basicAnimation.isRemovedOnCompletion = false
        
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func loadReviews() {
        
        self.taskerReviews = user.getReviewArray(tasker: true)
        self.posterReviews = user.getReviewArray(tasker: false)
        
        if self.taskerReviews.count == 0 {
            emptyView.showView(show: true, tableView: self.tableView)
        } else {
            emptyView.showView(show: false, tableView: self.tableView)
        }
        
        if self.posterReviews.count == 0 {
            emptyView.showView(show: true, tableView: self.tableView)
        } else {
            emptyView.showView(show: false, tableView: self.tableView)
        }

    }
    
    @objc func refresh() {

        if self.refreshControl.isRefreshing {
            
            DataService.ds.updateUser {
                Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.reloadDataWhenPulled), userInfo: nil, repeats: false)
                
            }
            
            
        }
        
        
        
    }
    
    @objc func reloadDataWhenPulled() {
        self.refreshControl.endRefreshing()
        self.loadReviews()
        self.tableView.reloadData()
    }
    
    @objc func valueChanged() {

        
        
        if segControl.index == 0 {
//            makePaymentMode = true
            taskerMode = true
            starView.rating = user.taskerStarRating
            totalReviewLbl.text = "(\(user.taskerNumRating!))"
            percentageLbl.text = ""
            let average = ( self.user.taskerStarRating / Double(self.user.taskerNumRating) ) / 5.0
            self.updateCircle(ratingValue:  average)
            

            if ( self.user.taskerStarRating / Double(self.user.taskerNumRating) ).isNaN {
                percentageLbl.text = "0.0"
            } else {
                percentageLbl.text = "\(( self.user.taskerStarRating / Double(self.user.taskerNumRating) ) )"
            }
            
            //            completionLbl.text = "(\(user.taskerCompletionRate!))"
//            rateLbl.text = "Tasker"
//            if user.taskerCompletionRate == 0.0 {
//                rateLbl.text = "No Completion Rate Yet"
//            } else {
//                rateLbl.text = "\(user.taskerCompletionRate!) % Completion Rate"
//            }
        } else if segControl.index == 1 {
//            makePaymentMode = false
            taskerMode = false
            starView.rating = user.posterStarRating
            totalReviewLbl.text = "(\(user.posterNumRating!))"
            let average = ( self.user.posterStarRating / Double(self.user.posterNumRating) ) / 5.0
            self.updateCircle(ratingValue:  average)
            
            if ( self.user.posterStarRating / Double(self.user.posterNumRating) ).isNaN {
                percentageLbl.text = "0.0"
            } else {
                percentageLbl.text = "\(( self.user.posterStarRating / Double(self.user.posterNumRating) ))"
            }
            
            
            
            //            completionLbl.text = "(\(user.posterCompletionRate!))"
            
//            rateLbl.text = "Poster"
//            if user.posterCompletionRate == 0.0 {
//                rateLbl.text = "No Completion Rate Yet"
//            } else {
//                rateLbl.text = "\(user.posterCompletionRate!) % Completion Rate"
//            }
        }
     
        
        self.tableView.reloadData()
        
        
        
    }
//    func switchType() {
//        switch segmentedControl.selectedSegmentIndex {
//        case 0:
//            taskerMode = true
//            starView.rating = user.taskerStarRating
//            totalReviewLbl.text = "(\(user.taskerNumRating!))"
//            //            completionLbl.text = "(\(user.taskerCompletionRate!))"
//            if user.taskerCompletionRate == 0.0 {
//                rateLbl.text = "No Completion Rate Yet"
//            } else {
//                rateLbl.text = "\(user.taskerCompletionRate!) % Completion Rate"
//            }
//
//        case 1:
//            taskerMode = false
//            starView.rating = user.posterStarRating
//            totalReviewLbl.text = "(\(user.posterNumRating!))"
//            //            completionLbl.text = "(\(user.posterCompletionRate!))"
//
//            if user.posterCompletionRate == 0.0 {
//                rateLbl.text = "No Completion Rate Yet"
//            } else {
//                rateLbl.text = "\(user.posterCompletionRate!) % Completion Rate"
//            }
//        default:
//            break
//        }
//
//        self.tableView.reloadData()
//    }
//
//    @IBAction func typeTapped(_ sender: Any) {
//        self.switchType()
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if taskerMode {
            return taskerReviews.count
        } else {
            return posterReviews.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell") as? ReviewCell
        cell?.selectionStyle = .none
        if taskerMode {
            cell?.configureCell(review: taskerReviews[indexPath.row])
        } else {
            cell?.configureCell(review: posterReviews[indexPath.row])
        }
        
        return cell!
        
//        if taskerMode {
//
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell") as? ReviewCell
//            cell?.selectionStyle = .none
//            cell?.configureCell(review: taskerReviews[indexPath.row])
//            return cell!
//        } else {
//            return UITableViewCell()
//        }
        
        
    }



}
