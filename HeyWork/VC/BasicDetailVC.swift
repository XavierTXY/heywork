//
//  BasicDetailVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class DetailCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var detailTxtField: UITextField!
    
    var vc: BasicDetailVC!
  
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(title: String, placeholder: String, userDetail: [String:String]) {
        titleLbl.text = title
        if title == "Email" {
            self.isUserInteractionEnabled = false
            detailTxtField.text = placeholder
        } else {
            self.isUserInteractionEnabled = true
            detailTxtField.placeholder = placeholder
        }
        
        if title == "Mobile" {
            
            self.detailTxtField.keyboardType = UIKeyboardType.phonePad
        }
        if title == "Date of Birth" || title == "Gender" || title == "Status" || title == "Disability" {
            self.detailTxtField.isUserInteractionEnabled = false
            
        } else {
             self.detailTxtField.isUserInteractionEnabled = true
        }
        
        if userDetail[title] != nil {
            detailTxtField.text = userDetail[title]
        }
        
        

        
    }
}
class BasicDetailVC: UIViewController,UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var details = [String]()
    var placeholders = [String]()
    var userDetails = [String:String]()
    
    var gender = ["Male", "Female"]
//    var status = ["Citizen", "Permenant Resident", "Work Permit"]
//    var disability = ["No", "Yes"]
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        details = ["Full Name", "Date of Birth", "Email", "Mobile", "Gender"]
        placeholders = ["As shown in IC", "dd/mm/yyyy", userDetails["Email"], "Mobile", "Select"] as! [String]
        
        if Checker.checker.userDetails.count != 0 {
            self.userDetails = Checker.checker.userDetails
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! DetailCell
        let text = cell.detailTxtField.text
        userDetails["Full Name"] = text
        
        let cell2 = tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! DetailCell
        let text2 = cell2.detailTxtField.text
        userDetails["Mobile"] = text2
        
        Checker.checker.userDetails = self.userDetails
        if userDetails["Full Name"] == "" || userDetails["Date of Birth"] == "" ||  userDetails["Mobile"] == "" || userDetails["Gender"] == ""  {
            Checker.checker.detailChecked = false
        } else {
            Checker.checker.detailChecked = true
        }
        
//        if userDetails["DOB"] != "" {
//            
//        }
//        
//        if userDetails["Mobile"] != "" {
//            
//        }
//        
//        if userDetails["Gender"] != "" {
//            
//        }
//        
//        if userDetails["Status"] != "" {
//            
//        }
//
//        if userDetails["Disability"] != "" {
//            
//        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as? DetailCell
        cell?.selectionStyle = .none
        cell?.configureCell(title: details[indexPath.row], placeholder: placeholders[indexPath.row], userDetail: userDetails)
        cell?.detailTxtField.delegate = self
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return details.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as? DetailCell

        let text = cell?.detailTxtField.text
        
        
        switch details[indexPath.row] {
        case "Full Name":

//            userDetails["Full Name"] = text
//            self.tableView.reloadData()
            break
        case "Date of Birth":


            
            let datePicker = ActionSheetDatePicker(title: "DOB", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
                picker, value, index in

                let a = "\(value!)"
                let fullNameArr = a.components(separatedBy: " ")
                let date = fullNameArr[0]

                let newArray = date.components(separatedBy: "-")
                print(newArray[0])
                print(newArray[1])
                print(newArray[2])
                
                if Int(newArray[0])! > 2005 {
                    ErrorAlert.errorAlert.showAlert(title: "Warning", msg: "You must be at least 13 years old to use this.", object: self)
                } else {
                    self.userDetails["Date of Birth"] = "\(newArray[2])-\(newArray[1])-\(newArray[0])"
                    cell?.detailTxtField.text = self.userDetails["Date of Birth"]
                }
                self.tableView.reloadData()
                
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
            let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
//            datePicker?.minimumDate = Date(timeInterval: -secondsInWeek, since: Date())
            datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
            
            datePicker?.show()

            
            break
        case "Mobile":
//            userDetails["Mobile"] = text
            break
        case "Gender":
            ActionSheetMultipleStringPicker.show(withTitle: "Gender", rows: [
                gender
                ], initialSelection: [0], doneBlock: {
                    picker, indexes, values in
                    
                    let idx = indexes?[0] as! Int
                    
                    self.userDetails["Gender"] = self.gender[idx]
                    cell?.detailTxtField.text = self.userDetails["Gender"]
                    self.tableView.reloadData()
     
                    return
            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: self.view)
            
            break
//        case "Status":
//            ActionSheetMultipleStringPicker.show(withTitle: "Status", rows: [
//                status
//                ], initialSelection: [0], doneBlock: {
//                    picker, indexes, values in
//                    
//                    let idx = indexes?[0] as! Int
//                    
//                    self.userDetails["Status"] = self.status[idx]
//                    cell?.detailTxtField.text = self.userDetails["Status"]
//                    self.tableView.reloadData()
//                    
//                    return
//            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: self.view)
//            
//            break
//        case "Disability":
//            ActionSheetMultipleStringPicker.show(withTitle: "Disability", rows: [
//                disability
//                ], initialSelection: [0], doneBlock: {
//                    picker, indexes, values in
//                    
//                    let idx = indexes?[0] as! Int
//                    
//                    self.userDetails["Disability"] = self.disability[idx]
//                    cell?.detailTxtField.text = self.userDetails["Disability"]
//                    self.tableView.reloadData()
//                    
//                    return
//            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: self.view)
//            
//            break
        default: break
            
        }

    }

    @objc func test() {
        
    }

}
