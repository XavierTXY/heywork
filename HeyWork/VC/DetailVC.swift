//
//  DetailVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 8/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
//import SkyFloatingLabelTextField
import TagListView
import Firebase
import SVProgressHUD
import PopupDialog


class DetailVC: UIViewController, UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource {

    var isFreelancer: Bool?
    var email: String!
    var pwd: String!
    var provider: String!
    var skills = [String]()
    var credential: AuthCredential?
    
    var statePicker: UIPickerView!
    var toolBar: UIToolbar!
    var pickerData: [String] = [String]()
    var selectedData: String?
    var username: String!
    
    @IBOutlet weak var firstName: UITextField!
//    @IBOutlet weak var mobileTF: UITextField!
//    @IBOutlet weak var countryTF: SkyFloatingLabelTextField!
    @IBOutlet weak var titleTF: UITextField!
//    @IBOutlet weak var stateTF: SkyFloatingLabelTextField!
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var tagView: TagListView!
    @IBOutlet weak var addSkillBtn: UIButton!
    @IBOutlet weak var skillView: UIStackView!
    @IBOutlet weak var dividerView: UIView!
    
  
    override func viewDidLoad() {
        super.viewDidLoad()

        statePicker = UIPickerView()
        statePicker.delegate = self
        statePicker.dataSource = self
        selectedData = ""
        
        
        firstName.delegate = self
//        mobileTF.delegate = self
//        countryTF.delegate = self
        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        self.loadPicker()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if self.isFreelancer! {
      
            
            tagView.isHidden = false
            //            addSkillBtn.isHidden = false
            skillView.isHidden = false
            dividerView.isHidden = false
        } else {

            
            tagView.isHidden = true
            //            addSkillBtn.isHidden = true
            skillView.isHidden = true
            dividerView.isHidden = true

        }
        
        
        tagView.removeAllTags()
        if Checker.checker.skills.count != 0 {
            
            
            for(skill, tick) in Checker.checker.skills {
                
                tagView.addTag(skill)
            }
        }
    }
    
    
    func loadPicker() {
        toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: Selector(("cancelPicker")))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: Selector(("cancelPicker")))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        statePicker.backgroundColor = UIColor.white
//        countryTF.inputView = self.statePicker
//        countryTF.inputAccessoryView = toolBar
        
        loadPickerData()
    }
    
    @objc func cancelPicker() {
//        countryTF.resignFirstResponder()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        if countryTF.isFirstResponder {
//            statePicker?.selectRow(0, inComponent: 0, animated: true)
//            if selectedData == "" {
//                selectedData = pickerData[0]
//            }
//
//            countryTF.text = selectedData
//        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        nameTxtField.resignFirstResponder()
        selectedData = pickerData[row]
//        countryTF.text = selectedData
    }
    
    func loadPickerData() {
//        Malacca
//        Negeri Sembilan
//        Pahang
//        Perak
//        Perlis
//        Penang
//        Sabah
//        Sarawak
//        Selangor
//        Terengganu
        pickerData = ["Johor", "Kedah", "Kelantan", "Malacca", "Negeri Sembilan","Pahang","Perak","Perlis","Penang","Sabah","Sarawak","Selangor","Terengganu"]
    }
    
    @IBAction func addSkillTapped(_ sender: Any) {
        view.endEditing(true)
        self.performSegue(withIdentifier: "SkillVC", sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? SkillVC {
            
            destinationVC.fromDetailVC = true
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func signupTapped(_ sender: Any) {
            Interaction().disableInteraction()

            if isFreelancer! {
                if Checker.checker.skills.count == 0 {
                    ErrorAlert.errorAlert.showAlert(title: "Warning", msg: "Must have at least one skill to start with", object: self)
                    
                } else {
//                    if checkName(name: titleTF.text!, errorMsg: "Title") {
                        if checkName(name: firstName.text!, errorMsg: "Name") {
//                            if checkName(name: lastnameTF.text!, errorMsg: "Last Name") {
//                                self.signUpTapped(userName: self.username!)
                                self.checkUserName()
//                            }
                        }
//                    }
                    
                    
                    
                    
                }
            } else {
//                if checkName(name: titleTF.text!, errorMsg: "Title") {
                    if checkName(name: firstName.text!, errorMsg: "Name") {
//                        if checkName(name: lastnameTF.text!, errorMsg: "Last Name") {
//                            self.signUpTapped(userName: self.username!)
                            self.checkUserName()
//                        }
                    }
//                }
//                checkUserName()
            }
    

            
//
//        if Checker.checker.skills.count == 0 {
//            ErrorAlert().createAlert(title: "Warning", msg: "Must have at least one skill to start with", object: self)
//
//        } else {
//         checkUserName()
//        }

    }
    
    func checkName(name: String, errorMsg: String) -> Bool {
        
        
        //        Interaction().disableInteraction(msg: "Checking user name...")
        //        SVProgressHUD.show()
        //        UIApplication.shared.beginIgnoringInteractionEvents()
//
        let trimmedName = name

        if( (trimmedName.contains(find: ".")) ) {
            ErrorAlert.errorAlert.showAlert(title: "Invalid \(errorMsg)", msg: "\(errorMsg) can't contain symbols", object: self)
            return false
        } else if (trimmedName.containsEmoji) {
            //            Interaction().enableInteraction()
            ErrorAlert.errorAlert.showAlert(title: "Invalid \(errorMsg)", msg: "\(errorMsg) can't contain emoji", object: self)
            return false
        } else if( trimmedName.count == 0 ){
            ErrorAlert.errorAlert.showAlert(title: "Invalid \(errorMsg)", msg: "\(errorMsg) can't be empty", object: self)
            return false
        } else {
            return true
        }
        
//        return true
    }
    
    func checkUserName() {

//        if self.usernameTF.isFirstResponder {
//            self.usernameTF.resignFirstResponder()
//        }


        let trimmedName = usernameTF.text?.trimmingCharacters(in: .whitespaces).removingWhitespaces().lowercased()

        if( (trimmedName?.containsSymbols())! ) {
            ErrorAlert.errorAlert.showAlert(title: "Invalid Name", msg: "Name can't contain symbols", object: self)
        } else if (trimmedName?.containsEmoji)! {
//            Interaction().enableInteraction()
            ErrorAlert.errorAlert.showAlert(title: "Invalid Name", msg: "Name can't contain emoji", object: self)
        } else if( trimmedName?.count == 0 ){
            ErrorAlert.errorAlert.showAlert(title: "Invalid Name", msg: "Name can't be empty", object: self)
        } else {

            DataService.ds.REF_USERNAME.child(trimmedName!).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {

                    ErrorAlert.errorAlert.showAlert(title: "Username is already in use", msg: "Please enter another user name", object: self)
                } else {
                    self.signUpTapped(userName: trimmedName!)

//                    print(trimmedName!)
                }
            })

        }

    }

//    func sendOTPToUser() {
//        PhoneAuthProvider.provider().verifyPhoneNumber(self.mobileTF.text!, uiDelegate: nil) { (verificationID, error) in
//            if let error = error {
//                ErrorAlert().createAlert(title: error.localizedDescription, msg: "", object: self)
//                return
//            }
//            // Sign in using the verificationID and the code sent to the user
//            // ...
//        }
//
//    }
//    func showCustomDialog(animated: Bool = true) {
//
//        // Create a custom view controller
//        let otpVC = OTPVC(nibName: "OTPVC", bundle: nil)
//
//
//        let popup = PopupDialog(viewController: otpVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
//
//        // Create first button
//        let buttonOne = CancelButton(title: "Cancel", height: 60) {
//
//        }
//
//        // Create second button
//        let buttonTwo = DefaultButton(title: "Ok", height: 60) {
//
//            var otp = otpVC.otpTF.text
//
//
//        }
//
//        // Add buttons to dialog
//        popup.addButtons([buttonOne, buttonTwo])
//
//        // Present dialog
//        present(popup, animated: animated, completion: nil)
//    }
    
    func signUpTapped(userName: String) {
        if provider == FIREBASE {
            Auth.auth().signIn(withEmail: email, password: pwd) { (user, error) in
                
                
                if error != nil {
//                    Interaction().enableInteraction()
                    ErrorAlert.errorAlert.showAlert(title: "Error", msg: "\((error?.localizedDescription)!)", object: self)
                } else {
                    if let user = user {
                        
                        
                        var reviewDict = [String: Bool]()
                        reviewDict["key"] = true
                        
                        var skillDict = [String: Bool]()
                        skillDict["key"] = true
                        skillDict["Use This App"] = true
                        
                        for (type, tick) in Checker.checker.skills {
                            skillDict[type] = true
                        }
                        
                        var taskerStarRating: Int!
                        var taskerNumRating: Int!
                        var taskerCompletionRate: Int!
                        var taskerTotalTaskAssigned: Int!
                        var taskerTotalTaskDone: Int!
                        
                        var posterStarRating: Int!
                        var posterNumRating: Int!
                        var posterCompletionRate: Int!
                        var posterTotalTaskPosted: Int!
                        var posterTotalTaskDone: Int!
                        
                        var uid = user.uid
//                        var uid = authData.user.uid
                        let userDict: Dictionary<String, AnyObject> = ["userName": userName as AnyObject, "name": self.firstName.text! as AnyObject,"email": self.email as AnyObject,"provider": self.provider as AnyObject, "skills": skillDict as AnyObject, "isFreelancer": self.isFreelancer! as AnyObject, "OS": DataService.ds.OS as AnyObject, "isVerified": false as AnyObject,"taskerStarRating": 0 as AnyObject,"taskerNumRating": 0 as AnyObject,"taskerCompletionRate": 0 as AnyObject,"taskerTotalTaskAssigned": 0 as AnyObject,"taskerTotalTaskDone": 0 as AnyObject, "posterStarRating": 0 as AnyObject,"posterNumRating": 0 as AnyObject,"posterCompletionRate": 0 as AnyObject,"posterTotalTaskPosted": 0 as AnyObject,"posterTotalTaskDone": 0 as AnyObject, "lastNotificationTime":NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970)) as AnyObject, "taskerReview": reviewDict as AnyObject, "posterReview": reviewDict as AnyObject, "bio": NO_BIO as AnyObject, "profilePicUrl": NO_PIC as AnyObject, "dob": NO_DOB as AnyObject,"totalCredit": 0 as AnyObject,"totalDebit": 0 as AnyObject, "creditBill": ["key":true] as AnyObject, "debitBill": ["key":true] as AnyObject, "isWorking": false as AnyObject, "notiCount": 0 as AnyObject,"msgCount": ["key": true] as AnyObject, "isActive": true as AnyObject]
                        DataService.ds.create(uid: uid, userDict: userDict)
                        
//                        self.addBtn.stopAnimation(animationStyle: .expand, completion: {
//                            let secondVC = UIViewController()
//                            self.present(secondVC, animated: true, completion: nil)
                        Interaction().enableInteraction()
                            VCSegue().segueVC(bundleName: "Home", controllerName: "TabBarVC", vc: self)
//                        })
                        
                    }
                }
            }
        } else {
            Auth.auth().signIn(with: self.credential!, completion: { (user, error) in
                if error != nil {
                    print("unable to auth with fire base - \(error)")
                    ErrorAlert.errorAlert.showAlert(title: (error?.localizedDescription)!, msg: error.debugDescription, object: self)
                } else {
                    print("Xavier: successful auth with firebase")
                    if let user = user {
                        var skillDict = [String: Bool]()
                        skillDict["key"] = true
                        skillDict["Use This App"] = true
                        
                        var reviewDict = [String: Bool]()
                        reviewDict["key"] = true
                        
                        for (type, tick) in Checker.checker.skills {
                            skillDict[type] = true
                        }
                        
                        
                        var uid = user.uid
                        let userDict: Dictionary<String, AnyObject> = ["userName": userName as AnyObject, "name": self.firstName.text! as AnyObject, "email": self.email as AnyObject,"provider": self.provider as AnyObject, "skills": skillDict as AnyObject, "isFreelancer": self.isFreelancer! as AnyObject, "OS": DataService.ds.OS as AnyObject,"isVerified": false as AnyObject,"taskerStarRating": 0 as AnyObject,"taskerNumRating": 0 as AnyObject,"taskerCompletionRate": 0 as AnyObject,"taskerTotalTaskAssigned": 0 as AnyObject,"taskerTotalTaskDone": 0 as AnyObject, "posterStarRating": 0 as AnyObject,"posterNumRating": 0 as AnyObject,"posterCompletionRate": 0 as AnyObject,"posterTotalTaskPosted": 0 as AnyObject,"posterTotalTaskDone": 0 as AnyObject, "lastNotificationTime":NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970)) as AnyObject,"taskerReview": reviewDict as AnyObject, "posterReview": reviewDict as AnyObject,"bio": NO_BIO as AnyObject, "profilePicUrl": NO_PIC as AnyObject, "dob": NO_DOB as AnyObject,"totalCredit": 0 as AnyObject,"totalDebit": 0 as AnyObject, "creditBill": ["key":true] as AnyObject, "debitBill": ["key":true] as AnyObject, "isWorking": false as AnyObject, "notiCount": 0 as AnyObject,"msgCount": ["key": true] as AnyObject,"isActive": true as AnyObject]
                        DataService.ds.create(uid: uid, userDict: userDict)
                        
//                        self.addBtn.stopAnimation(animationStyle: .expand, completion: {
                            //                            let secondVC = UIViewController()
                            //                            self.present(secondVC, animated: true, completion: nil)inter
                        Interaction().enableInteraction()
                            VCSegue().segueVC(bundleName: "Home", controllerName: "TabBarVC", vc: self)
//                        })
                        
                    }
                }
            })
        }

    }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        usernameTF.resignFirstResponder()
//        return true
//    }
}
