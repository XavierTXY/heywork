//
//  SignInVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import Firebase
//import SkyFloatingLabelTextField
import FBSDKLoginKit
import SVProgressHUD

class SignInVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailLbl: UITextField!
    @IBOutlet weak var passwordLbl: UITextField!
    
    var email: String!
    var password: String!
    var provider: String?
    var name: String?
    var gender: String?
    var credential: AuthCredential?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emailLbl.delegate = self
        passwordLbl.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func signInTapped(_ sender: Any) {
        
        self.resignAllResponder()
        Interaction().disableInteraction()
        
        
        if let email = emailLbl.text, let pwd = passwordLbl.text , (email.characters.count > 0 && pwd.characters.count > 0) {

            Auth.auth().signIn(withEmail: email, password: pwd) { (user, error) in
                self.email = email
                self.password = pwd
                self.provider = FIREBASE
                
                if error != nil {
                    Interaction().enableInteraction()
                    ErrorAlert.errorAlert.showAlert(title: "Error", msg: "\((error?.localizedDescription)!)", object: self)
                } else {
                    
                    self.checkUserProfile(id: user!.uid)
//                    if let user = user {
//                        self.checkUserProfile(id: user.uid)
////                        if user.isEmailVerified {
//
//                            var uid = (Auth.auth().currentUser?.uid)!
//                            DataService.ds.REF_USERS.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
////                                if snapshot.exists() {
//
////                                    let defaults = UserDefaults.standard
////                                    defaults.set(user.uid, forKey: "uid")
//
//                                    if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
//                                        //print("\(snapshot.value)")
//
//                                        if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
//                                            let key = snapshot.key
//                                            var currentUser = User(userKey: key, userData: userDict)
//                                            DataService.ds.signIn(uid: key)
////                                            DataService.ds.currentUser = currentUser
//
//
//                                        }
//                                    }
//
//                                    Interaction().enableInteraction()
//
//
//
//
////                                    VCSegue().segueVC(bundleName: "HomeSB", controllerName: "TabBarVC", vc: self)
//                                VCSegue().segueVC(bundleName: "Home", controllerName: "TabBarVC", vc: self)
//
////                                } else {
////                                    Interaction().enableInteraction()
////                                    self.performSegue(withIdentifier: "DetailVC", sender: nil)
////                                }
//                            })
//
//
////                        } else {
////                            Interaction().enableInteraction()
////                            self.performSegue(withIdentifier: "VerificationVC", sender: nil)
////                        }
//
//                    }
                }
            }
        } else {
            Interaction().enableInteraction()
            ErrorAlert.errorAlert.showAlert(title: "Username and Password Required", msg: "You must enter both a username and a password", object: self)
            //            self.coverImage.isHidden = true
        }
        
    }
    
    func resignAllResponder() {
        emailLbl.resignFirstResponder()
        passwordLbl.resignFirstResponder()
    }
    
    @IBAction func fbLoginTapped(_ sender: Any) {
        
        let facebookLogin = FBSDKLoginManager()
        
        
        facebookLogin.loginBehavior = .native
        
        facebookLogin.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            
            Interaction().enableInteraction()
            if error != nil {
                ErrorAlert.errorAlert.showAlert(title: (error?.localizedDescription)!, msg: (error?.localizedDescription)!, object: self)
                print("fucking \(error.debugDescription)")
            } else if result?.isCancelled == true {
             
                print("User cancelled fb auth")
            } else {

            
                let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                self.firebaseAuth(credential)
                
                
            }
        }
    }
    
    func firebaseAuth(_ credential: AuthCredential) {
        Auth.auth().signIn(with: credential, completion: { (user, error) in
            Interaction().enableInteraction()
            
            if error != nil {
                print("unable to auth with fire base - \(error)")
                ErrorAlert.errorAlert.showAlert(title: (error?.localizedDescription)!, msg: (error?.localizedDescription)!, object: self)
            } else {
                print("Xavier: successful auth with firebase")
                if let user = user {
                    self.credential = credential
                    self.email = user.email!
                    self.provider = credential.provider
                    self.password = ""
                    //                    let userData = ["provider": credential.provider, "email": user.email, "OS": DataService.ds.OS]
                    //                    //let userName = ["name": user.uid]
                    //
                    //                    self.provider = credential.provider
                    //                    self.email = user.email
                    //                    print(user.uid)
                    //                    print(userData)
                    //
                    //                    DataService.ds.createFirebaseDBUserWithPartial(uid: user.uid, userData: userData as! Dictionary<String, String>)
                    
                    self.checkUserProfile(id: user.uid)
                    
                }
            }
        })
    }
    
    func checkUserProfile(id: String) {
        
        DataService.ds.REF_USERS.child(id).observeSingleEvent(of: .value, with: { (snapshot) in
            
            Interaction().enableInteraction()
//            let data = snapshot.value as? String
            print(snapshot.value)
            if snapshot.exists() {
                
                
                DataService.ds.signIn(uid: id)
                VCSegue().segueVC(bundleName: "Home", controllerName: "TabBarVC", vc: self)
                print("logging in")
                
//                self.performSegue(withIdentifier: "SignUpVC", sender: nil)
            } else {
                

                
                print("havent enter detail")
//                SVProgressHUD.dismiss()
//                UIApplication.shared.endIgnoringInteractionEvents()
                
                self.performSegue(withIdentifier: "AccountTypeVC", sender: nil)
                //                VCSegue().segueVC(bundleName: "HomeSB", controllerName: "TabBarVC", vc: self)
            }
            
            
        })
        
    }


    func removeSpecialCharsFromString(str: String) -> String {
        let chars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890_".characters)
        
        return String(str.characters.filter { chars.contains($0) })
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
//        if let destinationVC = segue.destination as? WelcomeVC {
//            destinationVC.email = "xavier-889@hotmail.com"
//        }
        
        if let destinationVC = segue.destination as? AccountTypeVC {
            destinationVC.email = self.email
            destinationVC.pwd = self.password
            destinationVC.provider = self.provider
            destinationVC.credential = self.credential
            //        destinationVC.isFreelancer = self.isFreelancer
            
        }
    }
    @IBAction func forgotPwdTapped(_ sender: Any) {
        
    }
}
