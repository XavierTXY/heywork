//
//  AddEmploymentVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit

class AddCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var detailLbl: UITextField!
    
    override func awakeFromNib() {
        
    }
    
    func configureCell(title: String, placeholder: String, values: [String:String]) {
        titleLbl.text = title
        
        if values[title] != nil && values[title] != "" {
            detailLbl.text = values[title]
        } else {
            detailLbl.placeholder = placeholder
        }
//        detailLbl.placeholder = placeholder
    }
    
//    func configureCellWithout(title: String, placeholder: String) {
//        titleLbl.text = title
//        detailLbl.placeholder = placeholder
//    }
}
class AddEmploymentVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var descTxtView: UITextView!
    
    var titles = ["Company", "Job Title", "Industry","Job Type", "Start Date", "End Date"]
    var placeholder = ["Company", "Job Title", "Industry","Job Type", "Start Date", "End Date"]
    var values = [String:String]()
    var selectedIdx: Int?
    
//    var previousJob: Employment?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(dismissTapped))
        
        
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if values[titles[0]] != nil {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveTapped))
        } else {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
        }
    }
    
//    @objc func dismissTapped() {
//        self.navigationController?.popViewController(animated: true)
//    }
    @objc func saveTapped() {
        
        
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! AddCell
        let text = cell.detailLbl!.text
        
        values[titles[0]] = text
        
        let cell2 = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! AddCell
        let text2 = cell2.detailLbl!.text
        values[titles[1]] = text2
        
        let cell3 = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! AddCell
        let text3 = cell3.detailLbl!.text
        values[titles[2]] = text3
        
        let cell4 = tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! AddCell
        let text4 = cell4.detailLbl!.text
        values[titles[3]] = text4
        
        let cell5 = tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as! AddCell
        let text5 = cell5.detailLbl!.text
        values[titles[4]] = text5
        
        let cell6 = tableView.cellForRow(at: IndexPath(row: 5, section: 0)) as! AddCell
        let text6 = cell6.detailLbl!.text
        values[titles[5]] = text6
        
        values["desc"] = self.descTxtView.text
        
        if values[titles[0]] == "" || values[titles[1]] == "" || values[titles[2]] == "" || values[titles[3]] == "" || values[titles[4]] == "" || values[titles[5]] == ""{
            print("Please enter detail correctly")
        } else {
            
            Checker.checker.jobs.remove(at: selectedIdx!)
            Checker.checker.jobs.append(values)
            self.navigationController?.popViewController(animated: true)
        }
    }
    @objc func addTapped() {
        print("tapped")
        
     
        
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! AddCell
        let text = cell.detailLbl!.text

        values[titles[0]] = text
        
        let cell2 = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! AddCell
        let text2 = cell2.detailLbl!.text
        values[titles[1]] = text2
        
        let cell3 = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! AddCell
        let text3 = cell3.detailLbl!.text
        values[titles[2]] = text3
        
        let cell4 = tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! AddCell
        let text4 = cell4.detailLbl!.text
        values[titles[3]] = text4
        
        let cell5 = tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as! AddCell
        let text5 = cell5.detailLbl!.text
        values[titles[4]] = text5
        
        let cell6 = tableView.cellForRow(at: IndexPath(row: 5, section: 0)) as! AddCell
        let text6 = cell6.detailLbl!.text
        values[titles[5]] = text6
        
        values["desc"] = self.descTxtView.text
        
        if values[titles[0]] == "" || values[titles[1]] == "" || values[titles[2]] == "" || values[titles[3]] == "" || values[titles[4]] == "" || values[titles[5]] == ""{
            print("Please enter detail correctly")
        } else {

            Checker.checker.jobs.append(values)
            self.navigationController?.popViewController(animated: true)
        }
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddCell") as! AddCell
        

        cell.configureCell(title: titles[indexPath.row], placeholder: titles[indexPath.row], values: values)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }

}
