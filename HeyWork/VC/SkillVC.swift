//
//  SkillVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 8/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit

class skillCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var tickedImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(text: String) {
        self.selectionStyle = .none
        titleLbl.text = text
    }
//    func configureCell(text: String, ticked: Bool) {
//        self.selectionStyle = .none
//        titleLbl.text = text
//
//        if ticked {
////            tickedImg.backgroundColor = UIColor.yellow
//            tickedImg.image = UIImage(named: "selected")
//            titleLbl.textColor = MAIN_COLOR
//        } else {
//            titleLbl.textColor = UIColor.black
//            tickedImg.image = UIImage(named: "emptySelect")
//        }
//
//    }
}
class SkillVC: UIViewController , UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var skillTF: UITextField!
    
    var selectedSkills = [String: Int]()
//    var selectedTicks = [Int]()
    
    var ticks = [Bool]()
    
    var skills = [String]()
    var skillDict = [String:String]()
    
    var fromDetailVC = false
    var fromEditProfileVC = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        skillTF.delegate = self
        
        if fromDetailVC || fromEditProfileVC {
            descLbl.text = "Enter up to 10 skills that you have"
        } else {
            descLbl.text = "Enter up to 5 skills required for this task"
        }
        
        if fromEditProfileVC {
            
            for (skill, bol) in Checker.checker.skills {
                self.skillDict[skill] = skill
            }
            
            self.reloadData()
        }
        ticks = [Bool](repeating: false, count: SKILLS.count)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
//        if Checker.checker.skills.count != 0 {
////            print("walao\(selectedSkills)")
//
//            for (type, tick) in Checker.checker.skills {
//                self.selectedSkills = Checker.checker.skills
//                ticks[tick] = true
//
//            }
//        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
//        if self.selectedSkills.count != 0 {
//            Checker.checker.skills.removeAll()
            Checker.checker.skills = self.skillDict
//        } else {
           
//        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let text = textField.text {
            
            if text.count != 0 {
                
                if fromDetailVC || fromEditProfileVC {
                    if self.skills.count < 10 {
                        self.skillDict[text] = text
//                        textField.resignFirstResponder()
                        textField.text = nil
                        self.reloadData()
                    }
                } else {
                    if self.skills.count < 5 {
                        self.skillDict[text] = text
//                        textField.resignFirstResponder()
                        textField.text = nil
                        self.reloadData()
                    }
                }
              
                
                
            }
        }
        
        return true
    }
    
    func reloadData() {
         self.skills = Array(self.skillDict.values)
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var deleted = self.skills.remove(at: indexPath.row)
        self.skillDict.removeValue(forKey: deleted)
        self.reloadData()
//        if ticks[indexPath.row] {
//            ticks[indexPath.row] = false
//            selectedSkills.removeValue(forKey: SKILLS[indexPath.row])
////            selectedSkills.append(skills[indexPath.row])
////            selectedTicks.append(indexPath.row)
//        } else {
//            ticks[indexPath.row] = true
//            selectedSkills[SKILLS[indexPath.row]] = indexPath.row
////            selectedSkills.append(skills[indexPath.row])
////            selectedTicks.append(indexPath.row)
//
//        }
//
//
//        self.tableView.reloadData()
        
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "skillCell") as! skillCell
        


        cell.configureCell(text: skills[indexPath.row])
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return skills.count
    }
    

    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
