//
//  EducationVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit

class EducationCell: UITableViewCell {
    @IBOutlet weak var schoolLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(eduDict: [String:String]) {
        titleLbl.text = eduDict["Degree"]
        schoolLbl.text = eduDict["School"]
        
        dateLbl.text = "\(eduDict["Start Date"]) - \(eduDict["End Date"])"
    }
}

class EducationVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var placeholder = ["School", "Degree", "Field of Study","Type", "Start Date", "End Date"]
    var educations = [[String:String]]()
    var selectedData: [String:String]?
    var selectedIdx: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        if Checker.checker.educations.count == 0 {
            Checker.checker.educationChecked = false
        } else {
            Checker.checker.educationChecked = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if Checker.checker.educations.count != 0 {
            self.educations = Checker.checker.educations
        }
        
        self.tableView.reloadData()
    }
    @objc func addTapped() {
        self.selectedData = nil
        self.performSegue(withIdentifier: "AddEducationVC", sender: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return educations.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EducationCell") as! EducationCell
//        cell.configureCell(title: <#String#>)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedData = educations[indexPath.row]
        self.selectedIdx = indexPath.row
    
        self.performSegue(withIdentifier: "AddEducationVC", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            //            tableView.beginUpdates()
            

            self.educations.remove(at: indexPath.row)
            Checker.checker.educations.remove(at: indexPath.row)
            self.selectedData = nil
            
            //            tableView.endUpdates()
            tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? AddEducationVC {
            
            if let data = self.selectedData {
                destinationVC.values = data
                destinationVC.selectedIdx = selectedIdx!
            }
            
        }
    }
}
