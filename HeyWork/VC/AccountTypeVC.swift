//
//  AccountTypeVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 8/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import Firebase

class AccountTypeVC: UIViewController {

    @IBOutlet weak var firstView: RoundView!
    @IBOutlet weak var secondView: RoundView!
    
    var isFreelancer: Bool?
    var email: String!
    var pwd: String!
    var provider: String!
    var credential: AuthCredential?
    var username: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func findFreelancerTapped(_ sender: Any) {
        isFreelancer = false
        self.performSegue(withIdentifier: "DetailVC", sender: nil)
    }
    
    @IBAction func earnMoneyTapped(_ sender: Any) {
        isFreelancer = true
        self.performSegue(withIdentifier: "DetailVC", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? DetailVC {
            destinationVC.email = self.email
            destinationVC.pwd = self.pwd
            destinationVC.isFreelancer = self.isFreelancer
            destinationVC.credential = self.credential
            destinationVC.provider = self.provider
            destinationVC.username = self.username
            
        }
    }


}
