//
//  WelcomeVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController {

    @IBOutlet weak var detailView: RoundView!
    @IBOutlet weak var educationView: RoundView!
    @IBOutlet weak var languageView: RoundView!
    @IBOutlet weak var employmentView: RoundView!
    
    var email:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if Checker.checker.detailChecked {
            detailView.backgroundColor = UIColor.green
        } else {
            detailView.backgroundColor = BUTTON_COLOR
        }
        
        if Checker.checker.languageChecked {
            languageView.backgroundColor = UIColor.green
        } else {
            languageView.backgroundColor = BUTTON_COLOR
        }
        
        if Checker.checker.employmentChecked {
            employmentView.backgroundColor = UIColor.green
        } else {
            employmentView.backgroundColor = BUTTON_COLOR
        }
        
        if Checker.checker.educationChecked {
            educationView.backgroundColor = UIColor.green
        } else {
            educationView.backgroundColor = BUTTON_COLOR
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func nextTapped(_ sender: Any) {
        
        if Checker.checker.detailChecked && Checker.checker.employmentChecked && Checker.checker.educationChecked && Checker.checker.languageChecked{
            
        }
        
        VCSegue().segueVC(bundleName: "Home", controllerName: "TabBarVC", vc: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVC = segue.destination as? BasicDetailVC {
            destinationVC.userDetails["Email"] = self.email
        }
    }
    
}
