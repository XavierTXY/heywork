//
//  EmploymentVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit


class EmploymentCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(jobDict: [String:String]) {
        titleLbl.text = jobDict["Job Title"]!
        companyLbl.text = jobDict["Company"]!
        
        if jobDict["Company"] == "-" {
            dateLbl.text = ""
        } else {
            dateLbl.text = "\(jobDict["Start Date"]!) - \(jobDict["End Date"]!)"
        }
        
    }
}
class EmploymentVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var titles = ["Company", "Job Title", "Industry", "Job Type", "Start Date", "End Date"]
    var data = [[String:String]]()
    var selectedData: [String:String]?
    var selectedIdx: Int?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
        

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        if Checker.checker.jobs.count == 0 {
            Checker.checker.employmentChecked = false
        } else {
            Checker.checker.employmentChecked = true
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if Checker.checker.jobs.count != 0 {
            self.data = Checker.checker.jobs
        }
        
        self.tableView.reloadData()
        
    }
    
    @objc func addTapped() {
        let alert = UIAlertController(title: "Choose", message: "Choose o", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "No Work Before", style: .default, handler: { (action) in
            if !Checker.checker.noExperience && self.data.count == 0{
                var values = [String:String]()
                let emptyJob = Employment()
                values[self.titles[0]] = emptyJob.company
                values[self.titles[1]] = emptyJob.title
                values[self.titles[2]] = emptyJob.industry
                values[self.titles[3]] = emptyJob.type
                values[self.titles[4]] = emptyJob.startDate
                values[self.titles[5]] = emptyJob.endDate
                values["desc"] = emptyJob.desc
                
                Checker.checker.jobs.append(values)
                self.data.append(values)
                self.tableView.reloadData()
                Checker.checker.noExperience = true
            }
            

        }))
        
        alert.addAction(UIAlertAction(title: "Add a job", style: .default, handler: { (action) in
            
            if !Checker.checker.noExperience {
                self.selectedData = nil
                self.performSegue(withIdentifier: "AddEmploymentVC", sender: nil)
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmploymentCell") as! EmploymentCell
        cell.configureCell(jobDict: data[indexPath.row])
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if data[indexPath.row]["Company"] != "-" {
            selectedData = data[indexPath.row]
            selectedIdx = indexPath.row
            self.performSegue(withIdentifier: "AddEmploymentVC", sender: nil)
        }

        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            //            tableView.beginUpdates()
            
            if data[indexPath.row]["Company"] == "-" {
                Checker.checker.noExperience = false
            }
            self.data.remove(at: indexPath.row)
            Checker.checker.jobs.remove(at: indexPath.row)
            
            //            tableView.endUpdates()
            tableView.reloadData()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? AddEmploymentVC {
            
            if let data = self.selectedData {
                destinationVC.values = data
                destinationVC.selectedIdx = selectedIdx
                print("selected idx\(selectedIdx!)")
            }
            
        }
    }

}
