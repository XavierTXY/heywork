//
//  ForgotPwdVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 9/6/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import UIKit
import Firebase

class ForgotPwdVC: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func subTapped(_ sender: Any) {
        
        if emailField.text!.count != 0 {
            
            Auth.auth().sendPasswordReset(withEmail: emailField.text!) { error in
                
                if error != nil {
                    ErrorAlert.errorAlert.showAlert(title: "Error", msg: error.debugDescription, object: self)
                } else {
                    ErrorAlert.errorAlert.showAlert(title: "Sent", msg: "Please check your email for reset password link.", object: self)
                }
            }
        } else {
            ErrorAlert.errorAlert.showAlert(title: "Error", msg: "Email can't be empty.", object: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
