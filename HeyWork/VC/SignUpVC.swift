//
//  SignUpVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
//import SkyFloatingLabelTextField
import Firebase
import SVProgressHUD
import FBSDKLoginKit

class SignUpVC: UIViewController {

    @IBOutlet weak var emailLbl: UITextField!
    @IBOutlet weak var pwdLbl: UITextField!

    
    
    var email: String?
    var password: String?
    var provider: String?
    var credential: AuthCredential?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func resignAllResponder() {
        emailLbl.resignFirstResponder()
        pwdLbl.resignFirstResponder()
    }
    

    @IBAction func createTapped(_ sender: Any) {
        self.resignAllResponder()
        SVProgressHUD.show()
        
        email = self.emailLbl.text!
        password = self.pwdLbl.text!
        provider = FIREBASE
        
//        if self.checkUserName() {
            //        self.checkEmail()
            if (password?.count)! < 6 {
                ErrorAlert.errorAlert.showAlert(title: "Error", msg: "Password must be at least 6 in length", object: self)
            } else {
                Auth.auth().createUser(withEmail: email!, password: password!, completion: { (user, error) in
                    if error == nil {
                        
                        print("Xavier: SUccessfuly auth with firebase")
                        
                        if let user = user {
                            SVProgressHUD.dismiss()
                            //                        self.checkUserName()
                            self.performSegue(withIdentifier: "AccountTypeVC", sender: nil)
                        }
                        
                    } else {
                        
                        Auth.auth().signIn(withEmail: self.email!, password: self.password!) { (user, error) in
                            if error != nil {
                                SVProgressHUD.dismiss()
                                ErrorAlert.errorAlert.showAlert(title: "Email is already in use", msg: "Please enter another email address", object: self)
                                //                            ErrorAlert().show(title: "Error", msg: "\((error?.localizedDescription)!)", object: self)
                            } else {
                                if let user = user {
                                    //                            if user.isEmailVerified {
                                    
                                    var uid = (Auth.auth().currentUser?.uid)!
                                    DataService.ds.REF_USERS.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                                        if snapshot.exists() {
                                            
                                            SVProgressHUD.dismiss()
                                            ErrorAlert.errorAlert.showAlert(title: "Email is already in use", msg: "Please enter another email address", object: self)
                                        } else {
                                            SVProgressHUD.dismiss()
                                            //                                        self.checkUserName()
                                            self.performSegue(withIdentifier: "AccountTypeVC", sender: nil)
                                        }
                                    })
                                    
                                    
                                }
                            }
                        }
                        
                    }
                    
                })
            }
//        }


        
//        DataService.ds.REF_EMAIL.child(email!).observeSingleEvent(of: .value, with: { (snapshot) in
//            if snapshot.exists() {
//
//                SVProgressHUD.dismiss()
//                ErrorAlert().show(title: "Email is already in use", msg: "Please enter another email address", object: self)
//            } else {
//
//
//                SVProgressHUD.dismiss()
//                self.performSegue(withIdentifier: "AccountTypeVC", sender: nil)
//            }
//        })
        

    }
    
//    func checkEmail() {
//        if( self.email?.count == 0 ){
//            ErrorAlert().createAlert(title: "Invalid Name", msg: "Name can't be empty", object: self)
//        } else {
//            DataService.ds.REF_EMAIL.child(self.email!).observeSingleEvent(of: .value, with: { (snapshot) in
//                if snapshot.exists() {
//
//                    ErrorAlert().show(title: "Email is already in use", msg: "Please enter another email", object: self)
//                }
//            })
//        }
//
//    }
    
    @IBAction func termsTapped(_ sender: Any) {
        guard let url = URL(string: TC_LINK) else { return }
        UIApplication.shared.openURL(url)
    }
    
    @IBAction func fbTapped(_ sender: Any) {
        let facebookLogin = FBSDKLoginManager()


        facebookLogin.loginBehavior = .native

        facebookLogin.logIn(withReadPermissions: ["email"], from: self) { (result, error) in


            if error != nil {
                print("fucking \(error.debugDescription)")
            } else if result?.isCancelled == true {
                print("User cancelled fb auth")
            } else {
                SVProgressHUD.show()
                UIApplication.shared.beginIgnoringInteractionEvents()
                print("Xavier: Successfully logged in with fb")
                let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                self.firebaseAuth(credential)


            }
        }
    }
    
    func firebaseAuth(_ credential: AuthCredential) {
        Auth.auth().signIn(with: credential, completion: { (user, error) in
            if error != nil {
                print("unable to auth with fire base - \(error)")
                SVProgressHUD.showError(withStatus: "\(error)")
                UIApplication.shared.endIgnoringInteractionEvents()
            } else {
                print("Xavier: successful auth with firebase")
                if let user = user {
                    self.credential = credential
                    self.email = user.email!
                    self.provider = FB
                    self.password = ""
//                    let userData = ["provider": credential.provider, "email": user.email, "OS": DataService.ds.OS]
//                    //let userName = ["name": user.uid]
//
//                    self.provider = credential.provider
//                    self.email = user.email
//                    print(user.uid)
//                    print(userData)
//
//                    DataService.ds.createFirebaseDBUserWithPartial(uid: user.uid, userData: userData as! Dictionary<String, String>)
                    
                
                    self.checkUserProfile(id: user.uid)
                    
                }
            }
        })
    }
    
//    var username: String!
//    func checkUserName() -> Bool {
//
//        var retVal = false
//        if self.usernameLbl.isFirstResponder {
//            self.usernameLbl.resignFirstResponder()
//        }
//
//        //        Interaction().disableInteraction(msg: "Checking user name...")
//        //        SVProgressHUD.show()
//        //        UIApplication.shared.beginIgnoringInteractionEvents()
//
//        let trimmedName = usernameLbl.text?.trimmingCharacters(in: .whitespaces).removingWhitespaces().lowercased()
//
//        if( (trimmedName?.contains(find: "."))! ) {
//            ErrorAlert().createAlert(title: "Invalid Name", msg: "Name can't contain symbols", object: self)
//            return false
//        } else if (trimmedName?.containsEmoji)! {
//            //            Interaction().enableInteraction()
//            ErrorAlert().createAlert(title: "Invalid Name", msg: "Name can't contain emoji", object: self)
//            return false
//        } else if( trimmedName?.count == 0 ){
//            ErrorAlert().createAlert(title: "Invalid Name", msg: "Name can't be empty", object: self)
//            return false
//        } else {
//
//            DataService.ds.REF_USERNAME.child(trimmedName!).observeSingleEvent(of: .value, with: { (snapshot) in
//                if snapshot.exists() {
//
//                    ErrorAlert().show(title: "Username is already in use", msg: "Please enter another user name", object: self)
//
//                } else {
////                    self.signUpTapped(userName: trimmedName!)
//                    self.username = trimmedName!
//                    retVal = true
////                     self.performSegue(withIdentifier: "AccountTypeVC", sender: nil)
//
//                    //                    print(trimmedName!)
//                }
//            })
//
//            return retVal
//        }
//
//    }
    
    
    func checkUserProfile(id: String) {
        
        let _ = DataService.ds.REF_USERS.child(id).observeSingleEvent(of: .value, with: { (snapshot) in
            
//            let data = snapshot.value as? String
            
            if !snapshot.exists() {

                
                SVProgressHUD.dismiss()
                UIApplication.shared.endIgnoringInteractionEvents()
                
//                self.checkUserName()
                self.performSegue(withIdentifier: "AccountTypeVC", sender: nil)

            } else {
                
                SVProgressHUD.dismiss()
                UIApplication.shared.endIgnoringInteractionEvents()
                DataService.ds.signIn(uid: id)
                VCSegue().segueVC(bundleName: "Home", controllerName: "TabBarVC", vc: self)
                
                //                VCSegue().segueVC(bundleName: "HomeSB", controllerName: "TabBarVC", vc: self)
            }
            
            
        })
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? AccountTypeVC {
            destinationVC.email = self.email
//            destinationVC.username = self.username
            destinationVC.pwd = self.password
            destinationVC.provider = self.provider
            destinationVC.credential = self.credential
            
            //        destinationVC.isFreelancer = self.isFreelancer
            
        }
    }


}
