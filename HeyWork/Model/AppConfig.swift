//
//  AppConfig.swift
//  HeyWork
//
//  Created by XavierTanXY on 26/5/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//


import Foundation
import Firebase

class AppConfig: NSObject {
    
    var version: String?
    var maintenanceMsg: String?
    var isMaintenance: Bool?
    var mustUpdate: Bool?
    
    
    init(dictionary: [String: Any]) {

        version = dictionary["version"] as? String
        maintenanceMsg = dictionary["maintenanceMsg"] as? String
        isMaintenance = dictionary["isMaintenance"] as? Bool
        mustUpdate = dictionary["mustUpdate"] as? Bool
        
    }
}
