//
//  Bank.swift
//  HeyWork
//
//  Created by XavierTanXY on 23/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation

class BankCard {
    
    var cardNo: String!
//    var cardName: String!
    var cardExDate: String!
    var cardCVV: String!
    var cardBrand: String!
    
    init(no: String, /*name: String,*/ date:String, cvv: String, brand: String) {
        self.cardNo = no
//        self.cardName = name
        self.cardExDate = date
        self.cardCVV = cvv
        self.cardBrand = brand
    }
}
