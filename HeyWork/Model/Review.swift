//
//  Review.swift
//  HeyWork
//
//  Created by XavierTanXY on 3/11/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
class Review {
    
//    var reviewID: String!
//    var reviwerID: String!
//    var reviwerUsername: String!
//    var currentUserID: String!
//    var currentUsername: String!
    
//    var stars: Double!
    var taskID: String!
    var numStar: Double!
    var reviewMsg: String!
    var posterUsername: String!
    var posterID: String!
    var posterImageUrl: String!
    
    var time: NSNumber!
    
    
    init(posterID: String, posterUsername: String, posterImageUrl: String,taskID: String,reviewMsg: String, time: NSNumber, numStar: Double) {
        
//        self.reviewID = reviewID
//        self.reviwerID = reviwerID
//        self.reviwerUsername = reviwerUsername
//        self.currentUserID = currentUserID
        self.posterUsername =  posterUsername
        self.posterID = posterID
        self.posterImageUrl = posterImageUrl
        self.taskID = taskID
        self.reviewMsg = reviewMsg
        self.numStar = numStar
      
        self.time = time
//        self.time = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        
    }
    
    init(taskID:String, reviewDict: Dictionary<String, AnyObject>) {
//        self.reviewID = reviewID
        
//        if let reviwerID = reviewDict["reviwerID"] as? String {
//            self.reviwerID = reviwerID
//        }
//
//        if let reviwerUsername = reviewDict["reviwerUsername"] as? String {
//            self.reviwerUsername = reviwerUsername
//        }
//
//        if let reviwerUsername = reviewDict["reviwerUsername"] as? String {
//            self.reviwerUsername = reviwerUsername
//        }
//
//        if let currentUserID = reviewDict["currentUserID"] as? String {
//            self.currentUserID = currentUserID
//        }
//
//
//
//        if let stars = reviewDict["stars"] as? Double {
//            self.stars = stars
//        }
        
        if let posterID = reviewDict["posterID"] as? String {
            self.posterID = posterID
        }
        
        if let posterUsername = reviewDict["posterUsername"] as? String {
            self.posterUsername = posterUsername
        }
        
        if let posterImageUrl = reviewDict["posterImageUrl"] as? String {
            self.posterImageUrl = posterImageUrl
        }
        
        if let taskID = reviewDict["taskID"] as? String {
            self.taskID = taskID
        }
        
        if let reviewMsg = reviewDict["reviewMsg"] as? String {
            self.reviewMsg = reviewMsg
        }
        
        if let numStar = reviewDict["numStar"] as? Double {
            self.numStar = numStar
        }
        
        if let time = reviewDict["time"] as? NSNumber {
            self.time = time
        }
    }

}
