//
//  Offer.swift
//  HeyWork
//
//  Created by XavierTanXY on 24/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import Firebase

class Offer {
    
    var offerID: String!
    var offerUserName: String!
    var offerUserID: String!
    var offerUserProfilePicUrl: String!
    var offerMsg: String!
    var offerPrice: Int!
    var offerDate: NSNumber!
    
    var offerStatus: String!
    
    var time: NSNumber!
    
    var userStarRating: Double!
    var userNumRating: Int!
    var userCompletionRate: Double!
    
    init(offerID: String, offerUserName: String, offerUserID: String, offerUserProfilePicUrl: String, offerMsg: String, offerPrice: Int, offerDate: NSNumber,userStarRating: Double,userNumRating: Int, userCompletionRate: Double) {
        
        self.offerID = offerID
        self.offerUserName = offerUserName
        self.offerUserID = offerUserID
        self.offerUserProfilePicUrl = offerUserProfilePicUrl
        self.offerMsg =  offerMsg
        self.offerPrice = offerPrice
        self.offerDate = offerDate
        
        self.userStarRating = userStarRating
        self.userNumRating = userNumRating
        self.userCompletionRate = userCompletionRate
        
        self.offerStatus = NOT_WORKING
        
        self.time = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        
    }
    
    init(offerID: String, offerDict: Dictionary<String, AnyObject>) {
        self.offerID = offerID
        
        if let username = offerDict["offerUserName"] as? String {
            self.offerUserName = username
        }
        
        if let offerUserID = offerDict["offerUserID"] as? String {
            self.offerUserID = offerUserID
        }
        
        if let offerUserProfilePicUrl = offerDict["offerUserProfilePicUrl"] as? String {
            self.offerUserProfilePicUrl = offerUserProfilePicUrl
        }
        
        if let offerMsg = offerDict["offerMsg"] as? String {
            self.offerMsg = offerMsg
        }
        
     
        
        if let offerPrice = offerDict["offerPrice"] as? Int {
            self.offerPrice = offerPrice
        }
        
        if let offerDate = offerDict["offerDate"] as? NSNumber {
            self.offerDate = offerDate
        }
        
        if let userStarRating = offerDict["userStarRating"] as? Double {
            self.userStarRating = userStarRating
        }
        
        if let userNumRating = offerDict["userNumRating"] as? Int {
            self.userNumRating = userNumRating
        }
        
        if let userCompletionRate = offerDict["userCompletionRate"] as? Double {
            self.userCompletionRate = userCompletionRate
        }
        
        
        if let offerStatus = offerDict["offerStatus"] as? String {
            self.offerStatus = offerStatus
        }
        
        if let time = offerDict["time"] as? NSNumber {
            self.time = time
        }
    }
    
    func getDict() -> Dictionary<String,AnyObject> {
        var dict = [String:AnyObject]()
        dict["offerUserName"] = self.offerUserName as AnyObject
        dict["offerUserID"] = self.offerUserID as AnyObject
        dict["offerUserProfilePicUrl"] = self.offerUserProfilePicUrl as AnyObject
        dict["offerMsg"] = self.offerMsg as AnyObject
        dict["offerPrice"] = self.offerPrice as AnyObject
        dict["offerDate"] = self.offerDate as AnyObject
        dict["userStarRating"] = self.userStarRating as AnyObject
        dict["userNumRating"] = self.userNumRating as AnyObject
        dict["userCompletionRate"] = self.userCompletionRate as AnyObject
        dict["offerStatus"] = self.offerStatus as AnyObject
        dict["time"] = self.time as AnyObject
        return dict
    }
    
    func changeState(taskKey: String, id: String, newStatus: String,completion:@escaping ()-> Void) {
        print(self.offerStatus)
        print(newStatus)

        if self.offerStatus! == POSTER_REVIEWED {
            if newStatus == TASKER_REVIEWED {
                self.offerStatus = BOTH_REVIEWED
                DataService.ds.REF_TASK_ASSIGN.child(taskKey).child(id).updateChildValues(["offerStatus": BOTH_REVIEWED])
                self.offerStatus = BOTH_REVIEWED
                
                
                completion()
            }
        }
        
        if self.offerStatus! == TASKER_REVIEWED {
            if newStatus == POSTER_REVIEWED {
                self.offerStatus = BOTH_REVIEWED
                
                DataService.ds.REF_TASK_ASSIGN.child(taskKey).child(id).updateChildValues(["offerStatus": BOTH_REVIEWED])
                self.offerStatus = BOTH_REVIEWED
                print("update here alr")
                
                completion()
            }
        }
        
        if self.offerStatus! != POSTER_REVIEWED && self.offerStatus! != TASKER_REVIEWED && self.offerStatus! != BOTH_REVIEWED {
            DataService.ds.REF_TASK_ASSIGN.child(taskKey).child(id).updateChildValues(["offerStatus": newStatus])
            self.offerStatus = newStatus
            
            print("nrestatus")
            completion()
            
        }
        
        
 

//        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
//
//            if var taskDict = currentData.value as? Dictionary<String, AnyObject> {
//
//                //var confessionDict = currentData.value as? Dictionary<String, AnyObject>
//
//
//
//                var status = taskDict["offerStatus"] as? String
//                status = newStatus
//
//
//                taskDict["offerStatus"] = status as AnyObject
//
//
//
//
//                currentData.value = taskDict
//                print("scueess")
//                return TransactionResult.success(withValue: currentData)
//            }
//
//            return TransactionResult.success(withValue: currentData)
//        }) { (error, commited, snap) in
//
//            if commited {
//                completion()
//            }
//
//        }
    }
}
