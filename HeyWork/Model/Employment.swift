//
//  Employment.swift
//  HeyWork
//
//  Created by XavierTanXY on 2/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation

class Employment {
    
    var title: String!
    var type: String!
    var company: String!
    var startDate: String!
    var endDate: String!
    var desc: String!
    var industry: String!
    
    init(company: String, title: String, industry: String, type: String , startDate: String, endDate: String, desc: String) {
        self.title = title
        self.type = type
        self.company = company
        self.startDate = startDate
        self.endDate = endDate
        self.desc = desc
        self.industry = industry
    }
    
    init() {
        self.title = "No Work Before"
        self.type = "-"
        self.company = "-"
        self.startDate = "-"
        self.endDate = "-"
        self.desc = "-"
        self.industry = "-"
    }
    
}
