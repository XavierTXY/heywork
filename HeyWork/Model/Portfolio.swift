//
//  Portfolio.swift
//  HeyWork
//
//  Created by XavierTanXY on 23/2/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import Foundation

class Portfolio {
    
    var portID: String!
    var portTitle: String!
    var portDesc: String!
    var portImageUrls: [String: String]!
    var portImageUrl: String!
    var portImageHeight: Double!
    
    var posterID: String!
  
    init(portKey: String, dict: Dictionary<String, AnyObject>) {
        self.portID = portKey
        
        if let portTitle = dict["portTitle"] as? String {
            self.portTitle = portTitle
        }
        
        if let portDesc = dict["portDesc"] as? String {
            self.portDesc = portDesc
        }
        
        if let portImageUrl = dict["portImageUrl"] as? [String: String] {
            self.portImageUrls = portImageUrl
        }
        
        if let portImageHeight = dict["portImageHeight"] as? Double {
            self.portImageHeight = portImageHeight
        }
        
        if let posterID = dict["posterID"] as? String {
            self.posterID = posterID
        }
    }
        
}
