//
//  Task.swift
//  HeyWork
//
//  Created by XavierTanXY on 9/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import Firebase
import GooglePlaces
import GeoFire

class Task {
    
    var taskKey: String!
    var title: String!
    var desc: String!
    var deadline: NSNumber!
    var budget: Int!
    var peopleNeeded: Int!
    var category: String!
    var time: NSNumber!
    var status: String!
    var posterID: String!
    var posterUsername: String!
    
    var assigned: [String: Bool]!
    var skills: [String: Bool]!
    var offered: [String: Bool]!
    
    var attachmentUrls = [String]()
//    var completed: [String: Bool]!
    
    var place: Place?
    var isActive: Bool?
    
    var taskRef: DatabaseReference!
    var allTaskRef: DatabaseReference!
    
    init(taskKey: String, title: String, desc: String, deadline: NSNumber, budget: Int, peopleNeeded: Int, category: String,poster: String, posterKey: String, skills: [String:Bool], place: GMSPlace?) {
        self.taskKey = taskKey
        self.title = title
        self.desc = desc
        self.deadline = deadline
        self.budget = budget
        self.peopleNeeded = peopleNeeded
        self.category = category
        self.time = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        self.posterID = posterKey
        self.posterUsername = poster
        self.skills = skills
        self.taskRef = DataService.ds.REF_TASK.child(taskKey)
        self.allTaskRef = DataService.ds.REF_ALL_TASK.child(taskKey)
        self.status = OPEN
        self.isActive = true
        
        if let p = place {
            self.place = Place(name: p.name!, lat: p.coordinate.latitude, long: p.coordinate.longitude)
        }
        
      
    }
    
    init(taskKey: String, dict: Dictionary<String, AnyObject>) {
        self.taskKey = taskKey
        
        if let posterUsername = dict["posterUsername"] as? String {
            self.posterUsername = posterUsername
        }
        
        if let posterID = dict["posterID"] as? String {
            self.posterID = posterID
        }
        
        if let title = dict["title"] as? String {
            self.title = title
        }
        
        if let desc = dict["desc"] as? String {
            self.desc = desc
        }
        
        if let deadline = dict["deadline"] as? NSNumber {
            self.deadline = deadline
        }
        
        if let status = dict["status"] as? String {
            self.status = status
        }
        
        if let budget = dict["budget"] as? Int {
            self.budget = budget
        }
        
        if let peopleNeeded = dict["peopleNeeded"] as? Int {
            self.peopleNeeded = peopleNeeded
        }
        
        if let category = dict["category"] as? String {
            self.category = category
        }
        
        if let offered = dict["offered"] as? [String: Bool] {
            self.offered = offered
        }
        
        if let skills = dict["skills"] as? [String: Bool] {
            self.skills = skills
        }
        
        if let assigned = dict["assigned"] as? [String: Bool] {
            self.assigned = assigned
        }
        
        if let attachmentUrls = dict["attachmentUrls"] as? [String] {
            self.attachmentUrls = attachmentUrls
        }
        
//        if let completed = dict["completed"] as? [String: Bool] {
//            self.completed = completed
//        }
        
        if let time = dict["time"] as? NSNumber {
            self.time = time
        }
        
        if let isActive = dict["isActive"] as? Bool {
            self.isActive = isActive
        }
        
        if let placeDict = dict["place"] as? [String: AnyObject] {
            self.place = Place(name: placeDict["name"] as! String, lat: placeDict["latitude"] as! Double, long: placeDict["longitude"] as! Double)
        }
        

    }
    
    func getServiceCharge() -> Double {
        return Double(self.budget) * SERVICE_CHARGE
    }
    
    func getGrossEarning() -> Double {
        return Double(self.budget) - Double(self.budget) * SERVICE_CHARGE
    }
    
    func removeFromFirebase() {
        
        var dict = [String:AnyObject]()
        
        dict["isActive"] = false as AnyObject
        dict["status"] = REMOVED as AnyObject
        
        DataService.ds.REF_TASK.child(self.taskKey).updateChildValues(dict)
        DataService.ds.REF_Category.child(self.category).child(self.taskKey).updateChildValues(dict)
        DataService.ds.REF_USER_TASK.child(DataService.ds.currentUser.userKey!).child(self.taskKey).updateChildValues(dict)
        
        self.isActive = false
        self.status = REMOVED
    }
    
    func updateToFirebase(completion:@escaping ()-> Void) {
        
        
        var dict = [String:AnyObject]()
        dict["title"] = self.title as AnyObject
        dict["desc"] = self.desc as AnyObject
        dict["deadline"] = self.deadline as AnyObject
        dict["budget"] = self.budget as AnyObject
        dict["peopleNeeded"] = self.peopleNeeded as AnyObject
        dict["category"] = self.category as AnyObject
        dict["time"] = self.time as AnyObject
        dict["status"] = self.status as AnyObject
        dict["posterUsername"] = self.posterUsername as AnyObject
        dict["posterID"] = self.posterID as AnyObject
        
        dict["offered"] = ["key":true] as AnyObject
        dict["assigned"] = ["key":true] as AnyObject
//        dict["completed"] = ["key":true] as AnyObject
        dict["skills"] = self.skills as AnyObject
        
        dict["isActive"] = true as AnyObject
        
        
 
        if self.attachmentUrls != nil {
            dict["attachmentUrls"] = self.attachmentUrls as AnyObject
        }
        
        if self.place != nil {
            dict["place"] = ["name":self.place?.name, "latitude": self.place?.latitude, "longitude": self.place?.longitude]  as AnyObject
            
        }
        
        
        self.taskRef.updateChildValues(dict)
        self.allTaskRef.updateChildValues(dict)
        DataService.ds.REF_Category.child(self.category).child(self.taskKey).updateChildValues(dict)
        DataService.ds.REF_USER_TASK.child(DataService.ds.currentUser.userKey!).child(self.taskKey).updateChildValues(dict)
        Checker.checker.skills.removeAll()
        

        if self.place != nil  {
            let geofireRef = Database.database().reference()
            let geoFire = GeoFire(firebaseRef: geofireRef.child("task-location"))
            let taskLocation = CLLocation(latitude: self.place!.latitude,  longitude: self.place!.longitude)
            geoFire.setLocation(taskLocation, taskName: self.title, taskBudget: Int32(self.budget), taskDeadline: Int32(self.deadline), forKey: self.taskKey)
        }

        completion()
        
        
        
    }
    
    func uploadAttachment(images: [UIImage], completion:@escaping ()-> Void) {
//        var numPhoto = 0
//        var idx = 0
        
        let ref = DB_BASE
        var id = self.taskKey
        //        var portDict = [String:AnyObject]()
        //        portDict["portTitle"] = portTitle as AnyObject
        //        portDict["portDesc"] = portDesc as AnyObject
        
        var urlDict = Dictionary<String, Bool>()
        for img in images {
            //            if img != #imageLiteral(resourceName: "vipLogo") {
//            numPhoto = numPhoto + 1
            //                var compression = UIImage.jpegData(img)
            if let imgData = img.jpegData(compressionQuality:0.1) {
                
                let imgUid = NSUUID().uuidString
                let metaData = StorageMetadata()
                metaData.contentType = "image/jpeg"
                
                DataService.ds.REF_TASK_PIC.child(imgUid).putData(imgData, metadata: metaData) { (metaData, error) in
                    if error != nil {
                        print("Xavier: upload image to firebase failed")
                        print(error.debugDescription)
                    } else {
                        print("Xavier: successfuly upload img to firebase")
                        let downloadUrl = metaData?.downloadURL()?.absoluteString
                        if let url = downloadUrl {
                            //                                self.postToFirebase(imgUrl: url)
                            
                            print(id)
//                            print(portID)
                            var updateObj = [String:AnyObject]()
                            
                            
                            //                                portDict["portImageUrl"] = ["url\(idx)": url] as AnyObject
                            
                            
                            self.attachmentUrls.append(url)
                            
                            if images.count == self.attachmentUrls.count {
                                self.updateToFirebase(completion: {
                                    completion()
                                })
                            }
                            //                                updateObj["/users/\(id)/portfolio/\(portID)/portImageUrl/"] = NSNull() as AnyObject
//                            updateObj["/users/\(id)/portfolio/\(portID)/portImageUrl/url\(idx)"] = url as AnyObject

//                            ref.updateChildValues(updateObj, withCompletionBlock: { (error, ref) in
//
//                                if error != nil {
//                                    ErrorAlert.errorAlert.showAlert(title: (error?.localizedDescription)!, msg: error.debugDescription, object: vc)
//                                } else {
//                                    SVProgressHUD.dismiss()
//                                    //                                        self.currentUser.portfolio[portID] = Portfolio(portKey: portID, dict: portDict)
//                                    completion()
//                                    //                                        vc.dismiss(animated: true, completion: nil)
//                                }
//                            })
//                            //                                updateObj["/category/\(cat)/\(id)/urls/url\(idx)"] = url as AnyObject
//                            idx = idx + 1
                            
                            
                            //
                            //                                if idx == numPhoto {
                            //                                    SVProgressHUD.showSuccess(withStatus: "Done!")
                            //                                    vc.dismiss(animated: true, completion: nil)
                            //                                }
                            
                            
                        }
                        
                    }
                    
                }
            }
            //            }
        }
    }
    

    func updateAssignToComplete(taskerID: String,completion:@escaping ()-> Void) {
        
        DataService.ds.REF_TASK.child(self.taskKey).child("assigned").updateChildValues([taskerID:true]) { (error, ref) in
            if error != nil {
                
            } else {
                self.assigned[taskerID] = true
                self.updateTaskStatus {
                    completion()
                }
                
                
            }
        }
    }
    
    func updateTaskStatus(completion:@escaping ()-> Void) {
        
        var numCompleted = 0
        for(taskerID, completed) in self.assigned {
            
            if taskerID != "key" {
                if completed {
                    numCompleted = numCompleted + 1
                }
            }
            
        }
        print(self.peopleNeeded)
        print(numCompleted)
        
        //if task completed
        if self.peopleNeeded == numCompleted {
            print("task complete")
            
            updateStatus(status: COMPLETED)

        } else {
            
//            //if task is full and not finished
//            if self.peopleNeeded == ( self.assigned.count - 1 ) {
//                updateStatus(status: PENDING)
//            }
            
        }
        
        completion()
    }
    
    func updateStatus(status: String) {
        
        let lowercasedStatus = status.lowercased()
        let updatePrefix = "\(lowercasedStatus)-task"
        
        var updateObj = [String:AnyObject]()
        
        updateObj["task/\(taskKey!)/status"] = status as AnyObject
        //            updateObj["category/\(self.category!)/\(taskKey!)/status"] = COMPLETED as AnyObject
        updateObj["user-task/\(self.posterID!)/\(taskKey!)/status"] = status as AnyObject
        
        //Moved to other branches
        updateObj["\(updatePrefix)/\(taskKey!)/assigned"] = self.assigned as AnyObject
        updateObj["\(updatePrefix)/\(taskKey!)/budget"] = self.budget as AnyObject
        updateObj["\(updatePrefix)/\(taskKey!)/budget"] = self.budget as AnyObject
        updateObj["\(updatePrefix)/\(taskKey!)/category"] = self.category as AnyObject
        updateObj["\(updatePrefix)/\(taskKey!)/deadline"] = self.deadline as AnyObject
        updateObj["\(updatePrefix)/\(taskKey!)/desc"] = self.desc as AnyObject
        updateObj["\(updatePrefix)/\(taskKey!)/peopleNeeded"] = self.peopleNeeded as AnyObject
        updateObj["\(updatePrefix)/\(taskKey!)/posterID"] = self.posterID as AnyObject
        updateObj["\(updatePrefix)/\(taskKey!)/posterUsername"] = self.posterUsername as AnyObject
        updateObj["\(updatePrefix)/\(taskKey!)/skills"] = self.skills as AnyObject
        updateObj["\(updatePrefix)/\(taskKey!)/time"] = self.time as AnyObject
        updateObj["\(updatePrefix)/\(taskKey!)/title"] = self.title as AnyObject
        updateObj["\(updatePrefix)/\(taskKey!)/status"] = status as AnyObject
        
        updateObj["category/\(self.category!)/\(taskKey!)"] = NSNull() as AnyObject
//        updateObj["task/\(taskKey!)"] = NSNull() as AnyObject
        
        DB_BASE.updateChildValues(updateObj) { (error, ref) in
            if error != nil {
                
            } else {
                self.status = status
                DB_BASE.child("task").updateChildValues([self.taskKey!: NSNull()])
                
            }
        }
    }
    func updateAssign(offer: Offer, taskerID: String, add: Bool, offerDict: Dictionary<String, AnyObject>, offerKey: String, completion:@escaping ()-> Void) {
        
        

        let ref2 = DataService.ds.REF_TASK.child(self.taskKey)
        
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
            
            if var taskDict = currentData.value as? Dictionary<String, AnyObject> {
                
                //var confessionDict = currentData.value as? Dictionary<String, AnyObject>
                
                
                
                var assigned = taskDict["assigned"] as? [String: Bool]
                if add {
                    assigned?[taskerID] = false
                    //                    self.addUser(uid: uid)
                    self.assigned[taskerID] = false
                    
                } else {
                    // userLike.removeValue(forKey: uid)
                    //                    userLike?.removeValue(forKey: uid)
                    //                    self.removeUser(uid: uid)
                }
                
                taskDict["assigned"] = assigned as AnyObject
                
                
                
                
                currentData.value = taskDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in
            
            if commited {
                
                //This is the last person
                offer.offerStatus = WORKING
                DataService.ds.REF_USER_TASK.child(self.posterID).child(self.taskKey).child("assigned").updateChildValues([taskerID:false])
                
                DataService.ds.REF_Category.child(self.category).child(self.taskKey).child("assigned").updateChildValues([taskerID:false])
            
               
//                DataService.ds.REF_USER_NOTIFICATION.child(self.posterID).child(self.taskKey).child("assigned").updateChildValues([taskerID:false])
                
                //                DataService.ds.REF_USER_TASK.child(DataService.ds.currentUser.userKey!).child(self.taskKey).child("offered").updateChildValues([taskerID:true])
                
                DataService.ds.REF_TASK_ASSIGN.child(self.taskKey).child(taskerID).updateChildValues(offer.getDict())
//                DataService.ds.REF_TASK_ASSIGN.child(self.taskKey).child(taskerID).updateChildValues(["offerStatus":WORKING])

                DataService.ds.REF_TASK_OFFER.child(self.taskKey).child(offerKey).updateChildValues(offer.getDict())
                self.assigned[taskerID] = false
                
                self.updateTaskStatus(completion: {
                    completion()
                })
                
                
            }
        }
    }
    
    func updateOffer(taskerID: String, add: Bool, offerDict: Dictionary<String, AnyObject>,completion:@escaping ()-> Void) {
        let ref2 = DataService.ds.REF_TASK.child(self.taskKey)
        
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
            
            if var taskDict = currentData.value as? Dictionary<String, AnyObject> {
                
                //var confessionDict = currentData.value as? Dictionary<String, AnyObject>
                
                
                
                var offered = taskDict["offered"] as? [String: Bool]
                if add {
                    offered?[taskerID] = true
//                    self.addUser(uid: uid)
                    self.offered[taskerID] = true
                    
                } else {
                    // userLike.removeValue(forKey: uid)
//                    userLike?.removeValue(forKey: uid)
//                    self.removeUser(uid: uid)
                }
                
                taskDict["offered"] = offered as AnyObject
                

                
                
                currentData.value = taskDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in
            
            if commited {
                
                
                DataService.ds.REF_USER_TASK.child(self.posterID).child(self.taskKey).child("offered").updateChildValues([taskerID:true])
                
                DataService.ds.REF_Category.child(self.category).child(self.taskKey).child("offered").updateChildValues([taskerID:true])
                
                DataService.ds.REF_USER_NOTIFICATION.child(self.posterID).childByAutoId().updateChildValues(["userID": taskerID, "userName": "\(offerDict["offerUserName"]!)","message": "\(offerDict["offerUserName"]!) has made an offer on your task - \(self.title!)", "userProfilePicUrl":offerDict["offerUserProfilePicUrl"]!, "taskTitle": self.title, "timeStamp": NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))])
                let user = User()
                user.adjustNotiCount(addCount: true, userID: self.posterID)
//                DataService.ds..child(self.posterID).child(self.taskKey).child("offered").updateChildValues([taskerID:true])
//                DataService.ds.REF_USER_TASK.child(DataService.ds.currentUser.userKey!).child(self.taskKey).child("offered").updateChildValues([taskerID:true])
               
                DataService.ds.REF_TASK_OFFER.child(self.taskKey).child(taskerID).updateChildValues(offerDict)
                self.offered[taskerID] = true
                completion()
                
            }
        }
    }
    
    func isFull() -> Bool {
        if self.peopleNeeded == assigned.count - 1 {
            return true
        } else {
            return false
        }
    }
}
