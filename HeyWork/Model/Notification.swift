//
//  Notification.swift
//  HeyWork
//
//  Created by XavierTanXY on 26/5/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import Foundation
import Firebase

class Notification: NSObject {
    
    var notiKey: String
    var userProfilePicUrl: String?
    var userID: String?
    var userName: String?
    var taskTitle: String?
    var timeStamp: NSNumber?
    
    
    init(key: String, dictionary: [String: Any]) {
        notiKey = key
        userID = dictionary["userID"] as? String
        userName = dictionary["userName"] as? String
        taskTitle = dictionary["taskTitle"] as? String
        userProfilePicUrl = dictionary["userProfilePicUrl"] as? String
        timeStamp = dictionary["timeStamp"] as? NSNumber

    }
}
