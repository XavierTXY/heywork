//
//  Education.swift
//  HeyWork
//
//  Created by XavierTanXY on 4/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation

class Education {
    var school: String!
    var degree: String!
    var fieldOfStudy: String!
    var startDate: String!
    var endDate: String!
    var type: String!

    
    init(school: String, degree: String, fieldOfStudy: String, type: String , startDate: String, endDate: String) {
        self.school = school
        self.type = type
        self.degree = degree
        self.startDate = startDate
        self.endDate = endDate

    }
}
