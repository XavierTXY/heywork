//
//  ClusteredTaskLocation.swift
//  HeyWork
//
//  Created by XavierTanXY on 21/5/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import Foundation
import GoogleMaps


/// Point of Interest Item which implements the GMUClusterItem protocol.
class ClusteredTaskLocation: NSObject, GMUClusterItem  {
    var position: CLLocationCoordinate2D
    var taskTitle: String!
    var taskBudget: Int!
    
    
    
    init(position: CLLocationCoordinate2D, taskTitle: String, taskBudget: Int) {
        self.position = position
        self.taskTitle = taskTitle
        self.taskBudget = taskBudget
        
        
    }
}
