//
//  NetworkManager.swift
//  HeyWork
//
//  Created by XavierTanXY on 25/5/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import Foundation
import Reachability

class NetworkManager: NSObject {
    var reachability: Reachability!
    var emptyView: EmptyView!
    
    static let sharedInstance: NetworkManager = {
        return NetworkManager()
    }()
    override init() {
        super.init()
        // Initialise reachability
        reachability = Reachability()!
        
        emptyView = EmptyView.instanceFromNib(image: UIImage(named: "no-wifi")!, title: "You're offline", desc: "Please try to connect to the internet.") as! EmptyView
        
        // Register an observer for the network status
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        do {
            // Start the network status notifier
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        // Do something globally here!
        print("reach wtf")
        switch reachability.connection {
        case .wifi:
            print("Reachable via WiFi")
            emptyView.removeFromSuperview()
        case .cellular:
            print("Reachable via Cellular")
            emptyView.removeFromSuperview()
        case .none:
            emptyView.frame = UIApplication.shared.keyWindow!.bounds
            UIApplication.shared.keyWindow!.addSubview(emptyView)
            print("Network not reachable")
        }
    }
    
    static func stopNotifier() -> Void {
        do {
            // Stop the network status notifier
            try (NetworkManager.sharedInstance.reachability).startNotifier()
        } catch {
            print("Error stopping notifier")
        }
    }
    
    // Network is reachable
    static func isReachable(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection != .none {
            completed(NetworkManager.sharedInstance)
        }
    }
    
    // Network is unreachable
    static func isUnreachable(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection == .none {
            completed(NetworkManager.sharedInstance)
        }
    }
    
    // Network is reachable via WWAN/Cellular
    static func isReachableViaWWAN(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection == .cellular {
            completed(NetworkManager.sharedInstance)
        }
    }
    
    // Network is reachable via WiFi
    static func isReachableViaWiFi(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection == .wifi {
            completed(NetworkManager.sharedInstance)
        }
    }
}
