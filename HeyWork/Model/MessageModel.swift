//
//  MessageModel.swift
//  HeyWork
//
//  Created by XavierTanXY on 12/11/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import Firebase

class MessageModel: NSObject {
    
    var msgId: String?
    var fromId: String?
    var fromUserName: String?
    var toUserName: String?
    var toId: String?
    var timeStamp: NSNumber?
    var text: String?
    var imageUrl: String?
    var docUrl: String?
    var fileName: String?
    var isItem: Bool?
    var itemType: String?
    var itemUnit: String?
    
    
    var fromSeen: Bool?
    var toSeen: Bool?
    
    var imgWidth: NSNumber?
    var imgHeight: NSNumber?
    
    
    func chatPartnerId() -> String? {
        let chatPartnerId: String?
        
        if fromId == Auth.auth().currentUser?.uid {
            return toId
        } else {
            return fromId
        }
    }
    
    init(key: String, dictionary: [String: Any]) {
        super.init()
        msgId = key
        fromId = dictionary["fromId"] as? String
        toId = dictionary["toId"] as? String
        timeStamp = dictionary["timeStamp"] as? NSNumber
        text = dictionary["text"] as? String
        fromUserName = dictionary["fromUserName"] as? String
        toUserName = dictionary["toUserName"] as? String
        imageUrl = dictionary["imageUrl"] as? String
        docUrl = dictionary["docUrl"] as? String
        fileName = dictionary["fileName"] as? String
        imgWidth = dictionary["imgWidth"] as? NSNumber
        imgHeight = dictionary["imgHeight"] as? NSNumber
        
        fromSeen = dictionary["fromSeen"] as? Bool
        toSeen = dictionary["toSeen"] as? Bool
        isItem = dictionary["isItem"] as? Bool
        
        itemType = dictionary["itemType"] as? String
        itemUnit = dictionary["itemUnit"] as? String
    }
}
