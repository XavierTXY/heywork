//
//  Place.swift
//  HeyWork
//
//  Created by XavierTanXY on 29/4/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import Foundation

class Place {
    var name: String!
    var latitude: Double!
    var longitude: Double!
    
    init(name: String, lat: Double, long: Double) {
        self.name = name
        self.latitude = lat
        self.longitude = long
    }
}
