//
//  TaskTime.swift
//  HeyWork
//
//  Created by XavierTanXY on 14/11/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import Firebase

class TaskTime: NSObject {
    
    var taskID: String?
    var timeStamp: NSNumber?

    
    
    init(key: String, time: NSNumber) {
        self.taskID = key
        self.timeStamp = time
    }
}
