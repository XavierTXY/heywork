//
//  User.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import GeoFire

class User: NSObject {
    
    //    private var _userName: String?
    //    private var userKey: String?
    //
    //    var course: String?
    //    var email: String?
    //    var gender: String?
    //    var name: String?
    //    var provider: String?
    //    var university: String?
    //    var year: String?
    
    private var _userName: String?
    var userKey: String?
    var stripeKey: String?
    var connectKey: String?
    private var _course: String?
    private var _email: String?
    private var _gender: String?
    private var _name: String?
//    private var _lastName: String?
    private var _mobileNumber: Int?
    private var _provider: String?
    private var _university: String?
    private var _dob: String?
    private var _isVerified: Bool?
//    private var _bankCard: BankCard?
    private var _languages: [String: String]?
    
    private var _profilePicUrl: String?
    private var _notiCount: Int?

    private var _msgCount: [String: Bool]!
    

    private var _userRef: DatabaseReference!
    
    var taskerStarRating: Double!
    var taskerNumRating: Int!
    var taskerCompletionRate: Double!
    var taskerTotalTaskAssigned: Int!
    var taskerTotalTaskDone: Int!
    
    var posterStarRating: Double!
    var posterNumRating: Int!
    var posterCompletionRate: Double!
    var posterTotalTaskPosted: Int!
    var posterTotalTaskDone: Int!
    
    var taskerReview = [String: Review]()
    var posterReview = [String: Review]()
    
    var creditBill = [String: Bill]()
    var debitBill = [String: Bill]()
    
    var portfolio = [String: Portfolio]()
    
    var totalCredit: Int!
    var totalDebit: Int!
    
    var country: String!
    var title: String!
    var lastNotificationTime: NSNumber!
    
    var bio: String!
    var skills: [String: Bool]!
    
    var tempSkills = [String: Bool]()
    var cardID: String?
    var bankCard: BankCard?
    
    var bankAccount: BankAccount?
    
    var isWorking: Bool?
    var reviewedApp: Bool?
    var isActive: Bool?

    var currentLocation: Place?
    
    var userName: String {
        return _userName!
    }
    //
    //    var userKey: String {
    //        return _userKey!
    //    }
    
    var course: String {
        
        get {
            return _course!
        }
        
        set(newCourse) {
            _course = newCourse
        }
    }
    
    var email: String {
        return _email!
    }
    
    var gender: String {
        get {
            return _gender!
        }
        
        set(newGender) {
            _gender = newGender
        }
        
    }
    
    var name: String {
        get {
            return _name!
        }
        
        set(newName) {
            _name = newName
        }
        
    }
    
    
    var mobileNumber: Int {
        get {
            return _mobileNumber!
        }
        
        set(newMobile) {
            _mobileNumber = newMobile
        }
        
    }
    
    var provider: String {
        return _provider!
    }
    
    var isVerified: Bool {
        return _isVerified!
    }
    
    var university: String {
        get {
            return _university!
        }
        
        set(newUni) {
            _university = newUni
        }
        
    }
    

    
    var dob: String? {
        get {
            return _dob
        }
        
        set(newDOB) {
            _dob = newDOB
        }
        
    }
//    
//    var bankCard: BankCard {
//        get {
//            return _bankCard!
//        }
//        
//        set(bank) {
//            _bankCard = bankCard
//        }
//        
//    }

    var languages: [String: String] {
        return _languages!
    }
    
    
    var profilePicUrl: String {
        get {
            return _profilePicUrl!
        }
        
        set(newUrl) {
            _profilePicUrl = newUrl
        }
    }
    
    
    
    
    var userRef: DatabaseReference {
        return _userRef
    }
    

    
    var notiCount: Int {
        get {
            return _notiCount!
        }
        
        set(newCount) {
            _notiCount = newCount
        }
        
    }
    
    var msgCount: [String: Bool] {
        return _msgCount
    }
    

    
    override init() {
        
    }
    
    
    init(userKey: String, userData: Dictionary<String, AnyObject>) {
        
        
        self.userKey = userKey
        
        if let userName = userData["userName"] as? String {
            self._userName = userName
        }
        
        if let name = userData["name"] as? String {
            self._name = name
        }
        
        if let mobileNumber = userData["mobileNumber"] as? Int {
            self._mobileNumber = mobileNumber
        }
        
        if let v = userData["isVerified"] as? Bool {
            self._isVerified = v
        }
        
        if let course = userData["course"] as? String {
            self._course = course
        }
        
        if let email = userData["email"] as? String {
            self._email = email
        }
        
        if let gender = userData["gender"] as? String {
            self._gender = gender
        }
        
        if let provider = userData["provider"] as? String {
            self._provider = provider
        }
        
        if let taskerStarRating = userData["taskerStarRating"] as? Double {
            self.taskerStarRating = taskerStarRating
        }
        
        if let taskerNumRating = userData["taskerNumRating"] as? Int {
            self.taskerNumRating = taskerNumRating
        }
        
        if let taskerCompletionRate = userData["taskerCompletionRate"] as? Double {
            self.taskerCompletionRate = taskerCompletionRate
        }

        if let taskerTotalTaskAssigned  = userData["taskerTotalTaskAssigned"] as? Int {
            self.taskerTotalTaskAssigned = taskerTotalTaskAssigned
        }

        if let taskerTotalTaskDone = userData["taskerTotalTaskDone"] as? Int {
            self.taskerTotalTaskDone = taskerTotalTaskDone
        }
        
        //Poster
        if let posterStarRating = userData["posterStarRating"] as? Double {
            self.posterStarRating = posterStarRating
        }
        
        if let  posterNumRating = userData["posterNumRating"] as? Int {
            self.posterNumRating = posterNumRating
        }
        
        if let posterCompletionRate = userData["posterCompletionRate"] as? Double {
            self.posterCompletionRate = posterCompletionRate
        }
        
        if let posterTotalTaskPosted  = userData["posterTotalTaskPosted"] as? Int {
            self.posterTotalTaskPosted = posterTotalTaskPosted
        }
        
        if let posterTotalTaskDone = userData["posterTotalTaskDone"] as? Int {
            self.posterTotalTaskDone = posterTotalTaskDone
        }

        
        if let dob = userData["dob"] as? String {
            self._dob = dob
        }
        

        if let languages = userData["languages"] as? [String: String] {
            self._languages = languages
        }

        
        if let profilePicUrl = userData["profilePicUrl"] as? String {
            self._profilePicUrl = profilePicUrl
        }
        
        if let notiCount = userData["notiCount"] as? Int {
            self._notiCount = notiCount
        }
        
        
        if let msgCount = userData["msgCount"] as? [String: Bool] {
            self._msgCount = msgCount
        }
        
        if let country = userData["country"] as? String {
            self.country = country
        }
        
        if let title = userData["title"] as? String {
            self.title = title
        }
        
        if let lastNotificationTime = userData["lastNotificationTime"] as? NSNumber {
            self.lastNotificationTime = lastNotificationTime
        }
        
        if let bio = userData["bio"] as? String {
            self.bio = bio
        }
        
        if let skills = userData["skills"] as? [String: Bool] {
            self.skills = skills
        }

        if let card = userData["bank"] as? [String: String] {
            //            print(card["cardNo"])
            //            print(card["cardExDate"])
            //            print(card["cardCVV"])
            
            self.bankCard = BankCard(no: card["cardNo"]!, date: card["cardExDate"]!, cvv: card["cardCVV"]!, brand: card["cardBrand"]!)
        }
        
        if let isWorking = userData["isWorking"] as? Bool {
            self.isWorking = isWorking
        }
        
        if let isActive = userData["isActive"] as? Bool {
            self.isActive = isActive
        }
        
        if let reviewedApp = userData["reviewedApp"] as? Bool {
            self.reviewedApp = reviewedApp
        }
        
        if let placeDict = userData["currentLocation"] as? [String: AnyObject] {
            self.currentLocation = Place(name: "", lat: placeDict["latitude"] as! Double, long: placeDict["longitude"] as! Double)
        }
        
        if let account = userData["bankAccount"] as? Dictionary<String, AnyObject> {
            //            print(card["cardNo"])
            //            print(card["cardExDate"])
            //            print(card["cardCVV"])
            
            self.bankAccount = BankAccount(accNo: account["accNo"]! as! String, bankName: account["bankName"]! as! String, accName: account["accName"]! as! String, active: account["isActive"]! as! Bool, createdTime: account["createdTime"]! as! NSNumber, deactivatedTime: account["deactivatedTime"]! as! NSNumber, bankID: account["bankID"]! as! String)

//            self.bankAccount = BankAccount(accNo: account["accNo"]!, bankName: account["bankName"]!, accName: account["accName"]!)
//            BankAccount(accNo: account["accNo"]! as! String, bankName: account["bankName"]!, accName: account["accName"]!, active: account["isActive"]!, createdTime: <#T##NSNumber#>, deactivatedTime: <#T##NSNumber#>, bankID: <#T##String#>)
        }
        
        if let cardID = userData["cardID"] as? String {
            self.cardID = cardID
//            print(card["cardNo"])
//            print(card["cardExDate"])
//            print(card["cardCVV"])

//            self.bankCard = BankCard(no: card["cardNo"]!, date: card["cardExDate"]!, cvv: card["cardCVV"]!)
        }
        
        
        if let creditBill = userData["creditBill"] as? Dictionary<String, AnyObject> {
            
            for (billID, billDict) in creditBill {
                
                if billID != "key" {
                    
                    self.creditBill[billID] = Bill(billIdentifier: billID, billData: billDict as! Dictionary<String, AnyObject>)
 
                }
                
            }
            
        }
        
        
        if let debitBill = userData["debitBill"] as? Dictionary<String, AnyObject> {
            
            for (billID, billDict) in debitBill {
                
                if billID != "key" {
                    
                    self.debitBill[billID] = Bill(billIdentifier: billID, billData: billDict as! Dictionary<String, AnyObject>)
                    
                }
                
            }
            
        }
        
        if let totalCredit = userData["totalCredit"] as? Int {
            self.totalCredit = totalCredit
        }
        
        if let totalDebit = userData["totalDebit"] as? Int {
            self.totalDebit = totalDebit
        }
        
        
        if let portfolio = userData["portfolio"] as? Dictionary<String, AnyObject> {
            
            for (portID, portDict) in portfolio {
                
                if portID != "key" {
                    
                    self.portfolio[portID] = Portfolio(portKey: portID, dict: portDict as! Dictionary<String, AnyObject>)
//
                }
                
            }
            
        }
        
        
        if let posterReview = userData["posterReview"] as? Dictionary<String, AnyObject> {
            
            for (taskID, reviewDict) in posterReview {
                
                if taskID != "key" {

                    
                    self.posterReview[taskID] = Review(posterID: reviewDict["posterID"]! as! String,posterUsername: reviewDict["posterUsername"]! as! String, posterImageUrl: reviewDict["posterImageUrl"]! as! String, taskID: taskID as! String, reviewMsg: reviewDict["reviewMsg"]! as! String, time: reviewDict["time"]! as! NSNumber, numStar:reviewDict["numStar"]! as! Double )
                }
                
            }
            
        }
        
        if let taskerReview = userData["taskerReview"] as? Dictionary<String, AnyObject> {

            for (taskID, reviewDict) in taskerReview {

                if taskID != "key" {
                    
//                    print(reviewDict["posterImageUrl"]! as! String)
//                    print(reviewDict["posterUsername"]! as! String)
//                    print(reviewDict["taskTitle"]! as! String)
//                    print(reviewDict["reviewMsg"]! as! String)
//                    print(reviewDict["time"]! as! NSNumber)

//                    print(taskID)
//                    print(reviewDict["numStar"]! as! Double)
//                    print(reviewDict["posterUsername"]! as! String)
//                    print(reviewDict["posterImageUrl"]! as! String)
//                    print(reviewDict["time"]! as! NSNumber)
                    self.taskerReview[taskID] = Review(posterID: reviewDict["posterID"]! as! String, posterUsername: reviewDict["posterUsername"]! as! String, posterImageUrl: reviewDict["posterImageUrl"]! as! String, taskID: taskID as! String, reviewMsg: reviewDict["reviewMsg"]! as! String, time: reviewDict["time"]! as! NSNumber,numStar:reviewDict["numStar"]! as! Double)
                }

            }

        }

        
        
        
        
        if let stripeKey = userData["stripeKey"] as? String {
            self.stripeKey = stripeKey
        }
        
        if let connectKey = userData["connectKey"] as? String {
            self.connectKey = connectKey
        }
        
        
          _userRef = DataService.ds.REF_USERS.child(userKey)
    }
    
    func getBillArray(credit: Bool) -> [Bill] {
        if credit {
            var bills = [Bill]()
            for (_, bill) in self.creditBill {
                bills.append(bill)
            }
            return bills
        } else {
            var bills = [Bill]()
            for (_, bill) in self.debitBill {
                bills.append(bill)
            }
            return bills
        }
    }
    
    func getReviewArray(tasker: Bool) -> [Review] {
        
        if tasker {
            var reviews = [Review]()
            for (_, review) in self.taskerReview {
                reviews.append(review)
            }
            return reviews
        } else {
            var reviews = [Review]()
            for (_, review) in self.posterReview {
                reviews.append(review)
            }
            return reviews
        }
        
        
    }
    
    func getTotalReview() -> Int {
        return self.taskerNumRating + self.posterNumRating
    }
    
    func getTotalPortfolio() -> Int {
        return self.portfolio.count
    }
    
//    func updateDataToFirebase(userData: Dictionary<String, AnyObject>) {
//        self.userRef.updateChildValues(userData)
//    }
    
    func updateVerification(userData: Dictionary<String, AnyObject>) {
        self.userRef.updateChildValues(userData)
        
        if let profilePicUrl = userData["profilePicUrl"] as? String {
            self._profilePicUrl = profilePicUrl
        }
        
        if let dob = userData["dob"] as? String {
            self._dob = dob
        }
        
        if let languages = userData["languages"] as? [String: String] {
            self._languages = languages
        }

        if let cardID = userData["cardID"] as? String {
            self.cardID = cardID
//            self.bankCard = BankCard(no: card["cardNo"]!, date: card["cardExDate"]!, cvv: card["cardCVV"]!)
        }
        
        if let stripeKey = userData["stripeKey"] as? String {
            self.stripeKey = stripeKey
        }
        
        if let connectKey = userData["connectKey"] as? String {
            self.connectKey = connectKey
        }
//        if let cardNo = userData["cardNo"] as? String {
////            if let cardName = userData["cardName"] as? String {
//                if let cardExDate = userData["cardExDate"] as? String {
//                    if let cardCVV = userData["cardCVV"] as? String {
//                        self.bankCard = BankCard(no: cardNo, date: cardExDate, cvv: cardCVV)
//                    }
//                }
////            }
//        }
        
        self._isVerified = true
        
        
        
    }
    
    func removeBankAccount(completion:@escaping ()-> Void) {

        var updateObj = [String:AnyObject]()
        
        updateObj["bankAccount"] = NSNull() as AnyObject
        
        if self.bankAccount != nil {
            updateObj["bankAccountHistory/\((self.bankAccount?.bankID!)!)/isActive"] = false as AnyObject
            updateObj["bankAccountHistory/\((self.bankAccount?.bankID!)!)/deactivatedTime"] = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970)) as AnyObject
            
            updateObj["bankAccountHistory/\((self.bankAccount?.bankID!)!)/accNo"] = self.bankAccount?.accNo! as AnyObject
            updateObj["bankAccountHistory/\((self.bankAccount?.bankID!)!)/accName"] = self.bankAccount?.accName! as AnyObject
            updateObj["bankAccountHistory/\((self.bankAccount?.bankID!)!)/bankName"] = self.bankAccount?.bankName! as AnyObject
            updateObj["bankAccountHistory/\((self.bankAccount?.bankID!)!)/createdTime"] = self.bankAccount?.createdTime! as AnyObject
            
            //            updateObj["bankAccount"] = NSNull() as AnyObject
            
        }
        
        
        
        self.userRef.updateChildValues(updateObj) { (error, ref) in
            
            if error != nil {
                completion()
            } else {
                self.bankAccount = nil
                completion()
            }
        }
    }
    
    func removeCreditCard(completion:@escaping ()-> Void) {
        
        StripeUtil.stripeUtil.deleteCard(cardID: self.cardID!, stripeKey: self.stripeKey!) { (dict) in
            var updateObj = [String:AnyObject]()
            updateObj["cardID"] = NSNull() as AnyObject
            updateObj["bank"] = NSNull() as AnyObject
            updateObj["isVerified"] = false as AnyObject
            
            self.userRef.updateChildValues(updateObj) { (error, ref) in
                
                if error != nil {
                    completion()
                } else {
                    self.cardID = nil
                    self.bankCard = nil
                    self._isVerified = false
                    completion()
                }
            }
        }
 
    }
    
    func updateBankAccount(account: BankAccount, completion:@escaping ()-> Void) {
        
        var updateObj = [String:AnyObject]()
        
        
        //        updateObj["bankAccount/bankID"] = account.bankID as AnyObject
        updateObj["bankAccount/accName"] = account.accName as AnyObject
        updateObj["bankAccount/accNo"] = account.accNo as AnyObject
        updateObj["bankAccount/bankID"] = account.bankID as AnyObject
        updateObj["bankAccount/bankName"] = account.bankName as AnyObject
        updateObj["bankAccount/createdTime"] = account.createdTime as AnyObject
        updateObj["bankAccount/deactivatedTime"] = account.deactivatedTime as AnyObject
        updateObj["bankAccount/isActive"] = account.isActive as AnyObject
        
        
        
        
        if self.bankAccount != nil {
            updateObj["bankAccountHistory/\((self.bankAccount?.bankID!)!)/isActive"] = false as AnyObject
            updateObj["bankAccountHistory/\((self.bankAccount?.bankID!)!)/deactivatedTime"] = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970)) as AnyObject
            
            updateObj["bankAccountHistory/\((self.bankAccount?.bankID!)!)/accNo"] = self.bankAccount?.accNo! as AnyObject
            updateObj["bankAccountHistory/\((self.bankAccount?.bankID!)!)/accName"] = self.bankAccount?.accName! as AnyObject
            updateObj["bankAccountHistory/\((self.bankAccount?.bankID!)!)/bankName"] = self.bankAccount?.bankName! as AnyObject
            updateObj["bankAccountHistory/\((self.bankAccount?.bankID!)!)/createdTime"] = self.bankAccount?.createdTime! as AnyObject
            
//            updateObj["bankAccount"] = NSNull() as AnyObject
            
        }
        
        
        
        self.userRef.updateChildValues(updateObj) { (error, ref) in
            
            if error != nil {
                completion()
            } else {
                self.bankAccount = account
                completion()
            }
        }
    }
    
    func updateBank(cardID: String, card: BankCard, completion:@escaping ()-> Void) {
        
        var updateObj = [String:AnyObject]()
        updateObj["cardID"] = cardID as AnyObject
        updateObj["bank/cardExDate"] = card.cardExDate as AnyObject
        updateObj["bank/cardCVV"] = card.cardCVV as AnyObject
        updateObj["bank/cardNo"] = card.cardNo as AnyObject
        updateObj["bank/cardBrand"] = card.cardBrand as AnyObject
  
        self.userRef.updateChildValues(updateObj) { (error, ref) in
            
            if error != nil {
                completion()
            } else {
                self.cardID = cardID
                self.bankCard = card
                
                
                completion()
            }
        }
    }
    
//    func updateVerified() {
//
//        if self.profilePicUrl != NO_PIC && self.bankCard != nil && self.bankAccount != nil && self._dob != NO_DOB {
//            var updateObj = [String:AnyObject]()
//            updateObj["isVerified"] = true as AnyObject
//
//            self.userRef.updateChildValues(updateObj) { (error, ref) in
//
//                if error != nil {
//
//                } else {
//                    self._isVerified = true
//                }
//            }
//        } else {
//
//        }
//    }
    
    func updateReview(mode: Bool, msg: String, starNum: Double, taskID: String, posterID: String,posterUsername: String,posterImageUrl: String, completion:@escaping ()-> Void) {
        var updateObj = [String:AnyObject]()
        
        let time = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        let review = Review(posterID: posterID, posterUsername: posterUsername, posterImageUrl: posterImageUrl, taskID: taskID, reviewMsg: msg, time: time, numStar: starNum)
        
        
        if mode {
            
            
            updateObj["posterReview/\(taskID)/reviewMsg"] = msg as AnyObject
            updateObj["posterReview/\(taskID)/numStar"] = starNum as AnyObject
            updateObj["posterReview/\(taskID)/posterUsername"] = posterUsername as AnyObject
            updateObj["posterReview/\(taskID)/posterID"] = posterID as AnyObject
            updateObj["posterReview/\(taskID)/posterImageUrl"] = posterImageUrl as AnyObject
            updateObj["posterReview/\(taskID)/time"] = time as AnyObject
            
            updateObj["posterStarRating/"] = (posterStarRating + starNum) as AnyObject
            updateObj["posterNumRating/"] = (posterNumRating + 1) as AnyObject
            
        } else {

            
            
            updateObj["taskerReview/\(taskID)/reviewMsg"] = msg as AnyObject
            updateObj["taskerReview/\(taskID)/numStar"] = starNum as AnyObject
            updateObj["taskerReview/\(taskID)/posterUsername"] = posterUsername as AnyObject
            updateObj["taskerReview/\(taskID)/posterID"] = posterID as AnyObject
            updateObj["taskerReview/\(taskID)/posterImageUrl"] = posterImageUrl as AnyObject
            updateObj["taskerReview/\(taskID)/time"] = time as AnyObject
            
            updateObj["taskerStarRating/"] = (taskerStarRating + starNum) as AnyObject
            updateObj["taskerNumRating/"] = (taskerNumRating + 1) as AnyObject
        }
        
        self.userRef.updateChildValues(updateObj) { (error, ref) in
            
            if error != nil {
                
            } else {
                
                if mode {
                    self.posterReview[taskID] = review
                    completion()
                } else {
                    self.taskerReview[taskID] = review
                    completion()
                }
            }
        }
        
    }
    
    func updateReviewRates(reviewRate: String) {
        let ref2 = DataService.ds.REF_USERS.child(self.userKey!)
        
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
            
            if var userDict = currentData.value as? Dictionary<String, AnyObject> {
                
                var count = userDict["\(reviewRate)"] as? Int
                
                count = count! + 1
//                if addCount {
//                    msgCount?[fromId] = true
//                    //                    self.addUser(uid: uid)
//
//                } else {
//                    // userLike.removeValue(forKey: uid)
//                    msgCount?.removeValue(forKey: fromId)
//                    //                    self.removeUser(uid: uid)
//                }
                
                userDict["\(reviewRate)"] = count as AnyObject
                
                
                
                currentData.value = userDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in
            
        }
    }
    func adjustMsgCount(addCount: Bool,fromId: String) {
        
        let ref2 = DataService.ds.REF_USERS.child(self.userKey!)
        
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
            
            if var userDict = currentData.value as? Dictionary<String, AnyObject> {
                
                var msgCount = userDict["msgCount"] as? [String: Bool]
                
                if addCount {
                    msgCount?[fromId] = true
                    //                    self.addUser(uid: uid)
                    
                } else {
                    // userLike.removeValue(forKey: uid)
                    msgCount?.removeValue(forKey: fromId)
                    //                    self.removeUser(uid: uid)
                }
                
                userDict["msgCount"] = msgCount as AnyObject
                
                
                
                currentData.value = userDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in
            
        }
    }
    
    func adjustNotiCount(addCount: Bool, userID: String) {
        let ref2 = DataService.ds.REF_USERS.child(userID)
        
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
            
            if var userDict = currentData.value as? Dictionary<String, AnyObject> {
                
                var count = userDict["notiCount"] as? Int
                
                if addCount {
                    count = count! + 1
                } else {
                    if ( count! - 1 ) >= 0 {
                        
                        
                        count = count! - 1
                    }
                    
                }
                
                //                if addCount {
                //                    msgCount?[fromId] = true
                //                    //                    self.addUser(uid: uid)
                //
                //                } else {
                //                    // userLike.removeValue(forKey: uid)
                //                    msgCount?.removeValue(forKey: fromId)
                //                    //                    self.removeUser(uid: uid)
                //                }
                
                userDict["notiCount"] = count as AnyObject
                
                
                
                currentData.value = userDict
                
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in
            
        }
    }
    
    func updateProfile(userData: Dictionary<String, AnyObject>) {
//        self.userRef.updateChildValues(userData)
        self.userRef.updateChildValues(userData) { (error, ref) in
            if let name = userData["name"] as? String {
                self._name = name
            }
            
            if let mobileNumber = userData["mobileNumber"] as? Int {
                self._mobileNumber = mobileNumber
            }
            
            if let dob = userData["dob"] as? String {
                self._dob = dob
            }
            
            
            if let profilePicUrl = userData["profilePicUrl"] as? String {
                self._profilePicUrl = profilePicUrl
            }
            
            
            if let country = userData["country"] as? String {
                self.country = country
            }
            
            if let title = userData["title"] as? String {
                self.title = title
            }
            
            
            if let bio = userData["bio"] as? String {
                self.bio = bio
            }
            
            if let skills = userData["skills"] as? [String: Bool] {
                self.skills = skills
            }
        }
        

        
    }
    
    func createBill(time: Int, taskTitle: String, credit: Bool, payeeUserName: String, payerID: String, amount: Int) {
        
    }
    
    func uploadFeedback(feedback: [String:AnyObject], completion:@escaping(_ err: Error) -> Void) {
        DataService.ds.REF_FEEDBACK.updateChildValues(feedback) { (error, ref) in
//            if let err = error {
//                completion(err)
//            } else {
//                completion((error ?? nil)!)
//            }
            
        }
        
    }
    
    func clearMsgCount(completion:@escaping() -> Void) {
     
        
        
        var updateObj = [String:AnyObject]()
        updateObj["msgCount"] = ["key":true] as AnyObject

        
        self.userRef.updateChildValues(updateObj) { (error, ref) in
            completion()
        }
    }
    
    func clearNotiCount(completion:@escaping() -> Void) {
        
        
        
        var updateObj = [String:AnyObject]()
        updateObj["notiCount"] = 0 as AnyObject
        updateObj["lastNotificationTime"] = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970)) as AnyObject
        
        
        self.userRef.updateChildValues(updateObj) { (error, ref) in
            self.notiCount = 0
            self.lastNotificationTime = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
            completion()
            
        }
    }
    
    func updateNotificationPreference(add: Bool) {
        
        if add {
            if let location = DataService.ds.currentUserLocation {
                let geofireRef = Database.database().reference()
                let geoFire = GeoFire(firebaseRef: geofireRef.child("user-location"))
                
                geoFire.setLocation(location, taskName: "nil", taskBudget: 0, taskDeadline: 0, forKey: self.userKey!)
                self.currentLocation = Place(name: "", lat: location.coordinate.latitude, long: location.coordinate.longitude)
                self.userRef.child("currentLocation").updateChildValues(["latitude": currentLocation?.latitude!, "longitude": currentLocation?.longitude!])
            }

        } else {
            
            DataService.ds.REF_USER_LOCATION.updateChildValues([self.userKey!:NSNull()])
            self.userRef.updateChildValues(["currentLocation": NSNull()])
            self.currentLocation = nil
        }
        
    }

}

