//
//  BankAccount.swift
//  HeyWork
//
//  Created by XavierTanXY on 12/2/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//


import Foundation

class BankAccount {
    
    var bankID: String!
    var accNo: String!
    //    var cardName: String!
    var bankName: String!
    var accName: String!
    var isActive: Bool!
    var createdTime: NSNumber!
    var deactivatedTime: NSNumber!
    
    init(accNo: String, /*name: String,*/ bankName:String, accName: String, active: Bool, createdTime: NSNumber, deactivatedTime: NSNumber,bankID: String ) {
        self.accNo = accNo
        //        self.cardName = name
        self.bankName = bankName
        self.accName = accName
        self.isActive = active
        self.createdTime = createdTime
        self.deactivatedTime = deactivatedTime
        self.bankID = bankID
        
    }
    
    init(accNo: String, /*name: String,*/ bankName:String, accName: String, new:Bool) {
        self.accNo = accNo
        //        self.cardName = name
        self.bankName = bankName
        self.accName = accName
        self.isActive = true
        
        createdTime = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        deactivatedTime = 0
        
        self.bankID = "\(String(describing: createdTime!))_\(accName)"
    }
}
