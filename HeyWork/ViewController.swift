//
//  ViewController.swift
//  HeyWork
//
//  Created by XavierTanXY on 19/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import paper_onboarding

class ViewController: UIViewController, PaperOnboardingDelegate, PaperOnboardingDataSource {


    @IBOutlet weak var skipBtn: UIButton!
    var onboarding: PaperOnboarding!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        skipBtn.isHidden = true
        var needTutorial = UserDefaults.standard.value(forKey: "tutorial") as? Bool
        //        needTutorial = true

        if let t = needTutorial {

        } else {
            
            onboarding = PaperOnboarding(pageViewBottomConstant: 3)
            onboarding.delegate = self
            onboarding.dataSource = self
            onboarding.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(onboarding)
        
        
            // add constraints
            for attribute: NSLayoutConstraint.Attribute in [.left, .right, .top, .bottom] {
                let constraint = NSLayoutConstraint(item: onboarding,
                                                    attribute: attribute,
                                                    relatedBy: .equal,
                                                    toItem: view,
                                                    attribute: attribute,
                                                    multiplier: 1,
                                                    constant: 0)
                view.addConstraint(constraint)
                
               
            }

            view.bringSubviewToFront(skipBtn)
        

        }
    }
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        skipBtn.isHidden = index == 2 ? false : true
    }
    
    func onboardingItem(at index: Int) -> OnboardingItemInfo {
        let TITLE_COLOR = UIColor(red: 171/255, green: 177/255, blue: 196/255, alpha: 1.0)
        let DESC_COLOR = UIColor(red: 171/255, green: 177/255, blue: 196/255, alpha: 1.0)
        
        return [
            OnboardingItemInfo(informationImage: UIImage(named: "tutorial")!,
                               title: "POST ANY TASK",
                               description: "Need help in anything?\nE.g. Walk your dog, clean your house and even an admin.\nAsk for help from your community!",
                               pageIcon: UIImage(named: "one")!,
                               color: UIColor.white,
                               titleColor: TITLE_COLOR,
                               descriptionColor: DESC_COLOR,
                               titleFont: UIFont.systemFont(ofSize: 20, weight: .bold),
                               descriptionFont: UIFont.systemFont(ofSize: 15, weight: .regular)),
            
            OnboardingItemInfo(informationImage: UIImage(named: "tutorial2")!,
                               title: "OFFERS",
                               description: "Wait for someone nearby to make you an offer!",
                               pageIcon: UIImage(named: "two")!,
                               color: UIColor.white,
                               titleColor: TITLE_COLOR,
                               descriptionColor: DESC_COLOR,
                               titleFont: UIFont.systemFont(ofSize: 20, weight: .bold),
                               descriptionFont: UIFont.systemFont(ofSize: 15, weight: .regular)),
            
            OnboardingItemInfo(informationImage: UIImage(named: "tutorial3")!,
                               title: "DONE",
                               description: "Pay and get it done!",
                               pageIcon: UIImage(named: "three")!,
                               color: UIColor.white,
                               titleColor: TITLE_COLOR,
                               descriptionColor: DESC_COLOR,
                               titleFont: UIFont.systemFont(ofSize: 20, weight: .bold),
                               descriptionFont: UIFont.systemFont(ofSize: 15, weight: .regular))
            ][index]
    }
    
    func onboardingItemsCount() -> Int {
        return 3
    }
    
    func onboardingConfigurationItem(item: OnboardingContentViewItem, index: Int) {
        
        //    item.titleLabel?.backgroundColor = .redColor()
        //    item.descriptionLabel?.backgroundColor = .redColor()
        //    item.imageView = ...
    }
    @IBAction func skipTapped(_ sender: Any) {
        onboarding.removeFromSuperview()
        skipBtn.removeFromSuperview()
        UserDefaults.standard.setValue(false, forKey: "tutorial")
    }
    
}

