//
//  SummaryVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 9/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
//import SkyFloatingLabelTextField
import TagListView
import ActionSheetPicker_3_0
import GooglePlaces
import Gallery
import SVProgressHUD
import BLTNBoard

class SummaryCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(img: UIImage) {
        imgView.image = img
    }
    
}
class SummaryVC: UIViewController, UITextViewDelegate, UITextFieldDelegate,GMSAutocompleteViewControllerDelegate {

    var category: String!
    var budget: Int?
    var deadline: NSNumber?
    
    var place: GMSPlace?
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var charLbl: UILabel!
    @IBOutlet weak var tagView: TagListView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var peopleNeededLbl: UITextField!
    @IBOutlet weak var budgetLbl: UITextField!
    @IBOutlet weak var titleLbl: UITextField!
    @IBOutlet weak var deadlineLbl: UITextField!
    @IBOutlet weak var locationTF: UITextField!
    
    @IBOutlet weak var locationStackView: UIStackView!
    @IBOutlet weak var locationChoiceView: UIView!
    @IBOutlet weak var locationSwitch: UISwitch!
    
    @IBOutlet weak var collectionView: UICollectionView!
    var answerPlaceholder: String?
    
    var gallery: GalleryController!
    var taskImages = [UIImage]()
    
     var bulletinManager: BLTNItemManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        gallery = GalleryController()
        gallery.delegate = self
        Config.Camera.imageLimit = 6
        Config.tabsToShow = [.imageTab]
        
        peopleNeededLbl.delegate = self
        budgetLbl.delegate = self
        titleLbl.delegate = self
        deadlineLbl.delegate = self
    
        collectionView.delegate = self
        collectionView.dataSource = self
        
        textView.delegate = self
        scrollView.delegate = self
        
//        textView.layer.borderColor = UIColor.lightGray.cgColor
//        textView.layer.borderWidth = 1.0
        textView.layer.cornerRadius = 5.0
//        textView.textContainer.maximumNumberOfLines = 10
//        textView.textContainer.lineBreakMode = .byTruncatingTail
        
        
        
        

        
        self.navigationController?.isNavigationBarHidden = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
  
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Post", style: .plain, target: self, action: #selector(addTapped))
        // Do any additional setup after loading the view.
        
        self.hideKeyboardWhenTappedAround()
        
        addToolBar()
        
        if category == GRAPHIC_DESIGN || category == MEDIA || category == WRITING || category == TECH || category == WRITING {
            locationStackView.isHidden = true
            locationChoiceView.isHidden = true
        }
        
        if let ans = answerPlaceholder {
            textView.text = ans
            textView.textColor = UIColor.black
        } else {
            textView.text = "Describe your task..."
            textView.textColor = UIColor.lightGray
        }
        
        self.collectionView.isHidden = true
        locationSwitch.setOn(false, animated: false)
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.titleLbl.becomeFirstResponder()
        tagView.removeAllTags()
        if Checker.checker.skills.count != 0 {
            
            
            for(skill, tick) in Checker.checker.skills {
                
                tagView.addTag(skill)
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
//        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 950)
    }
    func addToolBar() {
        
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                               UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneTapped))]
        numberToolbar.sizeToFit()
        textView.inputAccessoryView = numberToolbar
        
        
    }
    
    @objc func doneTapped() {
        self.resignAll()
    }
    @objc func addTapped() {
        SVProgressHUD.show()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        
        if (titleLbl.text?.count)! < 10 {
            ErrorAlert.errorAlert.showAlert(title: "Error", msg: "Task title must be at least 10 characters", object: self)
        } else if budget! < 5 {
            ErrorAlert.errorAlert.showAlert(title: "Error", msg: "Budget must be at least SGD 5", object: self)
        } else if Int(peopleNeededLbl.text!)! == 0 || Int(peopleNeededLbl.text!)! > 10 || peopleNeededLbl.text == "" {
            ErrorAlert.errorAlert.showAlert(title: "Error", msg: "Minimum of 1 people and Maximum of 10 people", object: self)
        } else if textView.text.count < 30 {
            ErrorAlert.errorAlert.showAlert(title: "Error", msg: "Task description must be at least 30 characters", object: self)
        } else if( deadline == nil ) {
            ErrorAlert.errorAlert.showAlert(title: "Error", msg: "Please select a deadline", object: self)
        } else if( Checker.checker.skills.count == 0 ) {
            ErrorAlert.errorAlert.showAlert(title: "Error", msg: "Please select at least one skill", object: self)
        } else {
            let key = DataService.ds.REF_TASK.childByAutoId().key
            
            var skillDict = [String: Bool]()
            skillDict["key"] = true
            
            for (type, tick) in Checker.checker.skills {
                skillDict[type] = true
            }
            
            
            let task = Task(taskKey: key, title: titleLbl.text!, desc: textView.text!, deadline: self.deadline!, budget: budget!, peopleNeeded: Int(peopleNeededLbl.text!)!, category: self.category, poster: DataService.ds.currentUser.userName, posterKey: DataService.ds.currentUser.userKey!, skills: skillDict, place: self.place)
            //            Checker.checker.skills.removeAll()
            if self.taskImages.count != 0 {
                task.uploadAttachment(images: self.taskImages, completion: {
                    
//                    DataService.ds.showReview(vc: self, fromSetting: false)
                    self.navigationController?.popToRootViewController(animated: true)
                    
//                    if let review = DataService.ds.currentUser.reviewedApp {
//                        ErrorAlert.errorAlert.showAlert(title: "Posted!", msg: "Your task has been posted, you can sit back and wait for offers.", object: self)
//                    } else {
                    
//                    }
                    
                })
            } else {
                task.updateToFirebase(completion: {
//                    DataService.ds.showReview(vc: self, fromSetting: false)
                    self.navigationController?.popToRootViewController(animated: true)
                    
                    
//                    if let review = DataService.ds.currentUser.reviewedApp {
//                        ErrorAlert.errorAlert.showAlert(title: "Posted!", msg: "Your task has been posted, you can sit back and wait for offers.", object: self)
//                    } else {
//                        DataService.ds.showReview(vc: self)
//                    }
                    
                })
                
            }
            
            
            
            

//            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        
        print("will show")
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + tagView.frame.size.height
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }

    func resignAll() {
        deadlineLbl.resignFirstResponder()
        titleLbl.resignFirstResponder()
        textView.resignFirstResponder()
        budgetLbl.resignFirstResponder()
        peopleNeededLbl.resignFirstResponder()
    }
    
    @objc func enablePicker() {

        self.deadlineLbl.resignFirstResponder()
        
        let today = Date()
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
        let oneMonthLater = Calendar.current.date(byAdding: .day, value: 31, to: today)

        let page3 = DatePickerBulletinItem()
        
        page3.datePicker.minimumDate = tomorrow
        page3.datePicker.maximumDate = oneMonthLater
        
        page3.requiresCloseButton = true
        page3.appearance.actionButtonColor = MAIN_COLOR
        page3.isDismissable = true
        page3.image = UIImage(named: "deadline")
        
        page3.descriptionText = "Pick an end date for this task."
        page3.actionButtonTitle = "Confirm"
        
        page3.actionHandler = { (item: BLTNActionItem) in
            //            self.locationManager.requestWhenInUseAuthorization()
            let datePicked = page3.datePicker.date
            
            let dateFormatter = ISO8601DateFormatter()
            
            self.deadline = NSNumber(integerLiteral: Int(datePicked.timeIntervalSince1970))
            
            let a = "\(datePicked)"
            let fullNameArr = a.components(separatedBy: " ")
            let date = fullNameArr[0]
            
            let newArray = date.components(separatedBy: "-")
            self.deadlineLbl.text = "\(newArray[2])-\(newArray[1])-\(newArray[0])"
            
            
//            print(datePicked)
            print(self.deadline)
            
            item.manager?.dismissBulletin(animated: true)
        }
        
        bulletinManager = BLTNItemManager(rootItem: page3)
        bulletinManager.backgroundViewStyle = .blurredDark
        
        bulletinManager.showBulletin(above: self)
        
        
////
//        let today = Date()
//        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
//        let oneMonthLater = Calendar.current.date(byAdding: .day, value: 31, to: today)
//
//
//        let datePicker = ActionSheetDatePicker(title: "Deadline", datePickerMode: UIDatePicker.Mode.date, selectedDate: Calendar.current.date(byAdding: .day, value: 1, to: tomorrow!), doneBlock: {
//            picker, value, index in
//            self.deadlineLbl.resignFirstResponder()
//
//            print(value!)
//            let a = "\(value!)"
//            let fullNameArr = a.components(separatedBy: " ")
//            let date = fullNameArr[0]
//
//            let newArray = date.components(separatedBy: "-")
//            self.deadlineLbl.text = "\(newArray[2])-\(newArray[1])-\(newArray[0])"
//
//            var isoDate = "\(value!)"
//            isoDate.trimmingCharacters(in: .whitespaces)
//            let monthArray = isoDate.components(separatedBy: " ")
//
//            var newDate = "\(monthArray[0])T\(monthArray[1])\(monthArray[2])"
//            print(newDate)
//            let dateFormatter = ISO8601DateFormatter()
//            let wula = dateFormatter.date(from:newDate)!
//            print(wula.timeIntervalSince1970)
//            self.deadline = NSNumber(integerLiteral: Int(wula.timeIntervalSince1970))
//
//            return
//        }, cancel: { ActionStringCancelBlock in return
//            self.deadlineLbl.resignFirstResponder()
//        }, origin: self.view)
//
//
//        datePicker?.minimumDate = tomorrow
//
//        datePicker?.maximumDate = oneMonthLater
//
//        datePicker?.show()
    }
    
   
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == deadlineLbl {
            deadlineLbl.resignFirstResponder()
            print("wow")
            //            self.view.endEditing(true)
            
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(enablePicker), userInfo: nil, repeats: false)
            
            
        }
        

        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == budgetLbl {
            
            if budgetLbl.text!.count != 0 {
                budget = Int(budgetLbl.text!)
                budgetLbl.text = "SGD \(budgetLbl.text!) per tasker"
            } else {
                budget = 0
            }

        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if titleLbl.isFirstResponder {
            titleLbl.resignFirstResponder()
            deadlineLbl.becomeFirstResponder()
        } else if deadlineLbl.isFirstResponder {
            deadlineLbl.resignFirstResponder()
            budgetLbl.becomeFirstResponder()
        } else if budgetLbl.isFirstResponder {
            budgetLbl.resignFirstResponder()
            peopleNeededLbl.becomeFirstResponder()
        } else if peopleNeededLbl.isFirstResponder {
            peopleNeededLbl.resignFirstResponder()
            textView.becomeFirstResponder()
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        
        if textView.text.characters.count > 0 && !textView.text.trimmingCharacters(in: .whitespaces).isEmpty {
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            //            sendBtn.sendBtn.setBackgroundImage(#imageLiteral(resourceName: "postEnable"), for: .normal)
            //            sendBtn.sendBtn.isUserInteractionEnabled = true
        } else {
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 0.7)
            //            sendBtn.sendBtn.setBackgroundImage(#imageLiteral(resourceName: "postDisable"), for: .normal)
            //            sendBtn.sendBtn.isUserInteractionEnabled = false
        }
        
        
        if 30 - textView.text.characters.count < 0 {
            charLbl.isHidden = true
        } else {
            charLbl.isHidden = false
             charLbl.text = "Minimum \( 30 - textView.text.characters.count) characters"
        }
       
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            
            
            
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == budgetLbl {
            
            
            if let b = budget {

                budgetLbl.text = "\(b)"
            }
            
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Describe your task"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 150    // 10 Limit Value
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 80
    }
//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        print("aa")
//        view.endEditing(true)
//    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        view.endEditing(true)
//    }
    @IBAction func addSkillTapped(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "SkillVC") as! SkillVC
        self.present(newViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func locationTapped(_ sender: Any) {
        locationTF.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
//        present(acController, animated: true, completion: nil)
        // Specify the place data types to return.
//        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
//            UInt(GMSPlaceField.placeID.rawValue))!
//        acController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.country = "SG"
        acController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(acController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        locationTF.text = place.name
        self.place = place
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        ErrorAlert().showAlert(title: "Error", msg: error.localizedDescription, object: self)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    @IBAction func locationSwitchTapped(_ sender: Any) {
        if self.locationSwitch.isOn {
            self.locationStackView.isHidden = true
        } else {
            self.locationStackView.isHidden = false
        }
    }
    @IBAction func addAttachmentTapped(_ sender: Any) {
        present(gallery, animated: true, completion: nil)
    }
}

extension SummaryVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SummaryCell", for: indexPath) as! SummaryCell
        cell.configureCell(img: taskImages[indexPath.row])
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return taskImages.count
    }
}

extension SummaryVC: GalleryControllerDelegate {
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        var idx = 0
        self.resetImages()
        
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            print(resolvedImages.count)
            for img in resolvedImages {
                self?.taskImages.append(img!)
            }
            
            
            
            if (self?.taskImages.count)! > 0 {
                self?.collectionView.isHidden = false
            }
            self?.collectionView.reloadData()
            
            controller.dismiss(animated: true, completion: nil)
        })
        
        
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func resetImages() {
        self.taskImages = []
        self.collectionView.reloadData()
    }
}
