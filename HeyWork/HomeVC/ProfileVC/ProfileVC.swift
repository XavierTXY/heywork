
//
//  ProfileVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 3/11/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import Cosmos
import Firebase
import TagListView
import SVProgressHUD
import BetterSegmentedControl
import PopupDialog
//import SwipeTransition

protocol PassOfferProtocol
{
    func sendOfferToPreviousVC(offerID: String, task: Task)
}

class ProfileVC: BaseViewController {

    var userID: String!
    var user: User!
    var task: Task!
    var assignBar: AssignBar!
    var offer: Offer!
    
    var hasAssignBar: Bool!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var optionBtn: UIButton!
    
    // MARK: - PROFILE DETAILS
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!
//    @IBOutlet weak var onlineLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var starView: CosmosView!
    @IBOutlet weak var totalReviewLbl: UILabel!
    @IBOutlet weak var completionLbl: UILabel!
    
    
    @IBOutlet weak var bioLbl: UILabel!
    @IBOutlet weak var skillView: TagListView!
    
    @IBOutlet weak var languageLbl: UILabel!
    @IBOutlet weak var portLbl: UILabel!
    @IBOutlet weak var reviewLbl: UILabel!
    
    
    
    @IBOutlet weak var reviewsLbl: UILabel!
    
    @IBOutlet weak var assignTV: UITextView!
    @IBOutlet weak var assignHiddenBar: UIView!
    @IBOutlet weak var segControl: BetterSegmentedControl!
    var delegate: PassOfferProtocol?
    
    @IBOutlet weak var ratingStackView: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()

        segControl.segments = [LabelSegment(text: "Tasker"), LabelSegment(text: "Poster")]
        segControl.addTarget(self, action: #selector(valueChanged), for: .valueChanged)
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
        } else {
            // Fallback on earlier versions
        }
        

        if self.userID != DataService.ds.currentUser.userKey! {
            optionBtn.isHidden = false
        } else {
            optionBtn.isHidden = true
        }
    
//        let tapAction = UITapGestureRecognizer(target: self, action: #selector(tapReviews(_:)))
//        reviewsLbl?.isUserInteractionEnabled = true
//        reviewsLbl?.addGestureRecognizer(tapAction)
        
//
//        let navigationSegmentedControl = BetterSegmentedControl(
//            frame: CGRect(x: 35.0, y: 40.0, width: 200.0, height: 30.0),
//            segments: LabelSegment.segments(withTitles: ["Lights On", "Lights Off"],
//                                            normalFont: UIFont(name: "Avenir", size: 13.0)!,
//                                            normalTextColor: .lightGray,
//                                            selectedFont: UIFont(name: "Avenir", size: 13.0)!,
//                                            selectedTextColor: .white),
//            options:[.backgroundColor(.darkGray),
//                     .indicatorViewBackgroundColor(UIColor(red:0.55, green:0.26, blue:0.86, alpha:1.00)),
//                     .cornerRadius(3.0),
//                     .bouncesOnChange(false)])
//        navigationSegmentedControl.addTarget(self, action: #selector(navigationSegmentedControlValueChanged(_:)), for: .valueChanged)
//        navigationItem.titleView = navigationSegmentedControl
        
//        segmentedControl.segments = LabelSegment.segments(withTitles: ["Tasker", "Poster"],
//                                                  normalFont: UIFont(name: "HelveticaNeue-Light", size: 13.0)!,
//                                                  selectedFont: UIFont(name: "HelveticaNeue-Medium", size: 13.0)!)
//        segmentedControl.addTarget(self, action: #selector(navigationSegmentedControlValueChanged(_:)), for: .valueChanged)
        
        getUserDetails()
//        self.swipeToDismiss?.isEnabled = true
//        addAccessoryView()
    }
    
//    override func viewWillLayoutSubviews() {
//        super.viewWillLayoutSubviews()
//        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 1000)
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if hasAssignBar {
            addAccessoryView()
            changeBtnColor()
        }
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func valueChanged() {
        self.ratingStackView.alpha = 0.0
        
        
        if segControl.index == 0 {
            starView.rating = user.taskerStarRating
            totalReviewLbl.text = "(\(user.taskerNumRating!))"
            //            completionLbl.text = "(\(user.taskerCompletionRate!))"
            if user.taskerCompletionRate == 0.0 {
                completionLbl.text = "No Completion Rate Yet"
            } else {
                completionLbl.text = "\(user.taskerCompletionRate!) % Completion Rate"
            }
        } else if segControl.index == 1 {
            starView.rating = user.posterStarRating
            totalReviewLbl.text = "(\(user.posterNumRating!))"
            //            completionLbl.text = "(\(user.posterCompletionRate!))"
            
            if user.posterCompletionRate == 0.0 {
                completionLbl.text = "No Completion Rate Yet"
            } else {
                completionLbl.text = "\(user.posterCompletionRate!) % Completion Rate"
            }
        }
        
        UIView.animate(withDuration: 0.1, delay: 0.1, options: [], animations: { self.ratingStackView.alpha = 1.0 }, completion: { (bool) in
            
        })
        
//        self.tableView.reloadData()
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        self.swipeToDismiss?.isEnabled = false
    }
    
    @IBAction func indexChanged(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            starView.rating = user.taskerStarRating
            totalReviewLbl.text = "(\(user.taskerNumRating!))"
//            completionLbl.text = "(\(user.taskerCompletionRate!))"
            if user.taskerCompletionRate == 0.0 {
                completionLbl.text = "No Completion Rate Yet"
            } else {
                completionLbl.text = "\(user.taskerCompletionRate!) % Completion Rate"
            }
            
        case 1:
            starView.rating = user.posterStarRating
            totalReviewLbl.text = "(\(user.posterNumRating!))"
//            completionLbl.text = "(\(user.posterCompletionRate!))"
            
            if user.posterCompletionRate == 0.0 {
                completionLbl.text = "No Completion Rate Yet"
            } else {
                completionLbl.text = "\(user.posterCompletionRate!) % Completion Rate"
            }
        default:
            break
        }
    }
//    // MARK: - Action handlers
//    @objc func navigationSegmentedControlValueChanged(_ sender: BetterSegmentedControl) {
//        if sender.index == 0 {
//            starView.rating = user.taskerStarRating
//            totalReviewLbl.text = "(\(user.taskerNumRating!))"
//            completionLbl.text = "(\(user.taskerCompletionRate!))"
//        } else if sender.index == 1 {
//            starView.rating = user.posterStarRating
//            totalReviewLbl.text = "(\(user.posterNumRating!))"
//            completionLbl.text = "(\(user.posterCompletionRate!))"
//        }
//    }
//
//    @IBAction func segmentedControl1ValueChanged(_ sender: BetterSegmentedControl) {
//        print("The selected index is \(sender.index)")
//    }
    
    @IBAction func optionTapped(_ sender: Any) {
        
        super.showAlert(taskID: nil, userID: self.userID)
    }
    

    
    @IBAction func dismissTapped(_ sender: Any) {
//        if let presenter = presentingViewController as? OffersVC {
//            print("woww")
//            presenter.task = task
//            presenter.offerDict.removeValue(forKey: self.offer.offerID)
//            presenter.attemptToReloadTable()
//
//        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func getUserDetails() {
        DataService.ds.REF_USERS.child(userID).observeSingleEvent(of: .value, with: { (snapshot) in

            
            
//            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
//                for snap in snapshots {
            
                    //print("SNAP: \(snap)")
                    if let dict = snapshot.value as? Dictionary<String, AnyObject> {
                        
                        
                        
                        let key = snapshot.key
                        
                        self.user = User(userKey: key, userData: dict)
                        self.setupDetails()
                        
                        
//                        self.offerDict[key] = offer
                        
                        
                        
                        
                        
//                    }
//                }
                
            }
            
            
            //            self.actInd.stopAnimating()
            
        })
    }
    @IBOutlet weak var profileView: UIView!
    
    func setupDetails() {
        profilePic.layer.cornerRadius = profilePic.frame.size.width / 2
        profileView.layer.cornerRadius = profileView.frame.size.width / 2
        
        profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: self.user.profilePicUrl)
        
        profilePic.clipsToBounds = true
        usernameLbl.text = self.user.userName
//        stateLbl.text = user.country
//        titleLbl.text = user.title
        
//        starView.rating = user.
        
        bioLbl.text = user.bio
        skillView.alignment = .center
        
        for (skill, bo) in user.skills {
            if skill != "key" {
                skillView.addTag(skill)
            }
            
        }

        reviewLbl.text = "Reviews (\(user.getTotalReview()))"
        portLbl.text = "Portfolios (\(user.getTotalPortfolio()))"
//    
//        languageLbl.text = ""
//        var idx = 0
//        for (lan, bo) in user.languages {
//            if idx == user.languages.count - 1 {
//                languageLbl.text = "\(languageLbl.text!)\(lan)  -   \(bo)"
//            } else {
//                languageLbl.text = "\(languageLbl.text!)\(lan)  -   \(bo)\n"
//            }
//
//        }
        
        starView.rating = user.taskerStarRating
        totalReviewLbl.text = "(\(user.taskerNumRating!))"
        
        if user.taskerCompletionRate == 0.0 {
            completionLbl.text = "No Completion Rate Yet"
        } else {
            completionLbl.text = "\(user.taskerCompletionRate!) % Completion Rate"
        }
        
    }

    func assignTapped() {
        print("assign tapped")
        
        if DataService.ds.currentUser.bankCard != nil {
            if self.task.assigned[self.userID] != nil {
//                let alert = UIAlertController(title: "You've assigned this user.", message: "This user is working on it.", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "Got it.", style: .cancel, handler: nil))
                ErrorAlert.errorAlert.showAlert(title: "You've assigned this user.", msg: "This user is working on it.", object: self)
//                alert.addAction(UIAlertAction(title: "Assign", style: .default) { (action) in
//                    self.task.updateAssign(offer: self.offer, taskerID: self.userID, add: true, offerDict: self.offer.getDict(),offerKey: self.offer.offerID, completion: {
//                        self.offer.offerStatus = WORKING
//                        self.changeBtnColor()
//
//                        self.sendAssign(values: ["text": "Hey there, I have assigned you to this task!"], userID: self.offer.offerUserID, offer: self.offer)
//                        self.delegate?.sendOfferToPreviousVC(offerID: self.offer.offerID, task: self.task)
//                    })
//
//                })
                
//                self.present(alert, animated: true, completion: nil)
            } else {
                
                // Create the dialog
                let popup = PopupDialog(title: "Assign Task", message: "Are you sure to assign this task?")
                popup.buttonAlignment = .horizontal
                // Create buttons
                let buttonOne = CancelButton(title: "Cancel") {
                    print("You canceled the car dialog.")
                }
                
                // This button will not the dismiss the dialog
                let buttonTwo = DefaultButton(title: "Assign", dismissOnTap: true) {
                    SVProgressHUD.show()
                    
                    if self.task.isFull() {
                        ErrorAlert.errorAlert.showAlert(title: "Task unavailable", msg: "Your task is full!", object: self)
                    } else {
                        StripeUtil.stripeUtil.createCharge(amount: self.offer.offerPrice, desc: self.task.title, taskID: self.task.taskKey, payeeID: self.userID, payeeUsername: self.offer.offerUserName, payeeProfilePicUrl: self.offer.offerUserProfilePicUrl, payerStripeID: DataService.ds.currentUser.stripeKey!, completion: { (success) in
                            self.task.updateAssign(offer: self.offer, taskerID: self.userID, add: true, offerDict: self.offer.getDict(),offerKey: self.offer.offerID, completion: {
                                
                                
                                //Update offer user count in total assigned
                                DataService.ds.REF_USERS.child(self.offer.offerUserID).observeSingleEvent(of: .value, with: { (snapshot) in
                                    
                                    if ( snapshot.value is NSNull ) {
                                    } else {
                                        
                                        if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                                            let key = snapshot.key
                                            
                                            let offerUser = User(userKey: key, userData: userDict)
                                            offerUser.updateReviewRates(reviewRate: "taskerTotalTaskAssigned")
                                            self.offer.offerStatus = WORKING
                                            self.changeBtnColor()
                                            
                                            self.sendAssign(values: ["text": "Hey there, I have assigned you to this task!"], userID: self.offer.offerUserID, offer: self.offer)
                                            self.delegate?.sendOfferToPreviousVC(offerID: self.offer.offerID, task: self.task)
                                            
                                        }
                                    }
                                })
                                
                            })
                            
                        })
                    }
                }
                

                
                // Add buttons to dialog
                // Alternatively, you can use popup.addButton(buttonOne)
                // to add a single button
                popup.addButtons([buttonOne, buttonTwo])
                
                // Present dialog
                self.present(popup, animated: true, completion: nil)
                
//
//                let alert = UIAlertController(title: "Assign Task", message: "Are you sure to assign this task?", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//                alert.addAction(UIAlertAction(title: "Assign", style: .default) { (action) in
//
//
//
//
//                })
//
//                self.present(alert, animated: true, completion: nil)
            }

        } else {
            Checker.checker.verificationMode = true
            VCSegue().segueVC(bundleName: "VerificationSB", controllerName: "VerificationVC", vc: self)
        }
        
        

    }
    
    func sendAssign(values: [String: Any], userID: String, offer: Offer)  {
        
        

        
        let ref = DataService.ds.REF_MESSAGES
        let firebaseMessage = DataService.ds.REF_MESSAGES.childByAutoId()
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        let toId = userID
        let fromId = DataService.ds.currentUser.userKey!
        
        print("FROM ID: \(fromId)")

        user?.adjustMsgCount(addCount: true, fromId: "\(self.task.taskKey!)_\(DataService.ds.currentUser.userKey!)")



        var dict: [String: Any] = ["toId": toId, "fromId": fromId, "timeStamp": timestamp, "fromUserName": DataService.ds.currentUser.userName, "fromSeen": true, "toSeen": false ] as [String : Any]




        for (key, element) in values {

            dict[key] = element
            print(dict)
        }
        // values.forEach({dict[$0] = $1 })
        //      firebaseMessage.updateChildValues(values)

        firebaseMessage.updateChildValues(dict, withCompletionBlock: {(error, ref) in

            if error != nil {
                print(error)
            }

            //            self.inputContainerView.inputTextField.text = nil

            let userMsgRef = DataService.ds.REF_USER_MESSAGES.child(fromId).child(self.task.taskKey).child(toId)
            let msgId = firebaseMessage.key
            userMsgRef.updateChildValues([msgId: true])

            let recipientUserMsgRef = DataService.ds.REF_USER_MESSAGES.child(toId).child(self.task.taskKey).child(fromId)
            recipientUserMsgRef.updateChildValues([msgId: true])


        })
//
        
    }
    
    
    func changeBtnColor() {
        
//        assignBar.titleLbl.text = "Current Offer: \(task.offered.count - 1)\nCurrently Assigned: \(task.assigned.count - 1)"
        
        if task.assigned[self.userID] == nil {
            assignBar.assignView.backgroundColor = MAIN_COLOR
            assignBar.assignBtn.setTitle("Assign Task", for: .normal)
        } else {
            assignBar.assignView.backgroundColor = OPEN_COLOR
            assignBar.assignBtn.setTitle("Assigned", for: .normal)
        }
        
        SVProgressHUD.dismiss()
    }
    
    public func addAccessoryView() {
        
        assignTV.isHidden = true
//        assignHiddenBar.isHidden = true
        
        assignBar = Bundle.main.loadNibNamed("AssignBar", owner: self, options: nil)?.first as! AssignBar
        assignBar.profileVC = self
        self.view.addSubview(assignBar)
        
        
        assignBar.translatesAutoresizingMaskIntoConstraints = false
        
        assignBar.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        assignBar.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        assignBar.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        assignBar.heightAnchor.constraint(equalToConstant: 70).isActive = true
        assignBar.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
//        assignTV.becomeFirstResponder()
        
//        var contentInset:UIEdgeInsets = self.scrollView.contentInset
//        contentInset.bottom = tagView.frame.size.height + 70
//        scrollView.contentInset = contentInset
        
        
        
        
   
        
    }
    @objc func tapReviews(_ sender: Any) {
     print("tapp profile")
        let storyBoard: UIStoryboard = UIStoryboard(name: "SettingSB", bundle: nil)
        let reviewsVC = storyBoard.instantiateViewController(withIdentifier: "ReviewVC") as! ReviewVC
        self.navigationController?.pushViewController(reviewsVC, animated: true)
       
    }
    @IBAction func reviewsTapped(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "SettingSB", bundle: nil)
        let reviewsVC = storyBoard.instantiateViewController(withIdentifier: "ReviewVC") as! ReviewVC
        reviewsVC.user = self.user
      
        self.present(reviewsVC, animated: true, completion: nil)
    }
    
    @IBAction func portfolioTapped(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "SettingSB", bundle: nil)
        let portVC = storyBoard.instantiateViewController(withIdentifier: "PortfolioVC") as! PortfolioVC
        portVC.user = self.user
        
        self.present(portVC, animated:true, completion: nil)
    }
}
