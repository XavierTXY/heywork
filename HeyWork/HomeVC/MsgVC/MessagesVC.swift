//
//  MessagesVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 12/11/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import Firebase
//import PopupDialog
import SVProgressHUD


class MessageCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var notiImg: UIView!
    
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    

    @IBOutlet weak var profilePic: UIImageView!
    
    var msg: MessageModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profilePic.layer.cornerRadius = profilePic.frame.height / 2
        profilePic.clipsToBounds = true
//        outerView.layer.cornerRadius = profilePic.frame.height / 2
//        outerView.clipsToBounds = true
        notiImg.layer.cornerRadius = notiImg.frame.height / 2
        notiImg.clipsToBounds = true
    }
    
    
    func configureCell(message: MessageModel, toName: String, url: String) {
        
        self.msg = message
        nameLabel.text = toName
        
        if message.fromId == (Auth.auth().currentUser?.uid)! {
            notiImg.isHidden = true
        } else {
            if message.toSeen! {
                notiImg.isHidden = true
            } else {
                notiImg.isHidden = false
            }
        }
        
        
        if url != NO_PIC {
//            hideInitial()
            profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: url)
        } else {
            
//            showInitial()
            
            
        }
        
        
        
        if message.imageUrl != nil {
            messageLbl.text = "Sent an image"
        } else if message.docUrl != nil {
            messageLbl.text = "Sent a file"
        } else {
            messageLbl.text = message.text
        }
        
        
        
        
        
        if let seconds = message.timeStamp?.doubleValue {
            let timesStampDate = NSDate(timeIntervalSince1970: seconds)
            var dateFormatter = DateFormatter()
            
            
            let calendar = Calendar.current
            
            if calendar.isDateInToday(timesStampDate as Date) {
                dateFormatter.dateFormat = "hh:mm"
            } else {
                dateFormatter.dateFormat = "d MMM"
            }
            
            timeLabel.text = dateFormatter.string(from: timesStampDate as Date)
        }
        
        
    }
    

    
    
    
}

class MessagesVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
 
//    @IBOutlet weak var emptyMessageLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    //    private let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]
    var messages = [MessageModel]()
    var messagesDictionary = [String: MessageModel]()
    var taskKeyDict = [String: TaskTime]()
    var tasks = [TaskTime]()
    var blockDict = [String: Bool]()
    
    var users = [User]()
    var selectedIdx = 0
    var timer: Timer?
    var firstTime = true
    var been: Bool!
    var isListening: Bool!
    
    var emptyView: EmptyView!
    //    var emptyView: EmptyConfessionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        been = false
        isListening = false
        tableView.delegate = self
        tableView.dataSource = self
        
        
        //        navigationItem.rightBarButtonItem?.tintColor = PURPLE
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action: "")
        
        //        fetchBlockList()
        
        
        tableView.allowsMultipleSelectionDuringEditing = true
        
        
        tableView.tableFooterView = UIView()
        self.navigationItem.title = "Inbox"
        
        //        self.navigationController?.navigationBar.barTintColor = UIColor.white
        //        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        //        self.navigationController?.navigationBar.layer.borderColor = BORDER_COLOR_CGCOLOR
        
        //        self.navigationController?.navigationBar.tintColor = PURPLE
        //        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor: PURPLE]
        
        
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(_:)))
        longPressGesture.minimumPressDuration = 0.5
        self.tableView.addGestureRecognizer(longPressGesture)
        
        emptyView = EmptyView.instanceFromNib(image: UIImage(named: "noNoti")!, title: "Hmm...", desc: "It is very quiet here.") as! EmptyView
        emptyView.frame = self.tableView.bounds
        self.tableView.addSubview(emptyView)
        emptyView.showView(show: false, tableView: self.tableView)
//        emptyMessageLbl.isHidden = false
//        self.hideEmptyView()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.statusBarStyle = .default
        //        self.tabBarController?.tabBar.isHidden = false
        
//        clearDisplayBadge()
        self.clearBadge(index: 3)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if !been {
            if DataService.ds.login == false {
                print("wulal")
                been = true
                self.performSegue(withIdentifier: "NumberVC", sender: nil)
            }
        }
        
        if !DataService.ds.login {
            
            self.performSegue(withIdentifier: "NumberVC", sender: nil)
            tabBarController?.selectedIndex = 0
        } else {
            if !isListening {
                observeUserMessages()
            }
            
        }
        
        
    }
//
//    func showEmptyView() {
//        emptyMessageLbl.isHidden = false
//        //        self.tableView.bringSubview(toFront: emptyView)
//
//    }
//
//    func hideEmptyView() {
//        emptyMessageLbl.isHidden = true
//        //        self.tableView.sendSubview(toBack: emptyView)
//
//    }
    
    
    @objc func handleLongPress(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        
        if longPressGestureRecognizer.state == UIGestureRecognizer.State.began {
            
            let touchPoint = longPressGestureRecognizer.location(in: self.tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint) {
                //                self.optionTapped(index: indexPath.row)
                
            }
        }
    }
    
    func optionTapped(index: Int) {
        
        
        let alert = UIAlertController()
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler:{ (UIAlertAction)in
            
        }))
        
        var partnerID = messages[index].chatPartnerId()
        if self.blockDict[partnerID!] != nil {
            alert.addAction(UIAlertAction(title: "Unblock", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
                self.displayUnblockAlert(index: index)
            }))
        } else {
            alert.addAction(UIAlertAction(title: "Block", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
                self.displayBlockAlert(index: index)
            }))
            
        }
        
        alert.addAction(UIAlertAction(title: "Report", style: .destructive , handler:{ (UIAlertAction)in
            self.displayReportAlert(index: index)
        }))
        
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
            self.showDeleteOption(index: index)
        }))
        
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayUnblockAlert(index: Int) {
        
        // add back in later time
//        let popup = PopupDialog(title: "Warning", message: "Do you want to unblock?", image: nil)
//
//        var partnerID = messages[index].chatPartnerId()
//        let buttonOne = DefaultButton(title: "Unblock") {
//            var updateObj = [String:AnyObject]()
//            let ref = DB_BASE
//            updateObj["/userBlock-partnerID/\((Auth.auth().currentUser?.uid)!)/\(partnerID!)/blocked"] = NSNull()
//            ref.updateChildValues(updateObj)
//
//            SVProgressHUD.setMinimumDismissTimeInterval(0.5)
//            SVProgressHUD.showSuccess(withStatus: "Unblocked!")
//
//        }
//
//        let buttonTwo = CancelButton(title: "Cancel") {
//
//        }
//
//        popup.addButtons([buttonTwo,buttonOne])
//        popup.buttonAlignment = .horizontal
//
//        // Present dialog
//        self.present(popup, animated: true, completion: nil)
    }
    
    func displayBlockAlert(index: Int) {
//        let popup = PopupDialog(title: "Warning", message: "Are you sure you want to block?", image: nil)
//
//        var partnerID = messages[index].chatPartnerId()
//        let buttonOne = DestructiveButton(title: "Block") {
//            var updateObj = [String:AnyObject]()
//            let ref = DB_BASE
//            updateObj["/userBlock-partnerID/\((Auth.auth().currentUser?.uid)!)/\(partnerID!)/blocked"] = true as AnyObject
//            ref.updateChildValues(updateObj)
//
//            SVProgressHUD.setMinimumDismissTimeInterval(0.5)
//            SVProgressHUD.showSuccess(withStatus: "Blocked!")
//
//        }
//
//        let buttonTwo = CancelButton(title: "Cancel") {
//
//        }
//
//        popup.addButtons([buttonTwo,buttonOne])
//        popup.buttonAlignment = .horizontal
//
//        // Present dialog
//        self.present(popup, animated: true, completion: nil)
        
        
    }
    
    func displayReportAlert(index: Int) {
        
        let alert = UIAlertController()
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler:{ (UIAlertAction)in
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Spam", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            //            self.submitReport(index: index, reason: "Spam")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Inappropriate", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            //            self.submitReport(index: index,reason: "Inappropriate")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Bullying", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            //            self.submitReport(index: index,reason: "Bullying")
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    //    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    //        return true
    //    }
    //
    //    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    //        print("here")
    //        showDeleteOption(index: indexPath.row)
    //
    //    }
    
    func showDeleteOption(index: Int) {
//        let popup = PopupDialog(title: "Warning", message: "Are you sure you want to delete?", image: nil)
//
//        let buttonOne = DestructiveButton(title: "Delete") {
//            if let currentUserUid = Auth.auth().currentUser?.uid {
//
//                let msg = self.messages[index]
//                if let chatPartnerId = msg.chatPartnerId() {
//                    DataService.ds.REF_USER_MESSAGES.child(currentUserUid).child(chatPartnerId).removeValue(completionBlock: { (error, ref) in
//
//                        if error != nil {
//                            print("failed to delete messg", error)
//                            return
//                        }
//                        self.messagesDictionary.removeValue(forKey: chatPartnerId)
//
//                        self.attemptToReloadTable()
//
//                    })
//                }
//
//            }
//        }
//
//        let buttonTwo = CancelButton(title: "Cancel") {
//
//        }
//
//        popup.addButtons([buttonTwo,buttonOne])
//        popup.buttonAlignment = .horizontal
//
//        // Present dialog
//        self.present(popup, animated: true, completion: nil)
        
    }
    
    func observeUserMessages() {
        
        isListening = true
        
        let uid = DataService.ds.currentUser.userKey!
        print("haha\(uid)")
        let ref = DataService.ds.REF_USER_MESSAGES.child(uid)
        
        
        DataService.ds.msgObserverListener = ref.observe(.childAdded, with: {(snapshot) in
            
            let taskID = snapshot.key
            let taskRef = DataService.ds.REF_USER_MESSAGES.child(uid).child(taskID)
            print("TASK ID: \(taskID)")
            
             DataService.ds.msgTaskObserverListener = taskRef.observe(.childAdded, with: {(snapshot) in
                
                let userId = snapshot.key
                let userRef = DataService.ds.REF_USER_MESSAGES.child(uid).child(taskID).child(userId)
                print("USER ID: \(userId)")
                userRef.observe(.childAdded, with: { (snapshot) in
                    // print(snapshot)
                    let msgId = snapshot.key
                    
                    self.fetchMessageWithMessageId(msgId: msgId, taskID: taskID)
                    
                })
                
                
                
            })
            
            
            
        })
        
//        ref.observe(.childRemoved, with: { (snapshot) in
//            
//            print("here333")
//            self.messagesDictionary.removeValue(forKey: snapshot.key)
//       
//            //                    print(self.messagesDictionary)
//            self.attemptToReloadTable()
//            
//            
//            
//            
//        })
        
        
        
        self.attemptToReloadTable()
    }
    
    private func fetchMessageWithMessageId(msgId: String, taskID: String) {
        
        let msgRef = DataService.ds.REF_MESSAGES.child(msgId)
        
        msgRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: Any] {
                
                let message = MessageModel(key: snapshot.key, dictionary: dictionary)
                
                //self.messages.append(message)
                
                if let chatPartnerId = message.chatPartnerId() {
                    self.messagesDictionary[taskID] = message
                    self.taskKeyDict[taskID] = TaskTime(key: taskID, time: message.timeStamp!)
//                                         self.messages.append(message)
                    
                    print("TASK ID in msg \(taskID)")
                    
                }
                
                self.attemptToReloadTable()
                
                
                
            }
            
        })
    }
    
    func attemptToReloadTable() {
        self.timer?.invalidate()
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.reloadTable), userInfo: nil, repeats: false)
        //        reloadTable()
    }
    
    @objc func reloadTable() {
        
        self.messages = Array(self.messagesDictionary.values)
        self.tasks = Array(self.taskKeyDict.values)

        self.tasks.sort(by: { (m1, m2) -> Bool in

            return (m1.timeStamp?.intValue)! > (m2.timeStamp?.intValue)!
        })
        
        self.messages.sort(by: { (m1, m2) -> Bool in
            
            return (m1.timeStamp?.intValue)! > (m2.timeStamp?.intValue)!
        })
        self.tableView.reloadData()
        
        
        //        if firstTime {
        ////            UIView.animate(views: self.tableView.visibleCells, animations: animations, completion: {
        //            })
        //
        //        }
        
        firstTime = false
        
        if self.messages.count == 0 {
            emptyView.showView(show: true, tableView: self.tableView)
        } else {
            emptyView.showView(show: false, tableView: self.tableView)
        }
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ChatLogVC {
            destinationVC.assignedUser = self.selectedUser
            
            
        }
        
        
    }
    var chatLogVC: ChatLogVC!
    var selectedUser: User!
    
    func showChatControllerForUser(user: User) {
        //        chatLogVC = ChatLogVC(collectionViewLayout: UICollectionViewFlowLayout())
        //        chatLogVC.user = user
        selectedUser = user
        goToChat()
        //        self.present(chatLogVC, animated: true, completion: nil)
        //        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(goToChat), userInfo: nil, repeats: false)
        
    }
    
    func goToChat() {
        //        self.navigationController?.pushViewController(self.chatLogVC, animated: true)
//        performSegue(withIdentifier: "ChatLogVC", sender: nil)
        let chatVC = UIStoryboard(name: "MessageSB", bundle: nil).instantiateViewController(withIdentifier: "ChatLogVC") as! ChatLogVC
        chatVC.userID = self.selectedUser.userKey!
        chatVC.userName = self.selectedUser.userName
//        chatVC.offer = self.assigns[indexPath.row]
        chatVC.taskID = self.tasks[self.selectedIdx].taskID!
        self.navigationController?.pushViewController(chatVC, animated: true)
//        self.present(chatVC, animated: true, completion: nil)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chatPartnerId: String?
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as? MessageCell
        cell?.selectionStyle = .none
        let msg = messages[indexPath.row]
        
        
        
        if let id = msg.chatPartnerId() {
            DataService.ds.REF_USERS.child(id).observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let dict = snapshot.value as? Dictionary<String, AnyObject> {
                    let partner = User(userKey: id, userData: dict)
                    //                    let toName = dictionary["name"] as? String
                    print("TO NAME \(partner.userName) + \(id)")
                    cell?.configureCell(message: msg, toName: partner.userName, url: partner.profilePicUrl)
                }
            })
        }
        
        
        return cell!
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let msg = messages[indexPath.row]
        msg.toSeen = true
        selectedIdx = indexPath.row
        self.tableView.reloadData()
        let partnerId = msg.chatPartnerId()
        let ref = DataService.ds.REF_USERS.child(partnerId!)
        
//        clearDisplayBadge()
        self.clearBadge(index: 3)
        DataService.ds.currentUser.adjustMsgCount(addCount: false, fromId: partnerId!)
        
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            //            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
            //                for snap in snapshots.reversed() {
            //                    //print("SNAP: \(snap)")
            //                    if let userDict = snap.value as? Dictionary<String, AnyObject> {
            //                        let key = snap.key
            //                        let user = User(userKey: key, userData: userDict)
            //                        self.showChatControllerForUser(user: user)
            //
            //                    }
            //                }
            //
            //            }
            
            
            if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                
                let user = User(userKey: partnerId!, userData: userDict)
                //                user.userKey = partnerId
                //                user.setValuesForKeys(userDict)
                print(user.userKey)
                self.showChatControllerForUser(user: user)
                //print(userDict)
                // print(userDict["email"])
                //self.users.append(user)
                
            }
            
            
        })
    }
    
//    func clearDisplayBadge() {
//
//
//        if let tabItems = self.tabBarController?.tabBar.items as NSArray!
//        {
//            // In this case we want to modify the badge number of the third tab:
//            let tabItem = tabItems[3] as! UITabBarItem
//            if tabItem.badgeValue?.count != 0 {
//
//                if( tabItem.badgeValue != nil) {
//
//                    DataService.ds.currentUser.clearMsgCount {
//                        tabItem.badgeValue = nil
//                    }
////                    if ((tabItem.badgeValue?.count)! - 1) == 0 {
////                        tabItem.badgeValue = nil
////                    } else {
////                        tabItem.badgeValue = "\((tabItem.badgeValue?.count)! - 1)"
////                    }
//                }
//
//
//            }
//
//
//        }
//
//    }
//
    
}

