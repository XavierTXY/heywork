//
//  ChatLogVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 12/11/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import PopupDialog
import MobileCoreServices
import BLTNBoard
//import ImageSlideshow


class ChatLogVC: UIViewController, UITextFieldDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource  {
    
    lazy var inputContainerView: ChatInputContainerViewImage = {
        
        let chatInputContainerView = ChatInputContainerViewImage(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        chatInputContainerView.newChatLogVC = self
        return chatInputContainerView
        
        
    }()
    
    
    @IBOutlet weak var navLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var taskBtn: UIButton!
    @IBOutlet weak var taskTitleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    var offer: Offer!
    var assignedUser: User?
    var taskID: String!
    var userID: String!
    var userName: String!
    var assign: Offer!
    var task: Task!
    var bulletinManager: BLTNItemManager!

    
    var messages = [MessageModel]()
    var messagesID = [String]()
    var seenArray = [Bool]()
    var msgIndex = 0
    var msgDict = [String: MessageModel]()
    var originalFrame : CGRect?
    var startingFrame : CGRect?
    var blackBlackgroundView: UIView?
    var timer: Timer?
    
    var listener : DatabaseHandle!
    var msgListener : DatabaseHandle!
    var blockingListener : DatabaseHandle!
    
    var numberOfObjectsinArray = 10
    var totalMsg: Int!
    static var messageImageCache: NSCache<NSString, UIImage> = NSCache()
    
    var isAppear = true
    var documentPicker: UIDocumentPickerViewController!
    //    var selectedUnits: Int!
    //    var itemType: String!
    //    var item: Item!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypeText),String(kUTTypeContent),String(kUTTypeItem),String(kUTTypeData)], in: .import)
        documentPicker.delegate = self
        
       
        
        SVProgressHUD.show()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.taskBtn.layer.cornerRadius = 5.0
//        navigationItem.title = "VIP"
        
    
        
        //        originalFrame = self.tabBarController?.tabBar.frame
        //        let height = (self.tabBarController?.tabBar.frame.height)! - 49
        //        self.tabBarController?.tabBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: height)
        
        collectionView?.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 8, right: 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 40
        layout.minimumLineSpacing = 40
        collectionView!.collectionViewLayout = layout
        
        collectionView?.backgroundColor = UIColor(red: 248/255, green: 249/255, blue: 250/255, alpha: 1)
        collectionView?.keyboardDismissMode = .interactive
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(ChatMessageCell.self, forCellWithReuseIdentifier: "cellId")
        
        //
        //        let rightBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "option"), style: .plain, target: self, action: "optionTapped")
        //        rightBarItem.tintColor = UIColor(red: 0/255, green: 105/255, blue: 192/255, alpha: 1)
        //        self.navigationItem.setRightBarButton(rightBarItem, animated: true)
        
        
        SVProgressHUD.show()
        setupKeyboardObservers()
        getTask()
        getUser()
        getTotalMessages {
            self.observeMessages()
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.shouldRemoveShadow(false)
        
        //        UIApplication.shared.statusBarStyle = .default
        //        self.navigationController?.navigationBar.tintColor = PURPLE
        //        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
        
//        if isAppear {
//            getTotalMessages {
//                self.observeMessages()
//            }
//        }
        
        //        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let userId = self.userID
        let currentUid = DataService.ds.currentUser.userKey!
        
        
        if isAppear {
//        DataService.ds.REF_USER_MESSAGES.child(currentUid).child(taskID!).child(userId!)
        DataService.ds.REF_USER_MESSAGES.child(currentUid).child(taskID!).child(userId!).removeObserver(withHandle: self.listener)
//        DataService.ds.REF_MESSAGES.child(msgId).remo
            //            DataService.ds.REF_UserBlock_PartnerID.child((self.user?.userKey)!).child(currentUid!).removeObserver(withHandle: self.blockingListener)
        }
        
        //        self.tabBarController?.tabBar.frame = originalFrame!
    }
    
    @IBAction func showTask(_ sender: Any) {
        var storyboard = UIStoryboard(name: "Home", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "TaskDetailVC") as! TaskDetailVC
        controller.enableBottom = false
        controller.task = self.task

        print("tap to task")
        self.navigationController?.pushViewController(controller, animated: true)
//        self.present(controller, animated: true, completion: nil)
    }
    
    func showTaskAction(mode: String) {

    
        if mode == WORKING {
            let page = BLTNPageItem(title: "Are you finished?")
            page.requiresCloseButton = false
            page.isDismissable = false
            page.appearance.actionButtonColor = MAIN_COLOR
            page.image = UIImage(named: "request")
            
            page.descriptionText = "Are you sure you want to request the money?"
            page.actionButtonTitle = "Request the money"
            page.alternativeButtonTitle = "Cancel"
            
            
            page.actionHandler = { (item: BLTNActionItem) in
                
                self.assign.changeState(taskKey: self.task.taskKey, id: DataService.ds.currentUser.userKey!, newStatus: REQUEST) {
                    
                    self.changeBtnState()
                    self.handleSend(values: ["text": "I have finished this task and I am requesting for my payment to be released."])
                    item.manager?.dismissBulletin(animated: true)
                    
                }
                
            }
            
            page.alternativeHandler = { (item: BLTNActionItem) in
                
                item.manager?.dismissBulletin(animated: true)
                
            }
            
            bulletinManager = BLTNItemManager(rootItem: page)
            bulletinManager.backgroundViewStyle = .blurredDark
            
            bulletinManager.showBulletin(above: self)
            
        } else if mode == REQUEST {
            
            let page = BLTNPageItem(title: "Payment release")
            page.requiresCloseButton = false
            page.isDismissable = false
            page.appearance.actionButtonColor = MAIN_COLOR
            page.image = UIImage(named: "pay")
            
            page.descriptionText = "Are you sure you want to release the money?"
            page.actionButtonTitle = "Release the money"
            page.alternativeButtonTitle = "Cancel"
            
            
            page.actionHandler = { (item: BLTNActionItem) in
                
                Bill().createBill(billIdentifier: "\(self.taskID!)_\(self.assign.offerUserID!)", payerID: DataService.ds.currentUser.userKey!, payeeProfilePicUrl: self.assign.offerUserProfilePicUrl, payeeUsername: self.assign.offerUserName, payeeID: self.assign.offerUserID!, total_credit_debit: "totalCredit", taskID: self.taskID, billStatus: SUCCESS) { success in
                    
                    print(success)
                    if success {
                        self.task.updateAssignToComplete(taskerID: self.assign.offerUserID){
                            self.assignedUser!.updateReviewRates(reviewRate: "taskerTotalTaskDone")
                            self.assign.changeState(taskKey: self.task.taskKey, id: self.userID, newStatus: PAID) {
                                self.changeBtnState()
                                self.handleSend(values: ["text": "I have released your payment. Thanks!"])
                                item.manager?.dismissBulletin(animated: true)
                            }
                        }
                        
                        
                        
                    }
                }
                
            }
            
            page.alternativeHandler = { (item: BLTNActionItem) in
                
                item.manager?.dismissBulletin(animated: true)
                
            }
            
            bulletinManager = BLTNItemManager(rootItem: page)
            bulletinManager.backgroundViewStyle = .blurredDark
            
            bulletinManager.showBulletin(above: self)
        }

    }
    @IBAction func taskBtnTapped(_ sender: Any) {
        

        if self.assign.offerStatus == WORKING {
            if self.task.posterID == DataService.ds.currentUser.userKey! {
                ErrorAlert.errorAlert.showAlert(title: "Warning!", msg: "You posted this task", object: self)
            } else {
                
                self.showTaskAction(mode: WORKING)
//                let alert = UIAlertController(title: "Are you finished?", message: "Are you sure you want to request the money?", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//                alert.addAction(UIAlertAction(title: "Request the money", style: .default) { (action) in
//
//                    self.assign.changeState(taskKey: self.task.taskKey, id: DataService.ds.currentUser.userKey!, newStatus: REQUEST) {
//
//                            self.changeBtnState()
//                            self.handleSend(values: ["text": "I have finished this task and I am requesting for my payment to be released."])
//
//                        }
//
//
//                })
//
//                self.present(alert, animated: true, completion: nil)
            }
        } else if self.assign.offerStatus == REQUEST {
            if self.task.posterID != DataService.ds.currentUser.userKey! {
                ErrorAlert.errorAlert.showAlert(title: "Warning!", msg: "You already requested for funds", object: self)
            } else {
                //Release payment here
                self.showTaskAction(mode: REQUEST)
                
//                let alert = UIAlertController(title: "Time to pay", message: "Are you sure you want to release the money?", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//                alert.addAction(UIAlertAction(title: "Release the money", style: .default) { (action) in
////                    print(self.taskID)
////                    print(self.offer.offerUserID)
//
//                    Bill().createBill(billIdentifier: "\(self.taskID!)_\(self.assign.offerUserID!)", payerID: DataService.ds.currentUser.userKey!, payeeProfilePicUrl: self.assign.offerUserProfilePicUrl, payeeUsername: self.assign.offerUserName, payeeID: self.assign.offerUserID!, total_credit_debit: "totalCredit", taskID: self.taskID, billStatus: SUCCESS) { success in
//
//                        print(success)
//                        if success {
//                            self.task.updateAssignToComplete(taskerID: self.assign.offerUserID){
//                                self.assignedUser!.updateReviewRates(reviewRate: "taskerTotalTaskDone")
//                                self.assign.changeState(taskKey: self.task.taskKey, id: self.userID, newStatus: PAID) {
//                                    self.changeBtnState()
//                                    self.handleSend(values: ["text": "I have released your payment. Thanks!"])
//                                }
//                            }
//
//
//
//                        }
//                    }
//
//
//                })
//
//                self.present(alert, animated: true, completion: nil)
            }
        } else if self.assign.offerStatus == REQUEST_CANCEL {
//            print(self.assign.offerUserID)
//            print(DataService.ds.currentUser.userKey!)
            if self.task.posterID == DataService.ds.currentUser.userKey! {
                ErrorAlert.errorAlert.showAlert(title: "Warning!", msg: "You already requested for funds", object: self)
            } else {
                let alert = UIAlertController(title: "Poster has cancelled this task from you.", message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Mutually agree to cancel", style: .default, handler: { (action) in
                    //           print(self.task.taskKey)
                    //                print(self.assign.offerUserID)
                    DataService.ds.REF_REQUEST_TO_CANCEL.child(self.task.taskKey!).child(self.assign.offerUserID!).updateChildValues(["status":CANCELLED,"response": MUTUALLY_CANCEL, "respondedTime":NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))])
                    self.task.updateAssignToComplete(taskerID: self.assign.offerUserID) {
                            self.assign.changeState(taskKey: self.task.taskKey, id: self.assign.offerUserID!, newStatus: CANCELLED) {
                                self.handleSend(values: ["text": "I have responded to your cancellation. Thanks."])
                                self.changeBtnState()
                                Bill().updateStatus(billMode: CANCELLED, billIdentifier: "\(self.task.taskKey!)_\(self.assign.offerUserID!)", payerID: self.task.posterID, billStatus: CANCELLED)
                            }
                    }
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Other response", style: .default, handler: { (action) in
                    let innerAlert = UIAlertController(title: "Your response", message: nil, preferredStyle: .alert)
                    innerAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    innerAlert.addTextField(configurationHandler: { textField in
                        textField.placeholder = "Input your response here..."
                        
                        
                        innerAlert.addAction(UIAlertAction(title: "Thanks for your feedback!", style: .default, handler: { action in
                            if innerAlert.textFields?.first?.text?.count != 0 {
                                if let response = innerAlert.textFields?.first?.text {
                                DataService.ds.REF_REQUEST_TO_CANCEL.child(self.task.taskKey!).child(self.assign.offerUserID!).updateChildValues(["status":CANCELLED,"response": response, "respondedTime":NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))])
                                    self.task.updateAssignToComplete(taskerID: self.assign.offerUserID) {
                                        self.assign.changeState(taskKey: self.task.taskKey!, id: self.assign.offerUserID!, newStatus: CANCELLED) {
                                            self.handleSend(values: ["text": "I have responded to your cancellation. Thanks."])
                                            self.changeBtnState()
                                            Bill().updateStatus(billMode: CANCELLED, billIdentifier: "\(self.task.taskKey!)_\(self.assign.offerUserID!)", payerID: self.task.posterID, billStatus: CANCELLED)
                                        }
                                    }
                                    
                                }
                            }
                        }))
                        
                    })
                    
                    if let popoverController = innerAlert.popoverPresentationController {
                        popoverController.sourceView = self.view
                        popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                        popoverController.permittedArrowDirections = []
                    }
                    
                    self.present(innerAlert, animated: true)
                }))
                
                if let popoverController = alert.popoverPresentationController {
                    popoverController.sourceView = self.view
                    popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                    popoverController.permittedArrowDirections = []
                }
                
                self.present(alert, animated: true)
            }
            
        } else if self.assign.offerStatus == PAID {
            
            DataService.ds.showReview(vc: self, fromSetting: false)
            
            if self.task.posterID == DataService.ds.currentUser.userKey! {
//                print(self.userID)
                self.updateReview(mode: false, taskID: self.taskID, posterID: self.assign.offerUserID!, posterUsername: self.assign.offerUserName!, posterImgUrl: self.assign.offerUserProfilePicUrl!, newStatus: POSTER_REVIEWED)
//                self.assignedUser?.updateReview(mode: false, msg: "Good work", starNum: 5, taskID: self.taskID, posterID: DataService.ds.currentUser.userKey!, posterUsername: DataService.ds.currentUser.userName, posterImageUrl: DataService.ds.currentUser.profilePicUrl, completion: {
//
//                    self.assign.changeState(taskKey: self.task.taskKey, id: self.userID, newStatus: POSTER_REVIEWED) {
//                        self.changeBtnState()
//                    }
//                })
                print(self.taskID)
                print(DataService.ds.currentUser.userKey!)
                print(POSTER_REVIEWED)
                print(self.task.posterID)
            } else {
//                print(self.assign.offerUserID)
                self.updateReview(mode: true, taskID: self.taskID, posterID: DataService.ds.currentUser.userKey!, posterUsername: DataService.ds.currentUser.userName, posterImgUrl: DataService.ds.currentUser.profilePicUrl, newStatus: TASKER_REVIEWED)
//
                print(self.taskID)
                print(DataService.ds.currentUser.userKey!)
                print(TASKER_REVIEWED)
                print(self.task.posterID)
                
//                self.assignedUser?.updateReview(mode: true, msg: "Good work", starNum: 3, taskID: self.taskID, posterID: self.assign.offerUserID, posterUsername: self.assign.offerUserName, posterImageUrl: (self.assignedUser?.profilePicUrl)!, completion: {
//
//                    self.assign.changeState(taskKey: self.task.taskKey, id: DataService.ds.currentUser.userKey!, newStatus: TASKER_REVIEWED) {
//                        self.changeBtnState()
//                    }
//                })
            }
        } else if self.assign.offerStatus == POSTER_REVIEWED {
            if self.task.posterID != DataService.ds.currentUser.userKey! {
                
                self.updateReview(mode: true, taskID: self.taskID, posterID: DataService.ds.currentUser.userKey!, posterUsername: DataService.ds.currentUser.userName, posterImgUrl: DataService.ds.currentUser.profilePicUrl, newStatus: TASKER_REVIEWED)
                
//                self.assignedUser?.updateReview(mode: true, msg: "Good work", starNum: 3, taskID: self.taskID, posterID: self.assign.offerUserID, posterUsername: self.assign.offerUserName, posterImageUrl: (self.assignedUser?.profilePicUrl)!, completion: {
//
//                    self.assign.changeState(taskKey: self.task.taskKey, id: DataService.ds.currentUser.userKey!, newStatus: TASKER_REVIEWED) {
//                        self.changeBtnState()
//                    }
//                })
            }
        } else if self.assign.offerStatus == TASKER_REVIEWED {
            if self.task.posterID == DataService.ds.currentUser.userKey! {
                self.updateReview(mode: false, taskID: self.taskID, posterID: self.assign.offerUserID!, posterUsername: self.assign.offerUserName!, posterImgUrl: self.assign.offerUserProfilePicUrl!, newStatus: POSTER_REVIEWED)
                
//                self.assignedUser?.updateReview(mode: false, msg: "Good work", starNum: 5, taskID: self.taskID, posterID: DataService.ds.currentUser.userKey!, posterUsername: DataService.ds.currentUser.userName, posterImageUrl: DataService.ds.currentUser.profilePicUrl, completion: {
//
//                    self.assign.changeState(taskKey: self.task.taskKey, id: self.userID, newStatus: POSTER_REVIEWED) {
//                        self.changeBtnState()
//                    }
//                })
                
            }
        }
    }
    
//    func updateTaskReview(posterStatus: String, taskerStatus: String) {
//
//    }

    func updateReview(mode: Bool, taskID: String, posterID: String, posterUsername: String, posterImgUrl: String, newStatus: String) {
        let reviewVC = ReviewView(nibName: "ReviewView", bundle: nil)
        
        
        let popup = PopupDialog(viewController: reviewVC, buttonAlignment: .horizontal, transitionStyle: .zoomIn, tapGestureDismissal: false)
        
        // Create first button
        let buttonOne = CancelButton(title: "Cancel", height: 60) {
            
        }
        
        let buttonTwo = DefaultButton(title: "Submit", height: 60) {
            
            if reviewVC.reviewTF.text!.count == 0 {
                ErrorAlert.errorAlert.showAlert(title: "Review Needed!", msg: "Can't be emtpty", object: self)
            } else {
                
                self.assignedUser?.updateReview(mode: mode, msg: reviewVC.reviewTF.text!, starNum: reviewVC.ratingView.rating, taskID: taskID, posterID: posterID, posterUsername: posterUsername, posterImageUrl: posterImgUrl, completion: {
                    
                    self.assign.changeState(taskKey: self.task.taskKey, id: posterID, newStatus: newStatus) {
                        self.changeBtnState()
                    }
                })
            }
            
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    func changeBtnState() {
        if self.assign.offerStatus == WORKING {
            
            if self.assign.offerUserID == DataService.ds.currentUser.userKey! {
                self.taskBtn.setTitle(WORKING_DONE_TASKER, for: .normal)
            } else {
                self.taskBtn.setTitle(WORKING_DONE_POSTER, for: .normal)
            }
        } else if self.assign.offerStatus == REQUEST {
            
            if self.assign.offerUserID == DataService.ds.currentUser.userKey! {
                self.taskBtn.setTitle(REQUEST_DONE_TASKER, for: .normal)
            } else {
                self.taskBtn.setTitle(REQUEST_DONE_POSTER, for: .normal)
            }
        } else if self.assign.offerStatus == PAID {
//            self.taskBtn.backgroundColor = UIColor.green
            
            if self.assign.offerUserID == DataService.ds.currentUser.userKey! {
                self.taskBtn.setTitle(PAID_DONE_TASKER, for: .normal)
            } else {
                self.taskBtn.setTitle(PAID_DONE_POSTER, for: .normal)
            }
        } else if self.assign.offerStatus == COMPLETED {
//            self.taskBtn.backgroundColor = UIColor.green
//            if self.assign.offerUserID == DataService.ds.currentUser.userKey! {
//                self.taskBtn.setTitle(REVIEW_DONE_TASKER, for: .normal)
//            } else {
                self.taskBtn.setTitle(COMPLETED_TASK, for: .normal)
//            }
        } else if self.assign.offerStatus == REQUEST_CANCEL {
            self.taskBtn.backgroundColor = UIColor.red
            if self.assign.offerUserID == DataService.ds.currentUser.userKey! {
                self.taskBtn.setTitle(CANCEL_PENDING_TASKER, for: .normal)
            } else {
                self.taskBtn.setTitle(CANCEL_PENDING_POSTER, for: .normal)
            }
        } else if self.assign.offerStatus == CANCELLED {
            self.taskBtn.backgroundColor = UIColor.red
            self.taskBtn.setTitle(CANCELLED, for: .normal)
            
        } else if self.assign.offerStatus == BOTH_REVIEWED {
//            self.taskBtn.backgroundColor = UIColor.green
            self.taskBtn.setTitle("Task Completed", for: .normal)
        
        } else if self.assign.offerStatus == POSTER_REVIEWED {
            if self.assign.offerUserID == DataService.ds.currentUser.userKey! {
                self.taskBtn.setTitle(PAID_DONE_POSTER, for: .normal)
            } else {
                self.taskBtn.setTitle("Task Completed", for: .normal)
            }
        } else if self.assign.offerStatus == TASKER_REVIEWED {
            if self.assign.offerUserID == DataService.ds.currentUser.userKey! {
                self.taskBtn.setTitle("Task Completed", for: .normal)
            } else {
                self.taskBtn.setTitle(PAID_DONE_TASKER, for: .normal)
            }
        }
        
    }
    
    
    func getTask() {

        DataService.ds.REF_ALL_TASK.child(self.taskID).observeSingleEvent(of: .value, with: { (snapshot) in
            if ( snapshot.value is NSNull ) {
            } else {
                
                if let taskDict = snapshot.value as? Dictionary<String, AnyObject> {
                    //                    let key = snapshot.key
                    
                    self.task = Task(taskKey: self.taskID, dict: taskDict)
                    self.taskTitleLbl.text = self.task.title
                    
                    if self.task.posterID == self.userID {
                        self.getAssign(id: DataService.ds.currentUser.userKey!)
                    } else {
                        self.getAssign(id: self.userID)
                    }
   
                    
                }
            }
        })
       
      
    }
    
    func getAssign(id: String) {
        DataService.ds.REF_TASK_ASSIGN.child(self.taskID).child(id).observe(.value) { (snapshot) in
            
            if ( snapshot.value is NSNull ) {
            } else {
                
                if let assignDict = snapshot.value as? Dictionary<String, AnyObject> {
                    //                    let key = snapshot.key
                    
                    self.assign = Offer(offerID: "", offerDict: assignDict)
                    self.changeBtnState()
                    
                    self.priceLbl.text = "SGD\(self.assign.offerPrice!)"
                    
                    let seconds = self.assign.offerDate.doubleValue
                    let date = NSDate(timeIntervalSince1970: seconds)
                    
                    let dayTimePeriodFormatter = DateFormatter()
                    dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
                    
                    let dateString = dayTimePeriodFormatter.string(from: date as Date)
                    
                    self.dateLbl.text = dateString
                    
                }
            }
        }
    }
    
    func getUser() {
        DataService.ds.REF_USERS.child(self.userID).observeSingleEvent(of: .value, with: { (snapshot) in
            if ( snapshot.value is NSNull ) {
            } else {
//                print("YEAH")
                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                    let key = snapshot.key
                    
                    self.assignedUser = User(userKey: self.userID, userData: userDict)
                    self.navigationItem.title = self.assignedUser?.userName
//                     self.navLbl.text = self.user?.userName
                    
                }
            }
        })
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        
        if (textField.text?.characters.count)! > 0 && !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            //            enableSendButton(tf: textField)
            print("enable")
            //            inputContainerView.inputTextField.enablesReturnKeyAutomatically = true
        } else {
            print("disable")
            //            inputContainerView.inputTextField.enablesReturnKeyAutomatically = false
            //            disableSendButton(tf: textField)
        }
    }
    
    func setupKeyboardObservers() {
        //        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification2), name: UIResponder.keyboardDidHideNotification, object: nil)
        
    }
    
    
    
    @objc func handleKeyboardNotification(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print("walo")
            let userInfo = notification.userInfo!
            let animationDuration: TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
            
            self.collectionView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: keyboardSize.height+10, right: 0)
            
            UIView.animate(withDuration: animationDuration, animations: {
                self.view.layoutIfNeeded()
            }) { (completed) in
                if self.messages.count > 0 {
                    print("hi there")
                    
                    let indexPath = NSIndexPath(item: self.messages.count - 1, section: 0)
                    self.collectionView?.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
                    
                    
                    
                }
            }
        }
        
    }
    
    @objc func handleKeyboardNotification2(notification: NSNotification) {
        print("walo")
        let userInfo = notification.userInfo!
        let animationDuration: TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        self.collectionView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 8, right: 0)
        
        UIView.animate(withDuration: animationDuration, animations: {
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
        
        
    }
    
    
    func handleKeyBoardDidShow() {
        if messages.count > 0 {
            print("hi there")
            self.collectionView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 400+8, right: 0)
            let indexPath = NSIndexPath(item: self.messages.count - 1, section: 0)
            self.collectionView?.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
            
            
            
        }
        
    }
    
    func displayEmptyTxtFieldAlert() {
        
        ErrorAlert.errorAlert.showAlert(title: "Reminder", msg: "Message can't be blank", object: self)
        
    }
    
    //    func enableSendButton(tf: UITextField) {
    //        //        self.inputContainerView.sendButton.tintColor = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)
    //        //        self.inputContainerView.sendButton.isEnabled = true
    //        inputContainerView.inputTextField.enablesReturnKeyAutomatically = true
    //    }
    //
    //    func disableSendButton(tf: UITextField) {
    //        //        self.inputContainerView.sendButton.tintColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 0.7)
    //        //        self.inputContainerView.sendButton.isEnabled = false
    //        inputContainerView.inputTextField.enablesReturnKeyAutomatically = false
    //    }
    
    
    func getTotalMessages(completion: @escaping () -> ()) {
        let currentUid = Auth.auth().currentUser?.uid
        
        DataService.ds.REF_USER_MESSAGES.child(currentUid!).child(self.userID).observeSingleEvent(of: .value, with: { (snapshot) in
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                self.totalMsg = snapshots.count
                
                print("total \(snapshots.count)")
                completion()
                
            }
            
            
            
        })
    }
    
    
    
    func loadMore() {
        print("LOAD MORE")
        numberOfObjectsinArray = numberOfObjectsinArray + 10
        
        let currentUid = Auth.auth().currentUser?.uid
        let ref =  DataService.ds.REF_USER_MESSAGES.child(currentUid!)
        
        let userId = self.userID
        let userRef = DataService.ds.REF_USER_MESSAGES.child(currentUid!).child(userId!)
        
        userRef.queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            //            self.confessions = []
            //                    self.elements = []
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    print("SNAP: \(snap)")
                    if let dict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let key = snap.key
                        let message = MessageModel(key: snap.key, dictionary: dict)
                        
                        //                            let confession = Confession(confessionKey: key, postData: confessionDict)
                        
                        self.msgDict[key] = message
                        
                        
                        
                    }
                }
                self.attemptToReload()
                
            }
        })
    }
    
    func updateSeen(msg: MessageModel) {
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        updateObj["/messages/\(msg.msgId!)/toSeen"] = true as AnyObject
        ref.updateChildValues(updateObj)
    }
    
    func observeMessages() {
        let currentUid = DataService.ds.currentUser.userKey!
        print("WAHAHA \(currentUid)")
//        let ref =  DataService.ds.REF_USER_MESSAGES.child(currentUid!)
        
        //        ref.observe(.childAdded, with: { (snapshot) in
        
        let userId = self.userID
        let userRef = DataService.ds.REF_USER_MESSAGES.child(currentUid).child(taskID!).child(userId!)
        
        print(currentUid)
        print(taskID)
        print(userId!)
        self.listener = userRef.observe(.childAdded, with: { (snapshot) in
            print("listening from chaatlog")
//            self.collectionView?.reloadData()
            // print(snapshot)
//        DataService.ds.chatLogObserverListener = userRef.observe(.childAdded, with: { (snapshot) in
            let msgId = snapshot.key
            let msgRef = DataService.ds.REF_MESSAGES.child(msgId)

            msgRef.observe(.value, with: { (snapshot) in
                
                let dictionary = snapshot.value as? [String: Any]
                
                
                let message = MessageModel(key: snapshot.key, dictionary: dictionary!)
                if message.toId == currentUid {
                    self.updateSeen(msg: message)
                }
                
                // message.setValuesForKeys(dictionary!)
                
                if message.chatPartnerId() == self.userID {
                    print("WAD")
//                                            self.msgDict[message.msgId!] = message
                    //                        self.attemptToReload()
//                    self.messages = Array(self.msgDict.values)
                    if let index = self.messagesID.index(of: message.msgId!) {
                        print("replace")
                        self.messages[index] = message
                    } else {
                        print("add new")
                        self.messages.append(message)
                        self.messagesID.append(message.msgId!)
                    }
                    
                    
                    
                    self.collectionView?.reloadData()
                    //
                    if self.messages.count > 0 {
                        print("hi there 2")
                        let indexPath = NSIndexPath(item: self.messages.count - 1, section: 0)
                        self.collectionView?.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
                        SVProgressHUD.dismiss()
                    }
                    
                    //
                    //                    self.timer?.invalidate()
                    //
                    //                    self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.reloadTable), userInfo: nil, repeats: false)
                    //
                    
                    
                }
                
            })
            
            
            
            
        })
        SVProgressHUD.dismiss()
        //        })
        
    }
    
    func timerReload() {
        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(reloadAndSortTable), userInfo: nil, repeats: false)
    }
    
    @objc func reloadAndSortTable() {
        self.collectionView?.reloadData()
    }
    
    func showBeingBlockAlert() {
//
//        if inputContainerView.inputTextField.isFirstResponder {
//            inputContainerView.inputTextField.resignFirstResponder()
//        }
//        let popup = PopupDialog(title: "Message Not Sent", message: "This person isn't receiving messages at this time.", image: nil)
//
//        //        var partnerID = messages[index].chatPartnerId()
//        let buttonOne = DefaultButton(title: "OK") {
//
//        }
//
//
//        popup.addButtons([buttonOne])
//        popup.buttonAlignment = .horizontal
//
//        // Present dialog
//        self.present(popup, animated: true, completion: nil)
    }
    
    
    func attemptToReload() {
        self.messages = Array(self.msgDict.values)
        self.collectionView?.reloadData()
    }
    
    @objc func resetIsAppear() {
    
        self.isAppear = true
    }
    
    
    
    @objc func uploadImageTapped() {
        print("tapped")
        isAppear = false
        
        //        if self.inputContainerView.inputTextField.isFirstResponder {
        self.inputContainerView.inputTextField.resignFirstResponder()
        //        }
        let imgPicker = UIImagePickerController()
        
        imgPicker.allowsEditing = false
        imgPicker.delegate = self
        //        present(imgPicker, animated: true, completion: nil)
        
        let alert = UIAlertController()
        alert.addAction(UIAlertAction(title: "Document", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            self.present(self.documentPicker, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            imgPicker.sourceType = UIImagePickerController.SourceType.camera;
            
            self.present(imgPicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo library", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            imgPicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
            self.inputContainerView.inputTextField.resignFirstResponder()
            self.present(imgPicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            
            Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.resetIsAppear), userInfo: nil, repeats: false)
        })
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.resetIsAppear), userInfo: nil, repeats: false)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        SVProgressHUD.show()
        
        var selectedImageFromPicker: UIImage?

        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            selectedImageFromPicker = originalImage
        }

        if let selectedImage = selectedImageFromPicker {
            uploadImageToFirebase(selectedImage: selectedImage, documentPath: nil)
        }

        picker.dismiss(animated: true, completion: nil)
        Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.resetIsAppear), userInfo: nil, repeats: false)
    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        dismiss(animated: true, completion: nil)
//
//        SVProgressHUD.show()
//
//        var selectedImageFromPicker: UIImage?
//
//        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
//            selectedImageFromPicker = editedImage
//        } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
//            selectedImageFromPicker = originalImage
//        }
//
//        if let selectedImage = selectedImageFromPicker {
//            uploadImageToFirebase(selectedImage: selectedImage)
//        }
//
//
//
//
//
//
//        Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.resetIsAppear), userInfo: nil, repeats: false)
//    }
    
    private func uploadImageToFirebase(selectedImage: UIImage?, documentPath: URL?) {
//        SVProgressHUD.show()
        
        if let img = selectedImage {
            if let imgData = img.jpegData(compressionQuality: 0.2) {
                
                let imgUid = NSUUID().uuidString
                let metaData = StorageMetadata()
                metaData.contentType = "image/jpeg"
                
                DataService.ds.REF_MESSAGE_PIC.child(imgUid).putData(imgData, metadata: metaData) { (metaData, error) in
                    if error != nil {
                        print("Xavier: upload image to firebase failed")
                        print(error.debugDescription)
                    } else {
                        print("Xavier: successfuly upload img to firebase")
                        SVProgressHUD.dismiss()
                        let downloadUrl = metaData?.downloadURL()?.absoluteString
                        if let url = downloadUrl {
                            self.postToFirebase(imgUrl: url, img: img)
                            
                            
                        }
                        
                    }
                    
                }
                
                
            }
        }
        
        if let path = documentPath {
            let localFile = path
            let fileUID = NSUUID().uuidString
            // Create a reference to the file you want to upload
            let riversRef = DataService.ds.REF_CHAT_DOCUMENT.child(fileUID)
            
            // Upload the file to the path "images/rivers.jpg"
            let uploadTask = riversRef.putFile(from: localFile, metadata: nil) { metadata, error in
                guard let metadata = metadata else {
                    // Uh-oh, an error occurred!
                    return
                }
                // Metadata contains file metadata such as size, content-type.
                let size = metadata.size
                // You can also access to download URL after upload.
                let downloadUrl = metadata.downloadURL()?.absoluteString
                if let url = downloadUrl {
                    let theFileName = path.lastPathComponent
                    self.postDocToFirebase(url:url, filename: theFileName)
                    
                    
                }
            }
        }
    }
    
    private func postDocToFirebase(url: String, filename: String) {
        messageSendWithDocUrl(url:url, filename: filename)
        
    }
    
    private func postToFirebase(imgUrl: String, img: UIImage) {
        messageSendWithImageUrl(imgUrl: imgUrl, img: img)
        
    }
    
    //    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    //        if indexPath.row == 0 {
    //            if numberOfObjectsinArray < totalMsg {
    //                print("load more!")
    //
    //                loadMore()
    //            }
    //
    //        }
    //    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    //
    //    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //        // calculates where the user is in the y-axis
    //        let offsetY = scrollView.contentOffset.y
    //        let contentHeight = scrollView.contentSize.height
    //
    //        if offsetY < contentHeight - scrollView.frame.size.height {
    //
    //            print("load more")
    //            // increments the number of the page to request
    ////            indexOfPageRequest += 1
    //
    //            // call your API for more data
    ////            loadData()
    //
    //            // tell the table view to reload with the new data
    ////            self.tableView.reloadData()
    //        }
    //    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! ChatMessageCell
        let msg = messages[indexPath.row]
        cell.newChatLogVC = self
        cell.textView.delegate = self
        
        
        setupCell(cell: cell, msg: msg)
        
//
//        if msg.isItem == true {
//            cell.bubbleWidthAnchor?.constant = estimateFrameForText(text: msg.text!).width + 150
//            cell.textView.isHidden = true
//        } else
        if let text = msg.text {
            cell.textView.text = msg.text
            var newWidth = estimateFrameForText(text: msg.text!).width + 30
            
            
            var upperBound = abs(80 - newWidth)
            print("\(msg.text) - \(newWidth) - \(upperBound)")
            if newWidth < 80 {
                var addWidth = abs(80 - newWidth)
                
                cell.bubbleWidthAnchor?.constant = newWidth + addWidth + (newWidth * 0.05)
            } else {
                cell.bubbleWidthAnchor?.constant = newWidth
            }
            
//            cell.bubbleHeightAnchor?.constant = estimateFrameForText(text: msg.text!).height + 100
            cell.textView.isHidden = false
            
        } else if msg.imageUrl != nil {
            cell.bubbleWidthAnchor?.constant = 200
            cell.textView.isHidden = true
            
        } else if msg.docUrl != nil {
            cell.textView.text = msg.fileName!
            var newWidth = estimateFrameForText(text: msg.fileName!).width
            cell.bubbleWidthAnchor?.constant = newWidth + 30
            //            cell.bubbleHeightAnchor?.constant = estimateFrameForText(text: msg.text!).height + 100
            cell.textView.isHidden = false

        }
        
        
        return cell
    }
    
    private func setupCell(cell: ChatMessageCell, msg: MessageModel) {
        
        
        
        //            cell.initialLbl.isHidden = false
        //            var i = String((self.user?.name[(self.user?.name.startIndex)!])!).capitalized
        //            cell.initialLbl.text = i
        //            cell.profileImageView.backgroundColor = ColorHelper().pickColor(alphabet: Character(i))
        
        
        let seconds = msg.timeStamp?.doubleValue
        let timesStampDate = NSDate(timeIntervalSince1970: seconds!)
        var dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "MM/dd/yyyy"
        cell.dateLbl.text = dateFormatter.string(from: timesStampDate as Date)
        
        dateFormatter.dateFormat = "hh:mm:a"
        cell.innerDateLbl.text = dateFormatter.string(from: timesStampDate as Date)
        
        if msg.toSeen! {
            cell.tickImageView.image = UIImage(named: "doubleTick")
        } else {
            cell.tickImageView.image = UIImage(named: "oneTick")
        }
        // reset everytime for anchor problem
        cell.bubbleRightAnchor?.isActive = false
        cell.bubbleLeftAnchor?.isActive = false
        
        if msg.fromId == Auth.auth().currentUser?.uid {
            
            //            cell.profileImageView.isHidden = true
            
            cell.bubbleView.backgroundColor = CHAT_BG_COLOR
            //            cell.blueImg.isHidden = false
//            cell.textView.textColor = UIColor.black
//            cell.innerDateLbl.textColor = UIColor(hex: "D8D8D8")
//            cell.itemLbl.textColor = UIColor.white
//            cell.itemTypeLbl.textColor = UIColor.white
//            cell.unitLbl.textColor = UIColor.white
            
            cell.bubbleRightAnchor?.isActive = true
            cell.bubbleLeftAnchor?.isActive = false
            
            cell.innerDateRightAnchorBubble?.isActive = false
            cell.innerDateRightAnchorTick?.isActive = true

            cell.tickImgRightAnchor?.isActive = true
            cell.tickImageView.isHidden = false
        } else {
            
            //            cell.profileImageView.isHidden = false
            cell.bubbleView.backgroundColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1.0)
            //            cell.blueImg.isHidden = true
//            cell.textView.textColor = UIColor.black
//            cell.innerDateLbl.textColor = UIColor(hex: "D8D8D8")
//            cell.itemLbl.textColor = UIColor.black
//            cell.itemTypeLbl.textColor = UIColor.black
//            cell.unitLbl.textColor = UIColor.black
            
            cell.bubbleRightAnchor?.isActive = false
            cell.bubbleLeftAnchor?.isActive = true
            
            cell.innerDateRightAnchorBubble?.isActive = true
            cell.innerDateRightAnchorTick?.isActive = false
            
            cell.tickImgRightAnchor?.isActive = false
            cell.tickImageView.isHidden = true
            
        }
        
//        if msg.isItem == true {
//            cell.itemTypeLbl.isHidden = false
//            cell.unitLbl.isHidden = false
//            cell.itemImageView.isHidden = false
//            cell.itemLbl.isHidden = false
//
//            cell.itemLbl.text = msg.text!
//            cell.itemTypeLbl.text = msg.itemType!
//            cell.unitLbl.text = " - \(msg.itemUnit!) units"
//            cell.itemImageView.image = nil
//            cell.itemImageView.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: msg.imageUrl!)
//            cell.imageUrl = msg.imageUrl!
//
//            cell.messageImageView.isHidden = true
            
//        } else {
//            cell.itemTypeLbl.isHidden = true
//            cell.unitLbl.isHidden = true
//            cell.itemImageView.isHidden = true
//            cell.itemLbl.isHidden = true
            
            if let msgImgUrl = msg.imageUrl {
                cell.bubbleView.backgroundColor = UIColor.clear
                cell.messageImageView.backgroundColor = UIColor.clear
                cell.messageImageView.image = nil
                cell.messageImageView.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: msgImgUrl)
                cell.messageImageView.isHidden = false
                
            } else if let docUrl = msg.docUrl {
                cell.messageImageView.isHidden = true
                
                
                if msg.fromId == Auth.auth().currentUser?.uid  {
//                    cell.textView.textColor = UIColor.white
                    cell.textView.hyperLink(originalText: docUrl, hyperLink: docUrl, urlString: docUrl, textColor: UIColor.black)
                } else {
//                    cell.textView.textColor = UIColor.black
                    cell.textView.hyperLink(originalText: docUrl, hyperLink: docUrl, urlString: docUrl, textColor: UIColor.black)
                }
                
                
                
            } else {
                cell.messageImageView.isHidden = true
                cell.textView.isSelectable = false
                cell.textView.isUserInteractionEnabled = false
                cell.textView.linkTextAttributes = [.underlineStyle: 0]
//                cell.textView.removeLink(text: cell.textView.text)
//                cell.textView.backgroundColor = UIColor.clear
//                cell.textView.textColor = UIColor.white
//                cell.textView.font = UIFont(name: "HelveticaNeue", size: 16)
//                cell.textView.isEditable = false
//                cell.textView.isScrollEnabled = false
//
//
//                cell.textView.translatesAutoresizingMaskIntoConstraints = false
               
                
            }
//        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height: CGFloat = 80
        
        let msg = messages[indexPath.row]
        if let text = msg.text {
            height = estimateFrameForText(text: text).height + 35
        } else if let msgWidth = msg.imgWidth?.floatValue, let msgHeight = msg.imgHeight?.floatValue {
            
            height = CGFloat( msgHeight / msgWidth * 200 )
        } else if let filename = msg.fileName {
            height = estimateFrameForText(text: filename).height + 35
        }
        return CGSize(width: self.view.frame.width, height: height)
    }
    
    private func estimateFrameForText(text: String) -> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 16)], context: nil)
        
    }
    
    override var inputAccessoryView: UIView? {
        get {
            
            return inputContainerView
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    private func messageSendWithDocUrl(url: String, filename: String) {
        let values = ["docUrl": url, "fileName": filename] as [String : Any]
        handleSend(values: values)
        
    }
    
    private func messageSendWithImageUrl(imgUrl: String, img: UIImage) {
        
        let values = ["imageUrl": imgUrl, "imgWidth": img.size.width, "imgHeight": img.size.height] as [String : Any]
        handleSend(values: values)
        
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func messageSend() {
        
        
        let values = ["text": inputContainerView.inputTextField.text!]
        handleSend(values: values)
        
        
        
    }
    
    
    func handleSend(values: [String: Any])  {
        
        let ref = DataService.ds.REF_MESSAGES
        let firebaseMessage = DataService.ds.REF_MESSAGES.childByAutoId()
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        
//        var toId: String!
//        var fromId: String!
//        if self.assignedUser?.userKey! == DataService.ds.currentUser.userKey! {
//
//        } else {
//
//        }
        let toId = assignedUser?.userKey
//        let toId = self.userID
        let fromId = DataService.ds.currentUser.userKey!
        
//        assignedUser?.adjustMsgCount(addCount: true, fromId: fromId)
        
        assignedUser?.adjustMsgCount(addCount: true, fromId: "\(self.task.taskKey!)_\(DataService.ds.currentUser.userKey!)")
        
        var dict: [String: Any] = ["toId": toId, "fromId": fromId, "timeStamp": timestamp, "fromUserName": DataService.ds.currentUser.userName, "fromSeen": true, "toSeen": false ] as [String : Any]
        
        
        
        
        for (key, element) in values {
            
            dict[key] = element
            print(dict)
        }
        // values.forEach({dict[$0] = $1 })
        //      firebaseMessage.updateChildValues(values)
        
        firebaseMessage.updateChildValues(dict, withCompletionBlock: {(error, ref) in
            
            if error != nil {
                print(error)
            }
            
            self.inputContainerView.inputTextField.text = nil
            
            let userMsgRef = DataService.ds.REF_USER_MESSAGES.child(fromId).child(self.taskID).child(toId!)
            let msgId = firebaseMessage.key
            userMsgRef.updateChildValues([msgId: true])
            
            let recipientUserMsgRef = DataService.ds.REF_USER_MESSAGES.child(toId!).child(self.taskID).child(fromId)
            recipientUserMsgRef.updateChildValues([msgId: true])
            
            SVProgressHUD.dismiss()
        })
    }
    
    
    var startingImgView: UIImageView?
    
    
    func performZoomInForStartingImageView(startingImageView: UIImageView) {
        
        if self.inputContainerView.inputTextField.isFirstResponder {
            self.inputContainerView.inputTextField.resignFirstResponder()
        }
        
        startingFrame = startingImageView.superview?.convert(startingImageView.frame, to: nil)
        startingImgView = startingImageView
        startingImgView?.isHidden = true
        
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.contentMode = .scaleAspectFit
        zoomingImageView.clipsToBounds = true
        zoomingImageView.image = startingImageView.image
        zoomingImageView.isUserInteractionEnabled = true
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
        
        
        if let keyWindow = UIApplication.shared.keyWindow {
            blackBlackgroundView = UIView(frame: keyWindow.frame)
            blackBlackgroundView?.backgroundColor = UIColor.black
            blackBlackgroundView?.alpha = 0
            keyWindow.addSubview(blackBlackgroundView!)
            keyWindow.addSubview(zoomingImageView)
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackBlackgroundView?.alpha = 1
                self.inputContainerView.alpha = 0
                let height = (self.startingFrame?.height)! / (self.startingFrame?.width)! * keyWindow.frame.width
                
                //zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                
                zoomingImageView.center = keyWindow.center
            }, completion: { (comepleted: Bool) in
                
            })
        }
        
    }
    
    @objc func handleZoomOut(sender: UITapGestureRecognizer) {
        if let zoomOutImageView = sender.view {
            zoomOutImageView.layer.cornerRadius = 16
            zoomOutImageView.clipsToBounds = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                zoomOutImageView.frame = self.startingFrame!
                self.blackBlackgroundView?.alpha = 0
                self.inputContainerView.alpha = 1
                
            }, completion: { (comepleted: Bool) in
                self.startingImgView?.isHidden = false
                zoomOutImageView.removeFromSuperview()
                
            })
            
            
            
        }
    }
    
//    func sendOfferStatusMsg(values: [String: Any], userID: String, offer: Offer)  {
//
//
//
//
//        let ref = DataService.ds.REF_MESSAGES
//        let firebaseMessage = DataService.ds.REF_MESSAGES.childByAutoId()
//        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
//        let toId = userID
//        let fromId = Auth.auth().currentUser!.uid
//
////        user?.adjustMsgCount(addCount: true, fromId: "\(self.task.taskKey!)_\(Auth.auth().currentUser!.uid)")
//        assignedUser?.adjustMsgCount(addCount: true, fromId: "\(self.task.taskKey!)_\(Auth.auth().currentUser!.uid)")
//
//
//        var dict: [String: Any] = ["toId": toId, "fromId": fromId, "timeStamp": timestamp, "fromUserName": DataService.ds.currentUser.userName, "fromSeen": true, "toSeen": false ] as [String : Any]
//
//
//
//
//        for (key, element) in values {
//
//            dict[key] = element
//            print(dict)
//        }
//        // values.forEach({dict[$0] = $1 })
//        //      firebaseMessage.updateChildValues(values)
//
//        firebaseMessage.updateChildValues(dict, withCompletionBlock: {(error, ref) in
//
//            if error != nil {
//                print(error)
//            }
//
//            //            self.inputContainerView.inputTextField.text = nil
//
//            let userMsgRef = DataService.ds.REF_USER_MESSAGES.child(fromId).child(self.task.taskKey).child(toId)
//            let msgId = firebaseMessage.key
//            userMsgRef.updateChildValues([msgId: true])
//
//            let recipientUserMsgRef = DataService.ds.REF_USER_MESSAGES.child(toId).child(self.task.taskKey).child(fromId)
//            recipientUserMsgRef.updateChildValues([msgId: true])
//
//
//        })
//
//
//    }
    
}

extension ChatLogVC: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if textView.isUserInteractionEnabled {
//            print("jere")
            UIApplication.shared.openURL(URL)
            return true
        } else {
//            print("wow")
            return false
        }
//        if (URL.absoluteString == textView.text) {
//            UIApplication.shared.openURL(URL)
//            return true
//        } else {
//            return false
//        }
        
        return false
        
    }
    
    
}

extension ChatLogVC: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print(urls)
        SVProgressHUD.show()
        uploadImageToFirebase(selectedImage: nil, documentPath: urls[0])
        
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
}
