//
//  MyTaskVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 24/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import Firebase
//import AMProgressBar

class MyTaskCell: UITableViewCell {
    
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var taskTitleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
    //    @IBOutlet weak var progressBar: AMProgressBar!
    @IBOutlet weak var offersLbl: UILabel!
    @IBOutlet weak var assignLbl: UILabel!
    @IBOutlet weak var leftLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        statusView.layer.cornerRadius = 10
//        bottomView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10)
        //        AMProgressBar.config.textColor = MAIN_COLOR
        
        //        AMProgressBar.config.textFont = UIFont(name: "OpenSans-SemiBold", size: 15)!
        //        AMProgressBar.config.textPosition = . // AMProgressBarTextPosition
        
    }
    
    func configureCell(task: Task) {
        //
                let seconds = task.deadline.doubleValue
                let date = NSDate(timeIntervalSince1970: seconds)
        
                let dayTimePeriodFormatter = DateFormatter()
                dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
        
                let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
                dateLbl.text = "\(dateString)"
        
                taskTitleLbl.text = task.title
                priceLbl.text = "SGD\(task.budget!)"
        
                if task.assigned.count - 1 != 0 {
                    offersLbl.text = "\(task.offered.count - 1 - (task.assigned.count - 1))"
                } else {
        
                    offersLbl.text = "\(task.offered.count - 1)"
                }
                assignLbl.text = "\(task.assigned.count - 1)"
        
                statusLbl.text = task.status
        
                if task.status == OPEN {
                    //            statusLbl.font = UIFont.boldSystemFont(ofSize: 17.0)
                    statusLbl.textColor = UIColor.white
                    statusView.backgroundColor = OPEN_COLOR
                } else if task.status == CLOSED {
                    //            statusLbl.font = UIFont.boldSystemFont(ofSize: 17.0)
                    statusLbl.textColor = UIColor.white
                    statusView.backgroundColor = UIColor.red
                } else if task.status == COMPLETED {
                    statusLbl.textColor = UIColor.white
                    statusLbl.text = "DONE"
                    statusView.backgroundColor = OPEN_COLOR
                } else if task.status == REMOVED {
                    statusLbl.text = REMOVED
                    statusLbl.textColor = UIColor.white
                    statusView.backgroundColor = UIColor.red
        
                }
        
        
                leftLbl.text = "\(task.peopleNeeded - (task.assigned.count - 1))"
        
        
        
        
    }
}
class MyTaskVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    var taskArray = [Task]()
    var taskDict = [String:Task]()
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var selectedTask: Task!
    var emptyView: EmptyView!
    var expandedArray: [Bool]!
    
//    var emptyView: EmptyView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "My Tasks"
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = MAIN_COLOR
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        // Do any additional setup after loading the view.
        
        emptyView = EmptyView.instanceFromNib(image: UIImage(named: "EmptyWork")!, title: "No task", desc: "Start to post new task today.") as! EmptyView
        emptyView.frame = self.tableView.bounds
        self.tableView.addSubview(emptyView)
        emptyView.showView(show: false, tableView: self.tableView)
        
        fetchMyTask()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func refresh() {
        //        self.tableView.isUserInteractionEnabled = false
        //        displayNoInternetConnection()
        //        let ref = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).child("university")
        //        ref.removeAllObservers()
        
        // self.tableView.isScrollEnabled = false
        
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(reloadDataWhenPulled), userInfo: nil, repeats: false)
        
    }
    
    @objc func reloadDataWhenPulled() {
        
        if self.refreshControl.isRefreshing {
            
            // reloadTable()
            //  self.tableView.reloadData()
            fetchMyTask()
            // self.refreshControl.endRefreshing()
            
            
            
        }
        
    }

    func fetchMyTask() {
        if let uid = DataService.ds.currentUser.userKey {
            DataService.ds.REF_USER_TASK.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                
                self.taskDict.removeAll()
                self.taskArray = []
                
                
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    for snap in snapshots {
                        
                        //print("SNAP: \(snap)")
                        if let dict = snap.value as? Dictionary<String, AnyObject> {
                            
                            
                            
                            let key = snap.key
                            
                            let task = Task(taskKey: key, dict: dict)
                            
                            
                            self.taskDict[key] = task
                            
                            
                            
                            
                            
                        }
                    }
                    self.expandedArray = [Bool](repeating: false, count: self.taskDict.count)
                    self.attemptToReloadTable()
                }
                
                
                //            self.actInd.stopAnimating()
                
            })
        }

    }
    
    func attemptToReloadTable() {
        
        self.taskArray = Array(self.taskDict.values)
        
        self.taskArray.sort(by: { (c1, c2) -> Bool in
            
            return (c1.time.intValue) > (c2.time.intValue)
        })
        
        //        if self.loadingScreen.alpha != 0.0 {
        //            UIView.animate(withDuration: 0.2, delay: 0.0, options:[], animations: { self.loadingScreen.alpha = 0.0 }, completion: nil)
        //        }
        //
        
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        //        self.tableView.isUserInteractionEnabled = true
        //        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        //        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        //        SVProgressHUD.dismiss()
        
        
        if self.taskArray.count == 0 {
            emptyView.showView(show: true, tableView: self.tableView)
        } else {
            emptyView.showView(show: false, tableView: self.tableView)
        }
        
        
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyTaskCell") as? MyTaskCell
        cell?.selectionStyle = .none
        cell?.configureCell(task: taskArray[indexPath.row])
        return cell!
        
//        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                self.selectedTask = taskArray[indexPath.row]
                performSegue(withIdentifier: "MyTaskDetailsVC", sender: nil)

     


    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        if self.expandedArray[indexPath.row] {
//            return 250
//        } else {
//            return 50
//        }
//
//    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? MyTaskDetailsVC {
            destinationVC.task = self.selectedTask
            
        }
    }
}
