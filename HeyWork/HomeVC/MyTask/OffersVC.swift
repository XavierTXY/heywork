//
//  OffersVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 25/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Cosmos
import Firebase

class OfferCell: UITableViewCell {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var profilePicImg: UIImageView!
    @IBOutlet weak var starView: CosmosView!
    @IBOutlet weak var numReviewLbl: UILabel!
    @IBOutlet weak var completionRateLbl: UILabel!
    @IBOutlet weak var offerPriceLbl: UILabel!
    @IBOutlet weak var offerDateLbl: UILabel!
    @IBOutlet weak var offerMsgLbl: UILabel!
    
 
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func configureCell(offer: Offer) {
        
        profilePicImg.layer.cornerRadius = profilePicImg.frame.size.width / 2
        nameLbl.text = offer.offerUserName
        profilePicImg.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: offer.offerUserProfilePicUrl)
        starView.rating = Double(offer.userStarRating)
        numReviewLbl.text = "(\(offer.userNumRating!))"
        
        if offer.userCompletionRate == 0.0 {
            completionRateLbl.text = "No Completion Rate Yet"
        } else {
             completionRateLbl.text = "\(offer.userCompletionRate!) % Completion Rate"
        }
       
        offerPriceLbl.text = "SGD \(offer.offerPrice!)"
        
        let seconds = offer.offerDate.doubleValue
        let date = NSDate(timeIntervalSince1970: seconds)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        offerDateLbl.text = "Completed by: \(dateString)"
        offerMsgLbl.text = offer.offerMsg
    }
}

class OffersVC: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource,PassOfferProtocol {

    var task: Task!
    var offers = [Offer]()
    var offerDict = [String:Offer]()
    var refreshControl: UIRefreshControl!
    
    var selectedUserID: String!
    

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = MAIN_COLOR
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        

        
        fetchOffers()
        
    }
    

    @objc func optionTapped() {
        
    }

    @objc func refresh() {
        //        self.tableView.isUserInteractionEnabled = false
        //        displayNoInternetConnection()
        //        let ref = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).child("university")
        //        ref.removeAllObservers()
        
        // self.tableView.isScrollEnabled = false
        
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(reloadDataWhenPulled), userInfo: nil, repeats: false)
        
    }
    
    @objc func reloadDataWhenPulled() {
        
        if self.refreshControl.isRefreshing {
            
            // reloadTable()
            //  self.tableView.reloadData()
            fetchOffers()
            // self.refreshControl.endRefreshing()
            
            
            
        }
        
    }
    
    func fetchOffers() {
        DataService.ds.REF_TASK_OFFER.child(task.taskKey).observeSingleEvent(of: .value, with: { (snapshot) in
            
            self.offerDict.removeAll()
            self.offers = []
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    
                    //print("SNAP: \(snap)")
                    if let dict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        
                        let key = snap.key
                        
                        let offer = Offer(offerID: key, offerDict: dict)
                        
                        if offer.offerStatus == NOT_WORKING {
                            self.offerDict[key] = offer
                        }
//                        if !offer.offerStatus {
//                            
//                        }
                        
                        
                        
                        
                        
                        
                        
                    }
                }
                self.attemptToReloadTable()
            }
            
            
            //            self.actInd.stopAnimating()
            
        })
    }
    
    func attemptToReloadTable() {
        
        self.offers = Array(self.offerDict.values)
        
        self.offers.sort(by: { (c1, c2) -> Bool in
            
            return (c1.time.intValue) > (c2.time.intValue)
        })
        
        //        if self.loadingScreen.alpha != 0.0 {
        //            UIView.animate(withDuration: 0.2, delay: 0.0, options:[], animations: { self.loadingScreen.alpha = 0.0 }, completion: nil)
        //        }
        //
        
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        //        self.tableView.isUserInteractionEnabled = true
        //        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        //        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        //        SVProgressHUD.dismiss()
        
        
        //        if self.confessions.count == 0 {
        //            if self.mainConfession == nil {
        //                self.showEmptyView()
        //            }
        //            //            self.showEmptyView()
        //        } else {
        //            self.hideEmptyView()
        //        }
        
        
        self.tableView.reloadData()
    }
    
    func sendOfferToPreviousVC(offerID: String, task: Task) {
        self.task = task
        self.offerDict.removeValue(forKey: offerID)
        self.attemptToReloadTable()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OfferCell") as? OfferCell
        cell?.selectionStyle = .none
        cell?.configureCell(offer: offers[indexPath.row])
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offers.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        Checker.checker.userIDForProfileVC = offers[indexPath.row].offerUserID
//        Checker.checker.taskForProfileVC = task
//        VCSegue().segueVC(bundleName: "ProfileSB", controllerName: "ProfileVC", vc: self)
        
        var storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        controller.userID = offers[indexPath.row].offerUserID
        controller.task = task
        controller.offer = offers[indexPath.row]
        controller.delegate = self
        controller.hasAssignBar = true
        self.present(controller, animated: true, completion: nil)
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return "Offer"
    }

}
