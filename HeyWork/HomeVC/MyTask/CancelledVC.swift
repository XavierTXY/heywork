//
//  WorkingVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 21/3/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import UIKit
import Firebase
import XLPagerTabStrip

class CancelledCell: UITableViewCell {
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var completedDate: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(offer: Offer) {
        profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: offer.offerUserProfilePicUrl)
        usernameLbl.text = offer.offerUserName
        
        let seconds = offer.offerDate.doubleValue
        let date = NSDate(timeIntervalSince1970: seconds)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        completedDate.text = "Target Completion Date: \(dateString)"
        
        status.text = offer.offerStatus
        priceLbl.text = "SGD\(offer.offerPrice!)"
    }
}

class CancelledVC: UIViewController, UITableViewDelegate, UITableViewDataSource,IndicatorInfoProvider {
    @IBOutlet weak var tableView: UITableView!
    
    var cancelledOffer = [Offer]()
    var task: Task!
    
    var completed: Bool!
//    var assigns = [Offer]()
    var cancelledDict = [String:Offer]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
        
        
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        fetchCancelled()
        
    }
    
    func fetchCancelled() {
        DataService.ds.REF_TASK_ASSIGN.child(task.taskKey).observeSingleEvent(of: .value, with: { (snapshot) in
            
            self.cancelledDict.removeAll()
            self.cancelledOffer = []
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    
                    //print("SNAP: \(snap)")
                    if let dict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        
                        let assignedUserID = snap.key
                        
                        let assign = Offer(offerID: assignedUserID, offerDict: dict)
                        if self.completed {
                            if assign.offerStatus == COMPLETED || assign.offerStatus == TASKER_REVIEWED || assign.offerStatus == POSTER_REVIEWED || assign.offerStatus == BOTH_REVIEWED || assign.offerStatus == PAID {
                                self.cancelledDict[assignedUserID] = assign
                            }
                        } else {
                            if assign.offerStatus == CANCELLED {
                                self.cancelledDict[assignedUserID] = assign
                            }
                        }
                        
                        
                        
                    }
                }
                self.attemptToReloadTable()
            }
            
            
            //            self.actInd.stopAnimating()
            
        })
    }
    
    func attemptToReloadTable() {
        
        self.cancelledOffer = Array(self.cancelledDict.values)
        
        self.cancelledOffer.sort(by: { (c1, c2) -> Bool in
            
            return (c1.time.intValue) > (c2.time.intValue)
        })
        
        //        if self.loadingScreen.alpha != 0.0 {
        //            UIView.animate(withDuration: 0.2, delay: 0.0, options:[], animations: { self.loadingScreen.alpha = 0.0 }, completion: nil)
        //        }
        //
        
//        if refreshControl.isRefreshing {
//            refreshControl.endRefreshing()
//        }
        //        self.tableView.isUserInteractionEnabled = true
        //        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        //        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        //        SVProgressHUD.dismiss()
        
        
        //        if self.confessions.count == 0 {
        //            if self.mainConfession == nil {
        //                self.showEmptyView()
        //            }
        //            //            self.showEmptyView()
        //        } else {
        //            self.hideEmptyView()
        //        }
        
        
        self.tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cancelledOffer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CancelledCell") as? CancelledCell
        cell?.selectionStyle = .none
        cell?.configureCell(offer: cancelledOffer[indexPath.row])
        return cell!
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        if completed {
            return "Completed"
        } else {
            return "Cancelled"
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
