//
//  MyTaskDetailsVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 24/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
//import ExpandableCell
import XLPagerTabStrip

class MyTaskDetailsVC:ButtonBarPagerTabStripViewController/*,ExpandableDelegate*/ {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
   
    var task: Task!
//    var cell: UITableViewCell {
//        return tableView.dequeueReusableCell(withIdentifier: OfferCell.ID)!
//    }

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {

        
        settings.style.buttonBarBackgroundColor = .white
//            UIColor(red: 238/255, green: 239/255, blue: 240/255, alpha: 1.0)
        settings.style.buttonBarItemBackgroundColor = .white
//            UIColor(red: 238/255, green: 239/255, blue: 240/255, alpha: 1.0)
        settings.style.selectedBarBackgroundColor = MAIN_COLOR
        settings.style.buttonBarItemFont = UIFont(name: "OpenSans-Bold", size: 14)!
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.6)
            newCell?.label.textColor = MAIN_COLOR
        }
        super.viewDidLoad()
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "OpenSans-SemiBold", size: 18)!]
        self.navigationItem.title = "ACTIVITY";
        
        let moreBtn = UIBarButtonItem(image: UIImage(named: "optionWhite"), style: .plain, target: self, action: #selector(optionTapped))// action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = moreBtn
//        tableView.expandableDelegate = self
//        tableView.animation = .automatic
//        tableView.register(UINib(nibName: "OfferCell", bundle: nil), forCellReuseIdentifier: OfferCell.ID)
//        tableView.register(UINib(nibName: "ExpandableCell", bundle: nil), forCellReuseIdentifier: ExpandableCell2.ID)
//
////        tableView.estimatedRowHeight = 44
//        tableView.rowHeight = UITableView.automaticDimension
//
//        titleLbl.text = task.title
//        statusLbl.text = task.status
//        priceLbl.text = "RM\(task.budget)"
        

        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "OffersVC") as! OffersVC
        child_1.task = task
        let child_2 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AssignVC") as! AssignVC
        child_2.task = task
        let child_3 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CancelledVC") as! CancelledVC
        child_3.task = task
        child_3.completed = true
        let child_4 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CancelledVC") as! CancelledVC
        child_4.task = task
        child_4.completed = false
        return [child_1, child_2,child_3,child_4]
    }
    
    @objc func optionTapped() {
        

            let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            
            let reportAction = UIAlertAction(title: "Remove Task", style: .destructive) { (action) in
                //                self.displayReportAlert(taskID: taskID, userID: userID)
                self.displayRemoveAlert()
            }
            
            optionMenu.addAction(reportAction)
            optionMenu.addAction(cancelAction)
        
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
            self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    func displayRemoveAlert() {
        
        //        let alert = UIAlertController()
        
        let alert = UIAlertController(title: "Are you sure?", message: "Choose Option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler:{ (UIAlertAction)in
            
        }))
        
        alert.addAction(UIAlertAction(title: "Remove Task", style: UIAlertAction.Style.destructive, handler:{ (UIAlertAction)in
            self.submitRemove()
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func submitRemove() {
        print(task.taskRef)
        DataService.ds.REF_TASK.child(self.task.taskKey).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                if let dict = snapshot.value as? Dictionary<String, AnyObject> {
                    
                    
                    let key = snapshot.key
                    
                    let task = Task(taskKey: key, dict: dict)
                    
                    if task.assigned.count == 1 {
                        
                        self.task.removeFromFirebase()
                        self.navigationController?.popViewController(animated: true)
                        
                    } else {
                        
                        var isSomeoneWorking = false
                        for (id, assigned) in task.assigned {
                            
                            if id != "key" {
                                if assigned == false {
                                    isSomeoneWorking = true
                                    
                                    
                                }
                            }
                        }
                        
                        if isSomeoneWorking {
                            ErrorAlert().showAlert(title: "Can't Remove", msg: "Someone is still working in this task", object: self)
                        } else {
                            //remove
                            
                            self.task.removeFromFirebase()
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    
                }
            } else {
                ErrorAlert().showAlert(title: "Can't Remove", msg: "This task is completed", object: self)
            }
        })
        
        
    }

//    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
//        switch indexPath.section {
//        case 0:
//            switch indexPath.row {
//            case 0,1:
//                let cell1 = tableView.dequeueReusableCell(withIdentifier: OfferCell.ID) as! OfferCell
////                cell1.titleLabel.text = "First Expanded Cell"
//                let cell2 = tableView.dequeueReusableCell(withIdentifier: OfferCell.ID) as! OfferCell
////                cell2.titleLabel.text = "Sceond Expanded Cell"
//                let cell3 = tableView.dequeueReusableCell(withIdentifier: OfferCell.ID) as! OfferCell
////                cell3.titleLabel.text = "Third Expanded Cell"
//                return [cell1, cell2, cell3]
//
//            case 2:
//                return [cell, cell]
//            case 3:
//                return [cell]
//
//            default:
//                break
//            }
//        default:
//            break
//        }
//        return nil
//    }
//
//
//    func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
//        switch indexPath.section {
//        case 0:
//            switch indexPath.row {
//            case 0:
//                return [UITableView.automaticDimension, UITableView.automaticDimension, UITableView.automaticDimension]
//            case 1:
//                return [44, 44, 44]
//            case 2:
//                return [33, 33, 33]
//
//            case 3:
//                return [22]
//
//            default:
//                break
//            }
//        default:
//            break
//        }
//        return nil
//
//    }
//
//    func numberOfSections(in tableView: ExpandableTableView) -> Int {
//        return 1
//    }
//
//    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
//        return 3
//    }
//
//    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAt indexPath: IndexPath) {
//        //        print("didSelectRow:\(indexPath)")
//    }
//
//    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectExpandedRowAt indexPath: IndexPath) {
//        //        print("didSelectExpandedRowAt:\(indexPath)")
//    }
//
//    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCell: UITableViewCell, didSelectExpandedRowAt indexPath: IndexPath) {
//        if let cell = expandedCell as? OfferCell {
////            print("\(cell.titleLabel.text ?? "")")
//        }
//    }
//
//    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        switch indexPath.section {
//        case 0:
//            switch indexPath.row {
//            case 0, 1,4,2, 3:
//                guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: ExpandableCell2.ID) else { return UITableViewCell() }
//                return cell
////
////            case 1, 4:
////                guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: NormalCell.ID)  else { return UITableViewCell() }
////                return cell
//
//            default:
//                break
//            }
//        case 1:
//            switch indexPath.row {
//            case 0, 1, 2, 3, 4:
//                guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: NormalCell.ID) else { return UITableViewCell() }
//                return cell
//
//            default:
//                break
//            }
//        default:
//            break
//        }
//
//        return UITableViewCell()
//    }
//
//    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
////        return UITableView.automaticDimension
//        switch indexPath.section {
//        case 0:
//            switch indexPath.row {
//            case 0, 2, 3:
//                return 66
//
//            case 1, 4:
//                return 55
//
//            default:
//                break
//            }
//        case 1:
//            switch indexPath.row {
//            case 0, 1, 2, 3, 4:
//                return 55
//
//            default:
//                break
//            }
//        default:
//            break
//        }
//
//        return 44
//    }
//
//    func expandableTableView(_ expandableTableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
////        let cell = expandableTableView.cellForRow(at: indexPath)
////        cell?.contentView.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
////        cell?.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
//    }
//
//    func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }

}
