//
//  AssignVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 1/11/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import Firebase
import XLPagerTabStrip

class AssignCell: UITableViewCell {
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(offer: Offer) {
        profilePic.layer.cornerRadius = profilePic.frame.size.width / 2
        profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: offer.offerUserProfilePicUrl)
        nameLbl.text = offer.offerUserName
        
        let seconds = offer.offerDate.doubleValue
        let date = NSDate(timeIntervalSince1970: seconds)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        dateLbl.text = "Completed by: \(dateString)"
        priceLbl.text = "SGD \(offer.offerPrice!)"
        
        msgLbl.text = offer.offerMsg
        
        if offer.offerStatus == WORKING {
            statusLbl.text = WORKING
        } else if offer.offerStatus == REQUEST {
            statusLbl.text = REQUEST
        } else if offer.offerStatus == PAID {
            statusLbl.text = PAID
        } else if offer.offerStatus == COMPLETED {
            statusLbl.text = COMPLETED
        } else if offer.offerStatus == REQUEST_CANCEL {
            statusLbl.text = "Waitng tasker to response"
        } else if offer.offerStatus == CANCELLED {
            statusLbl.text = "Cancelled"
        }
        
    }
}

class AssignVC: UIViewController,IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource {

    var task: Task!
    var assigns = [Offer]()
    var assignDict = [String:Offer]()
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = MAIN_COLOR
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        fetchAssigns()
        
    }
    
    
    @objc func refresh() {
        //        self.tableView.isUserInteractionEnabled = false
        //        displayNoInternetConnection()
        //        let ref = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).child("university")
        //        ref.removeAllObservers()
        
        // self.tableView.isScrollEnabled = false
        
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(reloadDataWhenPulled), userInfo: nil, repeats: false)
        
    }
    
    @objc func reloadDataWhenPulled() {
        
        if self.refreshControl.isRefreshing {
            
            // reloadTable()
            //  self.tableView.reloadData()
            fetchAssigns()
            // self.refreshControl.endRefreshing()
            
            
            
        }
        
    }
    
    func fetchAssigns() {
        DataService.ds.REF_TASK_ASSIGN.child(task.taskKey).observeSingleEvent(of: .value, with: { (snapshot) in
            
            self.assignDict.removeAll()
            self.assigns = []
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    
                    //print("SNAP: \(snap)")
                    if let dict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        
                        let assignedUserID = snap.key
                        
                        let assign = Offer(offerID: assignedUserID, offerDict: dict)
                        
                        if assign.offerStatus == WORKING || assign.offerStatus == REQUEST || assign.offerStatus == REQUEST_CANCEL {
                           self.assignDict[assignedUserID] = assign
                        }

                        
                    }
                }
                self.attemptToReloadTable()
            }
            
            
            //            self.actInd.stopAnimating()
            
        })
    }
    
    func attemptToReloadTable() {
        
        self.assigns = Array(self.assignDict.values)
        
        self.assigns.sort(by: { (c1, c2) -> Bool in
            
            return (c1.time.intValue) > (c2.time.intValue)
        })
        
        //        if self.loadingScreen.alpha != 0.0 {
        //            UIView.animate(withDuration: 0.2, delay: 0.0, options:[], animations: { self.loadingScreen.alpha = 0.0 }, completion: nil)
        //        }
        //
        
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        //        self.tableView.isUserInteractionEnabled = true
        //        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        //        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        //        SVProgressHUD.dismiss()
        
        
        //        if self.confessions.count == 0 {
        //            if self.mainConfession == nil {
        //                self.showEmptyView()
        //            }
        //            //            self.showEmptyView()
        //        } else {
        //            self.hideEmptyView()
        //        }
        
        
        self.tableView.reloadData()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return assigns.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CancelledCell") as? CancelledCell
        cell?.selectionStyle = .none
        cell?.configureCell(offer: assigns[indexPath.row])
        return cell!
    
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        

        // 2
        let viewAction = UIAlertAction(title: "View Profile", style: .default) { (action) in
            self.goProfile(assign: self.assigns[indexPath.row])
            
        }
        
        optionMenu.addAction(viewAction)
        
        let msgAction = UIAlertAction(title: "Message Tasker", style: .default) { (action) in
            let chatVC = UIStoryboard(name: "MessageSB", bundle: nil).instantiateViewController(withIdentifier: "ChatLogVC") as! ChatLogVC
            chatVC.userID = self.assigns[indexPath.row].offerUserID
            chatVC.userName = self.assigns[indexPath.row].offerUserName
            //            chatVC.offer = self.assigns[indexPath.row]
            chatVC.taskID = self.task.taskKey
            //            self.sendAssign(values: ["text": "Hey there! I have assigned you this task!"], userID:self.assigns[indexPath.row].offerUserID, offer: self.assigns[indexPath.row])
            self.navigationController?.pushViewController(chatVC, animated: true)
            //            self.present(chatVC, animated: true, completion: nil)
        }
        
        optionMenu.addAction(msgAction)
        
        if self.assigns[indexPath.row].offerStatus == COMPLETED || self.assigns[indexPath.row].offerStatus == PAID || self.assigns[indexPath.row].offerStatus == CANCELLED {
            ErrorAlert.errorAlert.showAlert(title: "Can't be done", msg: "This assignment has been cancelled", object: self)
        } else if self.assigns[indexPath.row].offerStatus == REQUEST_CANCEL {
            ErrorAlert.errorAlert.showAlert(title: "Can't be done", msg: "Waiting tasker to response", object: self)
        } else {
            
            let kickAction = UIAlertAction(title: "Remove from Task", style: .destructive) { (action) in
                let alert = UIAlertController(title: "Why do you want to cancel?", message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
                alert.addTextField(configurationHandler: { textField in
                    textField.placeholder = "Input your reason here..."
                })
                
                alert.addAction(UIAlertAction(title: "Remove User", style: .destructive, handler: { action in
                    
                    if alert.textFields?.first?.text?.count != 0 {
                        if let reason = alert.textFields?.first?.text {
                            
                            var assign = self.assigns[indexPath.row]
                            
                            DataService.ds.REF_USERS.child(assign.offerID).observeSingleEvent(of: .value, with: { (snapshot) in
                                
                                //print("SNAP: \(snap)")
                                if let dict = snapshot.value as? Dictionary<String, AnyObject> {
                                    
                                    
                                    
                                    let key = snapshot.key
                                    
                                    var offererUser = User(userKey: key, userData: dict)
                                    
                                    DataService.ds.REF_REQUEST_TO_CANCEL.child(self.task.taskKey).child(self.assigns[indexPath.row].offerID).updateChildValues(["status":REQUEST_CANCEL, "createdTime": NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970)), "reason": reason, "response": "", "respondedTime":""])
                                    
                                    self.sendAssign(values: ["text": "I have cancelled this task from you."], userID: assign.offerUserID, offer: assign, offererUser: offererUser)
                                    
                                    self.assigns[indexPath.row].changeState(taskKey: self.task.taskKey, id: self.assigns[indexPath.row].offerID, newStatus: REQUEST_CANCEL) {
                                        self.tableView.reloadData()
                                        
                                    }

                                    
                                }
                            })
                            
                            
                        }
                    }
                    
                }))
                
                if let popoverController = alert.popoverPresentationController {
                    popoverController.sourceView = self.view
                    popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                    popoverController.permittedArrowDirections = []
                }
                
                self.present(alert, animated: true)
                
                
            }

            optionMenu.addAction(kickAction)
        }
        


        
        // 3
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        // 4
        
       
        
        
        optionMenu.addAction(cancelAction)
        
        // 5
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(optionMenu, animated: true, completion: nil)
    }

    

    
    func goProfile(assign: Offer) {
        var storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        controller.userID = assign.offerUserID
        controller.task = task
        controller.offer = assign
        controller.hasAssignBar = false
      
        
        self.present(controller, animated: true, completion: nil)
    }

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return "Working"
    }
    
    func sendAssign(values: [String: Any], userID: String, offer: Offer, offererUser: User)  {
        
        
        
        
        let ref = DataService.ds.REF_MESSAGES
        let firebaseMessage = DataService.ds.REF_MESSAGES.childByAutoId()
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        let toId = userID
        let fromId = DataService.ds.currentUser.userKey!
        
        print("FROM ID: \(fromId)")
        
        offererUser.adjustMsgCount(addCount: true, fromId: "\(self.task.taskKey!)_\(DataService.ds.currentUser.userKey!)")
        
        
        
        var dict: [String: Any] = ["toId": toId, "fromId": fromId, "timeStamp": timestamp, "fromUserName": DataService.ds.currentUser.userName, "fromSeen": true, "toSeen": false ] as [String : Any]
        
        
        
        
        for (key, element) in values {
            
            dict[key] = element
            print(dict)
        }
        // values.forEach({dict[$0] = $1 })
        //      firebaseMessage.updateChildValues(values)
        
        firebaseMessage.updateChildValues(dict, withCompletionBlock: {(error, ref) in
            
            if error != nil {
                print(error)
            }
            
            //            self.inputContainerView.inputTextField.text = nil
            
            let userMsgRef = DataService.ds.REF_USER_MESSAGES.child(fromId).child(self.task.taskKey).child(toId)
            let msgId = firebaseMessage.key
            userMsgRef.updateChildValues([msgId: true])
            
            let recipientUserMsgRef = DataService.ds.REF_USER_MESSAGES.child(toId).child(self.task.taskKey).child(fromId)
            recipientUserMsgRef.updateChildValues([msgId: true])
            
            
        })
        //
        
    }
}
