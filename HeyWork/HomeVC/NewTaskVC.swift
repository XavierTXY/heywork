//
//  NewTaskVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 9/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit

class AddTaskCell: UICollectionViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var bgImg: UIImageView!
    //    @IBOutlet weak var subTitleLbl: UILabel!
    
    @IBOutlet weak var roundView: RoundView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(title: String, bg: UIImage) {
        titleLbl.text = title
        bgImg.image = bg
//        subTitleLbl.text = subTitle
    }
}
class NewTaskVC: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    var category = CATEGORIES
//    var subtitle = ["Graphics & Design", "Writing & Translation", "Programming & Tech", "Business","Media" , "Other"]
    var selectedCat: String!
    var selectedIdx: Int!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Post New Task"
        // Do any additional setup after loading the view.
        collectionView.delegate = self
        collectionView.dataSource = self
        
//        tableView.tableFooterView = UIView()
        UIView.animate(views: collectionView.visibleCells, animations: TABLE_ANIMATION, completion: {
            
        })
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        self.navigationController?.isNavigationBarHidden = true
        
    }

    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CATEGORIES.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddTaskCell", for: indexPath) as? AddTaskCell
//        cell?.roundView.backgroundColor = CATEGORY_COLORS[indexPath.row]
        cell?.configureCell(title: category[indexPath.row], bg: UIImage(named: CATEGORY_IMAGES[indexPath.row])!)
//        cell?.configureCell(title: category[indexPath.row], bg: UIImage())
        //        cell.selectionStyle = .none
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCat = category[indexPath.row]
        selectedIdx = indexPath.row
        if QUESTIONS[indexPath.row].count == 0 {
            self.performSegue(withIdentifier: "SummaryVC", sender: nil)
        } else {
            self.performSegue(withIdentifier: "TaskSelectionVC", sender: nil)
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/2.0
        let yourHeight = yourWidth

        return CGSize(width: yourWidth, height: yourHeight)
    }
//
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "AddTaskCell") as? AddTaskCell
//        cell?.selectionStyle = .none
//        cell?.configureCell(title: category[indexPath.row], subTitle: subtitle[indexPath.row])
//        return cell!
//    }
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return category.count
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        selectedCat = category[indexPath.row]
//        self.performSegue(withIdentifier: "SummaryVC", sender: nil)
//    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? SummaryVC {
            destinationVC.category = self.selectedCat
            
        }
        
        if let destinationVC = segue.destination as? TaskSelectionVC {
            destinationVC.categoryIndex = self.selectedIdx
            destinationVC.category = self.selectedCat
            
        }
    }
}
