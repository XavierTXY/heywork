//
//  MapViewVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 23/5/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MapKit


class MapViewVC: UIViewController, GMSMapViewDelegate {

    @IBOutlet weak var googleMap: GMSMapView!
    var task: Task!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        googleMap.delegate = self
        googleMap.isIndoorEnabled = false
        // Do any additional setup after loading the view.
        updateMapWithMarkers()
    }
    
    func updateMapWithMarkers() {
        
        DispatchQueue.main.async {
            let marker = GMSMarker()
            marker.icon = UIImage(named: "map-marker")
            marker.position = CLLocationCoordinate2D(latitude: self.task.place!.latitude, longitude: self.task.place!.longitude)
            //        marker.title = task.title
            //        marker.snippet = task.desc
            marker.userData = self.task
            marker.map = self.googleMap
            
            self.googleMap.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: self.task.place!.latitude, longitude: self.task.place!.longitude), zoom: 15, bearing: 0, viewingAngle: 0)
        }
        
        
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func optionTapped(_ sender: Any) {

        
        let alert = UIAlertController(title: "Map Selection", message: "Choose Navigation App", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Apple Map", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            
            self.openAppleMap()
        }))
        
        alert.addAction(UIAlertAction(title: "Google Map", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            self.openGoogleMap()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler:{ (UIAlertAction)in
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true, completion: nil)
    }

    private func openGoogleMap() {
        let lat = self.task.place!.latitude!
        let long = self.task.place!.longitude!
        
        let url = "comgooglemaps://?center=\(lat),\(long)&zoom=14"
//        "comgooglemaps://?center=\(lat),\(long)&zoom=14"
        print(url)
        UIApplication.shared.openURL(URL(string:url)!)
    }
    
    private func openAppleMap() {
        
        let latitude:CLLocationDegrees =  self.task.place!.latitude
        let longitude:CLLocationDegrees =  self.task.place!.longitude
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.openInMaps(launchOptions: options)
    }

}
