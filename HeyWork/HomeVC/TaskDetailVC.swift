//
//  TaskDetailVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 15/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import TagListView
import XLPagerTabStrip
import GoogleMaps
import CoreLocation
import SVProgressHUD
import Firebase

class TaskDetailAttachmentCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(imgUrl: String) {
        imgView.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: imgUrl)
    }
}


class TaskDetailVC: BaseViewController, UIScrollViewDelegate,IndicatorInfoProvider, GMSMapViewDelegate {

    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var openView: UIView!
    @IBOutlet weak var detailTextView: UITextView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var nopeopleLbl: UILabel!
    @IBOutlet weak var budgetLbl: UILabel!
    @IBOutlet weak var tagView: TagListView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mapStack: UIStackView!
    @IBOutlet weak var attachmentStack: UIStackView!
    
    var startingImgView: UIImageView?
    var originalFrame : CGRect?
    var startingFrame : CGRect?
    var blackBlackgroundView: UIView?
    
    
    var task: Task!
    var offerBar: OfferBar!
    
    let cellIdentifier = "postCell"
    var blackTheme = false
    var itemInfo = IndicatorInfo(title: "View")
    var enableBottom = true

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
        collectionView.dataSource = self
        openView.layer.cornerRadius = 10
        mapView.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.profileTapped(sender:)))
        nameLbl.addGestureRecognizer(tap)
        nameLbl.isUserInteractionEnabled = true
        
//        if DataService.ds.currentUser.userKey! != self.task.posterID {
            let moreBtn = UIBarButtonItem(image: UIImage(named: "optionWhite"), style: .plain, target: self, action: #selector(moreTapped))
            self.navigationItem.rightBarButtonItem  = moreBtn
//        } else {
//
//        }
      
        
        scrollView.delegate = self
        titleLbl.text = task.title
        
        statusLbl.text = task.status
        
        if task.status == OPEN {
            //            statusLbl.font = UIFont.boldSystemFont(ofSize: 17.0)
//            statusLbl.textColor = OPEN_COLOR
            openView.backgroundColor = OPEN_COLOR
        } else if task.status == CLOSED {
            //            statusLbl.font = UIFont.boldSystemFont(ofSize: 17.0)
//            statusLbl.textColor = UIColor.red
            openView.backgroundColor = UIColor.red
        } else if task.status == COMPLETED {
//            statusLbl.textColor = MAIN_COLOR
             openView.backgroundColor = MAIN_COLOR
        }
        
        let seconds = task.deadline.doubleValue
        let date = NSDate(timeIntervalSince1970: seconds)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        dateLbl.text = dateString
        nameLbl.text = task.posterUsername
        descLbl.text = task.desc
        
        if task.peopleNeeded == 1 {
            nopeopleLbl.text = "\(task.peopleNeeded!) tasker"
        } else {
            nopeopleLbl.text = "\(task.peopleNeeded!) taskers"
        }
       
        budgetLbl.text = "SGD \(task.budget!)"
        
        for (skill, bo) in task.skills {
            if skill != "key" {
                tagView.addTag(skill)
            }
            
        }
//        self.loadMap()
//        self.changeBtnColor()
        

        
        
//        let now = Date()
//        let endDate = now.addingTimeInterval(24 * 3600 * 17)
//        
//        let formatter = DateComponentsFormatter()
//        formatter.allowedUnits = [.day, .weekOfMonth]
//        formatter.unitsStyle = .full
//        let string = formatter.string(from: now, to: endDate)!
        
        
    }
    

    override func viewWillLayoutSubviews() {
        roundView.clipsToBounds = true
        //            roundView.layer.cornerRadius = 10
        
        let path = UIBezierPath(roundedRect:roundView.bounds,
                                byRoundingCorners:[.topRight, .topLeft],
                                cornerRadii: CGSize(width: 40, height:  40))
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        roundView.layer.mask = maskLayer
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = tagView.frame.size.height + detailTextView.frame.size.height + 300
        scrollView.contentInset = contentInset
        
        if self.task.place != nil {
            self.loadMap()
            
        } else {
            self.mapStack.isHidden = true
            //            self.roundView.layoutIfNeeded()
        }
        
        if self.task.attachmentUrls.count == 0 {
            self.attachmentStack.isHidden = true
        } else {
            self.attachmentStack.isHidden = false
        }
        
        
    }
    func loadMap() {

        
        if let place = self.task.place {
            self.mapStack.isHidden = false
            // Create a GMSCameraPosition that tells the map to display the
            // coordinate -33.86,151.20 at zoom level 6.
//            roundView.clipsToBounds = true
////            roundView.layer.cornerRadius = 10
//
//            let path = UIBezierPath(roundedRect:roundView.bounds,
//                                    byRoundingCorners:[.topRight, .bottomLeft],
//                                    cornerRadii: CGSize(width: 20, height:  20))
//
//            let maskLayer = CAShapeLayer()
//
//            maskLayer.path = path.cgPath
//            roundView.layer.mask = maskLayer
            
            let camera = GMSCameraPosition.camera(withLatitude: place.latitude, longitude: place.longitude, zoom: 16.0)
            //        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            mapView.camera = camera
            
            // Creates a marker in the center of the map.
            let marker = GMSMarker()
            marker.icon = UIImage(named: "map-marker")
            marker.position = CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude)
//            marker.title = "Sydney"
            //        marker.snippet = "Australia"
            marker.map = mapView
            
            mapView.settings.scrollGestures = false
            mapView.settings.zoomGestures = false
            mapView.layer.cornerRadius = 8
            

        }
        
        

//
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if enableBottom {
            addAccessoryView()
        }
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        if let parentVC = self.parent {
            if let parentVC = parentVC as? Job {
                
                self.navigationController?.isNavigationBarHidden = true
                //        self.navigationController?.navigationBar.shouldRemoveShadow(true)
                self.navigationController?.navigationBar.shouldRemoveShadow(true)
                // parentVC is someViewController
            }
        }
    }
    
    
    @objc func moreTapped() {
        
        if DataService.ds.currentUser.userKey! != self.task.posterID {
            super.showAlert(taskID: self.task.taskKey, userID: nil)
        } else {
            let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            
            let reportAction = UIAlertAction(title: "Remove Task", style: .destructive) { (action) in
//                self.displayReportAlert(taskID: taskID, userID: userID)
                self.displayRemoveAlert()
            }
            
            optionMenu.addAction(reportAction)
            optionMenu.addAction(cancelAction)
            
            if let popoverController = optionMenu.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
            
            self.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    func displayRemoveAlert() {

//        let alert = UIAlertController()

        let alert = UIAlertController(title: "Are you sure?", message: "Choose Option", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler:{ (UIAlertAction)in

        }))

        alert.addAction(UIAlertAction(title: "Remove Task", style: UIAlertAction.Style.destructive, handler:{ (UIAlertAction)in
            self.submitRemove()

        }))

        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func submitRemove() {
        print(task.taskRef)
        DataService.ds.REF_TASK.child(self.task.taskKey).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                if let dict = snapshot.value as? Dictionary<String, AnyObject> {
                    
                    
                    let key = snapshot.key
        
                    let task = Task(taskKey: key, dict: dict)
                    
                    if task.assigned.count == 1 {
                        
                        self.task.removeFromFirebase()
                        self.navigationController?.popViewController(animated: true)
                        
                    } else {
                        
                        var isSomeoneWorking = false
                        for (id, assigned) in task.assigned {
                            
                            if id != "key" {
                                if assigned == false {
                                    isSomeoneWorking = true
                                    
                                    
                                }
                            }
                        }
                        
                        if isSomeoneWorking {
                            ErrorAlert().showAlert(title: "Can't Remove", msg: "Someone is still working in this task", object: self)
                        } else {
                            //remove
                            
                            self.task.removeFromFirebase()
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    
                }
            } else {
                ErrorAlert().showAlert(title: "Can't Remove", msg: "This task is completed", object: self)
            }
        })
        
        
    }
    
    @objc func profileTapped(sender:UITapGestureRecognizer) {
    
        var storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        controller.userID = task.posterID
//        controller.task = task
//        controller.offer = assign
        controller.hasAssignBar = false
        
        self.present(controller, animated: true, completion: nil)
        
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.performSegue(withIdentifier: "MapViewVC", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    public func addAccessoryView() {
        
        offerBar = Bundle.main.loadNibNamed("OfferBar", owner: self, options: nil)?.first as! OfferBar
        offerBar.detailVC = self
        detailTextView.inputAccessoryView = offerBar
        detailTextView.becomeFirstResponder()
        
        
        
        
        
        
        changeBtnColor()
        
    }
    
    func changeBtnColor() {
        
        if task.assigned.count - 1 != 0 {
            offerBar.titleLbl.text = "Current Offer: \(task.offered.count - 1 - (task.assigned.count - 1))\nCurrently Assigned: \(task.assigned.count - 1)"
        } else {
            offerBar.titleLbl.text = "Current Offer: \(task.offered.count - 1)\nCurrently Assigned: \(task.assigned.count - 1)"
        }
        
        
        if task.offered[DataService.ds.currentUser.userKey!] == nil {
             offerBar.changeState(userExist: false)
        } else {
       
            offerBar.changeState(userExist: true)
        }
    }
    
    func offerTapped() {
        
        
        if task.posterID == DataService.ds.currentUser.userKey! {
            ErrorAlert.errorAlert.showAlert(title: "Warning", msg: "You posted this task.", object: self)
        } else {
//
            if DataService.ds.currentUser.bankAccount != nil {
                if task.offered[DataService.ds.currentUser.userKey!] == nil {
                    
                    Checker.checker.taskForOffer = self.task
                    self.performSegue(withIdentifier: "OfferPriceVC", sender: nil)
//                    task.updateOffer(taskerID: DataService.ds.currentUser.userKey!, add: true) {
//                        self.changeBtnColor()
//                    }
                    
                } else {
                    ErrorAlert.errorAlert.showAlert(title: "Got it!", msg: "You have already made an offer.", object: self)
                }
               
        
            } else {
                print("not verified")
                Checker.checker.verificationMode = false
                VCSegue().segueVC(bundleName: "VerificationSB", controllerName: "VerificationVC", vc: self)
            }

        }

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let d = segue.destination as? OfferPriceVC {
            print("hey")
            d.task! = self.task!
        }
        
        if let d = segue.destination as? MapViewVC {
            d.task = self.task!
        }
    }
}

extension TaskDetailVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TaskDetailAttachmentCell", for: indexPath) as! TaskDetailAttachmentCell
        DispatchQueue.main.async() {
            
            cell.configureCell(imgUrl: self.task!.attachmentUrls[indexPath.row])
            
        }

        return cell
   
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! TaskDetailAttachmentCell
        self.performZoomInForStartingImageView(startingImageView: cell.imgView)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
//        if task.attachmentUrls != nil {
            return task.attachmentUrls.count
//        } else {
//            return 0
//        }
    }
    
    
    
    
    
    func performZoomInForStartingImageView(startingImageView: UIImageView) {
        
//        if self.inputContainerView.inputTextField.isFirstResponder {
//            self.inputContainerView.inputTextField.resignFirstResponder()
//        }
        
        startingFrame = startingImageView.superview?.convert(startingImageView.frame, to: nil)
        startingImgView = startingImageView
        startingImgView?.isHidden = true
        
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.contentMode = .scaleAspectFit
        zoomingImageView.clipsToBounds = true
        zoomingImageView.image = startingImageView.image
        zoomingImageView.isUserInteractionEnabled = true
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
        
        
        if let keyWindow = UIApplication.shared.keyWindow {
            blackBlackgroundView = UIView(frame: keyWindow.frame)
            blackBlackgroundView?.backgroundColor = UIColor.black
            blackBlackgroundView?.alpha = 0
            keyWindow.addSubview(blackBlackgroundView!)
            keyWindow.addSubview(zoomingImageView)
            
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackBlackgroundView?.alpha = 1
//                self.inputContainerView.alpha = 0
                let height = (self.startingFrame?.height)! / (self.startingFrame?.width)! * keyWindow.frame.width
                
                //zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                
                zoomingImageView.center = keyWindow.center
            }, completion: { (comepleted: Bool) in
                self.offerBar.isHidden = true
            })
        }
        
    }
    
    @objc func handleZoomOut(sender: UITapGestureRecognizer) {
        if let zoomOutImageView = sender.view {
            zoomOutImageView.layer.cornerRadius = 16
            zoomOutImageView.clipsToBounds = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                zoomOutImageView.frame = self.startingFrame!
                self.blackBlackgroundView?.alpha = 0
                
//                self.inputContainerView.alpha = 1
                
            }, completion: { (comepleted: Bool) in
                self.startingImgView?.isHidden = false
                self.offerBar.isHidden = false
                zoomOutImageView.removeFromSuperview()
                
            })
            
            
            
        }
    }
    
}
