//
//  JobsVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 6/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import Firebase
import InstantSearchClient
import Hero
import Spring
import GoogleMaps
import GooglePlaces
import GeoFire
import SVProgressHUD
import Reachability
import UserNotifications
import CoreData
import BLTNBoard
import TransitionableTab

//import Floaty
//import BubbleTransition

class CategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var catTitleLbl: UILabel!
    @IBOutlet weak var backView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        backView.layer.cornerRadius = 20
    }
    
    func configureCell(title: String) {
        catTitleLbl.text = title
    }
    
    func setSelected(selected: Bool) {
        if selected {
            self.backView.backgroundColor = MAIN_COLOR
            self.catTitleLbl.textColor = UIColor.white
        } else {
            self.backView.backgroundColor = UIColor.white
            self.catTitleLbl.textColor = UIColor.black
        }
    }
}
class JobCell: UITableViewCell {
    @IBOutlet weak var jobTitleLbl: UILabel!

    @IBOutlet weak var dueDateLbl: UILabel!
    @IBOutlet weak var noSourceLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
    @IBOutlet weak var bothStack: UIStackView!
    @IBOutlet weak var offerStack: UIStackView!
    @IBOutlet weak var assignStack: UIStackView!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var assignLbl: UILabel!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(task: Task) {
        jobTitleLbl.text = task.title
//        titleTV.text = task.title
        
        let seconds = task.deadline.doubleValue
        let date = NSDate(timeIntervalSince1970: seconds)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        dueDateLbl.text = dateString
        
        if task.peopleNeeded == 1 {
            noSourceLbl.text = "\(task.peopleNeeded!) tasker needed"
        } else {
            noSourceLbl.text = "\(task.peopleNeeded!) taskers needed"
        }
        
        priceLbl.text = "SGD\(task.budget!)"
        
        statusLbl.text = task.status
        
        if task.status == OPEN {
//            statusLbl.font = UIFont.boldSystemFont(ofSize: 17.0)
            statusLbl.textColor = OPEN_COLOR
        } else if task.status == CLOSED {
//            statusLbl.font = UIFont.boldSystemFont(ofSize: 17.0)
            statusLbl.textColor = UIColor.red
        } else if task.status == COMPLETED {
            statusLbl.textColor = MAIN_COLOR
        }
        
//        if task.assigned.count - 1 != 0 {
////            offersLbl.text = "\(task.offered.count - 1 - (task.assigned.count - 1))"
//            if (task.offered.count - 1 - (task.assigned.count - 1)) <= 0 {
//                offerStack.isHidden = true
//            } else {
//                offerStack.isHidden = false
//                offerLbl.text = "\(task.offered.count - 1 - (task.assigned.count - 1)) offers"
//            }
//
//            if (task.assigned.count - 1) <= 0 {
//                assignStack.isHidden = true
//            } else {
//                assignStack.isHidden = false
//                assignLbl.text = "\(task.assigned.count - 1) assigned"
//            }
//
//            if (task.assigned.count - 1) <= 0 && (task.offered.count - 1 - (task.assigned.count - 1)) <= 0 {
//                bothStack.isHidden = true
//            } else {
//                bothStack.isHidden = false
//            }
//        } else {
//            if (task.offered.count - 1) <= 0 {
//                offerStack.isHidden = true
//            } else {
//                offerStack.isHidden = false
//                offerLbl.text = "\(task.offered.count - 1) offers"
//            }
//
//            if (task.assigned.count - 1) <= 0 {
//                assignStack.isHidden = true
//            } else {
//                assignStack.isHidden = false
//                assignLbl.text = "\(task.assigned.count - 1) assigned"
//            }
//
//            if (task.assigned.count - 1) <= 0 && (task.offered.count - 1) <= 0 {
//                bothStack.isHidden = true
//            } else {
//                bothStack.isHidden = false
//            }
//
//        }

        

    }
}
class JobsVC: BaseViewController, UITableViewDelegate, UITableViewDataSource,UIViewControllerTransitioningDelegate,UISearchBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource, GMUClusterManagerDelegate, UNUserNotificationCenterDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var innerCollectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var innerTableView: UITableView!
    @IBOutlet weak var roundView: UIView!
    
    
    @IBOutlet weak var googleMap: GMSMapView!
    @IBOutlet weak var switchListBtn: UIButton!
    private var clusterManager: GMUClusterManager!
    private let locationManager = CLLocationManager()
    
    var currentCameraPosition:GMSCameraPosition!
    
    var taskArray = [Task]()
    var taskDict = [String:Task]()
    
    var tempArray = [Task]()
    var tempDict = [String:Task]()
    
    var category: String!
    var refreshControl: UIRefreshControl!
    var selectedTask: Task!
//    let transition = BubbleTransition()
    
    var currentCategory = ALL
    var sortingType = ALL
    
    var client: Client!
    var index: Index!
    var actInd: UIActivityIndicatorView!
    
    var mapView = false
    
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    var numberOfObjectsinArray: Int = 8
    var totalTask = 0
    
    var mode: Bool!
    var selectedCategoryIdxArray = Array(repeating: false, count: CATEGORIES_DISPLAY.count)
    var selectedCategoryIdxArrayMap = Array(repeating: false, count: CATEGORIES_DISPLAY_MAP.count)
    
    var geofireRef: DatabaseReference!
    var geoFire: GeoFire!
    
    @IBOutlet weak var addBtn: UIButton!
    
    var emptyView: EmptyView!
    var emptyViewInnerTable: EmptyViewInnerTable!
    
    private var infoWindow = MapMarkerView()
    fileprivate var locationMarker : GMSMarker? = GMSMarker()
    
    var bulletinManager: BLTNItemManager!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        googleMap.delegate = self
        googleMap.isIndoorEnabled = false
//        self.tabBarController?.delegate = self
        
        locationManager.delegate = self

        
//         self.askForNoti()
        
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { settings in

                switch settings.authorizationStatus {

                case .notDetermined:
                    // Authorization request has not been made yet
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.askForNoti()
                    })

                    break
                case .denied:
                    // User has denied authorization.
                    // You could tell them to change this in Settings
                    break
                case .authorized:
                    // User has given authorization.
                    break
                case .provisional:
                    break
                }
            })
        } else {
            // Fallback on earlier versions
            if UIApplication.shared.isRegisteredForRemoteNotifications {
                print("APNS-YES")
            } else {
                print("APNS-NO")
                DispatchQueue.main.async(execute: { () -> Void in
                    self.askForNoti()
                })
            }
        }
        
        geofireRef = Database.database().reference()
        geoFire = GeoFire(firebaseRef: geofireRef.child("task-location"))
        
        client = Client(appID: "3KZ3RIZFB0", apiKey: "91cec30f61f30e2cb78f1f57eac25458")
        index = client.index(withName: "task")
        
//        let iconGenerator = GMUDefaultClusterIconGenerator()
//        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
//
//        DispatchQueue.main.async {
//            let renderer = GMUDefaultClusterRenderer(mapView: self.googleMap,
//                                                     clusterIconGenerator: iconGenerator)
//            self.clusterManager = GMUClusterManager(map: self.googleMap, algorithm: algorithm,
//                                               renderer: renderer)
//        }

        
        // Generate and add random items to the cluster manager.
       
        
        
//        self.searchBar.delegate = self
//
//        var searchTextField: UITextField? = searchBar.value(forKey: "searchField") as? UITextField
//        if searchTextField!.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
//            let attributeDict = [NSAttributedString.Key.foregroundColor: UIColor.darkGray]
//            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributeDict)
//        }
        
//        self.searchBar.showsCancelButton = true
        
        
        
        self.navigationItem.title = category
        self.navigationController?.isNavigationBarHidden = false
        
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        innerTableView.delegate = self
        innerTableView.dataSource = self
//        innerTableView.tableFooterView = UIView()
        
        
        collectionView.delegate = self
        collectionView.dataSource = self
        innerCollectionView.delegate = self
        innerCollectionView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
//        setupBtn()
            
        actInd = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        actInd.frame = CGRect(x: 0, y: 0, width: 320, height: 40)
        actInd.hidesWhenStopped = true
            //        self.tableView.tableFooterView = actInd
            
            
        
        DataService.ds.jobVC = self
        fetchCategoryData()
        
        emptyView = EmptyView.instanceFromNib(image: UIImage(named: "noTask")!, title: "No Task", desc: "Start to post a new task today.") as! EmptyView
        emptyView.frame = self.tableView.bounds
        
        emptyViewInnerTable = EmptyViewInnerTable.instanceFromNib(image: UIImage(named: "noTask")!, title: "Hmm...", desc: "No tasks in this area.") as! EmptyViewInnerTable
        emptyViewInnerTable.frame = self.innerTableView.bounds
        
        
        self.tableView.addSubview(emptyView)
        self.innerTableView.addSubview(emptyViewInnerTable)
        emptyView.showView(show: false, tableView: self.tableView)
        emptyViewInnerTable.showView(show: false, tableView: self.innerTableView)

        self.infoWindow = loadNiB()
        
        self.mode = true
        self.changeCategory(filter: CATEGORIES_DISPLAY[0])
        self.resetArrayToFalse(selectedIdx: 0, category: CATEGORIES_DISPLAY[0])
        self.collectionView.reloadData()
        self.changeCategory(filter: CATEGORIES_DISPLAY_MAP[0])
        self.resetArrayToFalse(selectedIdx: 0, category: CATEGORIES_DISPLAY_MAP[0])
        self.innerCollectionView.reloadData()
    

        
        
    }
    
    func askForNoti() {
        
        
        let page = BLTNPageItem(title: "Push Notifications")
        page.requiresCloseButton = false
        page.isDismissable = false
        page.appearance.actionButtonColor = MAIN_COLOR
        page.image = UIImage(named: "notificationService")
        
        page.descriptionText = "Receive push notifications when there is new update."
        page.actionButtonTitle = "Subscribe"
        
        page.actionHandler = { (item: BLTNActionItem) in
            if #available(iOS 10.0, *) {
                // For iOS 10 display notification (sent via APNS)
                UNUserNotificationCenter.current().delegate = self
                
                let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                UNUserNotificationCenter.current().requestAuthorization(
                    options: authOptions,
                    completionHandler: {_, _ in })
            } else {
                let settings: UIUserNotificationSettings =
                    UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                UIApplication().registerUserNotificationSettings(settings)
            }
            
            item.manager?.displayNextItem()
  
        }
        
        let page2 = BLTNPageItem(title: "Location Service")
        page2.requiresCloseButton = false
        page2.appearance.actionButtonColor = MAIN_COLOR
        page2.isDismissable = false
        page2.image = UIImage(named: "locationService")
        
        page2.descriptionText = "By accessing your location, you can discover tasks nearby."
        page2.actionButtonTitle = "Send location data"
        
        page2.actionHandler = { (item: BLTNActionItem) in
            self.locationManager.requestWhenInUseAuthorization()
            item.manager?.dismissBulletin(animated: true)
//            item.manager?.displayNextItem()
        }
        
        
        page.next = page2
        
        
        bulletinManager = BLTNItemManager(rootItem: page)
        bulletinManager.backgroundViewStyle = .blurredDark

        bulletinManager.showBulletin(above: self)
    }
    

    
    func loadNiB() -> MapMarkerView{
        let infoWindow = MapMarkerView.instanceFromNib() as! MapMarkerView
        return infoWindow
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if mapView {
            return .default
        } else {
            return .lightContent
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        
        self.navigationController?.isNavigationBarHidden = true
        

//        self.initReachability()
//        if currentCategory != Checker.checker.categoryForJobVC {
//            currentCategory = Checker.checker.categoryForJobVC
//            fetchCategoryData()
//        }
//        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
         self.navigationController?.navigationBar.shouldRemoveShadow(false)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        roundView.clipsToBounds = true
        //            roundView.layer.cornerRadius = 10
        
        let path = UIBezierPath(roundedRect:roundView.bounds,
                                byRoundingCorners:[.topRight, .topLeft],
                                cornerRadii: CGSize(width: 70, height:  70))
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        roundView.layer.mask = maskLayer
    }

    
    func setBadgeNumber(notiCount: Int, index: Int) {
        if let tabItems = self.tabBarController?.tabBar.items as NSArray!
        {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[index] as! UITabBarItem
            tabItem.badgeValue = "\(notiCount)"
            
        }
    }
    
    func changeCategory(filter: String) {
        
        if mode {
//            if filter == nil {
//                self.currentCategory = ALL
//            } else {
//                self.currentCategory = category
//            }
            self.currentCategory = filter
//            Checker.checker.categoryForJobVC = filter
        } else {
            self.sortingType = filter
//            if filter == nil {
//                self.sortingType = ALL
//            } else {
//                self.sortingType = sort
//            }
        }
        
        if mapView {
            self.updateLoction(position: currentCameraPosition)
        } else {
            self.fetchCategoryData()
        }
        
        
        Checker.checker.categoryForJobVC = filter
        
    }
    
//    func changeCategory(category: String, sort: String) {
//        if category == nil {
//            self.currentCategory = ALL
//        } else {
//            self.currentCategory = category
//        }
//
//        if sort == nil {
//            self.sortingType = ALL
//        } else {
//            self.sortingType = sort
//        }
//
//
////        print("change cat \(category)")
//        self.fetchCategoryData()
//    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
//        self.searchBar.setShowsCancelButton(false, animated: true)
//        self.searchBar.resignFirstResponder()
    }
    @objc func maximizeView() {
//        self.blurView.isHidden = true
        UIView.animate(withDuration: 0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blurView.alpha = 0

            
            
        }, completion: { (comepleted: Bool) in
            
            if comepleted {
                self.blurView.isHidden = true
            }
        })
     UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.default, animated: true)
        print("blur")
    }
    
    @objc func minimizeView() {
//       self.blurView.isHidden = true
        UIView.animate(withDuration: 0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blurView.isHidden = false
            self.blurView.alpha = 1
            
            
            
        }, completion: { (comepleted: Bool) in
            if comepleted {
                
            }
        })
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text != "" {
            self.resetFilter()
            searchBar.setShowsCancelButton(true, animated: true)
            index.search(Query(query: searchBar.text!), completionHandler: { (content, error) -> Void in
                if error == nil {
                    if let c = content {
                        self.fetchFromAlgolia(content: c)
                    }
                    
                }
            })
        }
        
    }
    

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        print("cance")
        self.searchBar.text = ""
        self.searchBar.endEditing(true)
        self.searchBar.setShowsCancelButton(false, animated: true)
        if self.tempArray.count != 0 {
//            print("cance22")
            self.taskArray = self.tempArray
            self.taskDict = self.tempDict
            
            self.tempDict.removeAll()
            self.tempArray = []
            
            self.attemptToReloadTable()
            
            
        }
    }
    
    func resetFilter() {
        self.currentCategory = ALL
        
    }
    
    func fetchFromAlgolia(content: [String: Any]) {
        
        
        if let nbHits = content["nbHits"] as? Int {
            
            if nbHits != 0 {
                
                
                if let hitData = content["hits"] as? [Dictionary<String,AnyObject>] {
                    self.tempArray = self.taskArray
                    self.tempDict = self.taskDict
                    
                    self.taskDict.removeAll()
                    self.taskArray = []
                    
                    for task in hitData {
                        let key = task["objectID"] as! String
                        let task = Task(taskKey: key, dict: task)
                        self.taskDict[key] = task
                    }
                }
                
                
                self.attemptToReloadTable()
            }
        }

    }
//    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
////        self.navigationController!.navigationBar.isHidden = true
//        var r = self.view.frame
//        r.origin.y = -44
//        r.size.height += 44
//
//        self.view.frame = r
//        searchBar.setShowsCancelButton(true, animated: true)
//    }
//
//    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
//        searchBar.setShowsCancelButton(false, animated: true)
//    }

   
    
    
    
    func fetchCategoryData() {
//        self.hiddenPosts = []
//        .queryLimited(toLast: UInt(self.numberOfObjectsinArray))
        
        var ref: DatabaseReference!
        var queryRef: DatabaseQuery!
        
        if currentCategory == ALL{
            ref = DataService.ds.REF_TASK
        } else {
            ref = DataService.ds.REF_Category.child(currentCategory)
        }
        
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                self.totalTask = snapshots.count
            }
        })
   
        
        if sortingType == ALL {
            queryRef = ref
//                .queryLimited(toLast: UInt(self.numberOfObjectsinArray))
        } else if sortingType == PRICE_LOW_TO_HIGH {
            queryRef = ref.queryOrdered(byChild: "budget")
//                .queryLimited(toLast: UInt(self.numberOfObjectsinArray))
        } else if sortingType == PRICE_HIGH_TO_LOW {
            queryRef = ref.queryOrdered(byChild: "budget")
//                .queryLimited(toFirst: UInt(self.numberOfObjectsinArray))
        } else if sortingType == DEADLINE_NEAREST {
            queryRef = ref.queryOrdered(byChild: "deadline")
//                .queryLimited(toLast: UInt(self.numberOfObjectsinArray))
        } else if sortingType == DEADLINE_FURTHUREST {
            queryRef = ref.queryOrdered(byChild: "deadline")
//                .queryLimited(toFirst: UInt(self.numberOfObjectsinArray))
        }
       
        if self.tempArray.count != 0 {
//            fetchFromAlgolia(content: <#T##[String : Any]#>)
        } else {
           queryRef.observeSingleEvent(of: .value, with: { (snapshot) in
                
                            self.taskDict.removeAll()
                            self.taskArray = []
                
                
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    for snap in snapshots {
                        
                        //print("SNAP: \(snap)")
                        if let dict = snap.value as? Dictionary<String, AnyObject> {
                            
                            
                            
                            let key = snap.key
                            
                            let task = Task(taskKey: key, dict: dict)
                            if task.isActive! {
                                self.taskDict[key] = task
                            }
                            
                            
                            
                            
                            
                            
                            
                        }
                    }
                    self.attemptToReloadTable()
                }
                
                
                //            self.actInd.stopAnimating()
                
            })
        }
        
        
    }
    
    func attemptToReloadTable() {
        
        self.taskArray = Array(self.taskDict.values)
//
        if sortingType == ALL {
            self.taskArray.sort(by: { (c1, c2) -> Bool in
                
                return (c1.time.intValue) > (c2.time.intValue)
            })
            

        } else if sortingType == PRICE_LOW_TO_HIGH {
            self.taskArray.sort(by: { (c1, c2) -> Bool in
                
                return (c1.budget) < (c2.budget)
            })
            

        } else if sortingType == PRICE_HIGH_TO_LOW {
            self.taskArray.sort(by: { (c1, c2) -> Bool in
                
                return (c1.budget) > (c2.budget)
            })
        } else if sortingType == DEADLINE_NEAREST {
            self.taskArray.sort(by: { (c1, c2) -> Bool in
                
                return (c1.deadline.intValue) < (c2.deadline.intValue)
            })
        } else if sortingType == DEADLINE_FURTHUREST {
            self.taskArray.sort(by: { (c1, c2) -> Bool in
                
                return (c1.deadline.intValue) > (c2.deadline.intValue)
            })
        }
//        if self.loadingScreen.alpha != 0.0 {
//            UIView.animate(withDuration: 0.2, delay: 0.0, options:[], animations: { self.loadingScreen.alpha = 0.0 }, completion: nil)
//        }
//

        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        
        if actInd.isAnimating {
            actInd.stopAnimating()
        }
        
        SVProgressHUD.dismiss()
        
//        self.tableView.isUserInteractionEnabled = true
//        self.tabBarController?.tabBar.isUserInteractionEnabled = true
//        self.navigationController?.navigationBar.isUserInteractionEnabled = true
//        SVProgressHUD.dismiss()
        
        
//        if self.confessions.count == 0 {
//            if self.mainConfession == nil {
//                self.showEmptyView()
//            }
//            //            self.showEmptyView()
//        } else {
//            self.hideEmptyView()
//        }
        UIView.animate(views: tableView.visibleCells, animations: TABLE_ANIMATION, completion: {
        })
        
        
        
        
            
            if mapView {
                if self.taskArray.count == 0 {
                    emptyViewInnerTable.showView(show: true, tableView: self.innerTableView)
                } else {
                    emptyViewInnerTable.showView(show: false, tableView: self.innerTableView)
                }
                
            } else {
                if self.taskArray.count == 0 {
                    emptyView.showView(show: true, tableView: self.tableView)
                } else {
                    emptyView.showView(show: false, tableView: self.tableView)
                }
                
                
            }
      
        
        if mapView {
            self.innerTableView.reloadData()
            
//            DispatchQueue.main.async {
//                self.clusterManager.cluster()
//            }
//
            // Generate and add random items to the cluster manager.
//            generateClusterItems()
            
            // Call cluster() after items have been added to perform the clustering
            // and rendering on map.
            
        } else {
            self.tableView.reloadData()
        }
        
    }
    
    @objc func refresh() {
//        self.tableView.isUserInteractionEnabled = false
//        displayNoInternetConnection()
//        let ref = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).child("university")
//        ref.removeAllObservers()
        
        // self.tableView.isScrollEnabled = false
        
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(reloadDataWhenPulled), userInfo: nil, repeats: false)
        
    }
    
    @objc func reloadDataWhenPulled() {
        
        if self.refreshControl.isRefreshing {
            
            // reloadTable()
            //  self.tableView.reloadData()
            fetchCategoryData()
            // self.refreshControl.endRefreshing()
            
            
            
        }
        
    }
    
    func setupBtn() {
//        let floaty = Floaty()
//        floaty.addItem("I got a handler", icon: UIImage(named: "icon")!, handler: { item in
//            let alert = UIAlertController(title: "Hey", message: "I'm hungry...", preferredStyle: .Alert)
//            alert.addAction(UIAlertAction(title: "Me too", style: .Default, handler: nil))
//            self.presentViewController(alert, animated: true, completion: nil)
//            floaty.close()
//        })
//        self.view.addSubview(floaty)
//
//
//        floaty.translatesAutoresizingMaskIntoConstraints = false
//        floaty.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
//        floaty.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "JobCell") as? JobCell
            cell?.selectionStyle = .none
            cell?.configureCell(task: taskArray[indexPath.row])
            return cell!
        } else if tableView == self.innerTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "JobCell") as? JobCell
            cell?.selectionStyle = .none
            cell?.configureCell(task: taskArray[indexPath.row])
            return cell!
        }
        
        return UITableViewCell()

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tableView || tableView == self.innerTableView {
            return 1
        } else {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView || tableView == self.innerTableView {
            return taskArray.count
        } else {
            return taskArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath)
            if let annotateCell = cell as? CategoryCell {
                if selectedCategoryIdxArray[indexPath.row] {
                    annotateCell.setSelected(selected: true)
                } else {
                    annotateCell.setSelected(selected: false)
                }
                
                annotateCell.configureCell(title: CATEGORIES_DISPLAY[indexPath.row])
            }
            return cell
        } else if collectionView == self.innerCollectionView  {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath)
            if let annotateCell = cell as? CategoryCell {
                if selectedCategoryIdxArrayMap[indexPath.row] {
                    annotateCell.setSelected(selected: true)
                } else {
                    annotateCell.setSelected(selected: false)
                }
                
                annotateCell.configureCell(title: CATEGORIES_DISPLAY_MAP[indexPath.row])
            }
            return cell
        } else {
            return UICollectionViewCell()
        }

    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if mapView {
            return CATEGORIES_DISPLAY_MAP.count
        } else {
            return CATEGORIES_DISPLAY.count
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let idxPath = collectionView.indexPathsForSelectedItems
    
        self.mode = true
        
        let currentCell = collectionView.cellForItem(at: indexPath) as! CategoryCell
        //        selectedCategoryIdxArray[indexPath.row] = true
        
        
        infoWindow.removeFromSuperview()
        
        if collectionView == self.collectionView {
            
            
            self.changeCategory(filter: CATEGORIES_DISPLAY[indexPath.row])
            self.resetArrayToFalse(selectedIdx: indexPath.row, category: CATEGORIES_DISPLAY[indexPath.row])

            
            self.collectionView.reloadData()
            
        } else if collectionView == self.innerCollectionView {
            
            self.changeCategory(filter: CATEGORIES_DISPLAY_MAP[indexPath.row])
            self.resetArrayToFalse(selectedIdx: indexPath.row, category: CATEGORIES_DISPLAY_MAP[indexPath.row])
            
            self.innerCollectionView.reloadData()
        }

    }
    
//    func resetArrayToFalse(selectedIdx: Int, category: String) {
//
//        if mapView {
//
//            for idx in 0...self.selectedCategoryIdxArrayMap.count - 1{
//                selectedCategoryIdxArrayMap[idx] = false
//            }
//
//            selectedCategoryIdxArrayMap[selectedIdx] = true
//        } else {
//            for idx in 0...self.selectedCategoryIdxArray.count - 1{
//                selectedCategoryIdxArray[idx] = false
//            }
//
//            selectedCategoryIdxArray[selectedIdx] = true
//        }
//

//
//    }
    
    func resetArrayToFalse(selectedIdx: Int, category: String) {
        
        if mapView {
            
            for idx in 0...self.selectedCategoryIdxArrayMap.count - 1{
                selectedCategoryIdxArrayMap[idx] = false
            }
            
            selectedCategoryIdxArrayMap[selectedIdx] = true
            
            
            
            //reset the other side
            for idx in 0...self.selectedCategoryIdxArray.count - 1{
                selectedCategoryIdxArray[idx] = false
            }
            
            var idx = 0
            for cat in CATEGORIES_DISPLAY {
                if cat == category {
                    selectedCategoryIdxArray[idx] = true
                }
                
                idx = idx + 1
            }
            
        } else {
            for idx in 0...self.selectedCategoryIdxArray.count - 1{
                selectedCategoryIdxArray[idx] = false
            }
            
            selectedCategoryIdxArray[selectedIdx] = true
            
            
            for idx in 0...self.selectedCategoryIdxArrayMap.count - 1{
                selectedCategoryIdxArrayMap[idx] = false
            }
            
            var idx = 0
            for cat in CATEGORIES_DISPLAY_MAP {
                if cat == category {
                    selectedCategoryIdxArrayMap[idx] = true
                }
                
                idx = idx + 1
            }
        }
        

        
    }
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        if self.tempArray.count != 0 {
//            if indexPath.row == self.tempArray.count - 1  {
////                self.tableView.tableFooterView = actInd
////                self.actInd.startAnimating()
//                Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.addMoreObjects), userInfo: nil, repeats: false)
//            } else {
//                //            self.tableView.isUserInteractionEnabled = true
////                self.actInd.stopAnimating()
////                self.tableView.tableFooterView = UIView()
//            }
//        } else {
//            if indexPath.row == self.taskArray.count - 1  {
////                self.tableView.tableFooterView = actInd
////                self.actInd.startAnimating()
//                Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.addMoreObjects), userInfo: nil, repeats: false)
//            } else {
//                //            self.tableView.isUserInteractionEnabled = true
////                self.actInd.stopAnimating()
////                self.tableView.tableFooterView = UIView()
//            }
//        }
//
//    }
    
    @objc func addMoreObjects() {
        
//        animateTableView = false
        numberOfObjectsinArray += 10
        self.fetchCategoryData()
    }

    @IBAction func addTapped(_ sender: Any) {
//        if !DataService.ds.currentUser.isVerified {
//            Checker.checker.verificationMode = true
//            VCSegue().segueVC(bundleName: "VerificationSB", controllerName: "VerificationVC", vc: self)
//        } else {
//            self.performSegue(withIdentifier: "NewTaskVC", sender: nil)
//        }
        self.mode = false
        self.performSegue(withIdentifier: "FilterVC", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedTask = taskArray[indexPath.row]
        self.performSegue(withIdentifier: "TaskDetailVC", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? TaskDetailVC {
            destinationVC.task = self.selectedTask
            
        }
        
        if let destinationVC = segue.destination as? FilterVC {
            destinationVC.jobVC = self
            destinationVC.mode = self.mode
            
        }
    }
    @IBAction func filterTapped(_ sender: Any) {
//        self.tabBarController.i
        self.mode = true
        self.performSegue(withIdentifier: "FilterVC", sender: nil)
    }
    
    @IBAction func listViewTapped(_ sender: Any) {
        switchViewMode()
    }
    @IBAction func mapViewTapped(_ sender: Any) {
        switchViewMode()
    }
    
    func switchViewMode() {
        self.collectionView.reloadData()
        self.innerCollectionView.reloadData()
        infoWindow.removeFromSuperview()
        
        if mapView {
            mapView = false
            self.googleMap.isHidden = true
            self.innerTableView.isHidden = true
            self.switchListBtn.isHidden = true
            self.innerCollectionView.isHidden = true
            self.roundView.isHidden = true
            
            fetchCategoryData()
        } else {
            mapView = true
            self.googleMap.isHidden = false
            self.innerTableView.isHidden = false
            self.switchListBtn.isHidden = false
            self.innerCollectionView.isHidden = false
            self.roundView.isHidden = false
            
            googleMap.clear()
            self.taskDict.removeAll()
            self.taskArray = []
//            self.clusterManager.clearItems()
            self.attemptToReloadTable()
        }
    }
    @IBAction func searchTapped(_ sender: Any) {
//        self.present(searchBar, animated: true, completion: nil)
    }
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let controller = segue.destination
//        controller.transitioningDelegate = self
//        controller.modalPresentationStyle = .custom
//    }
//
//    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        transition.transitionMode = .present
//        transition.startingPoint = addBtn.center
//        transition.bubbleColor = addBtn.backgroundColor!
//        return transition
//    }
//
//    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        transition.transitionMode = .dismiss
//        transition.startingPoint = addBtn.center
//        transition.bubbleColor = addBtn.backgroundColor!
//        return transition
//    }
    
    
}

extension JobsVC: GMSMapViewDelegate {
    
    func didTapInfoButton(data: Task) {
        self.selectedTask = data
        self.performSegue(withIdentifier: "TaskDetailVC", sender: nil)
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("posittion get called \(position)")
        SVProgressHUD.show()
        currentCameraPosition = position
        updateLoction(position: position)
        
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        var markerData : Task?
    
        
        if let data = marker.userData! as? Task {
            markerData = data
            
        }
        locationMarker = marker
        infoWindow.removeFromSuperview()
        infoWindow = loadNiB()
        guard let location = locationMarker?.position else {
            print("locationMarker is nil")
            return false
        }
        // Pass the spot data to the info window, and set its delegate to self
//        infoWindow.spotData = markerData
        infoWindow.jobVC = self
        infoWindow.task = markerData!
        infoWindow.titleLabel.text = markerData!.title
        infoWindow.priceLabel.text = "SGD\(markerData!.budget!)"
        // Configure UI properties of info window
//        infoWindow.alpha = 0.9
//        infoWindow.layer.cornerRadius = 12
//        infoWindow.layer.borderWidth = 2
//        infoWindow.layer.borderColor = UIColor(named: "19E698")?.cgColor
//        infoWindow.infoButton.layer.cornerRadius = infoWindow.infoButton.frame.height / 2
//        
//        let address = markerData!["address"]!
//        let rate = markerData!["rate"]!
//        let fromTime = markerData!["fromTime"]!
//        let toTime = markerData!["toTime"]!
        
//        infoWindow.addressLabel.text = address as? String
//        infoWindow.priceLabel.text = "$\(String(format:"%.02f", (rate as? Float)!))/hr"
//        infoWindow.availibilityLabel.text = "\(convertMinutesToTime(minutes: (fromTime as? Int)!)) - \(convertMinutesToTime(minutes: (toTime as? Int)!))"
        // Offset the info window to be directly above the tapped marker
        infoWindow.center = mapView.projection.point(for: location)
        infoWindow.center.y = infoWindow.center.y - 82
        self.view.addSubview(infoWindow)
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        infoWindow.removeFromSuperview()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (locationMarker != nil){
            guard let location = locationMarker?.position else {
                print("locationMarker is nil")
                return
            }
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center.y = infoWindow.center.y - 82
        }
    }
//    override func viewWillLayoutSubviews() {
//        super.viewWillLayoutSubviews()
//        self.tableView.topAnchor.constraint(equalTo: self.collectionView.bottomAnchor).isActive = false
//        self.tableView.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
//
//    }
    func updateLoction(position: GMSCameraPosition) {
      
        
        googleMap.clear()
        self.taskDict.removeAll()
        self.taskArray = []
//        self.clusterManager.clearItems()
        
        
        self.attemptToReloadTable()
        
        let visibleLocation = CLLocation(latitude: position.target.latitude,  longitude: position.target.longitude)
        print("lat \(position.target.latitude) , long \(position.target.longitude)")
        print("radius \(googleMap.getRadius())")
        
        
//        let defaults = UserDefaults.standard
//        var uid = defaults.object(forKey: "uid") as? String
//        
//        geoFire.setLocation(visibleLocation, taskName: <#String#>, taskBudget: <#Int32#>, taskDeadline: <#Int32#>, forKey: uid!)
        
        let query = geoFire.query(at: visibleLocation, withRadius: googleMap.getRadius() / 1000.0 )
        var queryHandle = query.observe(.keyEntered, with: { (key: String!, location: CLLocation!) in
            print("Key '\(key)' entered the search area and is at location '\(location)'")
//            self.updateMapWithMarkers(destinationLocation: location)
            
                // Perform your async code here
            DispatchQueue.main.async {
                self.fetchTaskDetails(taskID: key!)
            }
            
            
            
        })

   
        

        
    }
    
    
    func updateMapWithMarkers(task: Task) {
        
        DispatchQueue.main.async {
            let marker = GMSMarker()
            marker.icon = UIImage(named: "map-marker")
            marker.position = CLLocationCoordinate2D(latitude: task.place!.latitude, longitude: task.place!.longitude)
            //        marker.title = task.title
            //        marker.snippet = task.desc
            marker.userData = task
            marker.map = self.googleMap
        }

        
    }
    
    private func generateClusterItems(task: Task) {
//        let extent = 0.2
//        var coor = [[1.323732, 103.684856], [1.329224,103.88879], [1.35119,103.992473], [1.457588,103.816692], [1.416403, 103.682796], [1.319613,103.936169], [1.325791, 103.956081], [1.323045, 103.988354]]
//        for index in 0...coor.count - 1 {
//
//            let lat = coor[index][0]
//            let lng = coor[index][1]
//            let name = "Item \(index)"
//            let item =
//                ClusteredTaskLocation(position: CLLocationCoordinate2DMake(lat, lng), name: name)
//            clusterManager.add(item)
//        }
        
        if let p = task.place {
            let lat = p.latitude
            let lng = p.longitude

            let item =
                ClusteredTaskLocation(position: CLLocationCoordinate2DMake(lat!, lng!), taskTitle: task.title, taskBudget: task.budget)
            clusterManager.add(item)
            
            
            
            print("added ")
        }
        
//        for task in self.taskArray {
//            if let p = task.place {
//                let lat = p.latitude
//                let lng = p.longitude
//
//                let item =
//                    ClusteredTaskLocation(position: CLLocationCoordinate2DMake(lat!, lng!), name: "name")
//                clusterManager.add(item)
//                print("added ")
//            }
//
//        }
//
//        self.clusterManager.cluster()
        
        
    }
    
    /// Returns a random value between -1.0 and 1.0.
    private func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
    
    func fetchTaskDetails(taskID: String){

        
        DataService.ds.REF_TASK.child(taskID).observeSingleEvent(of: .value, with: { (snapshot) in
            

                    if let dict = snapshot.value as? Dictionary<String, AnyObject> {
                        
                        
                        
                        let key = snapshot.key
                        
                        let task = Task(taskKey: key, dict: dict)
//                        self.updateMapWithMarkers(task: task)
                        
                        if task.isActive! {
                            if self.currentCategory == ALL || task.category == self.currentCategory {
                                self.taskDict[key] = task
                                self.updateMapWithMarkers(task: task)
                                //                            self.generateClusterItems(task: task)
                            }
                        }

                        
//                        if let p = task.place {
//                            let name = "Item \(task.title)"
////                            let item =
////                                ClusteredTaskLocation(position: CLLocationCoordinate2DMake(p.latitude, p.longitude), name: name)
////                            self.clusterManager.add(item)
////                            self.clusterManager.cluster()
//                        }
                        
                        
                        self.attemptToReloadTable()
//                        self.generateClusterItems()
                        
                    }

            
        })
    }
}

extension JobsVC: CLLocationManagerDelegate {
    // 2
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // 3
        guard status == .authorizedWhenInUse else {
            return
        }
        // 4
        locationManager.startUpdatingLocation()
        
        //5
        googleMap.isMyLocationEnabled = true
        
//        googleMap.settings.myLocationButton = true
    }
    
    // 6
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("did update")
        guard let location = locations.first else {
            return
        }
        
        // 7
        googleMap.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        DataService.ds.addLocationToUser(location: location)
      
        // 8
        locationManager.stopUpdatingLocation()
    }
    
    
}

//extension JobsVC: TransitionableTab, UITabBarControllerDelegate {
//    
//    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        return animateTransition(tabBarController, shouldSelect: viewController)
//    }
//}
