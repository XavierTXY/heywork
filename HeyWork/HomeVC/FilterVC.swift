//
//  FilterVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 16/3/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import UIKit
import Spring
import TagListView

class FilterVC: UIViewController,TagListViewDelegate {

    @IBOutlet weak var popupView: SpringView!
    @IBOutlet weak var catTagView: TagListView!
    @IBOutlet weak var sortTagView: TagListView!
    @IBOutlet weak var filterBtn: UIButton!
    
    var selectedCat: String?
    var selectedSort: String?
    
    @IBOutlet weak var modeLbl: UILabel!
    
    var jobVC: JobsVC!
    var mode: Bool!
    
    //mode = true = category
    //mode = false = sort
    override func viewDidLoad() {
        super.viewDidLoad()

        catTagView.delegate = self
        catTagView.textFont = UIFont.systemFont(ofSize: 18)
        catTagView.alignment = .left
        
        if mode {
            self.modeLbl.text = "Category"
            
            catTagView.addTags(CATEGORIES)
        
//            for type in CATEGORIES {
//
//                catTagView.addTag(type)
//
//            }
            
        } else {
            self.modeLbl.text = "Filter"
            catTagView.addTags(SORT)
//            for s in SORT {
//                catTagView.addTag(s)
//            }

        }

        if Checker.checker.categoryForJobVC != ALL {
            self.filterBtn.isHidden = false
        } else {
            self.filterBtn.isHidden = true
        }
        
        
//        sortTagView.delegate = self
//        sortTagView.textFont = UIFont.systemFont(ofSize: 18)
//        sortTagView.alignment = .left
        //        var array = self.item.types.keys
        //        for (type,_) in item.types {
        //            print(type)
        //            typesView.addTag(type)
        //        }
        
     
       
        
        // Do any additional setup after loading the view.
        popupView.transform = CGAffineTransform(translationX: 0, y: 300)
    }
    
    override func viewDidAppear(_ animated: Bool) {
//    UIApplication.shared.sendAction(#selector(JobsVC.minimizeView(_:)), to: nil, from: self, for: nil)
        jobVC.minimizeView()
        popupView.animate()
    }
    
    override func viewDidLayoutSubviews() {
        popupView.roundCorners(corners: [.topLeft, .topRight], radius: 10)
        
        
    }
    @IBAction func dismissTapped(_ sender: Any) {
        dismissView()
    }
    
    func dismissView() {
        dismiss(animated: true, completion: nil)
        jobVC.maximizeView()
//        UIApplication.shared.sendAction(#selector(JobsVC.maximizeView(_:)), to: nil, from: self, for: nil)
    }
    @IBAction func dismissViewTapped(_ sender: Any) {
        dismissView()
    }

    func setSelected(tagView: TagView, category: Bool){
        
        
//        if category {
            self.catTagView.borderColor = UIColor.yellow
            self.catTagView.textColor = UIColor.yellow
//        } else {
//            self.sortTagView.borderColor = UIColor.yellow
//            self.sortTagView.textColor = UIColor.yellow
//        }
        
        tagView.borderColor = MAIN_COLOR
        tagView.textColor = MAIN_COLOR

        
        
        
        
    }
    
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
       
        tagView.onTap = { tagView in
            
            print("Don’t tap me!")
//            if SORT.contains(title) {
//                self.setSelected(tagView: tagView, category: false)
//                self.selectedSort = title
//            } else {
                self.setSelected(tagView: tagView, category: true)
                self.selectedCat = title
//            }
        }
//        if tagView == catTagView {
      
//            self.jobVC.changeCategory(category: title)
//        } else if tagView == sortTagView {
        
//        }
        
        self.jobVC.changeCategory(filter: title)
        
        dismissView()
        
    }
    
    @IBAction func clearFilterTapped(_ sender: Any) {
        
        
//        if self.selectedCat == nil && self.selectedSort == nil {
//
//        } else {
//            if self.selectedCat == nil {
//                 self.jobVC.changeCategory(category: ALL, sort: selectedSort!)
//            } else if self.selectedSort == nil {
//                self.jobVC.changeCategory(category: selectedCat!, sort: ALL)
//            } else {
//                self.jobVC.changeCategory(category: selectedCat!, sort: selectedSort!)
//            }
//        }
//
//        print(self.selectedCat)
//        print(self.selectedSort)
//        dismissView()
//        var reset
        self.jobVC.changeCategory(filter: ALL)
        dismissView()
        
    }
    
}
