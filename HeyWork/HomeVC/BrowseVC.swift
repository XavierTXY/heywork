//
//  BrowseVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 10/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit

class FilterCell: UICollectionViewCell {
    @IBOutlet weak var bgImg: UIImageView!
    @IBOutlet weak var catLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(lbl: String, pic: UIImage) {
        if lbl == ALL {
            catLbl.text = "Clear All"
        } else {
            catLbl.text = lbl
        }
        
    }
}

class BrowseVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

//    @IBOutlet weak var tableView: UITableView!
    var category = ["Graphics & Design", "Writing & Translation", "Programming & Tech", "Business","Media" , "Other", ALL]
    var selectedCategory: String!
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionView.delegate = self
        collectionView.dataSource = self
//        tableView.tableFooterView = UIView()
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//        self.navigationController?.isNavigationBarHidden = true
//    }
////
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseCell") as? BrowseCell
//        cell?.selectionStyle = .none
//        cell?.configureCell(cat: category[indexPath.row])
//        return cell!
//    }
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return category.count
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        selectedCategory = category[indexPath.row]
//        self.performSegue(withIdentifier: "JobVC", sender: nil)
//    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return category.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCell", for: indexPath) as? FilterCell
        cell?.configureCell(lbl: category[indexPath.row], pic: UIImage(named: "closeBtn")!)
//        cell.selectionStyle = .none
       
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Checker.checker.categoryForJobVC = category[indexPath.row]
        self.dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? JobsVC {
            destinationVC.category = self.selectedCategory
            
        }
    }

    

}
