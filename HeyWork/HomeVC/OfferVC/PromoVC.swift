
//
//  PromoVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 3/6/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import UIKit

class PromoVC: UIViewController {

    @IBOutlet weak var promoTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func applyTapped(_ sender: Any) {
        ErrorAlert.errorAlert.showAlert(title: "Error", msg: "Invalid promo code entered", object: self)
    }
    @IBAction func removeTapped(_ sender: Any) {
        ErrorAlert.errorAlert.showAlert(title: "Error", msg: "Please enter a valid promo code before removing", object: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
