//
//  OfferPreviewVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 24/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit

class OfferPreviewVC: UIViewController {
    
    var task: Task!
    var offerDate: NSNumber!
    var serviceCharge: Double!
    var grossEarning: Double!
    var offerMsg: String!
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var totalAmountOfferedLbl: UILabel!
    
    @IBOutlet weak var serviceChargeLbl: UILabel!
    @IBOutlet weak var grossLbl: UILabel!
    @IBOutlet weak var btnView: RoundView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnView.backgroundColor = OPEN_COLOR
        
        let seconds = offerDate.doubleValue
        let date = NSDate(timeIntervalSince1970: seconds)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        dateLbl.text = dateString
        
        totalAmountOfferedLbl.text = "SGD\(Int(serviceCharge+grossEarning))"
        serviceChargeLbl.text = "SGD\(serviceCharge!)"
//        grossLbl.text = "SGD\(grossEarning!)"
        
        grossLbl.text = "SGD\(Int(serviceCharge+grossEarning))"
  
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        let key = DataService.ds.REF_OFFER.childByAutoId().key
        let offer = Offer(offerID: key, offerUserName: DataService.ds.currentUser.userName, offerUserID: DataService.ds.currentUser.userKey!, offerUserProfilePicUrl: DataService.ds.currentUser.profilePicUrl, offerMsg: offerMsg, offerPrice: Int(serviceCharge+grossEarning), offerDate: offerDate, userStarRating: DataService.ds.currentUser.taskerStarRating, userNumRating: DataService.ds.currentUser.taskerNumRating, userCompletionRate: DataService.ds.currentUser.taskerCompletionRate)
        
        
        task.updateOffer(taskerID: DataService.ds.currentUser.userKey!, add: true, offerDict: offer.getDict()) {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }

    }
    


}
