//
//  OfferMessageVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 24/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit

class OfferMessageVC: UIViewController, UITextViewDelegate {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnView: RoundView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var charLbl: UILabel!
    var task: Task!
    var offerDate: NSNumber!
    var serviceCharge: Double!
    var grossEarning: Double!
    var offerMsg: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textView.delegate = self
        // Do any additional setup after loading the view.
        textView.becomeFirstResponder()
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.layer.borderWidth = 1.0
        textView.layer.cornerRadius = 5.0
        addToolBar()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        
        print("will show")
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 500
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    func addToolBar() {
        
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                               UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneTapped))]
        numberToolbar.sizeToFit()
        textView.inputAccessoryView = numberToolbar
        
        
    }
    
    @objc func doneTapped() {
        textView.resignFirstResponder()
    }
    func textViewDidChange(_ textView: UITextView) {
        
        if 30 - textView.text.characters.count < 0 {
            charLbl.isHidden = true
        } else {
            charLbl.isHidden = false
            charLbl.text = "Minimum \( 30 - textView.text.characters.count) characters"
        }
        
        
        if textView.text.count < 30 {
            btnView.backgroundColor = UIColor.lightGray
        } else {
            btnView.backgroundColor = OPEN_COLOR
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    @IBAction func continueTapped(_ sender: Any) {
        
        if textView.text.count < 30 {
            ErrorAlert.errorAlert.showAlert(title: "Describe in at least 30 characters", msg: "Please give more information why you are the one.", object: self)
        } else {
            self.offerMsg = textView.text
            performSegue(withIdentifier: "OfferPreviewVC", sender: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? OfferPreviewVC {
            dest.task = self.task
            dest.offerDate = self.offerDate
            dest.serviceCharge = self.serviceCharge
            dest.grossEarning = self.grossEarning
            dest.offerDate = self.offerDate
            dest.offerMsg = self.offerMsg
        }
    }

}
