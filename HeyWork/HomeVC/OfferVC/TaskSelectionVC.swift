//
//  TaskSelectionVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 5/5/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import UIKit

class TaskSelectionCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(title: String) {
        titleLbl.text = title
    }
}
class TaskSelectionVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var selectionTitleLbl: UILabel!
    
    var questions = [String]()
    var category: String!
    
    var categoryIndex: Int!
    var questionIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        
        self.selectionTitleLbl.text = "What do you need?"
        
        self.questions = QUESTIONS[categoryIndex]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskSelectionCell") as? TaskSelectionCell
        cell?.selectionStyle = .none
        cell?.configureCell(title: questions[indexPath.row])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.questionIndex = indexPath.row
        self.performSegue(withIdentifier: "SummaryVC", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVC = segue.destination as? SummaryVC {
            
            if ANSWERS[self.categoryIndex].indices.contains(self.questionIndex) {
                destinationVC.answerPlaceholder = ANSWERS[self.categoryIndex][self.questionIndex]
            } else {
                
            }
            
            destinationVC.category = self.category
        }
        

    }

}
