//
//  OfferDateVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 24/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import BLTNBoard

class OfferDateVC: UIViewController, UITextFieldDelegate {

    var task: Task!
    var offerDate: NSNumber!
    var serviceCharge: Double!
    var grossEarning: Double!
    var bulletinManager: BLTNItemManager!
    
    @IBOutlet weak var dateTF: UITextField!
    @IBOutlet weak var btnView: RoundView!
    
    override func viewDidLoad() {
        dateTF.delegate = self
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addDatePicker()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == dateTF {
    
            print("wow")
            //            self.view.endEditing(true)
            
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(addDatePicker), userInfo: nil, repeats: false)
            
            
        }
        
        
        return true
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(copy(_:)) || action == #selector(paste(_:)) {
            return false
        }
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func continueTapped(_ sender: Any) {
        
        if self.btnView.backgroundColor != OPEN_COLOR {
            ErrorAlert.errorAlert.showAlert(title: "Completion Date Needed", msg: "Please offer a deadline.", object: self)
            
        } else {
            self.performSegue(withIdentifier: "OfferMessageVC", sender: nil)
        }
    }
    

    @objc func addDatePicker() {
        
        let today = Date()
        let tomorrow = Calendar.current.date(byAdding: .day, value: 0, to: today)
        let seconds = task.deadline.doubleValue
        let date = NSDate(timeIntervalSince1970: seconds)

        let minDate = date as Date
        
//        let oneMonthLater = Calendar.current.date(byAdding: .day, value: 31, to: today)
        
        let page3 = DatePickerBulletinItem()
        
        page3.datePicker.minimumDate = tomorrow
        page3.datePicker.maximumDate = minDate
        
        page3.requiresCloseButton = true
        page3.appearance.actionButtonColor = MAIN_COLOR
        page3.isDismissable = true
        page3.image = UIImage(named: "deadline")
        
        page3.descriptionText = "Pick a date which you can finish this task on."
        page3.actionButtonTitle = "Confirm"
        
        page3.actionHandler = { (item: BLTNActionItem) in
            //            self.locationManager.requestWhenInUseAuthorization()
            let datePicked = page3.datePicker.date
            
            let dateFormatter = ISO8601DateFormatter()
            
            self.offerDate = NSNumber(integerLiteral: Int(datePicked.timeIntervalSince1970))
            
            let a = "\(datePicked)"
            let fullNameArr = a.components(separatedBy: " ")
            let date = fullNameArr[0]
            
            let newArray = date.components(separatedBy: "-")
            self.dateTF.text = "\(newArray[2])-\(newArray[1])-\(newArray[0])"
            self.btnView.backgroundColor = OPEN_COLOR
            
            //            print(datePicked)
//            print(self.deadline)
            self.dateTF.resignFirstResponder()
            item.manager?.dismissBulletin(animated: true)
        }
        
        bulletinManager = BLTNItemManager(rootItem: page3)
        bulletinManager.backgroundViewStyle = .blurredDark
        
        bulletinManager.showBulletin(above: self)
        
        
//        let seconds = task.deadline.doubleValue
//        let date = NSDate(timeIntervalSince1970: seconds)
//
//        let minDate = date as Date
//
//        let today = Date()
//        let tomorrow = Calendar.current.date(byAdding: .day, value: 0, to: today)
//
//        let datePicker = ActionSheetDatePicker(title: "Date of Completion", datePickerMode: UIDatePicker.Mode.date, selectedDate: Calendar.current.date(byAdding: .day, value: 0, to: minDate), doneBlock: {
//            picker, value, index in
//            self.dateTF.resignFirstResponder()
//
//            print(value!)
//            let a = "\(value!)"
//            let fullNameArr = a.components(separatedBy: " ")
//            let date = fullNameArr[0]
//
//            let newArray = date.components(separatedBy: "-")
//            self.dateTF.text = "\(newArray[2])-\(newArray[1])-\(newArray[0])"
//
//            var isoDate = "\(value!)"
//            isoDate.trimmingCharacters(in: .whitespaces)
//            let monthArray = isoDate.components(separatedBy: " ")
//
//            var newDate = "\(monthArray[0])T\(monthArray[1])\(monthArray[2])"
//            print(newDate)
//            let dateFormatter = ISO8601DateFormatter()
//            let wula = dateFormatter.date(from:newDate)!
//            print(wula.timeIntervalSince1970)
//            self.offerDate = NSNumber(integerLiteral: Int(wula.timeIntervalSince1970))
//
//
//            self.btnView.backgroundColor = OPEN_COLOR
//
//
//
//            return
//        }, cancel: { ActionStringCancelBlock in return
//                        self.dateTF.resignFirstResponder()
//        }, origin: self.view)
//
//
//
//
//        datePicker?.minimumDate = tomorrow
//        datePicker?.maximumDate = minDate
//        //            datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
//
//        datePicker?.show()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? OfferMessageVC {
            dest.task = self.task
            dest.offerDate = self.offerDate
            dest.serviceCharge = self.serviceCharge
            dest.grossEarning = self.grossEarning
            dest.offerDate = self.offerDate
        }
    }
}
