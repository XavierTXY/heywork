//
//  OfferPriceVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 24/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit

class OfferPriceVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var priceTF: UITextField!
    @IBOutlet weak var serviceLbl: UILabel!
    @IBOutlet weak var grossLbl: UILabel!
    @IBOutlet weak var btnView: RoundView!
    @IBOutlet weak var continueBtn: UIButton!
    
    var task: Task!
    var finalAmount: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelTapped))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Promo Code", style: .plain, target: self, action: #selector(promoTapped))
        priceTF.delegate = self
        
        self.task = Checker.checker.taskForOffer
        // Do any additional setup after loading the view.
        print(task.budget)
        priceTF.text = "\(task.budget!)"
        serviceLbl.text = "\(task.getServiceCharge())"
        grossLbl.text = "\(task.getGrossEarning())"
        
        calculate()
        priceTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        addToolBar()
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
    }
    
    func addToolBar() {
        
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                               UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneTapped))]
        numberToolbar.sizeToFit()
        priceTF.inputAccessoryView = numberToolbar
        
        
    }
    
    @objc func doneTapped() {
        priceTF.resignFirstResponder()
    }
    
    @objc func promoTapped() {
//        self.dismiss(animated: true, completion: nil)
        self.performSegue(withIdentifier: "PromoVC", sender: nil)
    }
    
    @objc func cancelTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == priceTF {
            
            if priceTF.text!.count != 0 {
                calculate()
            } else {
                serviceLbl.text = "- -"
                grossLbl.text = "- -"
            }
            
        }
    }

    

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
  
    
    func calculate() {

        serviceLbl.text = "SGD\(Double(priceTF.text!)!.getServiceCharge())"
//        grossLbl.text = "SGD\(Double(priceTF.text!)!.getGrossEarning())"
        
        grossLbl.text = "SGD\(Double(priceTF.text!)!)"
        
        if Int(priceTF.text!)! >= MIN_PRICE {
            btnView.backgroundColor = OPEN_COLOR
//            continueBtn.isUserInteractionEnabled = true
        } else {
            btnView.backgroundColor = UIColor.lightGray
//            continueBtn.isUserInteractionEnabled = false
        }
        

        
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(copy(_:)) || action == #selector(paste(_:)) {
            return false
        }
        
        return true
    }

    @IBAction func continueTapped(_ sender: Any) {
        if Int(priceTF.text!)! >= MIN_PRICE {
           performSegue(withIdentifier: "OfferDateVC", sender: nil)
        } else {
            ErrorAlert.errorAlert.showAlert(title: "Something went wrong!", msg: "Offer must be at least SGD\(MIN_PRICE)", object: self)
        }
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? OfferDateVC {
            dest.task = self.task
            dest.serviceCharge = Double(priceTF.text!)!.getServiceCharge()
            dest.grossEarning = Double(priceTF.text!)!.getGrossEarning()
        }
    }
    @IBAction func infoTapped(_ sender: Any) {
        ErrorAlert.errorAlert.showAlert(title: "Service Charge", msg: "Our platform charge a 20% fees to keep our platform alive and help us to serve you better!", object: self)
    }
    
}
