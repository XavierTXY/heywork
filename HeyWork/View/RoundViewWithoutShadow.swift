//
//  RoundViewWithoutShadow.swift
//  HeyWork
//
//  Created by XavierTanXY on 9/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable
class RoundViewWithoutShadow: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = 6.0
        layer.masksToBounds = false
//        layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
//        layer.shadowOffset = CGSize(width:0, height: 0)
//        layer.shadowOpacity = 0.4
        
        
        //E6ECF0 table view and content view
        
        //        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 30.0).cgPath
        //        layer.shouldRasterize = true
        
        
    }
}
