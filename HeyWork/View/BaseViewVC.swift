//
//  BaseViewVC.swift
//  HeyWork
//
//  Created by XavierTanXY on 3/5/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SVProgressHUD

class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.4
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 4
        
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        

        
        self.tabBarController?.tabBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.tabBarController?.tabBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.tabBarController?.tabBar.layer.shadowRadius = 4
        self.tabBarController?.tabBar.layer.shadowOpacity = 0.4
        self.tabBarController?.tabBar.layer.masksToBounds = false
        
        self.tabBarController?.tabBar.shadowImage = UIImage()
        self.tabBarController?.tabBar.barStyle = .black
    }
    
    func showAlert(taskID: String?, userID: String?) {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        let reportAction = UIAlertAction(title: "Report", style: .destructive) { (action) in
            self.displayReportAlert(taskID: taskID, userID: userID)
        }
        
        optionMenu.addAction(reportAction)
        optionMenu.addAction(cancelAction)
        
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    private func displayReportAlert(taskID: String?,userID: String?) {
        
        let alert = UIAlertController()
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler:{ (UIAlertAction)in
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Spam", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            self.submitReport(taskID: taskID, userID: userID,reason: "Spam")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Inappropriate", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            self.submitReport(taskID: taskID, userID: userID,reason: "Inappropriate")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Bullying", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            self.submitReport(taskID: taskID, userID: userID,reason: "Bullying")
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func submitReport(taskID: String?,userID: String?,reason: String) {
        
        if let id = taskID {
   
                    let firebaseReport = DataService.ds.REF_TASK_REPORTS.child(id).childByAutoId()
                    
                    let reportValues: Dictionary<String, AnyObject> = ["reporter": Auth.auth().currentUser?.uid as AnyObject, "Reason": reason as AnyObject]
                    
                    firebaseReport.updateChildValues(reportValues)
                    
                    
    
  
        } else {
            let firebaseReport = DataService.ds.REF_User_Report.child(userID!).childByAutoId()
            
            let reportValues: Dictionary<String, AnyObject> = ["reporter": Auth.auth().currentUser?.uid as AnyObject, "Reason": reason as AnyObject]
            
            firebaseReport.updateChildValues(reportValues)
            
            
    
        }
        
        SVProgressHUD.setMinimumDismissTimeInterval(0.5)
        SVProgressHUD.showSuccess(withStatus: "Done!")
        

        
        
    }
    

}
