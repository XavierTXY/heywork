//
//  MapMarkerView.swift
//  HeyWork
//
//  Created by XavierTanXY on 23/5/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import Foundation
import UIKit

//protocol MapMarkerDelegate: class {
//    func didTapInfoButton(data: Task)
//}

class MapMarkerView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    
    weak var jobVC: JobsVC?
    var task: Task?
    
//    @IBAction func didTapInfoButton(_ sender: UIButton) {
//        
//    }
    @IBAction func tapTask(_ sender: Any) {
        print("tapped")
        jobVC?.didTapInfoButton(data: task!)
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "MapMarkerView", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    
}
