//
//  EmptyViewInnerTable.swift
//  HeyWork
//
//  Created by XavierTanXY on 22/5/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import Foundation
import UIKit

class EmptyViewInnerTable: UIView {
    
    @IBOutlet weak var view: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    class func instanceFromNib(image:UIImage, title: String, desc: String) -> UIView {
        
        var emptyView = UINib(nibName: "EmptyViewInnerTable", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EmptyViewInnerTable
        emptyView.view.image = image
        emptyView.label.text = title
        emptyView.descLbl.text = desc
        return emptyView
    }
    
    func showView(show: Bool, tableView: UITableView){
        
        if show {
            self.isHidden = false
            tableView.bringSubviewToFront(self)
            
        } else {
            self.isHidden = true
            tableView.sendSubviewToBack(self)
            
        }
    }
    
    func showViewCollectionView(show: Bool, collectionView: UICollectionView){
        
        if show {
            self.isHidden = false
            collectionView.bringSubviewToFront(self)
            
        } else {
            self.isHidden = true
            collectionView.sendSubviewToBack(self)
            
        }
    }
    
    
}
