//
//  ReviewView.swift
//  HeyWork
//
//  Created by XavierTanXY on 6/5/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class ReviewView: UIViewController {
    var chatlogVC: ChatLogVC!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var reviewTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
