//
//  OfferBar.swift
//  HeyWork
//
//  Created by XavierTanXY on 18/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import UIKit

class OfferBar: UIView {
    @IBOutlet weak var offerBtn: UIButton!
    @IBOutlet weak var offerView: RoundViewWithoutShadow!
    var detailVC: TaskDetailVC!
    @IBOutlet weak var titleLbl: UILabel!
    @IBAction func offerTapped(_ sender: Any) {
        detailVC.offerTapped()
    }
    
    func changeState(userExist: Bool) {
        if userExist {
            offerView.backgroundColor = OPEN_COLOR
            offerBtn.setImage(nil, for: .normal)
            offerBtn.setTitle("Offered", for: .normal)
        } else {
            offerView.backgroundColor = MAIN_COLOR
            offerBtn.setTitle("Make an Offer", for: .normal)
        }
        
    }
    func removeBg() {
        
    }
    //    var itemVC: ItemVC!
//    
//    @IBAction func addToCartTapped(_ sender: Any) {
//        itemVC.bottomCartTapped()
//    }
//    @IBAction func getQuoteTapped(_ sender: Any) {
//        itemVC.goToNumberVC()
//        //        let urlString = "Sending WhatsApp message through app in Swift"
//        //        let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
//        ////        let urlStringEncoded = urlString.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
//        //        let url  = NSURL(string: "whatsapp://send?text=\(urlStringEncoded!)&phone_number=+6591034123")
//        //
//        //        if UIApplication.shared.canOpenURL(url! as URL) {
//        //            UIApplication.shared.openURL(url! as URL)
//        //        } else {
//        //            let errorAlert = UIAlertView(title: "Cannot Send Message", message: "Your device is not able to send WhatsApp messages.", delegate: self, cancelButtonTitle: "OK")
//        //            errorAlert.show()
//        //        }
//    }
}
