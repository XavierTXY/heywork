//
//  AssignBar.swift
//  HeyWork
//
//  Created by XavierTanXY on 4/11/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import UIKit

class AssignBar: UIView {
    
    @IBOutlet weak var assignView: RoundViewWithoutShadow!
    @IBOutlet weak var assignBtn: UIButton!
    var profileVC: ProfileVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func assignTapped(_ sender: Any) {
        profileVC.assignTapped()
    }
}
