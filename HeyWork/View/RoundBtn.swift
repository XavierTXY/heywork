//
//  RoundBtn.swift
//  HeyWork
//
//  Created by XavierTanXY on 3/6/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable
class RoundBtn: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = 5.0
        layer.masksToBounds = true
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = .zero
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 10
        
        
        //E6ECF0 table view and content view
        
        //        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        //                layer.shouldRasterize = true
        
        
    }
}
