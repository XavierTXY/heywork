//
//  RoundView.swift
//  HeyWork
//
//  Created by XavierTanXY on 22/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//
import Foundation
import UIKit


@IBDesignable
class RoundView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()

        layer.cornerRadius = 5.0
        layer.masksToBounds = true
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = .zero
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 10
        
        
        //E6ECF0 table view and content view
        
//        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//                layer.shouldRasterize = true
        
        
    }
}
