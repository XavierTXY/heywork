//
//  DatePickerBulletinItem.swift
//  HeyWork
//
//  Created by XavierTanXY on 9/6/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import Foundation
import UIKit
import BLTNBoard

/**
 * A bulletin item that demonstrates how to integrate a date picker inside a bulletin item.
 */

class DatePickerBulletinItem: BLTNPageItem {
    
    lazy var datePicker = UIDatePicker()
    
    /**
     * Display the date picker under the description label.
     */
    
    
    override func makeViewsUnderDescription(with interfaceBuilder: BLTNInterfaceBuilder) -> [UIView]? {
        datePicker.datePickerMode = .date
        return [datePicker]
    }
//    override func viewsUnderDescription(_ interfaceBuilder: BulletinInterfaceBuilder) -> [UIView]? {
//        
//    }
    
}
