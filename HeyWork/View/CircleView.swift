import Foundation
import UIKit


@IBDesignable
class CircleView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = self.frame.width / 2
        layer.masksToBounds = true
//        layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
//        layer.shadowOffset = CGSize(width:0, height: 0)
//        layer.shadowOpacity = 0.4
        
        
        //E6ECF0 table view and content view
        
        //        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 30.0).cgPath
        //        layer.shouldRasterize = true
        
        
    }
}
