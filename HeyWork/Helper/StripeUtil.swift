//
//  StripeUtil.swift
//  HeyWork
//
//  Created by XavierTanXY on 17/12/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import Stripe

class StripeUtil {
    static let stripeUtil = StripeUtil()
    var stripeTool = StripeTools()
    var customerId: String?
    var connectID: String?
    
    let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
    var dataTask: URLSessionDataTask?
    
    func createConnectUser(card: STPCardParams, completion: @escaping (_ success: Bool) -> Void) {

        //Stripe iOS SDK will gave us a token to make APIs call possible
        print("456")
        stripeTool.generateToken(card: card) { (token) in
            print("123")
            if(token != nil) {
                 print("hi")
                //request to create the user
                //                let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/customers")! as URL)
                let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/accounts")! as URL)
                //params array where you can put your user informations
                var params = [String:String]()
                params["email"] = "enquiry.xavier@gmail.com"
                params["type"] = "standard"
                params["country"] = "SG"


                //transform this array into a string
                var str = ""
                params.forEach({ (key, value) in
                    str = "\(str)\(key)=\(value)&"
                })

                //basic auth
                request.setValue(self.stripeTool.getBasicAuth(), forHTTPHeaderField: "Authorization")

                //POST method, refer to Stripe documentation
                request.httpMethod = "POST"

                request.httpBody = str.data(using: String.Encoding.utf8)

                //create request block
                self.dataTask = self.defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in

                    //get returned error
                    if let error = error {
                        print(error)
                        completion(false)
                    }
                    else if let httpResponse = response as? HTTPURLResponse {
                        //you can also check returned response
                        print(httpResponse.statusCode)
                        if(httpResponse.statusCode == 200) {
                            if let data = data {
                                let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
//                                print("CONNECT CUSTOMER DETAILS")
                                print(json)
                                //serialize the returned datas an get the customerId
                                if let id = json["id"] as? String {
                                    self.connectID = id
                                    DataService.ds.currentUser.updateVerification(userData: ["connectKey":id as AnyObject])

//                                    self.createCard(stripeId: id, card: card) { (success) in
                                        completion(true)
//                                    }
                                }
                            }
                        }
                        else {
                            print("fail")
                            completion(false)
                        }
                    }
                }

                //launch request
                self.dataTask?.resume()
            }
        }
    }
    
//    //Original create charge using card
//    func createCharge(amount: Int, desc: String, taskID: String, payeeID: String, completion: @escaping (_ success: Bool) -> Void) {
//        //        self.getCardsList(completion: { (result) in
//        //            if let result = result {
//        //                var card = result
//
//        let card = STPCardParams()
//
//        //        self.getCard(cardID: "123") { (result) in
//        //            if let result = result {
//        //                card.cvc = result[]
//        //            }
//        //        }
//
//        card.cvc = DataService.ds.currentUser.bankCard?.cardCVV!
//        card.number = DataService.ds.currentUser.bankCard?.cardNo!
//        let cardDate =  DataService.ds.currentUser.bankCard?.cardExDate!.components(separatedBy: "/")
//        card.expMonth = UInt(cardDate![0])!
//        card.expYear = UInt(cardDate![1])!
//
//        stripeTool.generateToken(card: card) { (token) in
//            if(token != nil) {
//                print("here")
//                //request to create the user
//                let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/charges")! as URL)
//                //                let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/accounts")! as URL)
//                //params array where you can put your user informations
//                var params = [String:String]()
//                params["amount"] = "\(amount*100)"
//
//                params["currency"] = "sgd"
//                params["source"] = "tok_visa"
//                params["description"] = "\(desc)"
//
//
//                //transform this array into a string
//                var str = ""
//                params.forEach({ (key, value) in
//                    str = "\(str)\(key)=\(value)&"
//                })
//
//                //basic auth
//                request.setValue(self.stripeTool.getBasicAuth(), forHTTPHeaderField: "Authorization")
//
//                //POST method, refer to Stripe documentation
//                request.httpMethod = "POST"
//
//                request.httpBody = str.data(using: String.Encoding.utf8)
//
//                //create request block
//                self.dataTask = self.defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
//
//                    //get returned error
//                    if let error = error {
//                        print(error)
//                        completion(false)
//                    }
//                    else if let httpResponse = response as? HTTPURLResponse {
//                        //you can also check returned response
//                        if(httpResponse.statusCode == 200) {
//                            if let data = data {
//                                let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
//                                print("CHARGES")
//                                print(json)
//                                Bill().createPendingBill(billID: json["id"] as! String, billAmount: amount, billDate: json["created"] as! Double, billDesc: desc, payerID: DataService.ds.currentUser.userKey!, payeeID: payeeID, total_credit_debit: "totalDebit", billStatus: PENDING, taskID:taskID)
//                                completion(true)
//
//                            }
//                        }
//                        else {
//                            completion(false)
//                        }
//                    }
//                }
//
//                //launch request
//                self.dataTask?.resume()
//
//            }
//        }
//
//        //            }
//
//        //        })
//        //Stripe iOS SDK will gave us a token to make APIs call possible
//
//    }
    
    //create charge using cusomter id
    func createCharge(amount: Int, desc: String, taskID: String, payeeID: String, payeeUsername: String, payeeProfilePicUrl: String, payerStripeID: String,completion: @escaping (_ success: Bool) -> Void) {
//        self.getCardsList(completion: { (result) in
//            if let result = result {
//                var card = result
//        
//        let card = STPCardParams()
//        
////        self.getCard(cardID: "123") { (result) in
////            if let result = result {
////                card.cvc = result[]
////            }
////        }
//        
//        card.cvc = "123"
//        card.number = "4242"
////        let cardDate =  "02/26"
//        card.expMonth = 02
//        card.expYear = 26

//        stripeTool.generateToken(card: card) { (token) in
//            if(token != nil) {
                print("here")
                //request to create the user
                let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/charges")! as URL)
                //                let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/accounts")! as URL)
                //params array where you can put your user informations
                var params = [String:String]()
                params["amount"] = "\(amount*100)"
         
                params["currency"] = "sgd"
                params["customer"] = payerStripeID
                params["description"] = "\(desc)"
             
                
                //transform this array into a string
                var str = ""
                params.forEach({ (key, value) in
                    str = "\(str)\(key)=\(value)&"
                })
                
                //basic auth
                request.setValue(self.stripeTool.getBasicAuth(), forHTTPHeaderField: "Authorization")
                
                //POST method, refer to Stripe documentation
                request.httpMethod = "POST"
                
                request.httpBody = str.data(using: String.Encoding.utf8)
                
                //create request block
                self.dataTask = self.defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
                    
                    //get returned error
                    if let error = error {
                        print(error)
                        completion(false)
                    }
                    else if let httpResponse = response as? HTTPURLResponse {
                        //you can also check returned response
                        if(httpResponse.statusCode == 200) {
                            if let data = data {
                                let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                                print("CHARGES")
                                print(json)
                                Bill().createPendingBill(billID: json["id"] as! String, billAmount: amount, billDate: json["created"] as! Double, billDesc: desc, payerID: DataService.ds.currentUser.userKey!, payeeID: payeeID, total_credit_debit: "totalDebit", billStatus: PENDING, taskID:taskID, payeeUsername: payeeUsername, payeeProfilePicUrl: payeeProfilePicUrl)
                                completion(true)

                            }
                        }
                        else {
                            completion(false)
                        }
                    }
                }
                
                //launch request
                self.dataTask?.resume()
                
//            }
//        }
        
//            }

//        })
        //Stripe iOS SDK will gave us a token to make APIs call possible
        
    }
    //createUser
    func createStripeUser(card: STPCardParams, completion: @escaping (_ success: Bool) -> Void) {

        //Stripe iOS SDK will gave us a token to make APIs call possible
        stripeTool.generateToken(card: card) { (token) in
            if(token != nil) {
                
                //request to create the user
                let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/customers")! as URL)
//                let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/accounts")! as URL)
                //params array where you can put your user informations
                var params = [String:String]()
                params["email"] = DataService.ds.currentUser.email
//                params["type"] = "standard"

                //transform this array into a string
                var str = ""
                params.forEach({ (key, value) in
                    str = "\(str)\(key)=\(value)&"
                })
                
                //basic auth
                request.setValue(self.stripeTool.getBasicAuth(), forHTTPHeaderField: "Authorization")
                
                //POST method, refer to Stripe documentation
                request.httpMethod = "POST"
                
                request.httpBody = str.data(using: String.Encoding.utf8)
                
                //create request block
                self.dataTask = self.defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
                    
                    //get returned error
                    if let error = error {
                        print(error)
                        completion(false)
                    }
                    else if let httpResponse = response as? HTTPURLResponse {
                        //you can also check returned response
                        if(httpResponse.statusCode == 200) {
                            if let data = data {
                                let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                                print("Stripe CUSTOMER DETAILS")
                                print(json)
                                //serialize the returned datas an get the customerId
                                if let id = json["id"] as? String {
                                    self.customerId = id
                                    DataService.ds.currentUser.updateVerification(userData: ["stripeKey":id as AnyObject])
                                
                                    self.createCard(stripeId: id, card: card) { (success) in
                                        completion(true)
                                    }
                                }
                            }
                        }
                        else {
                            completion(false)
                        }
                    }
                }
                
                //launch request
                self.dataTask?.resume()

            }
        }
    }
    
    //create card for given user
    func createCard(stripeId: String, card: STPCardParams, completion: @escaping (_ success: Bool) -> Void) {
        
        stripeTool.generateToken(card: card) { (token) in
            if(token != nil) {
                let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/customers/\(stripeId)/sources")! as URL)
                
                //token needed
                var params = [String:String]()
                params["source"] = token!.tokenId
                
                var str = ""
                params.forEach({ (key, value) in
                    str = "\(str)\(key)=\(value)&"
                })
                
                //basic auth
                request.setValue(self.stripeTool.getBasicAuth(), forHTTPHeaderField: "Authorization")
                
                request.httpMethod = "POST"
                
                request.httpBody = str.data(using: String.Encoding.utf8)
                
                self.dataTask = self.defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
                    
                    if let error = error {
                        print(error)
                        completion(false)
                    }
                    else if let data = data {
                        let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        //                        print(json)
                        completion(true)
                    }
                }
                
                self.dataTask?.resume()
            }
        }
        
    }
    
    //get user card list
    func deleteCard(cardID: String, stripeKey: String, completion: @escaping (_ result: [String:Any]?) -> Void) {
        
        //request to create the user
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/customers/\(stripeKey)/sources/\(cardID)")! as URL)

        //basic auth
        request.setValue(self.stripeTool.getBasicAuth(), forHTTPHeaderField: "Authorization")
        
        //POST method, refer to Stripe documentation
        request.httpMethod = "DELETE"
        
        //create request block
        self.dataTask = self.defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            
            //get returned error
            if let error = error {
                print(error)
                completion(nil)
            }
            else if let httpResponse = response as? HTTPURLResponse {
                //you can also check returned response
                if(httpResponse.statusCode == 200) {
                    if let data = data {
                        let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                        //                        print(json)
                        //                        let card = json as? Dictionary<String, AnyObject>
                        completion(json)
                    }
                }
                else {
                    completion(nil)
                }
            }
        }
        
        //launch request
        self.dataTask?.resume()
        
    }
    
    //get user card list
    func getCard(cardID: String, stripeKey: String, completion: @escaping (_ result: [String:Any]?) -> Void) {
        
        //request to create the user
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/customers/\(stripeKey)/sources/\(cardID)")! as URL)
        print("here")
        //basic auth
        request.setValue(self.stripeTool.getBasicAuth(), forHTTPHeaderField: "Authorization")
        
        //POST method, refer to Stripe documentation
        request.httpMethod = "GET"
        
        //create request block
        self.dataTask = self.defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            
            //get returned error
            if let error = error {
                print(error)
                completion(nil)
            }
            else if let httpResponse = response as? HTTPURLResponse {
                //you can also check returned response
                if(httpResponse.statusCode == 200) {
                    if let data = data {
                        let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
//                        print(json)
//                        let card = json as? Dictionary<String, AnyObject>
                        completion(json)
                    }
                }
                else {
                    completion(nil)
                }
            }
        }
        
        //launch request
        self.dataTask?.resume()
        
    }
    
    //get user card list
    func getCardsList(completion: @escaping (_ result: [AnyObject]?) -> Void) {
        
        //request to create the user
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/customers/\(DataService.ds.currentUser.stripeKey!)/sources?object=card")! as URL)
        
        //basic auth
        request.setValue(self.stripeTool.getBasicAuth(), forHTTPHeaderField: "Authorization")
        
        //POST method, refer to Stripe documentation
        request.httpMethod = "GET"
        
        //create request block
        self.dataTask = self.defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            
            //get returned error
            if let error = error {
                print(error)
                completion(nil)
            }
            else if let httpResponse = response as? HTTPURLResponse {
                //you can also check returned response
                if(httpResponse.statusCode == 200) {
                    if let data = data {
                        let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                        let cardsArray = json["data"] as? [AnyObject]
                        completion(cardsArray)
                    }
                }
                else {
                    completion(nil)
                }
            }
        }
        
        //launch request
        self.dataTask?.resume()
        
    }
    
}
