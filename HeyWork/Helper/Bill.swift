//
//  Charge.swift
//  HeyWork
//
//  Created by XavierTanXY on 14/1/19.
//  Copyright © 2019 HeyWork. All rights reserved.
//

import Foundation
import Firebase

class Bill{
    

    
    var billID: String?
    var billAmount: Int?
    var billDate: Double?
    var billReleaseDate: Double?
    var billDesc: String?
    var payerID: String?
    var payeeID: String?
    var payeeUsername: String?
    var payeeProfilePicUrl: String?
    var total_credit_debit: String?
    var billStatus: String?
    var taskID: String?
    var billIdentifier: String?


    init() {
        
    }
    
    init(billIdentifier: String, billData: Dictionary<String, AnyObject>) {
        self.billIdentifier = billIdentifier
        
        if let billID = billData["billID"] as? String {
            self.billID = billID
        }
        
        if let billAmount = billData["billAmount"] as? Int {
            self.billAmount = billAmount
        }
        
        if let billDate = billData["billDate"] as? Double {
            self.billDate = billDate
        }
        
        if let billReleaseDate = billData["billReleaseDate"] as? Double {
            self.billReleaseDate = billReleaseDate
        }
        
        if let billDesc = billData["billDesc"] as? String {
            self.billDesc = billDesc
        }
        
        if let payerID = billData["payerID"] as? String {
            self.payerID = payerID
        }
        
        if let payeeID = billData["payeeID"] as? String {
            self.payeeID = payeeID
        }
        
        if let payeeUsername = billData["payeeUsername"] as? String {
            self.payeeUsername = payeeUsername
        }
        
        if let billStatus = billData["billStatus"] as? String {
            self.billStatus = billStatus
        }
        
        if let taskID = billData["taskID"] as? String {
            self.taskID = taskID
        }
        
        if let payeeProfilePicUrl = billData["payeeProfilePicUrl"] as? String {
            self.payeeProfilePicUrl = payeeProfilePicUrl
        }
  
    }
    
    func allChecked() {

    }
    
    func updateStatus(billMode: String, billIdentifier: String, payerID: String, billStatus: String) {
        let ref = DB_BASE
        var updateObj = [String:AnyObject]()
        updateObj["/bills/debitBill/\(billIdentifier)/billStatus/"] = billStatus as AnyObject
        updateObj["/users/\(payerID)/debitBill/\(billIdentifier)/billStatus/"] = billStatus as AnyObject
        ref.updateChildValues(updateObj)
    }
    
    func createPendingBill(billID: String, billAmount: Int, billDate: Double, billDesc: String, payerID: String, payeeID: String, total_credit_debit: String, billStatus: String, taskID: String, payeeUsername: String, payeeProfilePicUrl: String) {
        
        var billIdentifier = "\(taskID)_\(payeeID)"
//        self.billID = billID
//        self.billAmount = billAmount
//        self.billDate = billDate
//        self.billDesc = billDesc
//        self.payerID = payerID
//        self.payeeID = payeeID
//        self.total_credit_debit = total_credit_debit
//        self.billStatus = billStatus
//        self.taskID = taskID
        
        let ref = DB_BASE
        var updateObj = [String:AnyObject]()
        
        if total_credit_debit == "totalCredit" {
            updateObj["/bills/creditBill/\(billIdentifier)/"] = ["billAmount":billAmount,"billDate":billDate,"billDesc":billDesc,"taskID": taskID, "billStatus":billStatus, "billID": billID, "payeeUsername":payeeUsername, "payeeProfilePicUrl": payeeProfilePicUrl] as AnyObject
            updateObj["/users/\(payerID)/creditBill/\(billIdentifier)/"] = ["billAmount":billAmount,"billDate":billDate,"billDesc":billDesc, "taskID": taskID, "billStatus":billStatus,"billID": billID,"payeeUsername":payeeUsername, "payeeProfilePicUrl": payeeProfilePicUrl] as AnyObject
        } else {
            updateObj["/bills/debitBill/\(billIdentifier)/"] = ["billAmount":billAmount,"billDate":billDate,"billDesc":billDesc,"taskID": taskID, "billStatus":billStatus,"billID": billID,"payeeUsername":payeeUsername, "payeeProfilePicUrl":payeeProfilePicUrl] as AnyObject
            updateObj["/users/\(payerID)/debitBill/\(billIdentifier)/"] = ["billAmount":billAmount,"billDate":billDate,"billDesc":billDesc,"taskID": taskID, "billStatus":billStatus,"billID": billID,"payeeUsername":payeeUsername, "payeeProfilePicUrl" :payeeProfilePicUrl] as AnyObject
        }
        
        
        ref.updateChildValues(updateObj)
        
    }

    func createBill(billIdentifier: String,payerID: String, payeeProfilePicUrl: String, payeeUsername: String,payeeID: String,total_credit_debit: String, taskID: String, billStatus: String, completion: @escaping (_ success: Bool) -> Void) {
        
        DataService.ds.REF_BILL.child("debitBill").child(billIdentifier).observeSingleEvent(of: .value, with: { (snapshot) in
            if ( snapshot.value is NSNull ) {
                completion(false)
            } else {
              
                if let billDict = snapshot.value as? Dictionary<String, AnyObject> {
                    let key = snapshot.key
                    
                    var bill = Bill(billIdentifier: key, billData: billDict)
                    
                    let payerRef = DataService.ds.REF_USERS.child(payerID)
                    let payeeRef = DataService.ds.REF_USERS.child(payeeID)
                    
                    payeeRef.runTransactionBlock({ (currentData) -> TransactionResult in
                        
                        if var userDict = currentData.value as? Dictionary<String, AnyObject> {
                            
                            //var confessionDict = currentData.value as? Dictionary<String, AnyObject>
                            
                            
                            var total = userDict["totalCredit"] as? Int
                            
                            if total != nil {
                                total = total! + bill.billAmount!
                            }
                            
                            
                            userDict["totalCredit"] = total as AnyObject
                            
                            
                            currentData.value = userDict
                            print("scueess")
                            
                            return TransactionResult.success(withValue: currentData)
                        }
                        
                        return TransactionResult.success(withValue: currentData)
                    }) { (error, commited, snap) in
                    
//                        if error != nil {
//                            completion(false)
//                        }
//
                        if commited {
                            
                            payerRef.runTransactionBlock({ (currentData) -> TransactionResult in
                                
                                if var userDict = currentData.value as? Dictionary<String, AnyObject> {
                                    
                                    //var confessionDict = currentData.value as? Dictionary<String, AnyObject>
                                    
                                    
                                    var total = userDict["totalDebit"] as? Int
                                    
                                    if total != nil {
                                        total = total! + bill.billAmount!
                                    }
                                    
                                    
                                    userDict["totalDebit"] = total as AnyObject
                                    
                                    
                                    currentData.value = userDict
                                    print("scueess")
                                    
                                    return TransactionResult.success(withValue: currentData)
                                }
                                
                                return TransactionResult.success(withValue: currentData)
                            }) { (error, commited, snap) in
                                
//                                if error != nil {
//                                    completion(false)
//                                }
                                
                                let ref = DB_BASE
                                if commited {
                                    
                                    var updateObj = [String:AnyObject]()
                                    let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
                                    //For Payer
                                    updateObj["/bills/debitBill/\(billIdentifier)/"] = ["billAmount":bill.billAmount!,"billDate":bill.billDate!,"billDesc":bill.billDesc!,"taskID": taskID, "billStatus":billStatus,"billID": bill.billID!, "billReleaseDate" : timestamp,"payeeUsername": payeeUsername, "payeeProfilePicUrl" :payeeProfilePicUrl] as AnyObject
                                    updateObj["/users/\(payerID)/debitBill/\(billIdentifier)/"] = ["billAmount":bill.billAmount!,"billDate":bill.billDate!,"billDesc":bill.billDesc!,"taskID": taskID, "billStatus":billStatus,"billID": bill.billID!,"billReleaseDate" : timestamp, "payeeUsername": payeeUsername, "payeeProfilePicUrl": payeeProfilePicUrl] as AnyObject
                                    
                                    //For Payee
                                    updateObj["/bills/creditBill/\(billIdentifier)/"] = ["billAmount":bill.billAmount!,"billDate":bill.billDate!,"billDesc":bill.billDesc!,"taskID": taskID, "billStatus":billStatus, "billID": bill.billID!,"billReleaseDate" : timestamp, "payeeUsername": payeeUsername, "payeeProfilePicUrl" :payeeProfilePicUrl] as AnyObject
                                    updateObj["/users/\(payeeID)/creditBill/\(billIdentifier)/"] = ["billAmount":bill.billAmount!,"billDate":bill.billDate!,"billDesc":bill.billDesc!, "taskID": taskID, "billStatus":billStatus,"billID": bill.billID!,"billReleaseDate" : timestamp,"payeeUsername": payeeUsername, "payeeProfilePicUrl":payeeProfilePicUrl] as AnyObject
                                    
                                    
                                    
                                    ref.updateChildValues(updateObj)
                                    completion(true)
                                    
                    
                                }
                            }
                        } else {
                            //not commited
//                            completion(false)
                        }
                    }
                }
                
                //bill dict
//                completion(false)
            }
        })
        
        

    }
    
}
