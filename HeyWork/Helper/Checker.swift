//
//  Checker.swift
//  HeyWork
//
//  Created by XavierTanXY on 29/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation

class Checker {
    
    static let checker = Checker()
    
    var verificationMode = true
    var detailChecked: Bool!
    
    var employmentChecked: Bool!
    var educationChecked: Bool!
    
    var profileChecked: Bool!
    var languageChecked: Bool!
//    var bankChecked: Bool!
    var creditChecked: Bool!
    var bankAccountChecked: Bool!
    var dobChecked: Bool!
    
    var languages = [String]()
    var levels = [String]()
    var languageDict = [String:String]()
    
    var userDetails = [String:String]()
    
    var jobs = [[String:String]]()
    var noExperience = false
    
    
    var educations = [[String:String]]()
    
    var skills = [String:String]()
    
    var taskForOffer: Task!
    var userIDForProfileVC: String!
    var taskForProfileVC: Task!
    
    var categoryForJobVC = ALL
    
    init() {
        profileChecked = false
//        languageChecked = false
        creditChecked = false
        bankAccountChecked = false
        dobChecked = false
        
    }
    
    func allChecked() -> Bool {
        if self.verificationMode {
            if Checker.checker.dobChecked && Checker.checker.profileChecked && Checker.checker.creditChecked{
                return true
            } else {
                return false
            }
        } else {
            if Checker.checker.dobChecked && Checker.checker.profileChecked && Checker.checker.bankAccountChecked{
                return true
            } else {
                return false
            }
        }

    }
    func resetAll() {
        Checker.checker.dobChecked = false
        Checker.checker.languageChecked = false
        Checker.checker.profileChecked = false
        Checker.checker.creditChecked = false
        Checker.checker.bankAccountChecked = false
        
        self.languageDict.removeAll()
    }

}
