//
//  Constant.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import UIKit
import ViewAnimator

let TEST_PUBLISH_KEY = "pk_test_lRn5W0zyD3JXwqSwygmNKdxL"

let GOOGLE_MAP_API_KEY = "AIzaSyAiqFIqx-gAmfvnz0cy9L-d7wcKzVqP4bM"

let TEST_SECRET_KEY = "sk_test_qXc8mogJP8peRp8lF0CsBXvn"

let TABLE_ANIMATION = [AnimationType.from(direction: .bottom, offset: 30.0)]

let AMEX = "American Express"

let DINER = "Diners Club"

let DISCOVER = "Discover"

let JCB = "JCB"

let MASTER = "MasterCard"

let UNION_PAY = "UnionPay"

let VISA = "Visa"

let UNKNOWN = "Unknown"

let GRAPHIC_DESIGN = "Graphics & Design"

let TECH = "Programming & Tech"

let ADMIN = "Administrator"

let BUSINESS = "Business"

let MEDIA = "Media"

let WRITING = "Writing & Translation"

let FLYER = "Flyer"

let IT = "Computer/IT"

let OTHER = "Other"

let ALL = "ALL"

let CATEGORIES_DISPLAY = [ALL, ADMIN, BUSINESS, FLYER, GRAPHIC_DESIGN, IT, MEDIA, TECH, WRITING, OTHER]
let CATEGORIES_DISPLAY_MAP = [ALL, ADMIN, BUSINESS, FLYER, IT, OTHER]
let CATEGORIES = [ ADMIN, BUSINESS, FLYER, GRAPHIC_DESIGN, IT, MEDIA, TECH, WRITING, OTHER]

let Admin_Question = ["Fill in an excel with data", "Excel work", "Copy data from somewhere", "Take screenshots","Other"]
let Admin_Answer = ["I need someone to fill in data", "I need someone to do some excel work", "I need someone to copy some data", "I need someone to take screenshots"]

let G_D_Question = ["Design a logo", "Need some graphic design", "Design website mockup", "Design mobile app mockup","Other"]
let G_D_Answer = ["I need someone to design me a logo", "I need someone to do some graphic design", "I need someone to design website mockup", "I need someone to design mobile app mockup"]

let Writing_Question = ["Write an article", "Write a report", "Translation","Other"]
let Writing_Answer = ["I need someone to write me an article", "I need someone to write a report", "I need someone to translate an article"]

let Tech_Question = ["Design a website", "Rebuild an exisiting website", "Amendment on an existing website","Design an ios app", "Rebuild an exisiting ios app", "Amendment on an existing ios app","Design an android app","Rebuild an exisiting android app","Amendment on an android app","Other"]
let Tech_Answer = ["I need someone to design me a website", "I need someone to rebuild my website", "I need someone to fix my website","I need someone to design me an ios app", "I need someone to rebuild my ios app", "I need someone to fix my ios app","I need someone to design me an android app", "I need someone to rebuild my android app ", "I need someone to fix my android app"]

let Business_Question = ["Write a contract", "Help with my business", "Sales", "Marketing","Other"]
let Business_Answer = ["I need someone to help me to write a contract", "I need someone to help on my business","I need someone to sell something for me", "I need someone to do marketing"]

let Media_Question = ["Design a video", "Edit an existing video","Design a soundtrack", "Edit an existing soundtrack", "Photography", "Other"]
let Media_Answer = ["I need someone to design me a video", "I need someone to edit an existing video", "I need someone to design me a soundtrack", "I need someone to edit an existing soundtrack", "I need someone to be my photographer"]

let IT_Question = ["Install a software", "Fix my computer", "Other"]
let IT_Answer = ["I need someone to install me a software", "I need someone to fix my computer"]

let FLYER_Answer = ["I need someone to distribute flyers"]

let QUESTIONS = [Admin_Question,Business_Question,[],G_D_Question, IT_Question, Media_Question, Tech_Question, Writing_Question, []]

let ANSWERS = [Admin_Answer,Business_Answer,FLYER_Answer, G_D_Answer, IT_Answer, Media_Answer, Tech_Answer, Writing_Answer, []]

let BORDER_COLOR = UIColor(red: 226/255, green: 228/255, blue: 232/255, alpha: 1)

let SERVICE_CHARGE = 0.20

let MIN_PRICE = 5

let MIN_AGE = 16

//let NO_DOB = "Please set your birthday"

let NO_BIO = "Ops, I forgot to write something here..."

let OPEN = "OPEN"

let CLOSED = "CLOSED"

let COMPLETED = "COMPLETED"

let WORKING = "WORKING"

let REQUEST = "REQUESTED"

let PAID = "PAID"

let REQUEST_CANCEL = "REQUEST_CANCEL"

let PENDING = "PENDING"

let SUCCESS = "SUCCESS"

let POSTER_REVIEWED = "POSTER_REVIEWED"

let TASKER_REVIEWED = "TASKER_REVIEWED"

let BOTH_REVIEWED = "BOTH_REVIEWED"

let CANCEL_PENDING_POSTER = "Waiting tasker to cancel"

let CANCEL_PENDING_TASKER = "Poster has requested to cancel"

let WORKING_DONE_TASKER = "Request for fund."

let WORKING_DONE_POSTER = "Tasker working in progress."

let REQUEST_DONE_TASKER = "Waiting for fund to be released."

let REQUEST_DONE_POSTER = "Pay tasker for the job."

let PAID_DONE_POSTER = "Leave a review for the tasker"

let PAID_DONE_TASKER = "Leave a review for the poster."

let REVIEWED_DONE_POSTER = "Tasker gave you a review."

let REVIEWED_DONE_TASKER = "Poster gave you a review."

let COMPLETED_TASK = "Completed"

let NOT_WORKING = "NOT_WORKING"

let CANCELLED = "CANCELLED"

let REMOVED = "REMOVED"

let MUTUALLY_CANCEL = "MUTUALLY_CANCEL"

let SHADOW_COLOR: CGFloat = 157.0 / 255.0

let SHADOW_GRAY: CGFloat = 120.0 / 255.0

let INVALID_EMAIL = 17999

let INVALID_PASSWORD = 17026

let WRONG_PASSWORD = 17009

let USER_NOT_FOUND = 17011

let USER_EXISTED = 17007

let EMAIL_BADLY_FORMATTED = 17008

let ADMIN_EMAIL = "xavier-889@hotmail.com"

let DISCUSSION = "DISCUSSION"

let FOLLOWING = "following"

let FOLLOWER = "follower"

let LIKER = "userLike"

let KEY_UID = "uid"

let NO_PIC = "NoPicture"

let NO_DOB = "NoDOB"

let FIREBASE = "Firebase"

let FB = "facebook.com"

let MALE = "Male"

let FEMALE = "Female"

let PAY = "PAY"

let PRICE_LOW_TO_HIGH = "Price (low to high)"

let PRICE_HIGH_TO_LOW = "Price (high to low)"

let DEADLINE_NEAREST = "Deadline (nearest)"

let DEADLINE_FURTHUREST = "Deadline (furthest)"

let SORT = [PRICE_LOW_TO_HIGH, PRICE_HIGH_TO_LOW,DEADLINE_NEAREST,DEADLINE_FURTHUREST]

let DELETE_MODE_AFTER_POSTED = 0

let DELETE_MODE_BEFORE_POSTED = 1

let MAIN_COLOR = UIColor(red: 98/255, green: 135/255, blue: 187/255, alpha: 1)

let CHAT_BG_COLOR = UIColor(red: 204/255, green: 236/255, blue: 255/255, alpha: 1)

let OPEN_COLOR = UIColor(red: 134/255, green: 161/255, blue: 30/255, alpha: 1)

let BORDER_COLOR_CGCOLOR = UIColor(red: 226/255, green: 228/255, blue: 232/255, alpha: 1).cgColor

let BUTTON_COLOR = UIColor(red: 98/255, green: 135/255, blue: 187/255, alpha: 1)

let LIMIT_CHAR_NAME = 18

let DEFAULT_COUNT = "3"

let PURPLE = UIColor(red: 18/255, green: 102/255, blue: 215/255, alpha: 1)

let PURPLE2 = UIColor(red: 0/255, green: 105/255, blue: 192/255, alpha: 1)

let NAV_BAR_TITLE_COLOR = [NSAttributedString.Key.foregroundColor: UIColor(red: 73/255, green: 73/255, blue: 81/255, alpha: 1)]

let SKILLS = ["Asdajkfhsdjkfh shdjf kaskjsfsa sdjjkfhal ahs fjkahsfhaskjfhasdjklfshajkd flhs aljkfsh faskjldhfas jlfkadjks hajksjkhfasfhasjkas halsjkdh asdjkfhsahjf adjksfhjklahjks fhjlkfshf ajksdkfj", "B", "C", "d", "e", "f", "g" , "h", "i", "j", "k" , "l", "m", "n","o","p","q","r","S","t","u"]

let CATEGORY_COLORS = [UIColor(hex: "#A4E3DA"),UIColor(hex: "#FDA88B"),UIColor(hex: "#9EBEF1"),UIColor(hex: "#8786FB"),UIColor(hex: "#F88C8C"),UIColor(hex: "#8CDBF9"),UIColor(hex: "#977DB9")]
let CATEGORY_IMAGES = ["admin", "business", "flyer","design","it","media","coding", "writing", "other"]

let TC_LINK = "https://firebasestorage.googleapis.com/v0/b/heywork-69421.appspot.com/o/CompanyDoc%2FTerms%20and%20Conditions.pdf?alt=media&token=7e5b5034-3f1d-4b3f-abf5-0305954513bf"

let PRIVACY_LINK = "https://firebasestorage.googleapis.com/v0/b/heywork-69421.appspot.com/o/CompanyDoc%2FPrivacy%20Policy.pdf?alt=media&token=411b2fd3-b8bb-4b11-9b21-249324add696"
