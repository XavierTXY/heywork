//
//  StripeTool.swift
//  HeyWork
//
//  Created by XavierTanXY on 17/12/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import Stripe

struct StripeTools {
    
    //store stripe secret key
    private var stripeSecret = TEST_SECRET_KEY
    
    
    //generate token each time you need to get an api call
    func generateToken(card: STPCardParams, completion: @escaping (_ token: STPToken?) -> Void) {
        STPAPIClient.shared().createToken(withCard: card) { token, error in
            if let token = token {
                completion(token)
            }
            else {
                print(error)
                completion(nil)
            }
        }
        
    }
    
    func getBasicAuth() -> String{
        return "Bearer \(self.stripeSecret)"
    }
    
}
