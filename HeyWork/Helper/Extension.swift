//
//  Extension.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SDWebImage
import AVFoundation
import GoogleMaps
import GooglePlaces

extension GMSMapView {
    func getCenterCoordinate() -> CLLocationCoordinate2D {
        let centerPoint = self.center
        let centerCoordinate = self.projection.coordinate(for: centerPoint)
        return centerCoordinate
    }
    
    func getTopCenterCoordinate() -> CLLocationCoordinate2D {
        // to get coordinate from CGPoint of your map
        let topCenterCoor = self.convert(CGPoint(x: self.frame.size.width, y: 0), from: self)
        let point = self.projection.coordinate(for: topCenterCoor)
        return point
    }
    
    func getRadius() -> CLLocationDistance {
        let centerCoordinate = getCenterCoordinate()
        let centerLocation = CLLocation(latitude: centerCoordinate.latitude, longitude: centerCoordinate.longitude)
        let topCenterCoordinate = self.getTopCenterCoordinate()
        let topCenterLocation = CLLocation(latitude: topCenterCoordinate.latitude, longitude: topCenterCoordinate.longitude)
        let radius = CLLocationDistance(centerLocation.distance(from: topCenterLocation))
        
        
        self.clear()
        
//        let circleCenter : CLLocationCoordinate2D  = CLLocationCoordinate2DMake(centerCoordinate.latitude, centerCoordinate.longitude);
//        let circ = GMSCircle(position: centerCoordinate, radius: radius)
//        circ.fillColor = UIColor(red: 0.0, green: 0.7, blue: 0, alpha: 0.1)
//        circ.strokeColor = UIColor(red: 255/255, green: 153/255, blue: 51/255, alpha: 0.5)
//        circ.strokeWidth = 2.5;
//        circ.map = self
//
        return round(radius)
    }
}

extension Date {
    func getCurrentTime() -> String {
        
        let seconds = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        let date = NSDate(timeIntervalSince1970: TimeInterval(seconds))
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
        
        return dayTimePeriodFormatter.string(from: date as Date)
    }
}
extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
//        self.clipsToBounds = true
    }
}

extension UINavigationBar {
    
    func shouldRemoveShadow(_ value: Bool) -> Void {
        if value {
            self.setValue(true, forKey: "hidesShadow")
        } else {
            self.setValue(false, forKey: "hidesShadow")
        }
    }
}

let imageCache: NSCache<NSString, UIImage> = NSCache()
extension Double {
    func getServiceCharge() -> Double {
        return (self * SERVICE_CHARGE).roundTo(places: 2)
    }
    
    func getGrossEarning() -> Double{
        return (self - (self * SERVICE_CHARGE)).roundTo(places: 2)
    }
    
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension UITextView {

    func hyperLink(originalText: String, hyperLink: String, urlString: String, textColor: UIColor) {
        let style = NSMutableParagraphStyle()
        self.isSelectable = true
        self.isUserInteractionEnabled = true
        self.dataDetectorTypes = UIDataDetectorTypes.link
//        style.alignment = .center
        let attributedOriginalText = NSMutableAttributedString(string: originalText)
        let linkRange = attributedOriginalText.mutableString.range(of: hyperLink)
        let fullRange = NSMakeRange(0, attributedOriginalText.length)
        attributedOriginalText.addAttribute(NSAttributedString.Key.link, value: urlString, range: linkRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: fullRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: fullRange)
        self.linkTextAttributes = [
//            NSForegroundColorAttributeName: UIConfig.primaryColour,
//            NSAttributedString.Key.color: UIColor.lightGray,
            NSAttributedString.Key.backgroundColor: textColor,
            NSAttributedString.Key.foregroundColor: textColor,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
        ]
        
        self.attributedText = attributedOriginalText
    }
    
    func removeLink(text: String) {
        let attributedOriginalText = NSMutableAttributedString(string: text)
        let originalRange = NSMakeRange(0, attributedOriginalText.length)
        attributedOriginalText.setAttributes([:], range: originalRange)
//        attributedOriginalText.removeAttribute(NSAttributedString(string: "", attributes: [:]), range: originalRange)
        self.attributedText = attributedOriginalText
    }
    
    func makeHyperLink(url: String) {
        let attributedString = NSMutableAttributedString(string: url)
        let url = URL(string: url)!
        
        // Set the 'click here' substring to be the link
        attributedString.setAttributes([.link: url], range: NSMakeRange(5, 10))
        
        self.attributedText = attributedString
        self.isUserInteractionEnabled = true
        self.isEditable = false
        
        // Set how links should appear: blue and underlined
        self.linkTextAttributes = [
            .foregroundColor: UIColor.blue,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
    }

}


extension UITextField {
    
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer =     UITapGestureRecognizer(target: self, action:    #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func clearBadge(index: Int) {
        if let tabItems = self.tabBarController?.tabBar.items as NSArray!
        {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[index] as! UITabBarItem
            if tabItem.badgeValue?.count != 0 {
                
                if( tabItem.badgeValue != nil) {
                    
                    if index == 3 {
                        DataService.ds.currentUser.clearMsgCount {
                            tabItem.badgeValue = nil
                        }
                    } else if index == 4 {
//                        DataService.ds.currentUser.clearNotiCount {
                            tabItem.badgeValue = nil
//                        }
                    }
                    

                }
                
                
            }
            
            
        }
    }
}

extension UIButton {
    
    func pulsate() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.3
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = false
        pulse.repeatCount = 1
        pulse.initialVelocity = 0.5
        pulse.damping = 0.5
        
        layer.add(pulse, forKey: "pulse")
    }
    
    func flash() {
        
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 0.5
        flash.fromValue = 1
        flash.toValue = 0.1
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = 3
        
        layer.add(flash, forKey: nil)
    }
    
    
    func shake() {
        
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.1
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        
        layer.add(shake, forKey: "position")
    }
}

extension UISearchBar {

    public func setSerchTextcolor(color: UIColor) {
        let clrChange = subviews.flatMap { $0.subviews }
        guard let sc = (clrChange.filter { $0 is UITextField }).first as? UITextField else { return }
        sc.textColor = color
    }
    
    private var textField: UITextField? {
        return subviews.first?.subviews.flatMap { $0 as? UITextField }.first
    }
    
    private var activityIndicator: UIActivityIndicatorView? {
        return textField?.leftView?.subviews.flatMap{ $0 as? UIActivityIndicatorView }.first
    }
    
    var isLoading: Bool {
        get {
            return activityIndicator != nil
        } set {
            if newValue {
                if activityIndicator == nil {
                    let newActivityIndicator = UIActivityIndicatorView(style: .gray)
                    newActivityIndicator.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                    newActivityIndicator.startAnimating()
                    newActivityIndicator.backgroundColor = UIColor.white
                    textField?.leftView?.addSubview(newActivityIndicator)
                    let leftViewSize = textField?.leftView?.frame.size ?? CGSize.zero
                    newActivityIndicator.center = CGPoint(x: leftViewSize.width/2, y: leftViewSize.height/2)
                }
            } else {
                activityIndicator?.removeFromSuperview()
            }
        }
    }
}

extension UnicodeScalar {
    
    var isEmoji: Bool {
        
        switch value {
        case 0x1F600...0x1F64F, // Emoticons
        0x1F300...0x1F5FF, // Misc Symbols and Pictographs
        0x1F680...0x1F6FF, // Transport and Map
        0x2600...0x26FF,   // Misc symbols
        0x2700...0x27BF,   // Dingbats
        0xFE00...0xFE0F,   // Variation Selectors
        0x1F900...0x1F9FF,  // Supplemental Symbols and Pictographs
        65024...65039, // Variation selector
        8400...8447: // Combining Diacritical Marks for Symbols
            return true
            
        default: return false
        }
    }
    
    var isZeroWidthJoiner: Bool {
        
        return value == 8205
    }
}

extension String {
    

    func containsSymbols() -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if self.rangeOfCharacter(from: characterset.inverted) != nil {
            return true
        } else {
            return false
        }
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    
    var glyphCount: Int {
        
        let richText = NSAttributedString(string: self)
        let line = CTLineCreateWithAttributedString(richText)
        return CTLineGetGlyphCount(line)
    }
    
    var isSingleEmoji: Bool {
        
        return glyphCount == 1 && containsEmoji
    }
    
    var containsEmoji: Bool {
        
        return unicodeScalars.contains { $0.isEmoji }
    }
    
    var containsOnlyEmoji: Bool {
        
        return !isEmpty
            && !unicodeScalars.contains(where: {
                !$0.isEmoji
                    && !$0.isZeroWidthJoiner
            })
    }
    
    // The next tricks are mostly to demonstrate how tricky it can be to determine emoji's
    // If anyone has suggestions how to improve this, please let me know
    var emojiString: String {
        
        return emojiScalars.map { String($0) }.reduce("", +)
    }
    
    var emojis: [String] {
        
        var scalars: [[UnicodeScalar]] = []
        var currentScalarSet: [UnicodeScalar] = []
        var previousScalar: UnicodeScalar?
        
        for scalar in emojiScalars {
            
            if let prev = previousScalar, !prev.isZeroWidthJoiner && !scalar.isZeroWidthJoiner {
                
                scalars.append(currentScalarSet)
                currentScalarSet = []
            }
            currentScalarSet.append(scalar)
            
            previousScalar = scalar
        }
        
        scalars.append(currentScalarSet)
        
        return scalars.map { $0.map{ String($0) } .reduce("", +) }
    }
    
    fileprivate var emojiScalars: [UnicodeScalar] {
        
        var chars: [UnicodeScalar] = []
        var previous: UnicodeScalar?
        for cur in unicodeScalars {
            
            if let previous = previous, previous.isZeroWidthJoiner && cur.isEmoji {
                chars.append(previous)
                chars.append(cur)
                
            } else if cur.isEmoji {
                chars.append(cur)
            }
            
            previous = cur
        }
        
        return chars
    }
}

extension UIImageView {
    
    func getImage(imageUrl:String, completion:@escaping(_ image: UIImage) -> Void) {
        SDImageCache.shared().queryCacheOperation(forKey: (imageUrl as NSString) as String!, done: { (image2, data2, SDImageCacheType) in
            
            if( image2 != nil) {
//                UIView.transition(with: self,
//                                  duration:0.1,
//                                  options: .transitionCrossDissolve,
//                                  animations: {
//                                    self.image = image2
//                },
//                                  completion: nil)
                completion(image2!)
                print("it usese cached")
            } else {
                let ref = Storage.storage().reference(forURL: imageUrl)
                
                ref.getData(maxSize: 1 * 1024 * 1024, completion: { (data,error) in
                    if error != nil {
                        print("Xavier: unable to dowload image from fire")
                        self.image = #imageLiteral(resourceName: "ProfilePic")
                        print(error.debugDescription)
                    } else {
                        print("Xavier: image dowloaded from fire")
                        
                        // DispatchQueue.main.async(execute: { () -> Void in
                        if let imageData = data {
                            if let img = UIImage(data: imageData){
                                completion(img)
                                
                                // activityIndicatorView.stopAnimating()
                                // imageCache.setObject(img, forKey: imageUrl as NSString)
                                print("Xavier: cache saves img")
                                SDImageCache.shared().store(img, forKey: (imageUrl as NSString) as String!)
                                
                                
                            }
                            
                        }
                        //  })
                        
                    }
                    
                })
            }
        })

    }
    func loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: String) {
        self.image = nil
        if imageUrl == NO_PIC {
            self.image = UIImage(named: "portfolio")
        } else {
            SDImageCache.shared().queryCacheOperation(forKey: (imageUrl as NSString) as String!, done: { (image2, data2, SDImageCacheType) in
                
                if( image2 != nil) {
                    UIView.transition(with: self,
                                      duration:0.1,
                                      options: .transitionCrossDissolve,
                                      animations: {
                                        self.image = image2
                    },
                                      completion: nil)
                    
                    print("it usese cached")
                } else {
                    let ref = Storage.storage().reference(forURL: imageUrl)
                    
                    ref.getData(maxSize: 1 * 1024 * 1024, completion: { (data,error) in
                        if error != nil {
                            print("Xavier: unable to dowload image from fire")
                            self.image = #imageLiteral(resourceName: "ProfilePic")
                            print(error.debugDescription)
                        } else {
                            print("Xavier: image dowloaded from fire")
                            
                            // DispatchQueue.main.async(execute: { () -> Void in
                            if let imageData = data {
                                if let img = UIImage(data: imageData){
                                    //
                                    
                                    //self.image = img
                                    
                                    DispatchQueue.main.async() {
                                        
                                        
                                        //                                    let updateCell = tv .cellForRow(at: index)
                                        //                                    if updateCell != nil {
                                        UIView.transition(with: self,
                                                          duration:0.1,
                                                          options: .transitionCrossDissolve,
                                                          animations: {
                                                            self.image = img
                                        },
                                                          completion: nil)
                                        
                                        
                                        
                                        // }
                                        
                                    }
                                    
                                    // activityIndicatorView.stopAnimating()
                                    // imageCache.setObject(img, forKey: imageUrl as NSString)
                                    print("Xavier: cache saves img")
                                    SDImageCache.shared().store(img, forKey: (imageUrl as NSString) as String!)
                                    
                                    
                                    
                                }
                                
                            }
                            //  })
                            
                        }
                        
                    })
                }
            })
        }
        
        
    }
    
    //    func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    //        let size = image.size
    //
    //        let widthRatio  = targetSize.width  / image.size.width
    //        let heightRatio = targetSize.height / image.size.height
    //
    //        // Figure out what our orientation is, and use that to form the rectangle
    //        var newSize: CGSize
    //        if(widthRatio > heightRatio) {
    //            //newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
    //            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    //        } else {
    //            //newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
    //            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
    //        }
    //
    //        // This is the rect that we've calculated out and this is what is actually used below
    //        //let rect = CGRectMake(0, 0, newSize.width, newSize.height)
    //        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    //
    //        // Actually do the resizing to the rect using the ImageContext stuff
    //        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    //        image.draw(in: rect)
    //        let newImage = UIGraphicsGetImageFromCurrentImageContext()
    //        UIGraphicsEndImageContext()
    //
    //        return newImage!
    //    }
    
    func loadImageUsingCacheWithUrlStringCollection(imageUrl: String, cv: UICollectionView, index: IndexPath) {
        
        
        //
        SDImageCache.shared().queryCacheOperation(forKey: (imageUrl as NSString) as String!, done: { (image2, data3, SDImageCacheType) in
            
            if( image2 != nil) {
                
                
                self.image = image2
                
                
                print("it usese cached")
            } else {
                let ref = Storage.storage().reference(forURL: imageUrl)
                ref.getData(maxSize: 1 * 1024 * 1024, completion: { (data,error) in
                    if error != nil {
                        print("Xavier: unable to dowload image from fire")
                        print(error.debugDescription)
                        self.image = #imageLiteral(resourceName: "ProfilePic")
                    } else {
                        print("Xavier: image dowloaded from fire")
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                            if let imageData = data {
                                if let img = UIImage(data: imageData){
                                    //
                                    
                                    //self.image = img
                                    
                                    DispatchQueue.main.async() {
                                        
                                        
                                        let updateCell = cv.cellForItem(at: index)
                                        if updateCell != nil {
                                            UIView.transition(with: self,
                                                              duration:0.1,
                                                              options: .transitionCrossDissolve,
                                                              animations: {
                                                                self.image = img
                                                                
                                                                
                                                                
                                            },
                                                              completion: nil)
                                            
                                            
                                            
                                        }
                                        
                                    }
                                    
                                    // activityIndicatorView.stopAnimating()
                                    // imageCache.setObject(img, forKey: imageUrl as NSString)
                                    print("Xavier: cache saves img")
                                    SDImageCache.shared().store(img, forKey: (imageUrl as NSString) as String!)
                                    
                                    
                                    
                                    
                                }
                                
                            }
                        })
                        
                    }
                    
                })
            }
        })
        
    }
    
    func loadImageUsingCacheWithUrlString(imageUrl: String, tv: UITableView, index: IndexPath) {
        
        
        //
        SDImageCache.shared().queryCacheOperation(forKey: (imageUrl as NSString) as String!, done: { (image2, data3, SDImageCacheType) in
            
            if( image2 != nil) {
                
                
                self.image = image2
                
                
                print("it usese cached")
            } else {
                let ref = Storage.storage().reference(forURL: imageUrl)
                ref.getData(maxSize: 1 * 1024 * 1024, completion: { (data,error) in
                    if error != nil {
                        print("Xavier: unable to dowload image from fire")
                        print(error.debugDescription)
                        self.image = #imageLiteral(resourceName: "ProfilePic")
                    } else {
                        print("Xavier: image dowloaded from fire")
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                            if let imageData = data {
                                if let img = UIImage(data: imageData){
                                    //
                                    
                                    //self.image = img
                                    
                                    DispatchQueue.main.async() {
                                        
                                        
                                        let updateCell = tv .cellForRow(at: index)
                                        if updateCell != nil {
                                            UIView.transition(with: self,
                                                              duration:0.1,
                                                              options: .transitionCrossDissolve,
                                                              animations: {
                                                                self.image = img
                                                                
                                                                
                                                                
                                            },
                                                              completion: nil)
                                            
                                            
                                            
                                        }
                                        
                                    }
                                    
                                    // activityIndicatorView.stopAnimating()
                                    // imageCache.setObject(img, forKey: imageUrl as NSString)
                                    print("Xavier: cache saves img")
                                    SDImageCache.shared().store(img, forKey: (imageUrl as NSString) as String!)
                                    
                                    
                                    
                                    
                                }
                                
                            }
                        })
                        
                    }
                    
                })
            }
        })
        
    }
    
    
}
