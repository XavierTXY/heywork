//
//  Interaction.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

class Interaction {
    
    
    func enableInteraction() {
        
        
        UIApplication.shared.endIgnoringInteractionEvents()
        SVProgressHUD.dismiss()
    }
    
    /*
     func enable2(image: UIImageView) {
     image.isHidden = true
     UIApplication.shared.endIgnoringInteractionEvents()
     SVProgressHUD.dismiss()
     }*/
    
    /*
     func disable2(msg: String, image: UIImageView) {
     image.isHidden = false
     UIApplication.shared.beginIgnoringInteractionEvents()
     SVProgressHUD.show(withStatus: msg)
     }*/
    
    func disableInteraction() {
        
        UIApplication.shared.beginIgnoringInteractionEvents()
//        SVProgressHUD.show(withStatus: msg)
        SVProgressHUD.show()
    }
    
    func showSuccess() {
        SVProgressHUD.showSuccess(withStatus: "Done")
        UIApplication.shared.endIgnoringInteractionEvents()
        SVProgressHUD.dismiss(withDelay: 1)
    }
}
