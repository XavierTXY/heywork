//
//  ErrorAlert.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog
import  SVProgressHUD

class ErrorAlert {
    
    static let errorAlert = ErrorAlert()
    
    init() {
        
    }
    
//    func createAlert(title: String, msg: String, object: UIViewController) {
////                let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
////                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
////                object.present(alert, animated: true, completion: nil)
//        
//        self.showAlert(title: title, msg: msg, object: object)
        // Prepare the popup assets
//        let title = "THIS IS THE DIALOG TITLE"
//        let message = "This is the message section of the popup dialog default view"
////        let image = UIImage(named: "pexels-photo-103290")
//        UIApplication.shared.endIgnoringInteractionEvents()
//        SVProgressHUD.dismiss()
//        // Create the dialog
//        let popup = PopupDialog(title: title, message: msg)
//        popup.transitionStyle = .zoomIn        // Create buttons
//        let buttonOne = CancelButton(title: "Ok") {
////            print("You canceled the car dialog.")
//        }
//        buttonOne.backgroundColor = UIColor.red
//        buttonOne.titleColor = UIColor.white
////        buttonOne.tintColor = MAIN_COLOR
////        // This button will not the dismiss the dialog
////        let buttonTwo = DefaultButton(title: "ADMIRE CAR", dismissOnTap: false) {
////            print("What a beauty!")
////        }
////
////        let buttonThree = DefaultButton(title: "BUY CAR", height: 60) {
////            print("Ah, maybe next time :)")
////        }
//
//        // Add buttons to dialog
//        // Alternatively, you can use popup.addButton(buttonOne)
//        // to add a single button
//        popup.addButtons([buttonOne])
//        object.present(popup, animated: true, completion: nil)
        
        // Present dialog
//    }
    
//    func show(title: String, msg: String, object: UIViewController) {
//
//        self.showAlert(title: title, msg: msg, object: object)
        // self.loaderStopAnimate()
        //Interaction().enableInteraction()
//        UIApplication.shared.endIgnoringInteractionEvents()
//        SVProgressHUD.dismiss()
//
//
////        let popup = PopupDialog(title: title, message: msg, image: nil)
////
////
////        let buttonOne = CancelButton(title: "OK") {
////
////        }
////
////        popup.addButtons([buttonOne])
////
////        // Present dialog
////        object.present(popup, animated: true, completion: nil)
//
////                let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
////                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
////                object.present(alert, animated: true, completion: nil)
//
//        let popup = PopupDialog(title: title, message: msg)
//        popup.transitionStyle = .zoomIn
//        // Create buttons
//        let buttonOne = CancelButton(title: "Ok") {
//            //            print("You canceled the car dialog.")
//        }
//        buttonOne.backgroundColor = MAIN_COLOR
//        buttonOne.titleColor = UIColor.white
////        buttonOne.tintColor = MAIN_COLOR
//
//        //        // This button will not the dismiss the dialog
//        //        let buttonTwo = DefaultButton(title: "ADMIRE CAR", dismissOnTap: false) {
//        //            print("What a beauty!")
//        //        }
//        //
//        //        let buttonThree = DefaultButton(title: "BUY CAR", height: 60) {
//        //            print("Ah, maybe next time :)")
//        //        }
//
//        // Add buttons to dialog
//        // Alternatively, you can use popup.addButton(buttonOne)
//        // to add a single button
//        popup.addButtons([buttonOne])
//        object.present(popup, animated: true, completion: nil)
        
        
//    }
    
    func showAlert(title: String, msg: String, object: UIViewController) {
        
        UIApplication.shared.endIgnoringInteractionEvents()
        SVProgressHUD.dismiss()
        
        let popup = PopupDialog(title: title, message: msg)
        popup.transitionStyle = .zoomIn
        // Create buttons
        let buttonOne = CancelButton(title: "Ok") {
            //            print("You canceled the car dialog.")
        }
        buttonOne.backgroundColor = MAIN_COLOR
        buttonOne.titleColor = UIColor.white
  
        popup.addButtons([buttonOne])
        SVProgressHUD.dismiss()
        object.present(popup, animated: true, completion: nil)
    }
}
