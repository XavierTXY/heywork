//
//  Helper.swift
//  HeyWork
//
//  Created by XavierTanXY on 24/10/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//

import Foundation
import UIKit

class Helper {
    
    static let helper = Helper()
    
    var vc: ViewController!
    
    init() {
        
    }
    
    func addToolBar(vc: ViewController, tf: UITextField) {
        self.vc = vc
        
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                               UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(Helper.doneTapped))]
        numberToolbar.sizeToFit()
        tf.inputAccessoryView = numberToolbar
        
        
    }
    
    @objc func doneTapped() {
        
    }

}
