//
//  DataService.swift
//  HeyWork
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 HeyWork. All rights reserved.
//
import Foundation
import Firebase
import FirebaseDatabase
import SystemConfiguration
import FBSDKLoginKit
import SVProgressHUD
import PopupDialog
//import SwiftKeychainWrapper


let DB_BASE = Database.database().reference()
let STORAGE_BASE = Storage.storage().reference()
//let CURRENT_USER_UID = (FIRAuth.auth()?.currentUser!.uid)! Everytime it creates the same uid when logging in and out



class DataService {
    
    static let ds = DataService()
    var userObserverListener: DatabaseHandle!
    var msgObserverListener: DatabaseHandle!
    var msgTaskObserverListener: DatabaseHandle!
    var notiObserverListener: DatabaseHandle!
//    var chatLogObserverListener: DatabaseHandle!
    
    //DB References
    var courseArray = [String]()
    var categoryArray = [String]()
    var OS: String!
    var taskIDfromNotification: String?
    var jobVC: JobsVC!
    var emptyView: EmptyView!
    
    var currentUserLocation: CLLocation?
    
    private var _REF_BASE = DB_BASE
    private var _REF_USERS = DB_BASE.child("users")
    private var _REF_UNIVERSITIES = DB_BASE.child("universities")
    private var _REF_CONFESSIONS = DB_BASE.child("confessions")
    private var _REF_COMMENTS = DB_BASE.child("comments")
    private var _REF_MESSAGES = DB_BASE.child("messages")
    private var _REF_REPORTS = DB_BASE.child("reports")
    private var _REF_COMMENT_REPORTS = DB_BASE.child("comment-reports")
    private var _REF_TASK_REPORTS = DB_BASE.child("task-reports")
    private var _REF_USER_MESSAGES = DB_BASE.child("user-messages")
    private var _REF_CONFESSION_COMMENTS = DB_BASE.child("confession-comments")
    private var _REF_HASHTAG = DB_BASE.child("hashtag")
    private var _REF_FEEDBACK = DB_BASE.child("feedback")
    private var _REF_NOTIFICATION = DB_BASE.child("notification")
    private var _REF_CONFESSION_NOTIFICATION = DB_BASE.child("confession-notification")
    private var _REF_COMMENTER_COMMENT_NOTIFICATION = DB_BASE.child("commenter-commentID-notiID")
    
    private var _REF_MISSING_UNI = DB_BASE.child("missing-uni")
    private var _REF_MISSING_COURSE = DB_BASE.child("missing-course")
    
    var REF_UserBlock_Uni_Confession = DB_BASE.child("userBlock-uni-confession")
    var REF_UserBlock_PartnerID = DB_BASE.child("userBlock-partnerID")
    var REF_LIKER_CONFESSION_NOTIFICATION = DB_BASE.child("liker-confession-notification")
    var REF_LIKER_COMMENT_NOTIFICATION = DB_BASE.child("liker-comment-notification")
    var REF_CONFESSION_LIKER = DB_BASE.child("confession-liker")
    var REF_UniConfession = DB_BASE.child("uni-confession")
    var REF_UniConfessionCount = DB_BASE.child("uni-confessionCount")
    var REF_User_Confession = DB_BASE.child("user-confession")
    var REF_User_Comment = DB_BASE.child("user-comment")
    var REF_Version = DB_BASE.child("version")
    var REF_Update = DB_BASE.child("update")
    var REF_APP_CONFIG = DB_BASE.child("appConfig")
    var REF_Category = DB_BASE.child("category")
    var REF_User_Friend = DB_BASE.child("user-friend")
    var PUSH_TOKEN = ""
    
    var REF_EMAIL = DB_BASE.child("email")
    var REF_USERNAME = DB_BASE.child("username")
    var login = false
    
    var REF_TASK = DB_BASE.child("task")
    var REF_ALL_TASK = DB_BASE.child("all-task")
    var REF_USER_TASK = DB_BASE.child("user-task")
    var REF_OFFER = DB_BASE.child("offer")
    var REF_TASK_OFFER = DB_BASE.child("task-offer")
    var REF_TASK_ASSIGN = DB_BASE.child("task-assign")
    var REF_USER_NOTIFICATION = DB_BASE.child("user-notification")
    var REF_BILL = DB_BASE.child("bills")
    var REF_REQUEST_TO_CANCEL = DB_BASE.child("requestToCancel")
    var REF_APP_REVIEW = DB_BASE.child("app-review")
//    var REF_USER_OFFER = DB_BASE.child("task-offer")

    var REF_USER_PORT = DB_BASE.child("users/portfolio")
    
    
    var REF_University_Info = DB_BASE.child("university-info")
    var REF_Course_Info = DB_BASE.child("course-info")
    var REF_Category_Info = DB_BASE.child("category-info")
    var REF_Message_Report = DB_BASE.child("message-report")
    var REF_User_Report = DB_BASE.child("user-report")
    var REF_User_Following_Notification = DB_BASE.child("user-following-notification")
    var currentUser: User!
    
    var REF_Confessions_Discussion = DB_BASE.child("confessions").child("DISCUSSION")
    var REF_Discussion_Comments = DB_BASE.child("discussion-comments")
    
    
    var REF_USER_LOCATION = DB_BASE.child("user-location")
    
    //Storage reference
    private var _REF_PROFILE_PIC = STORAGE_BASE.child("profilePic")
    private var _REF_CONFESSION_PIC = STORAGE_BASE.child("confessionPic")
    private var _REF_MESSAGE_PIC = STORAGE_BASE.child("messagePic")
    var REF_Portfolio_Pic = STORAGE_BASE.child("portfolioPic")
    var REF_TASK_PIC = STORAGE_BASE.child("taskPic")
    var REF_CHAT_DOCUMENT = STORAGE_BASE.child("chatDocument")
    
    var REF_BASE: DatabaseReference {
        return _REF_BASE
    }
    
    var REF_USERS: DatabaseReference {
        return _REF_USERS
    }
    
    var REF_UNIVERSITIES: DatabaseReference {
        return _REF_UNIVERSITIES
    }
    
    var REF_PROFILE_PIC : StorageReference {
        return _REF_PROFILE_PIC
    }
    
    var REF_CONFESSION_PIC : StorageReference {
        return _REF_CONFESSION_PIC
    }
    
    var REF_MESSAGE_PIC : StorageReference {
        return _REF_MESSAGE_PIC
    }
    
    var REF_REPORTS : DatabaseReference {
        return _REF_REPORTS
    }
    
    var REF_CONFESSIONS: DatabaseReference {
        return _REF_CONFESSIONS
    }
    
    var REF_COMMENTS: DatabaseReference {
        return _REF_COMMENTS
    }
    
    var REF_MESSAGES: DatabaseReference {
        return _REF_MESSAGES
    }
    
    var REF_USER_MESSAGES: DatabaseReference {
        return _REF_USER_MESSAGES
    }
    
    var REF_CONFESSION_COMMENTS: DatabaseReference {
        return _REF_CONFESSION_COMMENTS
    }
    
    var REF_COMMENT_REPORTS: DatabaseReference {
        return _REF_COMMENT_REPORTS
    }
    
    var REF_TASK_REPORTS: DatabaseReference {
        return _REF_TASK_REPORTS
    }
    
    var REF_HASHTAG: DatabaseReference {
        return _REF_HASHTAG
    }
    
    var REF_FEEDBACK: DatabaseReference {
        return _REF_FEEDBACK
    }
    
    var REF_MISSING_UNI: DatabaseReference {
        return _REF_MISSING_UNI
    }
    
    var REF_MISSING_COURSE: DatabaseReference {
        return _REF_MISSING_COURSE
    }
    
    var REF_NOTIFICATION : DatabaseReference {
        return _REF_NOTIFICATION
    }
    
    var REF_CONFESSION_NOTIFICATION : DatabaseReference {
        return _REF_CONFESSION_NOTIFICATION
    }
    
    var REF_COMMENTER_COMMENT_NOTIFICATION : DatabaseReference {
        return _REF_COMMENTER_COMMENT_NOTIFICATION
    }
    
    
    
//    func createFirebaseDBUserWithPartial(uid: String, userData: Dictionary<String,String>) {
//
//        REF_USERS.child(uid).updateChildValues(userData)
//
//    }
    
    func signInWithData(uid: String, userData: Dictionary<String, AnyObject>)  {
        self.login = true
        let defaults = UserDefaults.standard
        defaults.set(uid, forKey: "uid")
        self.currentUser = User(userKey: uid, userData: userData)
//        defaults.set(self.currentUser, forKey: "user")
        self.addInstanceID()
        Checker.checker.skills.removeAll()
    }
    
    func signIn(uid: String)  {
        emptyView = EmptyView.instanceFromNib(image: UIImage(named: "banned")!, title: "Logged In Error", desc: "You have issue logging to this account\nPlease contact Support for more infomation at hello@heywork.co") as! EmptyView
        
        userObserverListener = DataService.ds.REF_USERS.child(uid).observe(.value, with: { (snapshot) in
            if ( snapshot.value is NSNull ) {
            } else {
                print("YEAH")
                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                    let key = snapshot.key
                    //                    var currentUser = User(userKey: key, userData: userDict)
                    self.login = true
                    let defaults = UserDefaults.standard
                    defaults.set(uid, forKey: "uid")
                    
                    self.currentUser = User(userKey: uid, userData: userDict)
//                    defaults.set(self.currentUser, forKey: "user")
           
                    print("CUURENTLY LOGGED IN USER \(self.currentUser.userKey!)")
                    self.addInstanceID()
                    
                    if (self.currentUser.msgCount.count - 1) > 0 {
                        self.jobVC.setBadgeNumber(notiCount: (self.currentUser.msgCount.count - 1), index: 3)
                    }
                    
                    if self.currentUser.notiCount > 0 {
                        self.jobVC.setBadgeNumber(notiCount: (self.currentUser.notiCount), index: 4)
                    }
                    
                    
                    if self.currentUser.isActive! {
                        self.emptyView.removeFromSuperview()
                    } else {
                        self.emptyView.frame = UIApplication.shared.keyWindow!.bounds
                        UIApplication.shared.keyWindow!.addSubview(self.emptyView)
         
                    }
                    
                }
            }
        })
        
        
    }
    
    func updateUser(completion:@escaping ()-> Void) {
        
        DataService.ds.REF_USERS.child(self.currentUser.userKey!).observeSingleEvent(of: .value, with: { (snapshot) in
            if ( snapshot.value is NSNull ) {
            } else {
                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                    self.currentUser = User(userKey: self.currentUser.userKey!, userData: userDict)
                    completion()
                }
            }
        })
    }
    
    func signOut(completion:@escaping ()-> Void) {
        let defaults = UserDefaults.standard
        for key in defaults.dictionaryRepresentation().keys{
            defaults.removeObject(forKey: key.description)
        }
        
        DataService.ds.REF_USERS.child(self.currentUser.userKey!).removeObserver(withHandle: self.userObserverListener)
        DataService.ds.REF_USER_MESSAGES.child(self.currentUser.userKey!).removeAllObservers()
//        removeObserver(withHandle: self.msgObserverListener)
//        DataService.ds.REF_USER_MESSAGES.child(currentUid!).child(userId!).removeObserver(withHandle: self.chatLogObserverListener)
        
        self.removeInstanceID()
        try! Auth.auth().signOut()
        completion()
    }
    
    func getUser(id: String) -> User {
        var user: User!
        DataService.ds.REF_USERS.child(id).observeSingleEvent(of: .value, with: { (snapshot) in
            if ( snapshot.value is NSNull ) {
            } else {
                print("YEAH")
                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                    let key = snapshot.key

                    user = User(userKey: id, userData: userDict)
            
                }
            }
        })
            
        return user
    }
        
    func createFirebaseDBUserWithFB(uid: String, userProvider: String, userEmail: String, name: String, userName: String, uni: String, uniShort: String, year: String, course: String, gender: String, completion: @escaping () -> ()){
        
        if((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, gender, email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    // print(result)
                    let data:[String:AnyObject] = result as! [String : AnyObject]
                    
                    let pictureURL = "https://graph.facebook.com/\(data["id"]!)/picture?type=large&return_ssl_resources=1"
                    // var imgURLString = "http://graph.facebook.com/\(data["id"])/picture?type=large" //type=normal
                    var imgURL = URL(string: pictureURL)
                    var imageData = try! Data(contentsOf: imgURL!)
                    var image = UIImage(data: imageData)
                    
                    // profilePicUrl = ["profilePicUrl": "\(imgURL)"]
                    if let img = image  {
//                        if let imgData = UIImage.jpegData(img) {
                        var imgData = img.jpegData(compressionQuality: 0.1)!
//                            UIImageJPEGRepresentation(img, 0.1) {
                        
                            let imgUid = NSUUID().uuidString
                            let metaData = StorageMetadata()
                            metaData.contentType = "image/jpeg"
                            
                            
                            DataService.ds.REF_PROFILE_PIC.child(imgUid).putData(imgData, metadata: metaData) { (metaData, error) in
                                if error != nil {
                                    print("Xavier: upload image to firebase failed")
                                    print(error.debugDescription)
                                } else {
                                    print("Xavier: successfuly upload img to firebase")
                                    let downloadUrl = metaData?.downloadURL()?.absoluteString
                                    if let url = downloadUrl {
                                        
                                        var updateObj = [String:AnyObject]()
                                        let ref = DataService.ds.REF_USERS.child(uid)
                                        
                                        updateObj["/provider"] = userProvider as AnyObject
                                        updateObj["/email"] = userEmail as AnyObject
                                        updateObj["/name"] = name as AnyObject
                                        updateObj["/userName"] = userName as AnyObject
                                        updateObj["/university"] = uni as AnyObject
                                        updateObj["/universityShort"] = uniShort as AnyObject
                                        updateObj["/course"] = course as AnyObject
                                        updateObj["/year"] = year as AnyObject
                                        updateObj["/gender"] = gender as AnyObject
//                                        updateObj["/genderCount"] = DEFAULT_COUNT as AnyObject
//                                        updateObj["/uniCount"] = DEFAULT_COUNT as AnyObject
//                                        updateObj["/courseCount"] = DEFAULT_COUNT as AnyObject
                                        updateObj["/profilePicUrl"] = url as AnyObject
                                        updateObj["/notiCount"] = 0 as AnyObject
                                        updateObj["/OS"] = DataService.ds.OS as AnyObject
                                        
                                        
                                        updateObj["/pushToken"] = self.PUSH_TOKEN as AnyObject
                                        
                                        
                                        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
                                        updateObj["/creationDate"] = timestamp as AnyObject
                                        
                                        var dict = [String: Bool]()
                                        dict["key"] = true
                                        
                                        
                                        updateObj["/follower"] = dict as AnyObject
                                        updateObj["/following"] = dict as AnyObject
                                        updateObj["/msgCount"] = dict as AnyObject
                                        
                                        ref.updateChildValues(updateObj)
                                        
                                        let defaults = UserDefaults.standard
                                        defaults.set(uid, forKey: "uid")
                                        completion()
                                    }
                                    
                                }
                                
                            }
//                        }
                        
                    }
                    
                }
            })
            
        }
        
        
        
    }
//    
//    func createFirebaseUser(uid: String, userProvider: String, userEmail: String, name: String, userName: String, uni: String, uniShort: String, year: String, course: String, gender: String,  completion: @escaping () -> ()) {
//        
//        var updateObj = [String:AnyObject]()
//        let ref = DataService.ds.REF_USERS.child(uid)
//        
//        updateObj["/provider"] = userProvider as AnyObject
//        updateObj["/email"] = userEmail as AnyObject
//        updateObj["/name"] = name as AnyObject
//        updateObj["/userName"] = userName as AnyObject
//        updateObj["/university"] = uni as AnyObject
//        updateObj["/universityShort"] = uniShort as AnyObject
//        updateObj["/course"] = course as AnyObject
//        updateObj["/year"] = year as AnyObject
//        updateObj["/gender"] = gender as AnyObject
////        updateObj["/genderCount"] = DEFAULT_COUNT as AnyObject
////        updateObj["/uniCount"] = DEFAULT_COUNT as AnyObject
////        updateObj["/courseCount"] = DEFAULT_COUNT as AnyObject
//        updateObj["/profilePicUrl"] = NO_PIC as AnyObject
//        updateObj["/notiCount"] = 0 as AnyObject
//        updateObj["/OS"] = DataService.ds.OS as AnyObject
//        
//        
//        updateObj["/pushToken"] = self.PUSH_TOKEN as AnyObject
//        
//        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
//        updateObj["/creationDate"] = timestamp as AnyObject
//        
//        var dict = [String: Bool]()
//        dict["key"] = true
//        
//        
//        updateObj["/follower"] = dict as AnyObject
//        updateObj["/following"] = dict as AnyObject
//        updateObj["/msgCount"] = dict as AnyObject
//        
//        ref.updateChildValues(updateObj)
//        
//        let defaults = UserDefaults.standard
//        defaults.set(uid, forKey: "uid")
//        
//        completion()
//        
//    }
    
    func create(uid: String, userDict: Dictionary<String, AnyObject>){
        Checker.checker.skills.removeAll()
        REF_USERS.child(uid).updateChildValues(userDict)
        REF_USERNAME.child(userDict["userName"] as! String).updateChildValues(["key": true])
//        REF_EMAIL.child(userDict["email"] as! String).updateChildValues(["key": true])
        signInWithData(uid: uid, userData: userDict)
        
//        REF_USERS.child(uid).updateChildValues(userEmail)
    }
    
    func removeInstanceID() {
        let ref = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!)
        var updateObj = ["/pushToken":""]
        ref.updateChildValues(updateObj)
    }
    
    func addInstanceID() {
        let ref = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!)
        var updateObj = ["/pushToken":"\(self.PUSH_TOKEN)"]
        ref.updateChildValues(updateObj)
    }
    


    
    func connectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    func deletePort(portID: String,completion:@escaping ()-> Void, vc: PortfolioDetailVC) {
        
        var updateObj = [String:AnyObject]()
        
        updateObj["/users/\(self.currentUser.userKey!)/portfolio/\(portID)/"] = NSNull() as AnyObject
        DB_BASE.updateChildValues(updateObj) { (error, ref) in
            
            if error != nil {
                ErrorAlert.errorAlert.showAlert(title: (error?.localizedDescription)!, msg: error.debugDescription, object: vc)
            } else {
                self.currentUser.portfolio[portID] = nil
                
                completion()
            }
        }
    }

  
    
    func uploadPortfolio(images: [UIImage],vc: UIViewController, portTitle: String, portDesc: String, portID:String, completion:@escaping() -> Void) {
        
        SVProgressHUD.show()
        
        var numPhoto = 0
        var idx = 0
        
        let ref = DB_BASE
        var id = self.currentUser.userKey!
//        var portDict = [String:AnyObject]()
//        portDict["portTitle"] = portTitle as AnyObject
//        portDict["portDesc"] = portDesc as AnyObject
        
        var urlDict = Dictionary<String, Bool>()
        for img in images {
//            if img != #imageLiteral(resourceName: "vipLogo") {
                numPhoto = numPhoto + 1
//                var compression = UIImage.jpegData(img)
                if let imgData = img.jpegData(compressionQuality:0.1) {
                    
                    let imgUid = NSUUID().uuidString
                    let metaData = StorageMetadata()
                    metaData.contentType = "image/jpeg"
                    
                    self.REF_Portfolio_Pic.child(imgUid).putData(imgData, metadata: metaData) { (metaData, error) in
                        if error != nil {
                            print("Xavier: upload image to firebase failed")
                            print(error.debugDescription)
                        } else {
                            print("Xavier: successfuly upload img to firebase")
                            let downloadUrl = metaData?.downloadURL()?.absoluteString
                            if let url = downloadUrl {
                                //                                self.postToFirebase(imgUrl: url)
                                
                                print(id)
                                print(portID)
                                var updateObj = [String:AnyObject]()
                                
                 
//                                portDict["portImageUrl"] = ["url\(idx)": url] as AnyObject
                                
                                

//                                updateObj["/users/\(id)/portfolio/\(portID)/portImageUrl/"] = NSNull() as AnyObject
                                updateObj["/users/\(id)/portfolio/\(portID)/portImageUrl/url\(idx)"] = url as AnyObject
                                updateObj["/users/\(id)/portfolio/\(portID)/portTitle/"] = portTitle as AnyObject
                                updateObj["/users/\(id)/portfolio/\(portID)/portDesc/"] = portDesc as AnyObject
                                updateObj["/users/\(id)/portfolio/\(portID)/porterID/"] = self.currentUser.userKey! as AnyObject
                                if idx == 0 {
                                    updateObj["/users/\(id)/portfolio/\(portID)/portImageHeight/"] = (img.size.height * 0.2) as AnyObject
//                                    portDict["portImageHeight"] = (img.size.height * 0.2) as AnyObject
                                   
                                }
                                ref.updateChildValues(updateObj, withCompletionBlock: { (error, ref) in
                                    
                                    if error != nil {
                                        ErrorAlert.errorAlert.showAlert(title: (error?.localizedDescription)!, msg: error.debugDescription, object: vc)
                                    } else {
                                        SVProgressHUD.dismiss()
//                                        self.currentUser.portfolio[portID] = Portfolio(portKey: portID, dict: portDict)
                                        completion()
//                                        vc.dismiss(animated: true, completion: nil)
                                    }
                                })
//                                updateObj["/category/\(cat)/\(id)/urls/url\(idx)"] = url as AnyObject
                                idx = idx + 1
                                
                                
//
//                                if idx == numPhoto {
//                                    SVProgressHUD.showSuccess(withStatus: "Done!")
//                                    vc.dismiss(animated: true, completion: nil)
//                                }
                                
                                
                            }
                            
                        }
                        
                    }
                }
//            }
        }
//        print(updateObj)
        
        
        
    }
    
    func addLocationToUser(location: CLLocation) {
        
        self.currentUserLocation = location
        
//        DispatchQueue.main.async(execute: { () -> Void in
//            if self.currentUser.currentLocation != nil {
//                if self.currentUser.currentLocation?.latitude != location.coordinate.latitude || self.currentUser.currentLocation?.longitude != location.coordinate.longitude {
//                    self.currentUser.updateNotificationPreference(add: true)
//                }
//            }
//        })

        
//        self.REF_USERS.child(self.currentUser.userKey!).updateChildValues(["locations": ["latitude": location.coordinate.latitude, "longitude": location.coordinate.longitude]])
    }
    
    func showReview(vc: UIViewController, fromSetting: Bool) {
        
        if fromSetting {
            showReviewDialog(vc: vc)
        } else {
            if let review = DataService.ds.currentUser.reviewedApp {
                
            } else {
                showReviewDialog(vc: vc)
            }
        }
        
        
    }
    
    
    func showReviewDialog(vc: UIViewController) {
        let reviewVC = ReviewView(nibName: "ReviewView", bundle: nil)
        
        
        
        let popup = PopupDialog(viewController: reviewVC, buttonAlignment: .horizontal, transitionStyle: .zoomIn, tapGestureDismissal: false)
        
        //        if let lbl = reviewVC.titleLbl {
        reviewVC.titleLbl.text = "How was everything?"
        //        }
        
        reviewVC.descLbl.text = "how's your experience with us so far?"
        
        let popup2 = PopupDialog(title: "Thank You!", message: "We appreciate your rating.\nWould you like to rate us on App Store?")
        // Create first button
        let buttonOne = CancelButton(title: "Cancel", height: 60) {
            
        }
        
        let buttonTwo = DefaultButton(title: "Submit", height: 60) {
            
            if reviewVC.reviewTF.text!.count == 0 {
                ErrorAlert.errorAlert.showAlert(title: "Review Needed!", msg: "Can't be emtpty", object: vc)
            } else {
                
                self.REF_APP_REVIEW.child(self.currentUser.userKey!).updateChildValues(["stars" : reviewVC.ratingView.rating, "review": reviewVC.reviewTF.text!])
                self.REF_USERS.child(self.currentUser.userKey!).updateChildValues(["reviewedApp": true])
                
                
                let button2 = DefaultButton(title: "Yes", height: 60) {
                    // 1.
                    
                    //                    let productURL = "https://apps.apple.com/app/id1467610312"
                    let productURL = NSURL(string: "https://apps.apple.com/app/id1467610312")
                    var components = URLComponents(url: productURL as! URL, resolvingAgainstBaseURL: false)
                    
                    
                    // 2.
                    components?.queryItems = [
                        URLQueryItem(name: "action", value: "write-review")
                    ]
                    
                    // 3.
                    guard let writeReviewURL = components?.url else {
                        return
                    }
                    
                    // 4.
                    UIApplication.shared.open(writeReviewURL)
                }
                
                
                if reviewVC.ratingView.rating > 2 {
                    popup2.addButtons([button2, buttonOne])
                    vc.present(popup2, animated: true, completion: nil)
                } else {
                    ErrorAlert.errorAlert.showAlert(title: "Thank You!", msg: "We appreciate your feedback. Your feedback is important for us.", object: vc)
                }
                
                
            }
            
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        vc.present(popup, animated: true, completion: nil)
    }
    
}


