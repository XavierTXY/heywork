// const functions = require('firebase-functions');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

// Import
const geofire = require('geofire');

// Usage
const geoFireInstance = new geofire.GeoFire(admin.database().ref('/user-location/'));

// var GeoFire = require('geofire');
// var geoFire = GeoFire.GeoFire(admin.database().ref());

// admin.initializeApp({
//   credential: admin.credential.cert({
//     projectId: 'heywork-69421',
//     clientEmail: 'firebase-adminsdk-o730o@heywork-69421.iam.gserviceaccount.com',
//     privateKey: 'AIzaSyBiyRmang15V7mpF1p9QR56M283zsNeq_s'
//   }),
//   databaseURL: 'https://heywork-69421.firebaseio.com/'
// });
// const logging = require('@google-cloud/logging')();
// const stripe = require('stripe')(functions.config().stripe.token);
// const currency = functions.config().stripe.currency || 'SGD';
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions


//uncomment below all for algolia

// const algoliasearch = require('algoliasearch');
// const dotenv = require('dotenv');
// // const firebase = require('firebase');

// // load values from the .env file in this directory into process.env
// dotenv.load();

// // configure firebase
// // firebase.initializeApp({
// //   databaseURL: process.env.FIREBASE_DATABASE_URL,
// // });
// // const database = firebase.database();

// const algolia = algoliasearch (
//   process.env.ALGOLIA_APP_ID,
//   process.env.ALGOLIA_API_KEY
// );

exports.newTaskNearby = functions.database.ref('/task/{taskID}').onCreate(( snapshot, context) => {
    let data = snapshot.val();

    if (data.place !== undefined) {

    	var geoQuery = geoFireInstance.query({
		  center: [data.place.latitude, data.place.longitude],
		  radius: 10
		});

		// console.log("task id here" + taskID);
		geoQuery.on("key_entered", function(key, location, distance) {

			if ( snapshot.val().posterID !== key ) {
				console.log("Sending notification to " + key + " entered query at " + location + " (" + distance + " km from center - )" + context.params.userId);


				var ref = '/users/' + key
			    let dbRef = admin.database().ref(ref);
		        dbRef.once('value', (snap) => {
		        	
		        	// console.log("task id here" + context.params.userId);
					// var token = getUserToken(key);

					let payload = {
						notification: {
							title: 'New task nearby!',
							body:  'Someone may need your help today.',
							sound: 'default',
							badge: '1',
							
							

						},
					    "data": {
					        "taskID": context.params.taskID
		
					    }
						


					};

					admin.messaging().sendToDevice(snap.val().pushToken, payload);

				});


				return null;
			}
			
		});


  	}
});

// const index = algolia.initIndex(process.env.ALGOLIA_INDEX_NAME);
exports.sendWelcomeEmail = functions.database.ref('/users/{userId}').onCreate(( snapshot, context) => {

      // return getAuthorDetails(context.params.userId).then(user => {

      // });


  return getAuthorDetails(context.params.userId).then(user => {
    // var email = user.email;
    // console.log('TRIGGERD email ', email);
    // console.log('TRIGGERD id ', event.params.userId);
    addToMailChimp(user.email)
    return null;
    // notifyMe(event.params.userId)
    

  });
});

// exports.deleteTaskOnAlgolia = functions.database.ref('/task/{taskID}').onDelete(( snapshot, context) => {

//     var taskID = context.params.taskID;

//     return getTaskDetails(taskID).then(task => {
//         return deleteIndexRecord(task, taskID);
//     });
// });

// exports.taskUpdated = functions.database.ref('/task/{taskID}').onUpdate(( snapshot, context) => {
//     console.log('taskUpdated get called');

//     var taskID = context.params.taskID;

//     return getTaskDetails(taskID).then(task => {
//         return addOrUpdateIndexRecord(task, taskID);
//     });

// });

// exports.newTaskCreated = functions.database.ref('/task/{taskID}').onCreate(( snapshot, context) => {
//     console.log('newTaskCreated get called');
//     // var user = firebase.auth().currentUser;
//     // var uid;

//     // if (user) {
//     //   uid = user.uid;
//     // } else {
//     //   uid = null;
//     // }
//     var taskID = context.params.taskID;

//     return getTaskDetails(taskID).then(task => {
//         return addOrUpdateIndexRecord(task, taskID);
//     });

// });

function addToMailChimp(email) {
  let fetch = require('isomorphic-fetch');
  let btoa = require('btoa');


      var MAILCHIMP_API_KEY = '8202ef72e8225bf820b562d5d881294b-us20'
      // NOTE: mailchimp's API uri differs depending on your location. us6 is the east coast. 
      var url = 'https://us20.api.mailchimp.com/3.0/lists/16408fcbab/members'
      var method = 'POST'
      var headers = {
        'authorization': "Basic " + btoa('randomstring:' + MAILCHIMP_API_KEY),
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
      var body = JSON.stringify({email_address: email, status: 'subscribed'})

      console.log('Adding this email to mailchimp', email);
      return fetch(url, {
        method,
        headers,
        body
      }).then(resp => resp.json())
}


function addOrUpdateIndexRecord(task, taskID) {

  // Get Firebase object
  const record = task;
  // Specify Algolia's objectID using the Firebase object key
  record.objectID = taskID;
  // Add or update object
  index
    .saveObject(record)
    .then(() => {
      return console.log('Firebase object indexed in Algolia', record.objectID);
    })
    .catch(error => {
      process.exit(1);
      return console.error('Error when indexing confession into Algolia', error);
      
    });
}

function deleteIndexRecord(task, taskID) {
  // Get Algolia's objectID from the Firebase object key
  const objectID = taskID;
  // Remove the object from Algolia
  index
    .deleteObject(objectID)
    .then(() => {
      return console.log('Firebase object deleted from Algolia', objectID);
    })
    .catch(error => {
      process.exit(1);
      return console.error('Error when deleting confession from Algolia', error);
      
    });
}

exports.removeExpiredTask = functions.pubsub.topic('RemoveExpiredTask').onPublish(() => {
	var currentTimeStamp = Math.round(new Date()*0.001)

  	// console.log('This will be run every 2 minutes!' + currentTimeStamp);

  	var ref = '/task/'
    let dbRef = admin.database().ref(ref);
	dbRef.once('value', function(snapshot) {
		snapshot.forEach(function(childSnapshot) {
			var childKey = childSnapshot.key;
			
			var childData = childSnapshot.val();

			if( childData.deadline < currentTimeStamp ) {
				console.log('Removing expired task' + childKey);
				updateTaskWithStatus(childKey, childData);
			}

			
		});
	});

});

function updateTaskWithStatus(taskID, taskData) {

  // Write the new post's data simultaneously in the posts list and the user's post list.
  var updates = {};
  taskData.status = 'EXPIRED'
  updates['/expired-task/' + taskID] = taskData;
  // updates['/expired-task/' + taskID + '/'] = taskData;
  updates['/task/' + taskID] = null;
  updates['/task-location/' + taskID] = null;
  updates['/category/' + taskData.category + '/' + taskID] = null;
  updates['/user-task/' + taskData.posterID + '/' + taskID + '/status' ] = 'EXPIRED';
  updates['/all-task/' + taskID ] = taskData;

  return admin.database().ref().update(updates);
}

exports.sendOfferNotification = functions.database.ref('/task-offer/{taskID}/{offererID}').onCreate(( snapshot, context) => {

	var txt = '';
  return getTaskDetails(context.params.taskID).then(task => {

  	return getOfferDetails(context.params.taskID, context.params.offererID).then(offer => {

		return getAuthorDetails(task.posterID).then(user => {

			txt = offer.offerUserName + ' has made an offer on ' + task.title + '.';
			console.log('New Offer' , txt);
			let payload = {
				notification: {
					title: 'New Offer!',
					body:  txt,
					sound: 'default',
					badge: '1'

				}
			};

			admin.messaging().sendToDevice(user.pushToken, payload);
			return null;



		});

  	
  	});

    
    // notifyMe(event.params.userId)
    

  });
})
//Notification for messageing
exports.sendMessageNotification = functions.database.ref('/messages/{msgId}').onCreate(( snapshot, context) => {

  var uid = context.auth.uid;
  var msgID = context.params.msgId;
  var txt = '';
  return getMessageDetails(msgID).then(msg => {
    if( msg.toId !== uid ) {
      return getAuthorDetails(msg.toId).then(user => {

      if( msg.text === undefined ) {

      	if( msg.fileName === undefined ) {
			txt = msg.fromUserName + ' sent you an image.';
      	}  else if ( msg.imageUrl === undefined ) {
      		txt = msg.fromUserName + ' sent you an attachment.';
      	}
        
      } else {
        txt = msg.fromUserName + ': ' + msg.text;
      }

	console.log('Msg' , txt);
      let payload = {
            notification: {
                //title: 'Firebase Notification',
                body:  txt,
                sound: 'default',
                badge: '1'

            }
      };
        
      admin.messaging().sendToDevice(user.pushToken, payload);
      return null;

      });
    }

    return null;
  });

});


function getOfferDetails(taskID, offerID) {
  var ref = '/task-offer/' + taskID + '/' + offerID
    let dbRef = admin.database().ref(ref);
    let defer = new Promise((resolve, reject) => {
        dbRef.once('value', (snap) => {
            let data = snap.val();
            // let users = [];
            // for (var property in data) {
            //     users.push(data[property]);
            // }
            resolve(data);
        }, (err) => {
            reject(err);
        });
    });
    return defer;
}

function getTaskDetails(taskID) {
  var ref = '/task/' + taskID
    let dbRef = admin.database().ref(ref);
    let defer = new Promise((resolve, reject) => {
        dbRef.once('value', (snap) => {
            let data = snap.val();
            // let users = [];
            // for (var property in data) {
            //     users.push(data[property]);
            // }
            resolve(data);
        }, (err) => {
            reject(err);
        });
    });
    return defer;
}



function getAuthorDetails(authorID) {
  var ref = '/users/' + authorID
    let dbRef = admin.database().ref(ref);
    let defer = new Promise((resolve, reject) => {
        dbRef.once('value', (snap) => {
            let data = snap.val();
            // let users = [];
            // for (var property in data) {
            //     users.push(data[property]);
            // }

            resolve(data);
        }, (err) => {
            reject(err);
        });
    });
    return defer;
}

function getMessageDetails(msgID) {
  var ref = '/messages/' + msgID
    let dbRef = admin.database().ref(ref);
    let defer = new Promise((resolve, reject) => {
        dbRef.once('value', (snap) => {
            let data = snap.val();
            // let users = [];
            // for (var property in data) {
            //     users.push(data[property]);
            // }

            resolve(data);
        }, (err) => {
            reject(err);
        });
    });
    return defer;

}

// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

// // When a user is created, register them with Stripe
// exports.createStripeCustomer = functions.auth.user().onCreate((user) => {
//   return stripe.customers.create({
//     email: user.email,
//   }).then((customer) => {
//     return admin.database().ref(`/stripe_customers/${user.uid}/customer_id`).set(customer.id);
//   });
// });
